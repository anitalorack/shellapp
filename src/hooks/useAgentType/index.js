import * as R from 'ramda'
import React from 'react'
import { useSelector, shallowEqual } from 'react-redux'

export const useAgentType = () => {
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const [state, setState] = React.useState(null)

    const type = R.cond([
        [R.equals('LegalEntity'), R.always({ type: R.defaultTo('LEGAL_ENTITY')(R.path(['type'])(agent)) })],
        [R.equals('SoleTrader'), R.always({ type: R.defaultTo('SOLE_TRADER')(R.path(['type'])(agent)) })],
        [R.equals('Individual'), R.always({ type: R.defaultTo('INDIVIDUAL')(R.path(['type'])(agent)) })],
        [R.equals('Counterparty'), R.always(R.path(['type'])(agent) ? { type: R.path(['type'])(agent) } : {})],
        [R.T, R.always(R.path(['type'])(agent) ? { type: R.path(['type'])(agent) } : {})],
        [R.F, R.always(R.path(['type'])(agent) ? { type: R.path(['type'])(agent) } : {})],
    ])(R.path(['__typename'])(agent))

    React.useEffect(() => {
        setState(type)
    }, [agent])

    const lenght = React.useMemo(() => {

    }, [agent])

    return [state, lenght]
}