import * as R from 'ramda';
import React from 'react';
import {
    DEPOT_QUERY,
    DESTROY_DEPOT,
    UPDATE_DEPOTS,
    RECEIVE_DEPOTS,
    RESERVED_DEPOTS,
    STOCK_TREE_DEPOTS
} from 'gql/garage';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm'
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

export const useDepotsHoks = ({ navigation, routeID }) => {
    const dispatchRedux = useDispatch()
    const { depots } = useSelector(state => state.garage, shallowEqual);

    React.useEffect(() => {
        if (R.equals(depots.id, routeID)) {
            loadGreeting({
                variables: { where: { id: { eq: routeID } } }
            })
        }
    }, [routeID])

    const [removedDepots] = useMutation(DESTROY_DEPOT, {
        onCompleted: (data) => {
            if (data) {
                showMessage({
                    message: "Склад успешно удален",
                    type: "success"
                });

                return navigation.goBack()
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                type: "danger",
            })
        }
    });

    const [updateDepots] = useMutation(UPDATE_DEPOTS, {
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'CURRENT_GARAGE',
                    payload: R.path(R.keys(data))(data)
                })
                showMessage({
                    message: "Данные склада обновлены успешно",
                    type: "success",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                type: "danger",
            })
        }
    });

    const [loadGreeting] = useLazyQuery(DEPOT_QUERY, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'CURRENT_GARAGE',
                    payload: R.path(R.keys(data))(data)
                })
            }
        }
    });

    const destroy = React.useCallback((data) => {
        const title = 'destroy_garage';
        const alertReq = (e) => R.cond([
            [R.equals('Cancel'), () => null],
            [R.equals('Destroy'), () => removedDepots({ variables: { id: data } })],
        ])(R.path(['type'])(e))

        return alertDestroyConfirm({ alertReq, title })
    }, [navigation])

    const update = React.useCallback((data) => updateDepots(data), [navigation])

    const load = React.useCallback((data) => loadGreeting({
        variables: { where: { id: { eq: data } } }
    }), [navigation])

    return [load, update, destroy]
}

export const useReceiveScreenHooks = () => {
    const [state, setState] = React.useState(null)
    const [info, setInfo] = React.useState(null)
    const { depots } = useSelector(state => state.garage, shallowEqual);

    const [loadgreeting] = useLazyQuery(RECEIVE_DEPOTS, {
        fetchPolicy: 'no-cache',
        onCompleted: (data) => {
            setState(R.path(R.concat(R.keys(data), ['items']))(data))
            setInfo(R.path(R.concat(R.keys(data), ['info']))(data))
        }
    })

    const [stockgreeting] = useLazyQuery(RESERVED_DEPOTS, {
        fetchPolicy: 'no-cache',
        onCompleted: (data) => {
            setState(R.path(R.concat(R.keys(data), ['items']))(data))
            setInfo(R.path(R.concat(R.keys(data), ['info']))(data))
        }
    })

    const [currentgreeting] = useLazyQuery(STOCK_TREE_DEPOTS, {
        fetchPolicy: 'no-cache',
        onCompleted: (data) => {
            setState(R.path(['stockGroupsTree'])(data))
        }
    });

    const receiveScreen = React.useCallback(({ screen, page }) => {
        if (!page) {
            setState(null)
        }

        return R.cond([
            [R.equals('Received'), () => loadgreeting({
                variables: {
                    where: { transactions: { depotItem: { depotId: R.path(['id'])(depots) ? { eq: R.path(['id'])(depots) } : {} } } },
                    order: { id: "desc" },
                    paginate: { page: R.defaultTo(1)(page), limit: 12 }
                }
            })],
            [R.equals('BLOCK'), () => stockgreeting({
                variables: {
                    where: {
                        amount: { reserved: { gt: 0 } },
                        depotItems: {
                            depotId: R.path(['id'])(depots) ? { eq: R.path(['id'])(depots) } : {},
                            amount: { reserved: { gt: 0 } }
                        },
                    },
                    order: { id: "desc" },
                    paginate: { page: R.defaultTo(1)(page), limit: 12 }
                }
            })],
            [R.equals('Current'), () => currentgreeting({
                variables: {
                    where: {
                        stocks: {
                            amount: { available: { gt: 0 } },
                            depotItems: {
                                depotId: R.path(['id'])(depots) ? { eq: R.path(['id'])(depots) } : {},
                                amount: { available: { gt: 0 } }
                            }
                        }
                    }
                }
            })],
            [R.T, () => console.log(12312)],
            [R.F, () => console.log(12312)],
        ])(screen)
    }, [depots])

    return [state, receiveScreen, info]
}

export const ElementHooks = () => {
    const { depots } = useSelector(state => state.garage, shallowEqual);

    const [state, setState] = React.useState([]);
    const [click, setClick] = React.useState(false);
    const [loadgreeting] = useLazyQuery(RESERVED_DEPOTS, {
        onCompleted: (data) => {
            setState(R.assocPath(['query'], data)(state))
        },
    });

    const success = React.useCallback((data) => {
        setClick(!click);
        loadgreeting({
            variables: {
                where: {
                    stockGroupId: { eq: data },
                    amount: { available: { gt: 0 } },
                    depotItems: {
                        amount: { available: { gt: 0 } },
                        ...R.path(['id'])(depots) ? {} : { depotId: { eq: R.path(['id'])(depots) } }
                    }
                },
                order: { id: "desc" },
                paginate: {
                    page: 1,
                    limit: 20
                }
            }
        })
    }, []);

    return [state, success]
};