import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';

export const useFileAttach = ({ hack }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const docsMime = [
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.oasis.opendocument.presentation",
        "application/vnd.oasis.opendocument.spreadsheet",
        "application/vnd.oasis.opendocument.text",
        "application/vnd.ms-powerpoint",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/rtf",
        "application/vnd.ms-excel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/xml",
        "application/vnd.visio",
        "application/json",
    ]

    const mimeTypeArray = React.useCallback((x) => R.innerJoin(
        (record, id) => R.cond([
            [R.includes(R.__, R.concat(['application/pdf'], docsMime)), R.pipe(R.equals('document'), R.always)(id)],
            [R.includes('image'), R.pipe(R.equals('image'), R.always)(id)],
            [R.includes('video'), R.pipe(R.equals('video'), R.always)(id)],
            [R.includes('audio'), R.pipe(R.equals('application'), R.always)(id)],
            [R.includes('application'), R.always(R.equals('application')(id))],
            [R.F, R.always(R.equals('application', id))],
            [R.T, R.always(R.equals('application', id))],
        ])(record.mimetype),
        R.defaultTo([])(hack ? R.path(['document', 'attachments'])(goods) : R.path(['attachments'])(task)),
        [R.defaultTo('image')(x)]
    ), [task, hack])

    const attacments = React.useCallback((x) => {
        if (hack) {
            if (R.path(['document', 'attachments'])(goods)) {
                return mimeTypeArray(x)
            }
        }
        if (R.path(['attachments'])(task)) {
            return mimeTypeArray(x)
        }

        return null
    }, [task, goods]);

    return [attacments]
}