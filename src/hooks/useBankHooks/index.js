import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import {
    DESTROY_BANKDETAIL,
    QUERY_COUNTERPARTY,
} from 'screens/Contragent/GQL';
import { UPDATE_CONTRYPARTY } from 'gql/depots'
import { isOdd } from 'utils/helper';
import { useAgentType } from 'hooks/useAgentType'

export const useBankHooks = ({ setState, storage }) => {
    const [agentType] = useAgentType()
    const [data, setData] = React.useState({});
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();

    const [loadGreeting] = useLazyQuery(QUERY_COUNTERPARTY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: R.path([R.keys(data)])(data)
                });
                setState({ isVisible: false });
                showMessage({
                    message: "Успешное обновление реквизитов",
                    description: "Success",
                    type: "info",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const [destroyBankQuery] = useMutation(DESTROY_BANKDETAIL, {
        onCompleted: (data) => {
            if (data) {
                loadGreeting(R.assocPath(['variables', 'where', 'id', 'eq'], R.path(['id'])(agent))({}));
                setState({ isVisible: false })
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const [updateBankQuery] = useMutation(UPDATE_CONTRYPARTY, {
        onCompleted: (data) => {
            if (data) {
                loadGreeting(R.assocPath(['variables', 'where', 'id', 'eq'], R.path(['id'])(agent))({}));
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => setState({ isVisible: false })],
        [R.equals('Destroy'), () => {
            if (R.path(['original', 'id'])(storage)) {
                return destroyBankQuery(R.assocPath(['variables', 'input'], R.path(['original', 'id'])(storage))({}))
            }
            dispatchRedux({
                type: "AGENT_BANK",
                payload: R.pipe(
                    R.path(['bankDetails']),
                    R.defaultTo([]),
                    R.without([R.path(['original'])(storage)])
                )(agent)
            });

            return setState({ isVisible: false })
        }],
    ])(R.path(['type'])(e));

    const updateQuery = React.useCallback((mode) => {
        const Memo = {
            name: R.path(['detail', 'name'])(data),
            currentAccount: R.path(['detail', 'currentAccount'])(data),
            correspondentAccount: R.path(['detail', 'correspondentAccount'])(data),
            rcbic: R.path(['detail', 'rcbic'])(data),
            mainBankDetail: R.path(['detail', 'mainBankDetail'])(data),
            ...R.path(['original', 'id'])(data) ? { id: R.path(['original', 'id'])(data) } : {}
        };

        const MemoUpdate = () => R.pipe(
            R.path(['bankDetails']),
            R.without([R.path(['original'])(storage)]),
            R.append(R.omit(['id'])(Memo)),
            R.reject(isOdd),
        )(agent);

        return R.cond([
            [R.equals('create'), () => updateBankQuery({
                variables: {
                    input: R.assocPath(['bankDetails'], [Memo])({
                        id: R.path(['id'])(agent),
                        inn: R.path(['inn'])(agent),
                        ...agentType
                    })
                }
            })],
            [R.equals('update'), () => updateBankQuery({
                variables: {
                    input: R.assocPath(['bankDetails'], [Memo])({
                        id: R.path(['id'])(agent),
                        inn: R.path(['inn'])(agent),
                        ...agentType
                    })
                }
            })],
            [R.equals('createRedux'), () => {
                dispatchRedux({
                    type: "AGENT_BANK",
                    payload: MemoUpdate()
                });
                setState({ isVisible: false })
            }],
            [R.equals('updateRedux'), () => {
                dispatchRedux({
                    type: "AGENT_BANK",
                    payload: MemoUpdate()
                });
                setState({ isVisible: false })
            }],
            [R.T, () => null],
            [R.F, () => null],
        ])(mode)
    }, [agent, data, agentType, storage])

    return [data, setData, updateQuery, alertReq]
};
