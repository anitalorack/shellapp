/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { ALL_EMPLOYERS } from 'gql/employee';
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper';

export const useEmployeeHooks = ({ actor, mode }) => {
    const [state, setState] = React.useState({});
    const userData = useSelector(state => state.profile, shallowEqual);
    const [employee] = useLazyQuery(ALL_EMPLOYERS, {
        onCompleted: (data) => {
            setState({ actor: R.head(R.path(["employees", "items"])(data)) })
        }
    });

    React.useEffect(() => {
        setState({ actor })
        if (R.anyPass([R.isNil, R.isEmpty, R.equals('author')])(mode)) {
            if (isOdd(actor)) {
                employee({ variables: { where: { phone: { eq: R.path(['phone', 'regex'])(userData) } } } })
            }
        }
    }, [actor]);

    return [state]
}