/* eslint-disable id-length */
/* eslint-disable max-statements */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-vars */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper'
import {
    CREATE_CONTREPARITY,
    DESTROY_CONTREPARITY,
    FIND_COUNTERPARITY,
    QUERY_COUNTERPARTY,
    UPDATE_CONTREPARITY
} from 'screens/Contragent/GQL';
import moment from 'moment'
import { useAgentType } from 'hooks/useAgentType'

export const useContragentHooks = () => {
    const [agentType] = useAgentType()
    const [state, setState] = React.useState({});
    const dispatchRedux = useDispatch();
    const { agent } = useSelector(state => state.agent, shallowEqual);

    const [loadGreeting] = useLazyQuery(QUERY_COUNTERPARTY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: R.path([R.keys(data)])(data)
                });
                showMessage({
                    message: "Успешно получилось обновить данные",
                    description: "Success",
                    type: "success",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const [createQuery] = useMutation(CREATE_CONTREPARITY, {
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: R.path(R.keys(data))(data)
                });
                showMessage({
                    message: "Успешно получилось обновить данные",
                    description: "Success",
                    type: "success",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const [updateQuery] = useMutation(UPDATE_CONTREPARITY, {
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: R.path(R.keys(data))(data)
                });
                showMessage({
                    message: "Успешно получилось обновить данные",
                    description: "Success",
                    type: "success",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const [destroyQuery] = useMutation(DESTROY_CONTREPARITY, {
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: R.path(R.keys(data))(data)
                });
                showMessage({
                    message: "Успешно получилось обновить данные",
                    description: "Success",
                    type: "success",
                });
            }
        },
        onError: (data) => {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const main = (x) => {
        const certificate = (x) => {
            if (R.path(['certificateDate'])(x)) {
                return R.mergeAll([x, { certificateDate: moment.utc(R.path(['certificateDate'])(x)) }])
            }

            return x
        }

        return R.pipe(
            certificate,
            R.omit(['physicalAddress', 'legalAddress', 'registrationAddress', 'bankDetails', '__typename']),
            R.reject(isOdd),
        )(x)
    };

    const bankDetailConfig = (x) => R.pipe(
        R.paths([['name'], ['currentAccount'], ['correspondentAccount'], ['rcbic'], ['mainBankDetail']]),
        R.zipObj(['name', 'currentAccount', 'correspondentAccount', 'rcbic', 'mainBankDetail'])
    )(x);

    const MERGE = () => R.mergeAll([
        main(agent),
        isOdd(R.path(['id'])(agent)) ? {} : { id: R.path(['id'])(agent) },
        agentType,
        { physicalAddress: R.pipe(R.path(['physicalAddress']), R.omit(['__typename']))(agent) },
        { legalAddress: R.pipe(R.path(['legalAddress']), R.omit(['__typename']))(agent) },
        { registrationAddress: R.pipe(R.path(['registrationAddress']), R.omit(['__typename']))(agent) },
        R.pipe(R.path(['id']), R.not)(agent) ? R.assocPath(['bankDetails'], R.pipe(
            R.path(['bankDetails']),
            R.defaultTo([]),
            R.map(bankDetailConfig),
            R.reject(isOdd))(agent),
            {}) : {},
    ]);

    const ContAgentrequest = React.useCallback((mode, data, origin) => R.cond([
        [R.equals('create'), () => {
            createQuery({
                variables: { input: MERGE() }
            })
        }],
        [R.equals('update'), () => {
            updateQuery({
                variables: { input: MERGE() }
            })
        }],
        [R.equals('destroy'), () => destroyQuery({
            variables: { input: R.path(['id'])(agent) }
        })],
        [R.equals('addedAddress'), () => dispatchRedux({
            type: 'AGENT_UPDATE',
            payload: R.mergeAll([agent, state])
        })],
        [R.equals('input'), () => dispatchRedux({
            type: 'AGENT_UPDATE',
            payload: R.mergeAll([agent, state])
        })],
        [R.equals('fetch'), () => loadGreeting({
            variables: { where: { id: { eq: R.path(['id'])(agent) } } }
        })],
        [R.T, () => null],
        [R.F, () => null]
    ])(mode), [agent])

    return [state, ContAgentrequest, (e) => setState(e)]
};
