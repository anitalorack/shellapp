/* eslint-disable prefer-template */
import * as R from 'ramda';
import React from 'react';
import RNFetchBlob from 'rn-fetch-blob';
import { getItem } from 'utils/async';
import { showMessage } from "react-native-flash-message"

export const useDownload = () => {
    const token = React.useCallback(async () => {
        const jswToken = await getItem();

        return R.assoc("Authorization", `Bearer ${jswToken}`, {})
    }, []);

    const download = React.useCallback((data) => RNFetchBlob
        .config({
            addAndroidDownloads: {
                useDownloadManager: true,
                mediaScannable: true,
                notification: true,
                mime: R.path(['mimetype'])(data),
                overwrite: true,
                path: RNFetchBlob.fs.dirs.DownloadDir + "/Shell-Mobile/" + R.path(['filename'])(data)
            }
        })
        .fetch('GET', R.path(['file', 'url'])(data), { headers: token })
        .then(() => showMessage({
            message: "File success download",
            description: "Success",
            type: "success",
        })
        ), []);

    return [download]
};
