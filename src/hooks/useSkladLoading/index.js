/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { CREATE_DEPOTS } from 'gql/billAccount';
import { MEASURE } from 'gql/billAccount/index';
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { MainScreen } from 'utils/routing/index.json';

export const useSkladLoading = ({ navigation }) => {
    const { depots } = useSelector(item => item.depots, shallowEqual);
    const dispatchRedux = useDispatch();

    const [depotsItem] = useLazyQuery(MEASURE, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: "DEPOTS_MERGE", payload: { ...data }
                })
            }
        }
    });

    const [createDepots] = useMutation(CREATE_DEPOTS, {
        onCompleted: (data) => {
            if (data) {
                mergeItem(data)
                dispatchRedux({
                    type: 'CURRENT_GARAGE',
                    payload: R.path(R.keys(data))(data)
                })
            }
        }
    });

    const mergeItem = React.useCallback((data) => {
        depotsItem()
        navigation.navigate(MainScreen.Garage, { stock: R.path(['createDepot', 'id'])(data) })
    }, [depots])

    const CreateDepots = React.useCallback((data) => createDepots(data), [])

    const queryDepots = React.useCallback(() => {
        depotsItem()
    }, [])


    return [R.defaultTo([])(R.path(['items'])(depots)), queryDepots, CreateDepots]
}
