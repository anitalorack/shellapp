/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import { Colors, IconButton } from 'react-native-paper';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper';

export const useGarageFace = () => {
    const dispatchRedux = useDispatch()
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const depots = useSelector(state => state.depots, shallowEqual);

    const update = React.useCallback((data) => R.cond([
        [R.equals(), () => { }],
        [R.equals(), () => { }],
        [R.equals(), () => { }],
    ])(data), [])

    const headerGroup = React.useCallback((data) => {
        const currentDroped = R.head(R.innerJoin(
            (record, id) => record.id === id,
            R.defaultTo([])(R.path(['depots', 'items'])(depots)),
            R.pipe(R.reject(isOdd))([
                R.defaultTo(R.path(['depotId'])(data))(R.path(['depots', 'id'])(data)),
            ])
        ));

        const list = [
            R.path(['name'])(currentDroped),
            R.join(' / ', R.paths([['depotItem', 'stock', 'stockGroup', 'name'], ['depotItem', 'stock', 'stockGroup', 'parent', 'name']])(data))
        ]

        return R.addIndex(R.map)((x, key) => (
            <View
                key={key}
                style={{ flex: 1 }}
            >
                <Text
                    style={[
                        stylex.text,
                        { textAlign: 'left' },
                        { fontFamily: R.equals(key, 0) ? 'ShellBold' : 'ShellLight' },
                        { fontSize: R.equals(key, 0) ? 13 : 11 }
                    ]}
                >
                    {x}
                </Text>
            </View>
        ))(list)
    }, [depots, goods])

    const summary = React.useCallback((data) => (
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>

            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <IconButton
                    color={Colors.red500}
                    icon="delete"
                    onPress={() => dispatchRedux({
                        type: 'ADD_GOODS',
                        payload: R.mergeAll([
                            goods, {
                                items: R.pipe(
                                    R.path(['items']),
                                    R.without([data])
                                )(goods)
                            }
                        ])
                    })}
                    size={20}
                />
            </View>
        </View>
    ), [goods])

    const totalCard = React.useCallback((data) => R.addIndex(R.map)((x, key) => (
        <Text
            key={key}
            style={[
                stylex.text,
                { textAlign: 'left', fontFamily: 'ShellMedium' },
                { color: R.equals(key, 0) ? Colors.green800 : Colors.black500 }
            ]}
        >
            {x}
        </Text>
    ))(R.paths([['depotItem', 'stock', 'brand'], ['depotItem', 'stock', 'name']])(data)), [])

    return [headerGroup, totalCard, summary, update]
}

const stylex = StyleSheet.create({
    card: {
        margin: 1,
        height: 80,
        backgroundColor: Colors.grey300,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontFamily: 'ShellBold',
        fontSize: 13,
        justifyContent: 'center',
        textAlign: 'center'
    },
    view: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 3,
        justifyContent: 'space-between',
    },
    start: {
        flex: 1,
    },
    view2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5
    },
    footer: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-between'
    }
})