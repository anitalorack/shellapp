import React from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda'
import { useLazyQuery } from '@apollo/react-hooks';
import { REPAIR_QUERY } from 'gql/task';

export const useTaskHook = () => {
    const dispatchRedux = useDispatch();
    const { task } = useSelector(state => state.task, shallowEqual);
    const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'TASK_UPDATE',
                    payload: R.path(R.keys(data))(data)
                })
            }
        }
    });

    const taskUpdate = React.useCallback((mode, data) => R.cond([
        [R.equals('createVehicle'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: {
                vehicle: {
                    vin: null,
                    plate: null,
                }
            }
        })],
        [R.equals('update'), () => {
            loadgreeting({
                variables: {
                    where: {
                        id: { eq: R.path(['id'])(data) }
                    }
                }
            })
        }],
        [R.equals('createList'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, { owner: { "__typename": "Owner" } }])
        })],
        [R.equals('create'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: {}
        })],
        [R.equals('SelectVehicle'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: {
                vehicle: data
            }
        })],
        [R.T, () => null]
    ])(mode), []);

    return [taskUpdate]
};