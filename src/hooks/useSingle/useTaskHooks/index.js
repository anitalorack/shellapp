/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { REPAIR_QUERY } from 'gql/task';
import { MainScreen, Preview } from 'utils/routing/index.json';

export const useTaskNavigation = ({ navigation }) => {
    const { task } = useSelector(state => state.task, shallowEqual);

    const ROUTER = Preview.PreviewTaskGarage;
    const ROUTERMAIN = MainScreen.Main
    const dispatchRedux = useDispatch()

    const [loadGreeting] = useLazyQuery(REPAIR_QUERY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'TASK_UPDATE',
                    payload: R.path([R.keys(data)])(data)
                });
            }
        },
        onError: (data) => {
            dispatchRedux({
                type: 'TASK_UPDATE',
                payload: {}
            });
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const Taskagent = React.useCallback((data) => {
        if (R.equals('Edit', R.path(['type'])(data))) {
            loadGreeting({
                variables: {
                    where: {
                        id: {
                            eq: R.path(['payload', 'id'])(data)
                        }
                    }
                }
            })
        }

        return R.cond([
            [R.equals('Create'), () => {
                dispatchRedux({
                    type: 'TASK_UPDATE',
                    payload: {}
                });
                navigation.navigate(ROUTER)
            }],
            [R.equals('Edit'), () => navigation.navigate(ROUTER)],
        ])(R.path(['type'])(data))
    }, [])

    const ListScreen = React.useCallback((data) =>
        navigation.navigate(
            ROUTERMAIN, { list: data }
        ), [])

    const FuctionTask = React.useCallback((data) => R.cond([
        [R.equals('fetch'), () => loadGreeting({
            variables: { where: { id: { eq: R.path(['payload'])(data) } } }
        })],
        [R.equals('image'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, R.path(['payload'])(data)])
        })],
        [R.equals('video'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, R.path(['payload'])(data)])
        })],
        [R.equals('vehicle'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, R.path(['payload'])(data)])
        })],
        [R.equals('owner'), () => dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, R.path(['payload'])(data)])
        })],
    ])(R.path(['type'])(data)))

    return [Taskagent, ListScreen, FuctionTask]
}
