/* eslint-disable max-statements */
import { useDispatch, shallowEqual, useSelector } from 'react-redux';
import { useLazyQuery } from '@apollo/react-hooks';
import { BILLACCOUNT } from 'gql/billAccount';
import * as R from 'ramda'
import React from 'react'
import moment from 'moment'
import { FIND_EMPLOYERS } from 'gql/employee';

export const useProfile = () => {
    const dispatchRedux = useDispatch()
    const userData = useSelector(state => state.profile, shallowEqual);
    const { employee } = useSelector(state => state.employee, shallowEqual);

    const [loadgreeting] = useLazyQuery(FIND_EMPLOYERS, {
        onCompleted: (data) => {
            dispatchRedux({
                type: 'EMPLOYEE_ADD',
                payload: R.assocPath(['employee'], R.path(R.keys(data))(data), {})
            })
        },
        fetchPolicy: "no-cache"
    });

    const update = React.useCallback(() => {
        loadgreeting({ variables: { input: { phone: { eq: R.path(['phone', 'regex'])(userData) } } } })
    }, [userData])

    return [employee, update]
}

export const useEmployeeProfile = () => {
    const dispatchRedux = useDispatch()
    const { employee } = useSelector(state => state.employee, shallowEqual);

    const [state, setState] = React.useState({
        now: moment().format("DD.MM.YYY Y HH:mm")
    });

    const [billAccount] = useLazyQuery(BILLACCOUNT, {
        onCompleted: (data) => {
            dispatchRedux({
                type: "DEPOTS_MERGE",
                payload: { bill: data }
            });
            setState(R.mergeAll([{
                now: moment()
                    .format("DD.MM.YYYY HH:mm")
            }, R.path(['billAccount'])(data)]))
        },
        fetchPolicy: "network-only"
    });

    const update = React.useCallback(() => {
        if (!R.path(['__typename'])(state)) {
            billAccount({
                variables: { where: { garageId: { eq: R.path(['garageId'])(employee) } } }
            })
        }
    }, [employee]);

    return [state, update]
}