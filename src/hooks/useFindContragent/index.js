/* eslint-disable id-length */
/* eslint-disable max-statements */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-vars */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper'
import {
    FIND_COUNTERPARITY,
} from 'screens/Contragent/GQL';
import { useAgentType } from 'hooks/useAgentType'

export const useFindContragent = () => {
    const [agentType] = useAgentType()
    const dispatchRedux = useDispatch();
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const [state, setState] = React.useState(null)

    const [findGreeting] = useLazyQuery(FIND_COUNTERPARITY, {
        fetchPolicy: "no-cache",
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'AGENT_UPDATE',
                    payload: isOdd(R.path(R.keys(data))(data)) ? { inn: state } : R.omit(['id'])(R.path(R.keys(data))(data))
                });
                showMessage({
                    message: isOdd(R.path(R.keys(data))(data)) ? "Ничего не найдено" : "Успешно получилось обновить данные",
                    description: isOdd(R.path(R.keys(data))(data)) ? 'Info' : "Success",
                    type: isOdd(R.path(R.keys(data))(data)) ? 'info' : "success",
                });
            }
        },
        onError: (data) => {
            dispatchRedux({
                type: 'AGENT_UPDATE',
                payload: { inn: state }
            });
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }
    });

    const update = React.useCallback((element, data) => {
        setState(data)
        const position = R.cond([
            [R.equals("LEGAL_ENTITY"), R.always(9)],
            [R.equals('INDIVIDUAL'), R.always(11)],
            [R.equals('SOLE_TRADER'), R.always(11)],
            [R.T, R.always(9)],
            [R.F, R.always(9)],
        ])(R.path(['type'])(agentType));

        if (R.pipe(R.path(['id']), R.not)(agent)) {
            if (R.equals(element.label, 'innByAgent')) {
                if (R.length(data) > position) {
                    setState(data)
                    findGreeting({
                        variables: { where: { inn: data } }
                    })
                }
            }
        }
    }, [agent, agentType])

    return [update]
}

