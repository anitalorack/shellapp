/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { WORKCATALOG } from 'gql/billAccount';
import { isOdd } from 'utils/helper';
import {
    CREATE_REPAIR_WORK, DESTROY_REPAIR_WORK, REPAIR_QUERY, UPDATE_REPAIR_WORK
} from 'gql/task';

export const useWorkHooks = () => {
    const [state, setState] = React.useState([])
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();

    const dataRepars = (data) => {
        if (data) {
            dispatchRedux({ type: 'TASK_UPDATE', payload: R.path(R.union(R.keys(data), ['repair']))(data) })
            showMessage({
                message: 'Данные успешно обновились',
                description: "Success",
                type: "success",
            })
        }
    }

    const errorRepars = (data) => {
        if (data) {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            })
        }
    }

    const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => dataRepars(data),
        onError: (data) => errorRepars(data)
    });

    const [destroyRepairWork] = useMutation(DESTROY_REPAIR_WORK, {
        onCompleted: (data) => dataRepars(data),
        onError: (data) => errorRepars(data)
    });

    const [cataloggreeting] = useLazyQuery(WORKCATALOG, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            setState(R.path(R.keys(data))(data))
        },
        onError: () => {
            showMessage({
                message: R.toString("ПРОВЕРЬТЕ ПРАВИЛЬНОСТЬ ДАННЫХ"),
                description: "Error",
                type: "info",
            })
            setState([])
        }
    });

    const fetch = React.useCallback(() => loadgreeting({ variables: { where: { id: { eq: R.path(['id'])(task) } } } }), [])
    const catalog = React.useCallback(() => {
        if (R.path(['vehicle', 'modification', 'id'])(task)) {
            return cataloggreeting({
                variables: { where: { modificationId: { eq: R.path(['vehicle', 'modification', 'id'])(task) } } }
            })
        }

        return setState([])
    }, [])

    const destroy = React.useCallback((data) => {
        if (R.path(['id'])(data)) {
            return destroyRepairWork({ variables: { input: R.path(['id'])(data) } })
        }
        dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, { works: R.without([data], R.path(['works'])(task)) }]),
        })
    }, [])

    return [state, fetch, catalog, destroy]
}

export const useWorkBankHooks = ({ navigation, state }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();

    const dataRepars = (data, mode) => {
        if (data) {
            dispatchRedux({ type: 'TASK_UPDATE', payload: R.path(R.union(R.keys(data), ['repair']))(data) })
            if (!mode) {
                navigation({ type: "CLOSED" })
            }
            showMessage({
                message: 'Данные успешно обновились',
                description: "Success",
                type: "success",
            })
        }
    }

    const errorRepair = (data) => {
        if (data) {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            })
        }
    }

    const [updateRepairWork] = useMutation(UPDATE_REPAIR_WORK, {
        onCompleted: (data) => dataRepars(data),
        onError: (data) => errorRepair(data)
    });

    const [createRepairWork] = useMutation(CREATE_REPAIR_WORK, {
        onCompleted: (data) => {
            const mode = true
            dataRepars(data, mode)
        },
        onError: (data) => errorRepair(data)
    });

    const closed = React.useCallback(() => {
        navigation({ type: 'CLOSED' })
    }, [])

    const create = React.useCallback((data) => {
        createRepairWork({
            variables: {
                input: R.pipe(
                    R.reject(isOdd),
                    R.pipe(R.reject(isOdd),
                        R.omit(['id']))
                )({
                    repairId: R.path(['id'])(task),
                    unitCount: parseFloat(R.path(['detail', 'unitCount'])(data)),
                    unitPrice: {
                        amount: parseFloat(R.defaultTo('0')(R.path(['detail', 'unitPrice', 'amount'])(data)))
                    },
                    timeHrs: parseFloat(R.path(['detail', 'timeHrs'])(data)),
                    discount: {
                        percent: parseFloat(R.defaultTo('0')(R.path(['detail', 'discount', 'percent'])(data))),
                        amount: parseFloat(R.defaultTo('0')(R.path(['detail', 'discount', 'amount'])(data)))
                    },
                    name: R.path(['detail', 'name'])(data),
                    id: R.path(['original', 'id'])(data),
                })
            }
        })
    }, [])

    const update = React.useCallback((data) => {
        updateRepairWork({
            variables: {
                input: R.pipe(R.reject(isOdd),
                    R.pipe(R.reject(isOdd),
                        R.omit(['repairId']))
                )({
                    repairId: R.path(['id'])(task),
                    unitCount: parseFloat(R.path(['detail', 'unitCount'])(data)),
                    unitPrice: {
                        amount: parseFloat(R.defaultTo('0')(R.path(['detail', 'unitPrice', 'amount'])(data)))
                    },
                    timeHrs: parseFloat(R.path(['detail', 'timeHrs'])(data)),
                    discount: {
                        percent: parseFloat(R.defaultTo('0')(R.path(['detail', 'discount', 'percent'])(data))),
                        amount: parseFloat(R.defaultTo('0')(R.path(['detail', 'discount', 'amount'])(data)))
                    },
                    name: R.path(['detail', 'name'])(data),
                    id: R.path(['original', 'id'])(data),
                })
            }
        })
    }, [])

    const redux = React.useCallback((data) => {
        dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, {
                works: R.append({
                    repairId: R.path(['id'])(task),
                    unitCount: parseFloat(R.path(['detail', 'unitCount'])(data)),
                    unitPrice: {
                        amount: parseFloat(R.defaultTo('0')(R.path(['detail', 'unitPrice', 'amount'])(data)))
                    },
                    timeHrs: parseFloat(R.path(['detail', 'timeHrs'])(data)),
                    discount: {
                        percent: parseFloat(R.defaultTo('0')(R.path(['detail', 'discount', 'percent'])(data))),
                        amount: parseFloat(R.defaultTo('0')(R.path(['detail', 'discount', 'amount'])(data)))
                    },
                    name: R.path(['detail', 'name'])(data),
                    id: R.path(['original', 'id'])(data),
                },
                    R.without([state])(R.pipe(R.path(['works']), R.defaultTo([]))(task)))
            }]),
        })
        showMessage({
            message: 'Данные успешно изменились',
            description: "Success",
            type: "success",
        })

        return navigation({ type: 'CLOSED' })
    }, [])

    return [create, update, redux, closed]
}
