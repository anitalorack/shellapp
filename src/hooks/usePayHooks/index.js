/* eslint-disable no-unused-vars */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable max-statements */
import { useMutation } from '@apollo/react-hooks';
import { CreateBillPayment, CREATE_BILL_INVOICE } from 'gql/billAccount';
import * as R from 'ramda';
import React from 'react';
import { Linking } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { shallowEqual, useSelector } from 'react-redux';
import { usePdfManipulationFile } from 'hooks/usePdfManipulationFile'

export const usePayHooks = ({ dispatch, initialStorage }) => {
    const [state, setState] = React.useState(initialStorage);
    const [document, shareFile, alertPdfMessage] = usePdfManipulationFile()

    const selected = useSelector(state => R.path(['depots', 'bill', 'billAccount', 'id'])(state), shallowEqual);

    const [billAccount] = useMutation(CREATE_BILL_INVOICE, {
        onCompleted: (data) => {
            billPayment(R.assocPath(['variables', 'input'], {
                billInvoiceId: R.path(['createBillInvoice', 'id'])(data),
                amount: {
                    amount: R.pipe(
                        R.paths([['mode', 'price', 'amount'], ['mode', 'priceCount']]),
                        R.product
                    )(state)
                }
            }, {}));
            if (R.path(['check'])(state)) {
                alertPdfMessage({ title: "confirm_payment", params: { id: R.path(['params', 'createBillInvoice', 'id'])(data), url: "invoice" } })

                return dispatch({ type: "Create", payload: null })
            }
            showMessage({
                message: "Подготавливается файл отчет",
                description: "Success",
                type: "success",
            });
        }
    });
    const [billPayment] = useMutation(CreateBillPayment, {
        onCompleted: (data) => request(R.path(['bill', 'data'])(state), data)
    });

    const request = React.useCallback((id, data) => R.cond([
        [R.equals('CLOSED'), () => {
            dispatch(false)
            setState({})
        }],
        [R.equals('pay'), () => Linking.openURL(R.path(['createBillPayment', 'confUrl'])(data))],
        [R.equals('APPLY'), () => setState(R.assocPath(['modal'], 'Update', state))],
        [R.equals('BACK'), () => setState(R.assocPath(['check'], false)(state))],
        [R.equals('COAST'), () => billAccount(
            R.assocPath(['variables', 'input'], {
                billAccountId: selected,
                billServiceIds: R.path(['mode', 'id'])(state)
            }, {}))],
        [R.equals('PAY'), () => {
            if (R.equals(R.path(['bill', 'data'])(state), 'pay')) {
                return billAccount(R.assocPath(['variables', 'input'],
                    { billAccountId: selected, billServiceIds: R.path(['mode', 'id'])(state) }, {}))
            }

            return setState(R.assocPath(['check'], true)(state))
        }],
        [R.T, () => null],
        [R.F, () => null],
    ])(id), [selected, state])

    const useData = React.useMemo(() => state, [state])

    return [request, useData, setState]
}
