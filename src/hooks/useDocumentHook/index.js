/* eslint-disable no-unused-vars */
/* eslint-disable jsx-quotes */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { usePdfManipulationFile } from 'hooks/usePdfManipulationFile'
import { useDownload } from 'hooks/useDownload';
import i18n from 'localization';
import ADD from 'assets/svg/docs';
import PDF from 'assets/svg/pdf';
import * as R from 'ramda';
import Video from 'react-native-video';
import React from 'react';
import { SvgXml } from 'react-native-svg';
import {
    ImageBackground,
    Alert,
    View,
    StyleSheet
} from 'react-native';
import { WebView } from 'react-native-webview';
import { shallowEqual, useSelector } from 'react-redux';
import { OfficeViewer } from '@sishuguojixuefu/react-native-office-viewer'
import { isOdd } from 'utils/helper'

export const useDocumentHook = ({ hack }) => {
    const [select, shareFile, alertPdfMessage] = usePdfManipulationFile()
    const [download] = useDownload()
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const ref = React.useRef()

    const docsMime = [
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.oasis.opendocument.presentation",
        "application/vnd.oasis.opendocument.spreadsheet",
        "application/vnd.oasis.opendocument.text",
        "application/vnd.ms-powerpoint",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/rtf",
        "application/vnd.ms-excel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/xml",
        "application/vnd.visio",
        "application/json"
    ]

    const actionFunction = React.useCallback((x) => {
        const modification = hack ? goods : task

        if (isOdd(x)) return null
        if (R.includes(x.mimetype, docsMime)) {
            return R.assocPath(['inOfficeView'], x)(modification)
        }
        if (R.equals(x.mimetype, "application/pdf")) {
            alertPdfMessage({ params: { url: R.path(['file', 'url'])(x) } })

            return null
        }
        if (R.includes('text', x.mimetype)) {
            return R.assocPath(['inWebView'], x)(modification)
        }
        if (R.includes('html', x.mimetype)) {
            return R.assocPath(['inWebView'], x)(modification)
        }
        if (R.includes('json', x.mimetype)) {
            return R.assocPath(['inWebView'], x)(modification)
        }
        if (R.includes('image', x.mimetype)) {
            return R.assocPath(['imageView'], x)(modification)
        }
        if (R.includes('audio', x.mimetype)) {
            return R.assocPath(['videoView'], x)(modification)
        }
        if (R.includes('video', x.mimetype)) {
            return R.assocPath(['videoView'], x)(modification)
        }
        const title = 'destroy_goods';

        Alert.alert(
            i18n.t("HeaderAlert"),
            i18n.t(title),
            [
                { text: i18n.t('Cancel'), onPress: () => null, style: "cancel" },
                { text: i18n.t("Share"), onPress: () => shareFile(R.path(['files', 'url'])(x)) },
                { text: i18n.t("Download"), onPress: () => download(x) },
            ],
            { cancelable: false }
        )

        return null;
    }, [task, goods, hack])

    const currentFunction = React.useCallback((document) => {
        if (R.includes(document.mimetype, docsMime)) {
            return (
                <OfficeViewer
                    containerStyle={stylesSheets.image}
                    source={R.path(['file', 'url'])(document)}
                />
            )
        }
        if (R.equals(document.mimetype, "application/pdf")) {
            return (
                <View />
            )
        }
        if (R.includes('text', document.mimetype)) {
            return (
                <WebView
                    source={{ uri: R.path(['file', 'url'])(document) }}
                    style={{ flex: 1 }}
                />
            )
        }
        if (R.includes('html', document.mimetype)) {
            return (
                <WebView
                    source={{ uri: R.path(['file', 'url'])(document) }}
                    style={{ flex: 1 }}
                />
            )
        }
        if (R.includes('json', document.mimetype)) {
            return (
                <WebView
                    source={{ uri: R.path(['file', 'url'])(document) }}
                    style={{ flex: 1 }}
                />
            )
        }
        if (R.includes('audio', document.mimetype)) {
            return (
                <View style={{ flex: 1 }}>
                    <ImageBackground
                        source={{ uri: 'https://gazeta.a42.ru/uploads/d3a/d3ab7a20-f14e-11e8-a7aa-d139c7dab871.jpg' }}
                        style={stylesSheets.image}
                    />
                    <Video
                        ref={ref}
                        repeat
                        source={{ uri: R.path(['file', 'url'])(document) }}
                    />
                </View>
            )
        }
        if (R.includes('video', document.mimetype)) {
            return (
                <Video
                    ref={ref}
                    repeat
                    source={{ uri: R.path(['file', 'url'])(document) }}
                    style={stylesSheets.image}
                />
            )
        }
        if (R.includes('image', document.mimetype)) {
            return (
                <ImageBackground
                    source={{ uri: R.path(['file', 'url'])(document) }}
                    style={stylesSheets.image}
                />
            )
        }

        return (
            <View />
        )
    }, [])

    return [currentFunction, actionFunction]
}

const stylesSheets = StyleSheet.create({
    icon: {
        position: 'absolute',
        width: 50,
        height: 50,
        right: 0,
        backgroundColor: 'white',
        borderBottomLeftRadius: 50
    },
    text: {
        padding: 10,
        margin: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        position: 'absolute',
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    touch: {
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        width: 200,
    }
});