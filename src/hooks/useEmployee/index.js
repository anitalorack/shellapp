/* eslint-disable max-statements */
import React from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { CreateScreen, MainScreen } from 'utils/routing/index.json';
import { CREATE_EMPLOYEE, DESTROY_EMPLOYEE, UPDATE_EMPLOYEE } from 'screens/Master/CreateEmployeeScreen/GQL';
import { useMutation } from '@apollo/react-hooks';
import * as R from 'ramda'
import { showMessage } from "react-native-flash-message";

export const useEmployee = ({ navigation }) => {
    const dispatchRedux = useDispatch();

    const navigate = React.useCallback((data) => {
        dispatchRedux({
            type: 'employee_update',
            payload: data
        })
        navigation.navigate(CreateScreen.MasterScreen)
    }, [])

    return [navigate]
}

export const useMutateEmployee = ({ navigation }) => {
    const { employees } = useSelector(state => state.employees, shallowEqual);
    const { employee } = useSelector(state => state.employee, shallowEqual);

    const dispatchRedux = useDispatch();

    const [createEmployee] = useMutation(CREATE_EMPLOYEE, {
        onCompleted: (data) => {
            showMessage({
                message: "Success",
                description: "Данные успешно обновлены",
                type: "success",
            })
            dispatchRedux({
                type: 'employee_update',
                payload: R.path(R.keys(data))(data)
            })
        },
        onError: (data) => showMessage({
            message: "Error",
            description: R.toString(data),
            type: "danger",
        })
    });
    const [updateEmployee] = useMutation(UPDATE_EMPLOYEE, {
        onCompleted: (data) => {
            showMessage({
                message: "Success",
                description: "Данные успешно обновлены",
                type: "success",
            })
            dispatchRedux({
                type: 'employee_update',
                payload: R.path(R.keys(data))(data)
            })
            navigation.navigate(MainScreen.Master)
        },
        onError: (data) => showMessage({
            message: "Error",
            description: R.toString(data),
            type: "danger",
        })
    });

    const [removeEmployee] = useMutation(DESTROY_EMPLOYEE, {
        onCompleted: () => {
            showMessage({
                message: "Success",
                description: "Пользователь успешно удален",
                type: "success",
            });
            navigation.navigate(MainScreen.Master)
        },
        onError: (data) => showMessage({
            message: "Error",
            description: R.toString(data),
            type: "danger",
        })
    });

    const create = React.useCallback(() => {
        createEmployee({
            variables: {
                input: {
                    garageId: R.path(['garage', 'id'])(employee),
                    role: R.path(['role'])(employees),
                    phone: R.path(['phone'])(employees),
                    blocked: R.path(['blocked'])(employees),
                    name: R.pipe(R.path(['name']), R.omit(['__typename']))(employees),
                }
            }
        })
    }, [employees])

    const update = React.useCallback(() => updateEmployee({
        variables: {
            input: {
                role: R.path(['role'])(employees),
                id: R.path(['id'])(employees),
                phone: R.path(['phone'])(employees),
                blocked: R.path(['blocked'])(employees),
                name: R.pipe(R.path(['name']), R.omit(['__typename']))(employees),
            }
        }
    }), [employees])

    const destroy = React.useCallback(() => removeEmployee({ variables: { input: R.path(['id'])(employees) } }), [])

    const mergeKeys = React.useCallback((data) => R.cond([
        [R.equals('role'), () => dispatchRedux({
            type: 'employee_update',
            payload: R.path(['payload'])(data)
        })],
        [R.equals('last'), () => dispatchRedux({
            type: 'employee_update',
            payload: R.mergeAll([employees, { name: R.mergeAll([R.path(['name'])(employees), R.path(['payload', 'name'])(data)]) }])
        })],
        [R.equals('first'), () => dispatchRedux({
            type: 'employee_update',
            payload: R.mergeAll([employees, { name: R.mergeAll([R.path(['name'])(employees), R.path(['payload', 'name'])(data)]) }])
        })],
        [R.equals('middle'), () => dispatchRedux({
            type: 'employee_update',
            payload: R.mergeAll([employees, { name: R.mergeAll([R.path(['name'])(employees), R.path(['payload', 'name'])(data)]) }])
        })],
        [R.equals('phone'), () => dispatchRedux({
            type: 'employee_update',
            payload: R.assocPath(['phone'], R.path(['payload', 'phone'])(data))(employees)
        })],
        [R.equals('blocked'), () => dispatchRedux({
            type: 'employee_update',
            payload: R.assocPath(['blocked'], !R.path(['blocked'])(employees))(employees)
        })],
        [R.T, () => null],
        [R.F, () => null],
    ])(R.path(['type'])(data)), [employees])

    return [create, update, destroy, mergeKeys]
}