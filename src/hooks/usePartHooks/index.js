/* eslint-disable radix */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { WORKCATALOG } from 'gql/billAccount';
import {
    CREATE_REPAIR_PARTS,
    DESTROY_REPAIR_PARTS,
    REPAIR_QUERY,
    UPDATE_REPAIR_PARTS
} from 'gql/task'

export const usePartsHooks = () => {
    const [state, setState] = React.useState([]);
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();

    const dataRepars = (data) => {
        if (data) {
            dispatchRedux({
                type: 'TASK_UPDATE',
                payload: R.path(R.keys(data))(data)
            });
            showMessage({
                message: 'Данные успешно обновились',
                description: "Success",
                type: "success",
            })
        }
    };

    const errorRepars = (data) => {
        if (data) {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            })
        }
    };

    const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => dataRepars(data),
        onError: (data) => errorRepars(data)
    });

    const [destroyRepairWork] = useMutation(DESTROY_REPAIR_PARTS, {
        onCompleted: (data) => {
            if (data) {
                dispatchRedux({
                    type: 'TASK_UPDATE',
                    payload: R.path(R.union(R.keys(data), ['repair']))(data)
                });
                showMessage({
                    message: 'Данные успешно обновились',
                    description: "Success",
                    type: "success",
                })
            }
        },
        onError: (data) => errorRepars(data)
    });

    const [cataloggreeting] = useLazyQuery(WORKCATALOG, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            setState(R.path(R.keys(data))(data))
        },
        onError: () => {
            showMessage({
                message: R.toString("ПРОВЕРЬТЕ ПРАВИЛЬНОСТЬ ДАННЫХ"),
                description: "Error",
                type: "info",
            });
            setState([])
        }
    });

    const fetch = React.useCallback(() => loadgreeting({
        variables: {
            where: {
                id: {
                    eq: R.path(['id'])(task)
                }
            }
        }
    }), []);
    const catalog = React.useCallback(() => {
        if (R.path(['vehicle', 'modification', 'id'])(task)) {
            return cataloggreeting({
                variables: { where: { modificationId: { eq: R.path(['vehicle', 'modification', 'id'])(task) } } }
            })
        }
        setState([])
    }, [task]);

    const destroy = React.useCallback((data) => {
        if (R.path(['id'])(data)) {
            return destroyRepairWork({
                variables: {
                    input: R.path(['id'])(data)
                }
            })
        }
        dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, {
                parts: R.without([data],
                    R.pipe(R.path(['parts']), R.defaultTo([]))(task))
            }]),
        })
    }, [task]);

    return [state, fetch, catalog, destroy]
};

export const usePartsCreateHooks = ({ navigation, state }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();

    const dataRepars = (data, mode) => {
        if (data) {
            dispatchRedux({
                type: 'TASK_UPDATE',
                payload: R.path(R.union(R.keys(data), ['repair']))(data)
            });
            if (mode) {
                navigation({ isVisible: false })
            }
            showMessage({
                message: 'Данные успешно обновились',
                description: "Success",
                type: "success",
            })
        }
    };

    const errorRepair = (data) => {
        if (data) {
            showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            })
        }
    };

    const [updateRepairParts] = useMutation(UPDATE_REPAIR_PARTS, {
        onCompleted: (data) => {
            const mode = true
            dataRepars(data, mode)
        },
        onError: (data) => errorRepair(data)
    });

    const [createRepairParts] = useMutation(CREATE_REPAIR_PARTS, {
        onCompleted: (data) => {
            dataRepars(data)
        },
        onError: (data) => {
            errorRepair(data)
        }
    });

    const closed = React.useCallback(() => {
        navigation({ type: 'CLOSED' })
    }, [task]);

    const create = React.useCallback((data) => {
        createRepairParts({
            variables: {
                "input": {
                    unitCount: parseInt(R.path(['unitCount'])(data)),
                    unitPrice: {
                        amount: parseInt(R.path(['depotItem', 'priceOut', 'amount'])(data))
                    },
                    discount: {
                        percent: R.defaultTo(0)(parseInt(R.path(['discount', 'percent'])(data))),
                        amount: R.defaultTo(0)(parseInt(R.path(['discount', 'discount', 'amount'])(data)))
                    },
                    depotItemId: R.path(['depotItem', 'id'])(data),
                    repairId: R.path(['id'])(task),
                }
            }
        })
    }, []);

    const update = React.useCallback((data) => {
        updateRepairParts({
            variables: {
                input: {
                    unitCount: parseInt(R.path(['unitCount'])(data)),
                    unitPrice: {
                        amount: parseInt(R.path(['depotItem', 'priceOut', 'amount'])(data))
                    },
                    discount: {
                        percent: R.defaultTo(0)(parseInt(R.path(['discount', 'percent'])(data))),
                        amount: R.defaultTo(0)(parseInt(R.path(['discount', 'discount', 'amount'])(data)))
                    },
                    depotItemId: R.path(['depotItem', 'id'])(data),
                    id: R.path(['id'])(data),
                }
            }
        })
    }, [task]);

    const redux = React.useCallback((data) => {
        dispatchRedux({
            type: 'TASK_UPDATE',
            payload: R.mergeAll([task, {
                parts: R.append(
                    data,
                    R.pipe(R.path(['parts']), R.defaultTo([]), R.without([state]))(task))
            }]),
        });
        showMessage({
            message: 'Данные успешно изменились',
            description: "Success",
            type: "success",
        });

        return navigation({})
    }, [task]);

    return [create, update, redux, closed]
};
