/* eslint-disable scanjs-rules/call_setTimeout */
import React from 'react'
import * as R from 'ramda'

export const TimerHook = () => {
    const [time, setTime] = React.useState(0);
    const [loading, setLoading] = React.useState(0);

    React.useEffect(() => {
        let timeOut = null;
        if (R.equals(true, loading)) {
            timeOut = setTimeout(() => {
                setTime(R.add(time, 10));
            }, 1)
        }
        if (R.isNil(loading)) {
            setTime(0)
        }

        return () => clearInterval(timeOut)
    }, [loading, time]);

    return [time, loading, (e) => setLoading(e)]
};