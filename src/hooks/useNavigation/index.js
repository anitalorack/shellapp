import React from 'react'
import * as R from 'ramda'

export const useNavigation = ({ navigation, move }) => {
    const [state, setState] = React.useState(null);


    const useguest = R.cond([
        [R.equals('toggle'), () => navigation.toggleDrawer()],
        [R.equals('goBack'), () => navigation.goBack()],
        [R.isNil, () => setState(null)],
    ]);

    React.useEffect(() => {
        useguest(state);

        return useguest(null)
    }, [state]);

    return [R.defaultTo(move)(state), setState]
};