import * as R from 'ramda';
import React from 'react';
import { PermissionsAndroid } from 'react-native';
import { showMessage } from 'react-native-flash-message';

export const usePermissions = () => {
    const [state, setState] = React.useState(null);
    const permissions = async () => {
        try {
            const granted = await PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                ],
                {
                    title: "Storage Permission",
                    message: "App needs access to memory to download the file "
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                setState(true);
            } else {
                showMessage({
                    message: "You need to give storage permission to download the file",
                    description: "Permission Denied!",
                    type: "Error",
                })
            }
        } catch (err) {
            showMessage({
                message: R.join(' ', ['Unknown Error: ', R.toString(err)]),
                description: "Success",
                type: "Info",
            })
        }
    };

    React.useEffect(() => {
        // permissions()
    }, [state]);

    return [state]
};