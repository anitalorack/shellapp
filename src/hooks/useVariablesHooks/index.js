/* eslint-disable no-undefined */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { MainScreen } from 'utils/routing/index.json';

export const useVariablesHooks = ({ navigation }) => {
    const dispatchRedux = useDispatch();
    const { nav, variables } = useSelector(state => state.variables, shallowEqual);
    const MainROUTER = MainScreen.Main;

    const navigationDispatch = React.useCallback(({ data, mode }) => {
        R.cond([
            [R.equals('Status'), () => {
                return dispatchRedux({
                    type: "VAR_UPLOAD",
                    payload: { nav: "MAIN", variables: data }
                })
            }],
            [R.equals('Page'), () => {
                return dispatchRedux({
                    type: "VAR_PAGE_UPLOAD",
                    payload: { page: data, limit: 10 }
                })
            }],
            [R.equals('Text'), () => {
                return dispatchRedux({
                    type: "VAR_UPLOAD",
                    payload: { nav: "MAIN", variables: R.assocPath(['q'], data, R.path(['where'])(variables)) }
                })
            }],
            [R.T, () => dispatchRedux({
                type: "VAR_UPLOAD",
                payload: { nav: "SKAZKA", variables: {} }
            })],
            [R.F, () => dispatchRedux({
                type: "VAR_UPLOAD",
                payload: { nav: "SKAZKA", variables: {} }
            })]
        ])(mode)

        return navigation.navigate(MainROUTER)
    }, [nav, variables])

    return [navigationDispatch]
}
