/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable id-length */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { useQuery } from '@apollo/react-hooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { Colors, IconButton } from 'react-native-paper';
import { QUERY_VEHICLES } from 'screens/Main/CarsScreen/GQL';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { ManufacturerModal } from './ManufacturerModal';

export const VehicleManufactures = ({ storage, dispatch, filter }) => {
    const [modal, setModal] = React.useState({ isVisible: false });
    const { data, error, loading } = useQuery(QUERY_VEHICLES, R.assocPath(['variables', 'order', 'name'], 'asc')({}));
    const dispatchStorage = useDispatch();
    const filterData = useSelector(state => state.filter, shallowEqual);

    if (error) return <View />;
    if (loading) return <View />;

    if (filter) {
        return (
            <View>
                <Text
                    style={{ fontSize: 10, paddingBottom: 10, paddingTop: 10 }}
                >
                    {i18n.t('vehicleManufacturers')}
                </Text>
                <View
                    style={{
                        borderWidth: 1, height: 50, flexDirection: 'row', alignItems: 'center'
                    }}
                >
                    <TouchableOpacity
                        onPress={() => setModal({ isVisible: true })}
                        style={{ flex: 1 }}
                    >
                        <Text style={{ fontSize: 12, paddingLeft: 10 }}>
                            {R.pipe(
                                R.path(['filter', 'modification', 'model', 'manufacturer', 'name']),
                            )(filterData)}
                        </Text>
                    </TouchableOpacity>
                    <IconButton
                        color={Colors.grey300}
                        icon={R.pipe(
                            R.path(['filter', 'modification', 'model', 'manufacturer', 'name']),
                        )(filterData) ? "close" : "play"}
                        onPress={() => {
                            dispatch({ modification: null });
                            dispatchStorage({ type: "FILTER_DESTROY", payload: 'modification' })
                        }}
                        size={20}
                    />
                </View>
                <Modal isVisible={R.path(['isVisible'])(modal)} >
                    <ManufacturerModal
                        dispatch={(e) => {
                            dispatchStorage({ type: "FILTER_ADD", payload: e });
                            dispatch(e)
                        }}
                        setModal={setModal}
                        state={data}
                    />
                </Modal>
            </View>
        )
    }

    const success = (e) => {
        const vehicles = R.head(
            R.innerJoin((record, id) => R.equals(record.id, id),
                R.path(R.keys(data))(data)
            )([e])
        );

        return dispatch(R.assocPath(['modification', 'model'], {
            manufacturer: vehicles, manufacturerId: R.path(['id'])(vehicles)
        })({}))
    };

    const placeholder = () => {
        if (R.hasPath(['modification', 'model', 'manufacturerId', 'eq'])(storage)) {
            return { id: R.path(['modification', 'model', 'manufacturerId', 'eq'])(storage) }
        }
        if (R.hasPath(['modification', 'model', 'manufacturer', 'id'])(storage)) {
            return { id: R.path(['modification', 'model', 'manufacturer', 'id'])(storage) }
        }
        if (isOdd(R.path(['modification', 'model', 'manufacturerId'])(storage))) {
            return { id: null }
        }
    };

    const items = R.append(
        { label: null, value: null },
        R.map(x => {
            return {
                key: R.path(['id'])(x),
                label: R.path(['name'])(x),
                value: R.path(['id'])(x)
            }
        })(R.path(R.keys(data))(data)));

    return (
        <UiPicker
            dispatch={(e) => success(e)}
            element={{ path: "vehicleManufacturers" }}
            Items={items}
            main
            placeholder={placeholder()}
        />
    )
};
