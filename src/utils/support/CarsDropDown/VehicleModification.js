/* eslint-disable no-unsanitized/method */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { QUERY_VEHICLES_MODIFICATION } from 'screens/Main/CarsScreen/GQL';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper'
import { modification } from 'screens/Main/CarsScreen/Preview/config'

export const VehicleModification = ({ state, dispatch }) => {
    const [start, setStart] = React.useState(null);
    const [storage, setStorage] = React.useState(null);
    const [loadGreeting] = useLazyQuery(QUERY_VEHICLES_MODIFICATION, {
        onCompleted: (data) => setStorage(data),
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        if (!R.path(['modification', 'id'])(state)) {
            if (!R.equals(start, state)) {
                setStorage(null);
                setStart(state)
            }
        }
        if (R.path(['modification', 'model', 'id'])(state)) {
            loadGreeting(R.assocPath(['variables', 'where', 'model', 'id', 'eq'], R.path(['modification', 'model', 'id'])(state))({}))
        }
    }, [state]);

    const success = (e) => {
        if (storage) {
            if (e) {
                const modification = R.head(
                    R.innerJoin((record, id) => R.equals(record.id, id),
                        R.defaultTo([{ label: null, value: null }])(R.path(R.keys(storage))(storage))
                    )([e])
                );

                if (!R.path(['label'])(storage)) {
                    return dispatch(R.assocPath(['modification'],
                        R.mergeAll([
                            R.path(['modification'])(state),
                            modification
                        ])
                    )({}))
                }
            }

            return null
        }
    };

    const items = () => {
        if (storage) {
            const data = R.prepend({ label: null, value: null })(
                R.map(x => ({
                    label: modification({ modification: x }),
                    value: R.path(['id'])(x)
                }))(R.path(R.keys(storage))(storage)));

            return data
        }

        return [{ label: null, value: null }]
    };

    if (R.path(['modification', 'model', 'id'])(state)) {
        return (
            <UiPicker
                disabled={R.all(R.equals(true))([R.hasPath(['modification', 'model', 'id'])(state), !isOdd(storage)])}
                dispatch={(e) => success(e)}
                element={{ path: "vehicleModifications" }}
                Items={items()}
                main
                placeholder={{ id: R.defaultTo(null)(R.path(['modification', 'id'])(state)) }}
            />
        )
    }

    return (<View />)
};
