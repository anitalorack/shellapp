/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable id-length */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView, Text, TouchableOpacity, View
} from 'react-native';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { InputComponents } from 'utils/component';
import { isOdd } from 'utils/helper'
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';

const grees = [{
    id: "Фамилия",
    path: 'last',
    keyboardType: "default",
    label: 'vehicleManufacturers',
    title: "Error message",
    value: ["name", "last"],
    maxLength: 120,
    testID: "LastInput",
    error: "Error message",
    errorMessage: 'PhoneError',
}];

export const ManufacturerModal = ({ setModal, state, dispatch }) => {
    const [data, setData] = React.useState(R.path(['vehicleManufacturers'])(state));
    const success = (e) => {
        dispatch(R.assocPath(['modification', 'model'], {
            manufacturer: e, manufacturerId: R.path(['id'])(e)
        })({}));

        return setModal({ isVisible: false })
    };

    const ComponentIndex = () => {
        if (data) return (
            <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 10 }}>
                {R.map(x => (
                    <CardComponents
                        key={x.id}
                        style={[styles.shadow, { height: 50, margin: 1 }]}
                    >
                        <TouchableOpacity onPress={() => success(x)} >
                            <Text>{R.path(['name'])(x)}</Text>
                        </TouchableOpacity>
                    </CardComponents>
                ))(data)}
            </ScrollView>
        );

        return (
            <View style={{ flex: 1 }} />
        )
    };

    return (
        <ModalDescription style={{ flex: 1 }}>
            <HeaderApp
                button="CLOSED"
                dispatch={() => setModal({ isVisible: false })}
                title={i18n.t('listVehicleManufacturers')}
            />
            <View style={{ flex: 1 }}>
                <View style={{ padding: 10 }}>
                    {R.addIndex(R.map)((element, keys) => (
                        <InputComponents
                            key={keys}
                            {...element}
                            onChangeText={(change) => {
                                if (isOdd(change)) return setData(R.path(['vehicleManufacturers'])(state));

                                return setData(
                                    R.innerJoin(
                                        (record, id) => R.pipe(R.path(['name']), R.toLower, R.includes(id))(record),
                                        R.path(['vehicleManufacturers'])(state),
                                        [R.toLower(change)]
                                    )
                                )
                            }}
                            storage={state}
                            style={{ textTransform: 'none' }}
                            valueExtractor={(a, b, c) => c.value}
                        />
                    ))(grees)}
                </View>
                {ComponentIndex()}
            </View>
        </ModalDescription >
    )
};