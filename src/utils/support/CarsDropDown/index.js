/* eslint-disable newline-after-var */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import i18n from 'localization';
import React from 'react';
import { modification } from 'screens/Main/CarsScreen/Preview/config'
import { Text, TouchableOpacity, View } from 'react-native';
import { IconButton, Colors } from 'react-native-paper';
import Modal from 'react-native-modal';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper'
import { VehicleManufactures } from './VehicleManufactures';
import { VehicleModal } from './VehicleModal';
import { VehicleModels } from './VehicleModels';
import { VehicleModification } from './VehicleModification';

export const CarsDropDown = ({
    storage, dispatch, notModification, filter
}) => (
        <View>
            <VehicleManufactures
                dispatch={dispatch}
                filter={filter}
                storage={storage}
            />
            <Model
                dispatch={(e) => dispatch(e)}
                filter={filter}
                state={storage}
            />
            <Modification
                dispatch={dispatch}
                filter={filter}
                notModification={notModification}
                state={storage}
            />
        </View>
    );

const Model = ({ dispatch, state, filter }) => {
    const [modal, setModal] = React.useState({ isVisble: false });
    const dispatchStorage = useDispatch();
    const filterData = useSelector(state => state.filter, shallowEqual);

    if (filter) {
        if (!R.hasPath(['filter', 'modification', 'model', 'manufacturer', 'id'])(filterData)) {
            return (<View />)
        }

        return (
            <View>
                <Text style={{ fontSize: 10, paddingBottom: 10, paddingTop: 10 }}>
                    {i18n.t('vehicleModels')}
                </Text>
                <View
                    style={{
                        borderWidth: 1, height: 50, flexDirection: 'row', alignItems: 'center'
                    }}
                >
                    <TouchableOpacity
                        onPress={() => setModal({ isVisible: true })}
                        style={{ flex: 1 }}
                    >
                        <Text style={{ fontSize: 12, paddingLeft: 10 }}>
                            {R.pipe(
                                R.paths([
                                    ['filter', 'modification', 'model', 'name'],
                                    ['filter', 'modification', 'model', 'subbody']
                                ]),
                                R.join(' '))(filterData)}
                        </Text>
                    </TouchableOpacity>
                    <IconButton
                        color={Colors.black500}
                        icon={R.pipe(
                            R.path(['filter', 'modification', 'model', 'name']), R.isNil
                        )(filterData) ? "play" : "close"}
                        onPress={() => {
                            const data = R.assocPath(['modification', 'model'], {
                                manufacturer: R.path(['filter', 'modification', 'model', 'manufacturer'])(filterData),
                                manufacturerId: R.path(['filter', 'modification', 'model', 'manufacturer', 'id'])(filterData)
                            })({});
                            dispatch(data);
                            dispatchStorage({ type: "FILTER_DESTROY_MODELS", payload: data })
                        }}
                        size={20}
                    />
                </View>
                <Modal isVisible={R.path(['isVisible'])(modal)} >
                    <VehicleModal
                        dispatch={dispatch}
                        setModal={setModal}
                        state={state}
                    />
                </Modal>
            </View>
        )
    }

    if (!R.hasPath(['modification', 'model', 'manufacturer', 'id'])(state)) {
        return (<View />)
    }

    return (
        <VehicleModels
            dispatch={dispatch}
            filter
            setModal={setModal}
            state={state}
        />
    )
};

const Modification = ({
    state, dispatch, notModification,
}) => {
    if (!R.hasPath(['modification', 'model', 'id'])(state)) {
        return (<View />)
    }
    if (notModification) {
        return <View />
    }

    return (
        <VehicleModification
            dispatch={dispatch}
            state={state}
        />
    )
};