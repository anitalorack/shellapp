/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { QUERY_VEHICLES_MODELS } from 'screens/Main/CarsScreen/GQL';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { InputComponents } from 'utils/component';
import { isOdd } from 'utils/helper';

const vehiclesInp = [{
    path: 'vehicleModels',
    keyboardType: "default",
    label: 'vehicleModels',
    title: "Error message",
    value: ["name", "last"],
    maxLength: 120,
    testID: "LastInput",
    error: "Error message",
    errorMessage: 'PhoneError',
}];

export const VehicleModal = ({ setModal, state, dispatch }) => {
    const [start, setStart] = React.useState(null);
    const [storage, setStorage] = React.useState([]);
    const dispatchStorage = useDispatch();
    const [data, setData] = React.useState(R.path(['vehicleModels'])(storage));

    const [loadGreeting] = useLazyQuery(QUERY_VEHICLES_MODELS, {
        onCompleted: (data) => {
            setStorage(data);
            setData(R.path(['vehicleModels'])(data))
        },
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        if (!R.path(['modification', 'model', 'id'])(state)) {
            if (!R.equals(start, state)) {
                setStorage(null);
                setStart(state)
            }
        }
        if (isOdd(R.path(['vehicleManufacturers'])(state))) {
            if (R.path(['modification', 'model', 'manufacturer', 'id'])(state)) {
                loadGreeting(R.assocPath(
                    ['variables', 'where', 'manufacturerId', 'eq'],
                    R.path(['modification', 'model', 'manufacturer', 'id']
                    )(state)
                )({}))
            }
        }
    }, [state]);

    const success = (e) => {
        const upload = {
            modification: R.assocPath(['model'],
                R.mergeAll([{
                    modelId: R.path(['modification', 'model', 'id'])(state),
                    ...R.path(['modification', 'model'])(state)
                }, e]))({})
        };
        dispatch(upload);
        dispatchStorage({ type: "FILTER_ADD", payload: upload });

        return setModal({ isVisible: false })
    };

    const ComponentIndex = () => {
        if (data) return (
            <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 10 }}>
                {R.addIndex(R.map)((x, key) => (
                    <CardComponents
                        key={R.join('_', [key, x.id])}
                        style={[styles.shadow, { height: 50, margin: 1, justifyContent: 'center' }]}
                    >
                        <TouchableOpacity onPress={() => success(x)} >
                            <Text>{R.pipe(R.paths([['name'], ['subbody']]), R.join(' '))(x)}</Text>
                        </TouchableOpacity>
                    </CardComponents>
                ))(data)}
            </ScrollView>
        );

        return (<View style={{ flex: 1 }} />)
    };

    return (
        <ModalDescription style={{ flex: 1 }}>
            <HeaderApp
                button="CLOSED"
                dispatch={() => setModal({ isVisible: false })}
                title={i18n.t('listVehicleManufacturers')}
            />
            <View style={{ padding: 10 }}>
                {R.addIndex(R.map)((element, keys) => (
                    <InputComponents
                        key={keys}
                        {...element}
                        onChangeText={(change) => {
                            if (isOdd(change)) return setData(R.path(['vehicleModels'])(storage));

                            return setData(
                                R.innerJoin(
                                    (record, id) => R.pipe(R.paths([['name'], ['subbody']]), R.join(''), R.toLower, R.includes(id))(record),
                                    R.path(['vehicleModels'])(storage),
                                    [R.toLower(change)]
                                )
                            )
                        }}
                        storage={state}
                        style={{ textTransform: 'none' }}
                        valueExtractor={(a, b, c) => c.value}
                    />
                ))(vehiclesInp)}
            </View>
            {ComponentIndex()}
        </ModalDescription>
    )
};