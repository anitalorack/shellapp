/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView, Text, TouchableOpacity, View
} from 'react-native';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { InputComponents } from 'utils/component';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { QUERY_VEHICLES_MODIFICATION } from 'screens/Main/CarsScreen/GQL';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { isOdd } from 'utils/helper';
import { modification } from 'screens/Main/CarsScreen/Preview/config'
import { styles } from 'screens/Main/AddGoods/helper/style';

const vehiclesInp = [{
    path: 'vehicleModels',
    keyboardType: "default",
    label: 'vehicleModels',
    title: "Error message",
    value: ["name", "last"],
    maxLength: 120,
    testID: "LastInput",
    error: "Error message",
    errorMessage: 'PhoneError',
}];

export const VehicleModificationModal = ({ setModal, state, dispatch }) => {
    const [start, setStart] = React.useState(null);
    const [storage, setStorage] = React.useState([]);
    const [data, setData] = React.useState(R.path(['vehicleModels'])(storage));
    const [loadGreeting] = useLazyQuery(QUERY_VEHICLES_MODIFICATION, {
        onCompleted: (data) => {
            setStorage(data)
        },
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        if (!R.path(['modification', 'id'])(state)) {
            if (!R.equals(start, state)) {
                setStorage(null);
                setStart(state)
            }
        }
        if (R.path(['modification', 'model', 'id'])(state)) {
            loadGreeting(R.assocPath(['variables', 'where', 'model', 'id', 'eq'], R.path(['modification', 'model', 'id'])(state))({}))
        }
    }, [state]);

    const success = (e) => {
        dispatch(R.assocPath(['modification'],
            R.mergeAll([
                R.path(['modification'])(state),
                e
            ])
        )({}));

        return setModal({ isVisible: false })
    };

    const ComponentIndex = () => {
        if (R.isEmpty(storage)) return (<View />);
        if (!isOdd(storage)) return (
            <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 10 }}>
                {R.map(x => (
                    <CardComponents
                        key={x.id}
                        style={[styles.shadow, { height: 50, margin: 1, justifyContent: 'center' }]}
                    >
                        <TouchableOpacity onPress={() => success(x)} >
                            <Text>{modification({ modification: x })}</Text>
                        </TouchableOpacity>
                    </CardComponents>
                ))(R.path(['vehicleModifications'])(storage))}
            </ScrollView>
        );

        return (<View style={{ flex: 1 }} />)
    };

    const VehicleInput = () => R.addIndex(R.map)((element, keys) => (
        <InputComponents
            key={R.join('_', ['Vehicle', keys])}
            {...element}
            onChangeText={(change) => {
                if (isOdd(change)) return setData(R.path(['vehicleModels'])(storage));

                return setData(
                    R.innerJoin(
                        (record, id) => R.pipe(R.paths([['name'], ['subbody']]), R.join(''), R.toLower, R.includes(id))(record),
                        R.path(['vehicleModels'])(storage),
                        [R.toLower(change)]
                    )
                )
            }}
            storage={state}
            style={{ textTransform: 'none' }}
            valueExtractor={(a, b, c) => c.value}
        />
    ))(vehiclesInp);

    return (
        <ModalDescription style={{ flex: 1 }}>
            <HeaderApp
                button="CLOSED"
                dispatch={() => setModal({ isVisible: false })}
                header
                title={i18n.t('listVehicleManufacturers')}
            />
            <View style={{ padding: 10 }}>
                {VehicleInput()}
            </View>
            {ComponentIndex()}
        </ModalDescription>
    )
};