/* eslint-disable arrow-body-style */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { QUERY_VEHICLES_MODELS } from 'screens/Main/CarsScreen/GQL';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper';

export const VehicleModels = ({ state, dispatch }) => {
    const [start, setStart] = React.useState(null);
    const [storage, setStorage] = React.useState(null);
    const [loadGreeting] = useLazyQuery(QUERY_VEHICLES_MODELS, {
        onCompleted: (data) => setStorage(data),
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        if (!R.path(['modification', 'model', 'id'])(state)) {
            if (!R.equals(start, state)) {
                setStorage(null);
                setStart(state)
            }
        }
        if (isOdd(R.path(['vehicleManufacturers'])(state))) {
            if (R.path(['modification', 'model', 'manufacturer', 'id'])(state)) {
                loadGreeting(R.assocPath(
                    ['variables', 'where', 'manufacturerId', 'eq'],
                    R.path(['modification', 'model', 'manufacturer', 'id']
                    )(state)
                )({}))
            }
        }
    }, [state]);

    const success = (e) => {
        if (storage) {
            if (!R.isNil(e)) {
                try {
                    const models = R.head(
                        R.innerJoin((record, id) => R.equals(record.id, id),
                            R.defaultTo([{
                                label: null, value: null
                            }])(R.path(R.keys(storage))(storage))
                        )([e])
                    );

                    return dispatch({
                        modification: R.assocPath(['model'],
                            R.mergeAll([
                                {
                                    modelId: R.path(['modification', 'model', 'id'])(state),
                                    ...R.path(['modification', 'model'])(state)
                                },
                                models
                            ]))({})
                    })
                } catch (error) {
                    console.log({ error: e })
                }
            }
        }
    };

    const items = () => {
        if (storage) {
            if (R.path(R.keys(storage))(storage)) {
                return R.prepend(
                    { label: null, value: null },
                    R.map(x => {
                        return {
                            key: R.path(['id'])(x),
                            label: R.pipe(R.paths([['name'], ['subbody']]), R.join(' '))(x),
                            value: R.path(['id'])(x)
                        }
                    })(R.path(R.keys(storage))(storage)))
            }
        }

        return [{ label: null, value: null }]
    };

    const placeholder = R.cond([
        [isOdd, R.always({ id: null })],
        [R.T, R.always({ id: R.path(['modification', 'model', 'id'])(state) })],
    ])(R.path(['modification', 'model', 'id'])(state));

    if (R.path(['vehicle', 'modification', 'modelId', 'eq'])(state)) {
        return (
            <UiPicker
                disabled={R.all(R.equals(true))([R.hasPath(['modification', 'model', 'manufacturerId'])(state), !isOdd(storage)])}
                dispatch={(e) => success(e)}
                element={{ path: "vehicleModels" }}
                Items={items()}
                main
                placeholder={placeholder}
            />
        )
    }

    if (R.path(['modification', 'model', 'manufacturer', 'id'])(state)) {
        return (
            <UiPicker
                disabled={R.all(R.equals(true))([R.hasPath(['modification', 'model', 'manufacturer', 'id'])(state), !isOdd(storage)])}
                dispatch={(e) => success(e)}
                element={{ path: "vehicleModels" }}
                Items={items()}
                main
                placeholder={placeholder}
            />
        )
    }

    return (
        <View />
    )
};
