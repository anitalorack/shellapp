/* eslint-disable space-before-function-paren */
/* eslint-disable react/no-set-state */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-undef */
/* eslint-disable react/require-optimization */
import React, { Component } from "react";
import { Alert, View } from "react-native";
import * as UpdateAPK from "rn-update-apk";
import {
    apkVersionUrl, DescriptionRnUpdate, fileProviderAuthority, TitleRnUpdate
} from 'utils/update/app.json';
import { AppLoader } from './AppLoader';
import { DownloadProgress } from './DownloadProgress';

export default class APKUPDATE extends Component {
    constructor(props) {
        super(props);
        this.state = {
            downloadProgress: -1,
            label: "Status"
        };

        updater = new UpdateAPK.UpdateAPK({
            apkVersionUrl,
            fileProviderAuthority,
            needUpdateApp: needUpdate => {
                Alert.alert(
                    TitleRnUpdate,
                    DescriptionRnUpdate,
                    [
                        {
                            text: "Cancel",
                            onPress: () => {
                            }
                        },
                        {
                            text: "Update",
                            onPress: () => needUpdate(true)
                        }
                    ]
                );
            },
            forceUpdateApp: () => {
                this.setState({ label: "forceUpdateApp callback called" });
            },
            notNeedUpdateApp: () => {
                this.setState({ label: "notNeedUpdateApp callback called" });
            },
            downloadApkStart: () => {
                this.setState({ label: "downloadApkStart callback called" });
            },
            downloadApkProgress: progress => {
                this.setState({ downloadProgress: progress });
            },
            downloadApkEnd: () => {
                this.setState({ label: "downloadApkEnd callback called" });
            },
            onError: err => {
                this.setState({ label: err.message });
            }
        })
    }

    async componentDidMount() {
        UpdateAPK.getApps()
            .then(apps => {
                this.setState({ allApps: apps });
            })
            .catch(e => this.setState({ label: e }));

        UpdateAPK.getNonSystemApps()
            .then(apps => {
                this.setState({ allNonSystemApps: apps });
            })
            .catch(e => this.setState({ label: e }));
    }

    _onCheckServerVersion = () => {
        updater.checkUpdate();
    };

    render() {
        return (
            <View>
                <View style={{ flex: 1 }}>
                    {this.state.downloadProgress === -1 ?
                        <AppLoader
                            dispatch={this._onCheckServerVersion}
                        /> :
                        <DownloadProgress
                            state={this.state.downloadProgress}
                        />}
                </View>
            </View>
        );
    }
}
