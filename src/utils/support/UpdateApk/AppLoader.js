/* eslint-disable react/no-multi-comp */
/* eslint-disable no-negated-condition */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable object-curly-newline */
/* eslint-disable newline-after-var */
/* eslint-disable max-statements */
/* eslint-disable space-before-function-paren */
/* eslint-disable react/jsx-handler-names */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-implicit-coercion */
/* eslint-disable react/no-set-state */
/* eslint-disable no-undef */
/* eslint-disable padded-blocks */
/* eslint-disable react/require-optimization */
import i18n from 'localization';
import React from "react";
import { TouchableOpacity, View } from "react-native";
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { versionName } from 'utils/update/app.json';

export const AppLoader = ({ dispatch }) => (
    <View>
        <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items="SRDescription"
            style={styles.items}
            testID="BlockLaker"
        >
            Текущая Версия - {versionName}
        </LabelText>
        <TouchableOpacity
            onPress={() => dispatch()}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SRDescription"
                style={styles.items}
                testID="BlockLaker"
            >
                {i18n.t("Update")}
            </LabelText>
        </TouchableOpacity>
    </View>
);