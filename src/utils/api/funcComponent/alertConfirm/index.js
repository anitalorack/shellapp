import i18n from 'localization'
import { Alert } from 'react-native'

export const alertDestroyConfirm = ({ alertReq, title, params }) =>
    Alert.alert(
        i18n.t("HeaderAlert"),
        i18n.t(title),
        [
            {
                text: i18n.t('Cancel'),
                onPress: () => alertReq({ type: "Cancel" }),
                style: "cancel"
            },
            { text: i18n.t("Yes"), onPress: () => alertReq({ type: "Destroy", params }) }
        ],
        { cancelable: false }
    );
