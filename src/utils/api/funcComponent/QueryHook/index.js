/* eslint-disable max-statements-per-line */
import { useApolloClient } from '@apollo/react-hooks';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as R from 'ramda'

// TODO DEPRECATED POSITION OR UPDATE STATE

export const QueryHook = (props) => {
    const { QUERY, variables, type } = props;
    const { jswToken } = useSelector(state => R.path(['profile'])(state), shallowEqual);
    const dispatch = useDispatch();
    const client = useApolloClient();
    const [data, setRequest] = useState(null);

    const request = async () => {
        const answer = await client.query({
            query: QUERY,
            variables,
            fetchPolicy: 'cache-first'
        });

        if (type) dispatch({ type, payload: R.path(['data'])(answer) });

        return setRequest(R.path(['data'])(answer))
    };

    useEffect(() => {
        if (!data && jswToken) {
            request()
        }
    }, [props]);

    return [data]
};

QueryHook.propTypes = {
    QUERY: PropTypes.string,
    type: PropTypes.string,
    variables: PropTypes.object,
};

QueryHook.defaultProps = {
    QUERY: null,
    type: null,
    variables: {},
};