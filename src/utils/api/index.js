/* eslint-disable max-statements */
/* eslint-disable scanjs-rules/call_addEventListener */
import * as R from 'ramda'
import { showMessage } from "react-native-flash-message"
import i18n from 'localization'
import { getItem } from 'utils/async'

const REST_API_URL = 'http://devapi.zengine.it';

const regex = R.replace(/([\(\ )])/g, '');

const AuthSmsCode = (phone, code) => {
    const data = `{ "type":"employee", "phone": "${regex(phone)}", "code": "${regex(code)}"}`;
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.addEventListener("load", () => {
            try {
                const data = JSON.parse(xhr.response)

                if (R.hasPath(['errors'])(data)) {
                    return showMessage({
                        message: i18n.t('invavidSms'),
                        description: "Error",
                        type: "danger",
                    });
                }

                return resolve(xhr.response)
            } catch (e) {
                return showMessage({
                    message: R.toString(e),
                    description: "Error",
                    type: "danger",
                });
            }
        });
        xhr.open("POST", 'http://devapi.zengine.it/auth/sms');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('accept', 'application/json');
        xhr.send(data)
    })
};

const CreateSmsCode = (phone) => {
    const data = `{ "type":"employee", "phone": "${phone}"}`;

    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.addEventListener("load", () => {
            try {
                const data = JSON.parse(xhr.response)

                if (R.hasPath(['errors'])(data)) {
                    return showMessage({
                        message: i18n.t('invalidPhone'),
                        description: "Error",
                        type: "danger",
                    });
                }

                return resolve(data)
            } catch (e) {
                return showMessage({
                    message: R.toString(e),
                    description: "Error",
                    type: "danger",
                });
            }
        });
        xhr.open("POST", `${REST_API_URL}/auth/sms/code`);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('accept', 'application/json');
        xhr.send(data)
    })
};

const UploadPhoto = async (file, id) => {
    const jswToken = await getItem();
    const token = `Bearer ${jswToken}`;
    const data = {
        "upload": null,
        "objectId": id,
        "objectType": "Repair",
        "objectProperty": "attachments"
    };

    const xhr = new XMLHttpRequest();
    const formData = new FormData();

    formData.append('operations', R.toString({
        "query": "mutation($input: CreateFileObjectInput!){createFileObject(input: $input){id}}",
        "variables": { "input": data }
    }));
    formData.append('map', '{ "files": ["variables.input.upload"] }');
    formData.append('files', file);

    xhr.upload.addEventListener('progress', (event) => showMessage({
        message: R.toString(event),
        description: "Progress",
        type: "info",
    })
    );

    xhr.addEventListener("load", () => {
        try {
            if (xhr.response.errors) {
                return showMessage({
                    message: R.toString(xhr.response.error),
                    description: "Error",
                    type: "danger",
                })
            }

            return showMessage({
                message: "File Upload",
                description: "Success",
                type: "danger",
            })
        } catch (e) {
            return showMessage({
                message: R.toString(e),
                description: "Success",
                type: "danger",
            })
        }
    });
    xhr.open("POST", R.join('/', [REST_API_URL, 'graphql']));
    xhr.setRequestHeader('Authorization', token);
    xhr.send(formData)
};

export {
    CreateSmsCode,
    AuthSmsCode,
    UploadPhoto
}
