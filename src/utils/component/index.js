/* eslint-disable nonblock-statement-body-position */
/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import UiTextInput from 'uiComponents/UiPhoneInput/PhoneText';
import { LabelInputText } from 'utils/component/LabelInputText';
import { ValidationText } from 'utils/component/ValidationText';
import { ColorCard, priceModed } from 'utils/helper';

export const InputComponents = (props) => {
    const [error] = useState(null);
    const [blur, setBlur] = React.useState(false);
    const [Error, setError] = React.useState(false);
    const { storage } = props;

    const hardCore = R.cond([
        [R.is(Number), () => R.toString(R.path(props.value)(storage))],
        [R.T, () => R.path(props.value)(storage)]
    ])(R.path(R.path(['value'])(props))(storage));

    return (
        <View>
            <LabelInputText
                props={props}
            />
            <UiTextInput
                blurOnSubmit={() => null}
                containerStyle={StyleInput.container}
                editable={R.path(['editable'])(props)}
                errorMessage={error}
                keyboardType={R.path(['keyboardType'])(props)}
                labelStyle={StyleInput.label}
                mask={R.path(['mask'])(props)}
                maxLength={R.path(['maxLength'])(props)}
                multiline={R.path(['multiline'])(props)}
                onBlur={() => R.hasPath(['errorLabel'])(props) ? setBlur(true) : null}
                onChangeText={(formatted, extracted) => {
                    const data = R.path(['mask'])(props) ? extracted : formatted;
                    const { error, value } = ValidationText(props, data);
                    setError(error);

                    return props.onChangeText(value, error)
                }}
                onFocus={() => null}
                onSubmitEditing={() => null}
                placeholder={R.path(['example'])(props)}
                placeholderTextColor={{}}
                returnKeyType="next"
                scrollEnabled={R.path(['scrollEnabled'])(props)}
                showLabel={i18n.t(R.path(['label'])(props))}
                style={StyleInput.style}
                value={R.path(['price'])(props) ? priceModed(hardCore) : hardCore}
            />
            <CompErrorLenght
                blur={blur}
                props={props}
                storage={storage}
            />
        </View>
    )
};

const StyleInput = StyleSheet.create({
    style: {
        fontFamily: "ShellMedium",
        fontSize: 13,
        borderWidth: 1,
        paddingLeft: 10,
        paddingRight: 10,
        borderColor: uiColor.Light_Grey,
    },
    label: {
        backgroundColor: uiColor.Light_Grey,
        fontFamily: "ShellMedium",
    },
    container: {
    }
});

export const CompErrorLenght = ({ props, storage, blur }) => {
    if (R.path(R.path(['value'])(props))(storage)) {
        if (R.all(R.equals(true))([blur, R.hasPath(['errorLabel'])(props)])) {
            if (R.pipe(
                R.splitEvery(1),
                R.length,
                R.equals(
                    R.path(['maxLength'])(props)
                )
            )(R.defaultTo(null)(
                R.path(
                    R.path(['value'])(props)
                )(storage))) === false) {
                return (
                    <LabelText
                        color={ColorCard('error').font}
                        fonts="ShellMedium"
                        items={R.path(['bold'])(props) ? "SRDescription" : "SDescription"}
                        style={[{
                            textAlign: 'left',
                            multiline: R.path(['multiline'])(props),
                            paddingBottom: 15,
                        }]}
                    >
                        {R.path(['error'])(props)}
                    </LabelText>
                )
            }
        }
    }

    return (
        <View />
    )
};