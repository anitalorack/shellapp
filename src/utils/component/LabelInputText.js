/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { Text, View } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';

export const LabelInputText = ({ props }) => {


    if (isOdd(R.path(['label'])(props))) return (
        <View style={{ paddingBottom: 10 }} />
    );

    return (
        <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items={R.path(['bold'])(props) ? "SRDescription" : "SDescription"}
            style={[{
                textAlign: 'left',
                multiline: R.path(['multiline'])(props),
            }]}
            testID="BlockLaker"
        >
            <Text
                style={{
                    textTransform: 'none',
                }}
            >
                {i18n.t(R.path(['label'])(props))}
            </Text>
            <Text style={{ color: 'red' }}>
                {R.path(['main'])(props) ? "  *" : ""}
            </Text>
        </LabelText >
    )
};