import gql from 'graphql-tag'

export const GaragesId = gql`
query(
    $where: WhereGarageInput
    $order: OrderGarageInput
    $paginate: PageInput
){
    garages(
        where:$where
        order:$order
        paginate: $paginate
    ) {
      items {
        id
        name
        settings {
          costPerHour {
            amount
          }
          defaultPartSurcharge
          timezone {
            name
            offset
          }
        }
        address {
          postcode
          state
          city
          street
          building
          __typename
        }
        phone
        email
        legalName
        legalAddress {
          postcode
          state
          city
          street
          building
          __typename
        }
        bankName
        checkingAccount
        correspondentAccount
        rcbic
        inn
        kpp
        psrn
        createdAt
        updatedAt
        __typename
      }
    }
  }
`;

export const UPGRADE_GARAGE = gql`
mutation($input: UpdateGarageInput!) {
  updateGarage(input: $input) {
    id
    name
    phone
    email
    checkingAccount
    correspondentAccount
    rcbic
    inn
    kpp
    legalName
    bankName
    psrn
    createdAt
    updatedAt
    defaultPartSurcharge
  }
}
`;

export const CREATE_GARAGE = gql`
mutation($input:CreateGarageInput){
  createGarage(input: $input){
    id
    name
    phone
    email
    checkingAccount
    correspondentAccount
    rcbic
    inn
    kpp
    legalName
    bankName
    psrn
    createdAt
    updatedAt
    defaultPartSurcharge
    }
}
`;

export const SHELLRECOMP = gql`
query($where:WhereVehicleModificationInfoInput){
  vehicleModificationInfo(where: $where) {
    engine {
      oils {
        capacity {
          other
          range
          value
          minimum
          maximum
        }
        options {
          temperature {
            minimum
            maximum
          }
          classifications {
            value
            qualifier
          }
          grades {
            value
            qualifier
          }
        }
        tighteningTorques {
          description
          value
          note
        }
      }
    }
    lubricants {
      type
      description
      classifications {
        description
        value
      }
      grades {
        description
        value
        qualifier
      }
      specifications {
        description
        value
      }
      capacities {
        description
        value
        minimum
        maximum
        range
      }
      others {
        info
        description
        value
      }
    }
  }
}
`;