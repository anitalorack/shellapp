import gql from 'graphql-tag';

export const DELETE_EMPLOYEE = gql`
mutation destroyEmployee($id: Int!) {
    destroyEmployee(id: $id) {
      id
      name {
        last
        first
        middle
      }
      phone
      blocked
      garageId
      garage {
        id
        name
      }
      role
      createdAt
    }
  }
`;

export const CREATE_EMPLOYER = gql`
mutation createEmployee($input: CreateEmployeeInput) {
    createEmployee(input: $input) {
      id
      name {
        last
        first
        middle
      }
      phone
      blocked
      garageId
      garage {
        id
        name
      }
      role
      createdAt
    }
  }
`;

export const UPDATE_EMPLOYEE = gql`
mutation updateEmployee($input: UpdateEmployeeInput!) {
    updateEmployee(input: $input) {
      id
      name {
        last
        first
        middle
      }
      phone
      blocked
      garageId
      garage {
        id
        name
      }
      role
      createdAt
    }
  }
`;

export const FIND_EMPLOYERS = gql`
query employee($where: WhereEmployeeInput) {
    employee(where: $where) {
      id
      name {
        last
        first
        middle
      }
      phone
      blocked
      garageId
      garage {
        id
        name
      }
      role
      createdAt
    }
  }
`;

export const ALL_EMPLOYERS = gql`
query(
    $where: WhereEmployeeInput
    $order: OrderEmployeeInput
    $paginate: PageInput
  ) {
    employees(where: $where, order: $order, paginate: $paginate) {
      items {
        id
        name {
          last
          first
          middle
        }
        role
        phone
        blocked
        garageId
        createdAt
      }
    }
  }
`;
