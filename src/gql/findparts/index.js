import gql from 'graphql-tag'

export const QUERY_FIND_PARTS = gql`
query ($where: WhereOrderingInput) {
    findParts(where: $where) {
      article
      brand {
        id
        name
      }
      name
      offers {
        warehouse {
          id
          name
          type
        }
        price
        average_period
        assured_period
        reliability
        is_transit
        quantity
        available_more
        multiplication_factor
        delivery_type
      }
    }
  }
`;
