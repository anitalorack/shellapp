import gql from 'graphql-tag'

export const ALL_CLIENT = gql`
  query(
    $where: WhereClientInput
    $order: OrderClientInput
    $paginate: PageInput
  ){
    clients(where: $where, order: $order, paginate: $paginate) {
      info{
        page
        nextPage
        previousPage
        totalCount
      }
      items {
        id
        name {
          first
          last
          middle
        }
        phone
        createdAt
        updatedAt
      }
      info {
        totalCount
        page
        limit
      }
    }
  }
`;