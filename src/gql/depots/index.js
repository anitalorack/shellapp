// /* eslint-disable template-tag-spacing */
// /* eslint-disable no-unused-expressions */
// /* eslint-disable no-unexpected-multiline */
import gql from 'graphql-tag'

export const DESTROY_DEPOTS = gql`
mutation($id:Int!) {
    destroyCounterparty(id:$id) {
      id
    }
  }
  `;

export const DESTROY_FILE = gql`
mutation($id:Int!) {
  destroyFileObject(id: $id) {
    id
    objectId
    objectType
    objectProperty
    filename
    mimetype
    encoding
    file {
      url
      path
    }
  }
}
`;

export const DEPOTS_QUERY = gql`
query(
    $where: WhereDepotInput
    $order: OrderDepotInput
    $paginate: PageInput
){
    depots(
        where: $where
        order: $order
        paginate: $paginate
    ) {
      items {
        id
        name
        description
        createdAt
        updatedAt
      }
      info{
        page
        nextPage
        previousPage
        totalCount
      }
    }
  }
  `;

export const UPDATE_CONTRYPARTY = gql`mutation($input: UpdateCounterpartyInput!) {
  updateCounterparty(input: $input) {
    type
    id
    name
    phone
    email
    inn
    physicalAddress {
      postcode
      state
      city
      street
      building
    }
    bankDetails {
      id
      name
      currentAccount
      correspondentAccount
      rcbic
      counterpartyId
      mainBankDetail
    }
    description
    psrn
    kpp
    legalAddress {
      postcode
      state
      city
      street
      building
    }
    registrationAddress {
      postcode
      state
      city
      street
      building
    }
    personalData
    psrnsp
    certificateNumber
    certificateDate
  }
}
`;

export const MUTATION_COUNTRYPARTY = gql`
mutation($input: CreateCounterpartyInput!) {
    createCounterparty(
        input: $input
    ) {
        type
        id
        name
        phone
        email
        inn
        physicalAddress {
            postcode
            state
            city
            street
            building
        }
        bankDetails {
            id
            name
            currentAccount
            correspondentAccount
            rcbic
            counterpartyId
            mainBankDetail
        }
        description
        psrn
        kpp
        legalAddress {
            postcode
            state
            city
            street
            building
        }
        registrationAddress {
            postcode
            state
            city
            street
            building
        }
        personalData
        psrnsp
    }
}
`;

export const QUERY_COUNTRYPARTY = gql`
query(
  $where:WhereCounterpartyInput!,
  $paginate: PageInput
  ){
    counterparty(
      where:$where,
      paginate:$paginate
      ) {
      type
      id
      name
      phone
      email
      inn
      physicalAddress {
        postcode
        state
        city
        street
        building
      }
      bankDetails {
        id
        name
        currentAccount
        correspondentAccount
        rcbic
        counterpartyId
        mainBankDetail
      }
      description
      psrn
      kpp
      legalAddress {
        postcode
        state
        city
        street
        building
      }
      registrationAddress {
        postcode
        state
        city
        street
        building
      }
      personalData
      psrnsp
      certificateNumber
      certificateDate
    }
  }
`;

export const QUERY_COUNTERPARTIES = gql`
query(
  $where: WhereCounterpartyInput
  $order: OrderCounterpartyInput
  $paginate: PageInput
) {
  counterparties(where: $where, order: $order, paginate: $paginate) {
    items {
      type
      id
      name
      phone
      email
      inn
      physicalAddress {
        postcode
        state
        city
        street
        building
      }
      bankDetails {
        id
        name
        currentAccount
        correspondentAccount
        rcbic
        counterpartyId
        mainBankDetail
      }
      description
      psrn
      kpp
      legalAddress {
        postcode
        state
        city
        street
        building
      }
      registrationAddress {
        postcode
        state
        city
        street
        building
      }
      personalData
      psrnsp
      certificateNumber
      certificateDate
    }
    info {
      page
      nextPage
      previousPage
      totalCount
    }
  }
}
`;

export const QUERY_LEGAL = gql`
query(
  $where: WhereLegalEntityInput
  $order: OrderLegalEntityInput
  $paginate: PageInput
) {
  legalEntities(where: $where, order: $order, paginate: $paginate) {
    items {
      id
      name
      phone
      email
      inn
      physicalAddress {
        postcode
        state
        city
        street
        building
      }
      bankDetails {
        id
        name
        currentAccount
        correspondentAccount
        rcbic
        counterpartyId
        mainBankDetail
      }
      description
      psrn
      kpp
      legalAddress {
        postcode
        state
        city
        street
        building
      }
    }
    info {
      page
      nextPage
      previousPage
      totalCount
    }
  }
}
`;

export const QUERY_SOLO = gql`query(
  $where: WhereSoleTraderInput
  $order: OrderSoleTraderInput
  $paginate: PageInput
) {
  soleTraders(where: $where, order: $order, paginate: $paginate) {
    items {
      id
      name
      phone
      email
      inn
      physicalAddress {
        postcode
        state
        city
        street
        building
      }
      bankDetails {
        id
        name
        currentAccount
        correspondentAccount
        rcbic
        counterpartyId
        mainBankDetail
      }
      description
      kpp
      legalAddress {
        postcode
        state
        city
        street
        building
      }

      psrnsp
      certificateNumber
      certificateDate
    }
    info {
      page
      nextPage
      previousPage
      totalCount
    }
  }
}
`;

export const DESTROY_BANKDETAIL = gql`
mutation ($id:Int!) {
  destroyBankDetail(id: $id){
    id
  }
}`;

export const QUERY_INDIVIDUAL = gql`
query(
  $where: WhereIndividualInput
  $order: OrderIndividualInput
  $paginate: PageInput
) {
  individuals(where: $where, order: $order, paginate: $paginate) {
    items {
      id
      name
      phone
      email
      inn
      physicalAddress {
        postcode
        state
        city
        street
        building
      }
      bankDetails {
        id
        name
        currentAccount
        correspondentAccount
        rcbic
        counterpartyId
        mainBankDetail
      }
      description
      registrationAddress {
        postcode
        state
        city
        street
        building
      }
      personalData
    }
    info {
      page
      nextPage
      previousPage
      totalCount
    }
  }
}
`;