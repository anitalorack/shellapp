import gql from 'graphql-tag'

export const CREATE_GOODS = gql`
mutation ($input:CreateReceiveDepotOperationInput) {
    createReceiveDepotOperation(
      input: $input
    ) {
      id
      items {
        stockId
        depotId
        depotItem {
          id
          stockId
          stock {
            id
            name
            description
            article
            oem
            brand
            measure {
              code
              precision
              name
              abbr
            }
            garageId
            stockGroupId
            stockGroup {
              id
              name
              parent {
                id
                name
              }
            }
            depotItems {
              id
              amount {
                available
              }
              priceOut {
                amount
                currency
              }
            }
            amount {
              available
              reserved
              released
            }
            createdAt
            updatedAt
          }
          depotId
          depot {
            id
            name
            description
            createdAt
            updatedAt
          }
          priceIn {
            amount
            currency
          }
          priceOut {
            amount
            currency
          }
          measure {
            code
            precision
            name
            abbr
          }
          amount {
            available
            reserved
            released
          }
          vat
          createdAt
          updatedAt
        }
        priceIn {
          amount
          currency
        }
        vat
        amount
      }
      counterparty {
        type
        id
        name
        phone
        email
        inn
        physicalAddress {
          postcode
          state
          city
          street
          building
        }
        bankDetails {
          id
          name
          currentAccount
          correspondentAccount
          rcbic
          counterpartyId
          mainBankDetail
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
        description
        psrn
        kpp
        legalAddress {
          postcode
          state
          city
          street
          building
        }
        registrationAddress {
          postcode
          state
          city
          street
          building
        }
        personalData
        psrnsp
        certificateNumber
        certificateDate
      }
      actor {
        id
        name {
          first
          last
          middle
        }
        phone
        garageId
        garage {
          id
          name
          address {
            postcode
            state
            city
            street
            building
          }
          phone
          email
          legalName
          legalAddress {
            postcode
            state
            city
            street
            building
          }
          bankName
          checkingAccount
          correspondentAccount
          rcbic
          inn
          kpp
          psrn
          createdAt
          updatedAt
          settings {
            costPerHour {
              amount
              currency
            }
            defaultPartSurcharge
          }
        }
        role
        blocked
        createdAt
        updatedAt
      }
      document {
        id
        type {
          code
          name
          objectTypes
        }
        number
        date
        objectId
        objectType
        attachments {
          id
          objectId
          objectType
          objectProperty
          filename
          mimetype
          encoding
          file {
            url
            path
          }
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
  `