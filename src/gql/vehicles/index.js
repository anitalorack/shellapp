import gql from 'graphql-tag'

export const VEHICLESQUERY = gql`
{
  vehicles(where: {}, order: { id: desc }, paginate: { page: 1, limit: 20 }) {
    items {
      id
      vin
      frameNumber
      year
      color
      plate
      plateHistory {
        value
        createdAt
      }
      mileage
      mileageHistory {
        value
        createdAt
      }
      modification {
        id
        provider
        providerId
        name
        enginecode
        dinHp
        fuel
        kw
        litres
        rpm
        startYear
        endYear
        subbody
        modelId
        model {
          id
          name
          subbody
          manufacturerId
          manufacturer {
            id
            name
          }
        }
      }
      owner {
        id
        name {
          first
          last
          middle
        }
        phone
        createdAt
        updatedAt
      }
      clients {
        id
        name {
          first
          last
          middle
        }
        phone
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
    info {
      totalCount
      page
      limit
    }
  }
}
`;