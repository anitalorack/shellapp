import gql from 'graphql-tag'

export const BILLACCOUNT = gql`
query ($where:WhereBillAccountInput!) {
    billAccount(where: $where) {
      id
      balance
      totalBalance
      trialStartAt
      trialEndAt
      garageId
      createdAt
      updatedAt
        }
    }
`;

export const BILL_INVOICE = gql`
query(
    $where: WhereBillInvoiceInput
    $order: OrderBillInvoiceInput
    $paginate: PageInput
  ) {
    billInvoices(where: $where, order: $order, paginate: $paginate) {
      info{
        page
        nextPage
        previousPage
        totalCount
      }
      items {
        id
        name
        paid
        paidAt
        canceled
        canceledAt
        paymentAmount {
          amount
          currency
        }
        amountPayable {
          amount
          currency
        }
        billPayments {
          id
          amount {
            amount
            currency
          }
          billInvoiceId
          description
          confUrl
          status
        }
        billServices {
          id
          name
          amount
          price {
            amount
            currency
          }
          priceCount
          startAt
          endAt
        }
        legalName
        legalAddress {
          postcode
          state
          city
          street
          building
        }
        inn
        kpp
        createdAt
        updatedAt
      }
      info {
        totalCount
        page
        limit
      }
    }
  }
  `;

export const ACTIVATE_PROMO = gql`
mutation($input: ActivatePromocodeInput!)  {
    activatePromocode(input: input) {
      id
      name
      paid
      paidAt
      canceled
      canceledAt
      paymentAmount {
        amount
        currency
      }
      amountPayable {
        amount
        currency
      }
      billPayments {
        id
        amount {
          amount
          currency
        }
        billInvoiceId
        description
        confUrl
        status
        createdAt
        updatedAt
      }
      billServices {
        id
        name
        amount
        price {
          amount
          currency
        }
        priceCount
        startAt
        endAt
        createdAt
        updatedAt
      }
      legalName
      legalAddress {
        postcode
        state
        city
        street
        building
      }
      inn
      kpp
      createdAt
      updatedAt
    }
  }
`;

export const BILL_SERVICES = gql`
query($where:WhereBillServiceInput){
    billServices(where: $where) {
      items {
        id
        name
        amount
        price {
          amount
          currency
        }
        priceCount
        startAt
        endAt
        createdAt
        updatedAt
      }
    }
  }
  `;

export const CREATE_BILL_INVOICE = gql`
  mutation ($input:CreateBillInvoiceInput!){
      createBillInvoice(input: $input) {
        id
        name
        paid
        paidAt
        canceled
        canceledAt
        paymentAmount {
          amount
          currency
        }
        amountPayable {
          amount
          currency
        }
        billPayments {
          id
          amount {
            amount
            currency
          }
          billInvoiceId
          description
          confUrl
          status
          createdAt
          updatedAt
        }
        billServices {
          id
          name
          amount
          price {
            amount
            currency
          }
          priceCount
          startAt
          endAt
          createdAt
          updatedAt
        }
        legalName
        legalAddress {
          postcode
          state
          city
          street
          building
        }
        inn
        kpp
        createdAt
        updatedAt
      }
    }
    `;

export const CREATE_DEPOTS = gql`
mutation($input:CreateDepotInput) {
    createDepot(
      input: $input) {
      id
      name
      description
    }
}
`;

export const DEPOTS_ITEMS = gql`
query {
    depots {
      items {
        id
        name
        description
        createdAt
        updatedAt
      }
      info {
        totalCount
        page
        limit
      }
    }
  }
`;

export const CreateBillPayment = gql`
mutation ($input:CreateBillPaymentInput!){
    createBillPayment(input:$input) {
      id
      amount {
        amount
        currency
      }
      billInvoiceId
      description
      confUrl
      status
      createdAt
      updatedAt
    }
  }
  `;

export const MEASURE = gql`
query {
  measures {
    code
    precision
    name
    abbr
  }
  depots {
    items {
      id
      name
      description
      createdAt
      updatedAt
    }
    info{
      page
      nextPage
      previousPage
      totalCount
    }
  }
}
  `;

export const WORKCATALOG = gql`
query($where: WhereVehicleModificationWorkCatalogInput){
    vehicleModificationWorkCatalogs(where: $where) {
      id
      name
      groups {
        id
        parentId
        path
        name
        subGroups {
          id
          parentId
          path
          name
          works {
            id
            name
            action
            timeHrs
            unitPrice {
              amount
              currency
            }
          }
        }
        works {
          id
          name
          action
          timeHrs
          unitPrice {
            amount
            currency
          }
        }
      }
    }
    }
`;

export const CreateRepairWorkInput = gql`
  mutation($input: CreateRepairWorkInput) {
      createRepairWork(input:$input) {
        id
        name
        action
        modificationWorkId
        timeHrs
        unitPrice {
          amount
          currency
        }
        unitCount
        discount {
          amount
          percent
        }
        originalPrice {
          amount
          currency
        }
        totalPrice {
          amount
          currency
        }
      }
    }
    `;