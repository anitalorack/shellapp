import gql from 'graphql-tag'

export const UPDATE_DOCUMENT = gql`
mutation($input: UpdateDocumentInput) {
  updateDocument(input: $input) {
    id
    type {
      code
      name
      objectTypes
    }
    number
    date
    objectId
    objectType
    attachments {
      id
      objectId
      objectType
      objectProperty
      filename
      mimetype
      encoding
      file {
        url
        path
      }
    }
    createdAt
    updatedAt
  }
}
`;

export const DOCUMENTS_TYPE = gql`
query($where: WhereDocumentTypeInput) {
  documentTypes(where: $where) {
    code
    name
    objectTypes
  }
}
`;

export const CREATEDOCUMENT = gql`
mutation($input: CreateDocumentInput) {
  createDocument(input: $input) {
    id
    type {
      code
      name
      objectTypes
    }
    number
    date
    objectId
    objectType
    attachments {
      id
      objectId
      objectType
      objectProperty
      filename
      mimetype
      encoding
      file {
        url
        path
      }
    }
    createdAt
    updatedAt
  }
}
`;