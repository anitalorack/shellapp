import { uiColor } from "./uiColor.json"
import { label } from "./label.json"

export const buttom = {
    Red: {
        "border": uiColor.Red,
        "background": uiColor.Very_Pole_Grey,
        "font": uiColor.Red
    },
    Yellow: {
        "border": uiColor.Yellow,
        "background": uiColor.Yellow,
        "font": uiColor.Very_Dark_Grey
    },
    LightRed: {
        "border": uiColor.Very_Dark_Grey,
        "background": uiColor.Very_Pole_Grey,
        "font": uiColor.Red
    },
    VeryRed: {
        "border": uiColor.Red,
        "background": uiColor.Red,
        "font": uiColor.Very_Pole_Grey
    },
    LightGrey: {
        "border": uiColor.Polo_Grey,
        "background": uiColor.Polo_Grey,
        "font": uiColor.Very_Dark_Grey
    },
    Grey: {
        "border": uiColor.Very_Dark_Grey,
        "background": 'transparent',
        "font": uiColor.Very_Dark_Grey
    },
    MonoGrey: {
        "border": uiColor.Very_Pole_Grey,
        "background": uiColor.Very_Pole_Grey,
        "font": uiColor.Very_Dark_Grey
    },
    VeryGrey: {
        "border": uiColor.Very_Dark_Grey,
        "background": uiColor.Very_Dark_Grey,
        "font": uiColor.Very_Pole_Grey
    }
};

export const drawler = {
    Header: {
        font: uiColor.Red,
        size: '18px',
        weight: 'bold',
        left: '0px',
        align: 'center',
        js: 'center',
        background: uiColor.Very_Dark_Grey,
    },
    Description: {
        font: uiColor.Very_Dark_Grey,
        size: '10px',
        weight: 'normal',
        left: '0px',
        align: 'left',
        js: 'center',

    },
    Title: {
        font: uiColor.Very_Dark_Grey,
        size: '16px',
        weight: 'bold',
        left: '0px',
        js: 'center',
        align: 'left',
    },
    Item: {
        font: uiColor.Very_Dark_Grey,
        size: '12px',
        weight: 'normal',
        background: uiColor.Very_Pole_Grey,
        left: '25px',
        align: 'left',
        js: 'center',
    },
    Next: {
        font: uiColor.Very_Dark_Grey,
        size: '12px',
        weight: 'normal',
        background: uiColor.Very_Pole_Grey,
        left: '25px',
        align: 'left',
        js: 'center',
    },
    Famous: {
        font: uiColor.Very_Dark_Grey,
        size: '12px',
        weight: 'normal',
        background: uiColor.Very_Pole_Grey,
        left: '25px',
        align: 'left',
        js: 'center',
    },
    ItemsA: {
        font: uiColor.Very_Dark_Grey,
        size: '12px',
        weight: 'bold',
        background: uiColor.Yellow,
        left: '25px',
        align: 'left',
        js: 'center',
    },
};

export default {
    colors: {
        primary: uiColor.Yellow,
        secondary: uiColor.Very_Pole_Grey,
        disabled: uiColor.Very_Dark_Grey,
        placeholderGrey: uiColor.Light_Grey,
        gold: uiColor.Gold,
        text: {
            primary: "#000",
            secondary: uiColor.Red,
            exited: uiColor.Very_Pole_Grey,
        },
    },
    color: { uiColor },
    label,
    buttom,
    drawler,
    padding: {
        normal: 20,
        small: 15,
    },
    "font-Size": {
        normal: 16,
    },
}
