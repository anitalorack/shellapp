/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/display-name */
// /* eslint-disable react/jsx-indent-props */
import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { FilledTextField } from 'react-native-material-textfield';
import { uiColor } from 'styled/colors/uiColor.json';
// import { font, keyboard } from './style'


export const FilledTextFieldComponents = ({
    disabled,
    onFocus,
    onChangeText,
    onSubmitPassword,
    autoCorrect,
    clearTextOnFocus,
    enablesReturnKeyAutomatically,
    error,
    formatText,
    keyboardType,
    label,
    maxLength,
    secureTextEntry,
    title,
    value
}) => {
    const refContainer = useRef();

    return (
        <FilledTextField
            ref={refContainer}
            autoCapitalize="none"
            autoCorrect={autoCorrect}
            baseColor={uiColor.Very_Dark_Grey}
            characterRestriction={maxLength}
            clearTextOnFocus={clearTextOnFocus}
            disabled={disabled}
            editable
            enablesReturnKeyAutomatically={enablesReturnKeyAutomatically}
            error={error}
            errorColor={error ? uiColor.Red : uiColor.Very_Dark_Grey}
            fontSize={font.fontSize}
            formatText={formatText}
            keyboardType={keyboard(keyboardType)}
            label={label}
            labelFontSize={font.label}
            labelTextStyle={{ color: 'green' }}
            maxLength={maxLength}
            multiline
            onChangeText={(e) => onChangeText(e)}
            onFocus={() => onFocus()}
            onSubmitEditing={() => onSubmitPassword()}
            returnKeyType="next"
            secureTextEntry={secureTextEntry}
            style={{ flex: 1 }}
            textColor={uiColor.Very_Dark_Grey}
            tintColor={uiColor.Very_Dark_Grey}
            title={title}
            titleTextStyle={{ textAlign: "right" }}
            value={value}
        />
    )
};

FilledTextFieldComponents.propTypes = {
    autoCorrect: PropTypes.bool,
    clearTextOnFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    enablesReturnKeyAutomatically: PropTypes.bool,
    error: PropTypes.string,
    formatText: PropTypes.string,
    keyboardType: PropTypes.string,
    label: PropTypes.string,
    maxLength: PropTypes.number,
    onChangeText: PropTypes.func,
    onFocus: PropTypes.func,
    onSubmitPassword: PropTypes.func,
    secureTextEntry: PropTypes.bool,
    title: PropTypes.string,
    value: PropTypes.string,

};

FilledTextFieldComponents.defaultProps = {
    autoCorrect: false,
    clearTextOnFocus: false,
    disabled: false,
    enablesReturnKeyAutomatically: false,
    error: false,
    formatText: null,
    keyboardType: "phone-pad",
    label: null,
    maxLength: 30,
    onChangeText: () => null,
    onFocus: () => null,
    onSubmitPassword: () => null,
    secureTextEntry: false,
    title: null,
    value: null

};