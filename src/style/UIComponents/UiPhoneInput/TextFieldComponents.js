/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/display-name */
// /* eslint-disable react/jsx-indent-props */
import PropTypes from 'prop-types';
import * as R from 'ramda';
import React, { useRef } from 'react';
import { TextField } from 'react-native-material-textfield';
import { uiColor } from 'styled/colors/uiColor.json';
// import { font, keyboard } from './style';

export const UITextField = ({
    disabled,
    onFocus,
    onChangeText,
    onSubmitPassword,
    clearTextOnFocus,
    enablesReturnKeyAutomatically,
    error,
    formatText,
    keyboardType,
    label,
    maxLength,
    mode,
    secureTextEntry,
    title,
    value
}) => {
    const refContainer = useRef();

    return (
        <TextField
            ref={refContainer}
            baseColor={uiColor.Very_Dark_Grey}
            clearTextOnFocus={clearTextOnFocus}
            disabled={disabled}
            editable
            enablesReturnKeyAutomatically={enablesReturnKeyAutomatically}
            error={error}
            errorColor={error ? uiColor.Red : uiColor.Very_Dark_Grey}
            fontSize={font.size}
            formatText={formatText}
            keyboardType={keyboard(keyboardType)}
            label={label}
            labelFontSize={14}
            labelTextStyle={{ fontFamily: "ShellMedium", fontSize: 10 }}
            maxLength={maxLength}
            multiline
            onChangeText={(e) => onChangeText(e)}
            onFocus={() => onFocus()}
            onSubmitEditing={() => onSubmitPassword()}
            prefix={R.equals(mode, 'phone') ? "+7" : null}
            returnKeyType="done"
            secureTextEntry={secureTextEntry}
            style={{ fontFamily: 'ShellMedium', fontSize: 10 }}
            title={title}
            value={value}
        />
    )
};

UITextField.propTypes = {
    clearTextOnFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    enablesReturnKeyAutomatically: PropTypes.bool,
    error: PropTypes.string,
    formatText: PropTypes.string,
    keyboardType: PropTypes.string,
    label: PropTypes.string,
    maxLength: PropTypes.number,
    onChangeText: PropTypes.func,
    onFocus: PropTypes.func,
    onSubmitPassword: PropTypes.func,
    secureTextEntry: PropTypes.bool,
    title: PropTypes.string,
    value: PropTypes.string,

};

UITextField.defaultProps = {
    clearTextOnFocus: false,
    disabled: false,
    enablesReturnKeyAutomatically: false,
    error: null,
    formatText: null,
    keyboardType: "phone-pad",
    label: null,
    maxLength: 30,
    onChangeText: () => null,
    onFocus: () => null,
    onSubmitPassword: () => null,
    secureTextEntry: false,
    title: null,
    value: null,
};