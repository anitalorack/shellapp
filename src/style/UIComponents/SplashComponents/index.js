/* eslint-disable dot-notation */
/* eslint-disable no-confusing-arrow */
import React from 'react'
import styled from 'styled-components/native'
import { Dimensions, View } from 'react-native'
import { isOdd } from 'utils/helper'
import { uiColor } from 'styled/colors/uiColor.json';

const windowWidth = Dimensions.get('window').height;

// TODO VERY CUSTOM USER SCREEN NEED TO REFACTORING
const SplashContainer = (props) => (
    <View style={{
        flex: 1,
        backgroundColor: uiColor.Very_Pole_Grey,
        justifyContent: 'space-between',
    }}
    >
        <View style={{ flex: 1 }}>
            {props.children}
        </View>
    </View>
);

const LabelText = styled.Text`
font-family: ${props => isOdd(props.fonts) ? 'ShellLight' : props.fonts};
font-size:${props => props.items ? props.theme.label[props.items]["size"] : "14px"};
color:${props => props.color ? props.theme.color.uiColor[props.color] : "#404040"}; 
`;

const LoaderBlockBottom = styled.View`
justify-content:flex-end;
bottom:25px;
`;

const LogoCenterBlock = styled.View`
/* height:${windowWidth / 4}; */
justify-content:center;
align-items:center;
`;

export {
    LabelText, SplashContainer, LogoCenterBlock, LoaderBlockBottom
}