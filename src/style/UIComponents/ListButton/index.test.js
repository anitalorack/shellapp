import React from 'react';
import renderer from 'react-test-renderer';
import ListButton from './index';
import { ThemeProvider } from 'styled-components/native';
import Theme from 'styled/colors'

describe('ListButton', () => {

    it('render correct ListButton ', () => {
        const shallowRenderer = renderer.create(
            <ThemeProvider theme={Theme}>
                <ListButton title="Slava" mode="Item" />
            </ThemeProvider>);
        expect(shallowRenderer).toMatchSnapshot()
    });
    it('render correct ListButton ', () => {
        const shallowRenderer = renderer.create(
            <ThemeProvider theme={Theme}>
                <ListButton title="Slava" mode='ItemsA' />
            </ThemeProvider>);
        expect(shallowRenderer).toMatchSnapshot()
    });
    it('render correct LabelText ', () => {
        const shallowRenderer = renderer.create(
            <ThemeProvider theme={Theme}>
                <ListButton title="Slava" mode='Next'  >
                    Slava
                </ListButton >
            </ThemeProvider>);
        expect(shallowRenderer).toMatchSnapshot()
    })
});