/* eslint-disable dot-notation */
/* eslint-disable react/jsx-sort-default-props */
/* eslint-disable react/sort-prop-types */
/* eslint-disable react/jsx-indent-props */
import React from 'react'
import Famous from 'assets/svg/famous'
import next from 'assets/svg/next'
import searchSvg from 'assets/svg/search'
import PropTypes from 'prop-types'
import * as R from 'ramda'
import { DrawlerText, DrawlerView, SvgImage } from './style'

const ListButton = ({ onPress, title, mode }) => {
    const svg = R.cond([
        [R.equals("Item"), R.always(searchSvg)],
        [R.equals("ItemsA"), R.always(searchSvg)],
        [R.equals("Famous"), R.always(Famous)],
        [R.equals("Next"), R.always(next)],

    ])(mode);

    return (
        <DrawlerView
            items={mode}
            onPress={(click) => onPress(click)}
        >
            <DrawlerText items={mode}>{title}</DrawlerText>
            <SvgImage xml={svg} />
        </DrawlerView>
    )
};


ListButton.propTypes = {
    mode: PropTypes.string,
    onPress: PropTypes.func,
    title: PropTypes.string,

};

ListButton.defaultProps = {
    mode: "Item",
    onPress: () => { return null },
    title: "Кликните для перехода для поиска"
};

export default ListButton