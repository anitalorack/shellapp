/* eslint-disable react/forbid-prop-types */
/* eslint-disable max-len */
/* eslint-disable padded-blocks */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable array-callback-return */
/* eslint-disable object-curly-newline */
/* eslint-disable arrow-body-style */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable no-unused-expressions */
import React from 'react'
import UiText from 'uiComponents/UiText'
import * as R from 'ramda'
import Logo from 'assets/svg/shell'
import PropTypes from 'prop-types'
import { BottomContainer, BottomItem, BottomSvg } from './style'

const BottomTabNavigator = ({ items, color }) => (
    <BottomContainer>
        {R.map((element) => {
            return (
                <BottomItem key={element.key} lenght={R.length(items)} onPress={() => element.onPress()}>
                    <BottomSvg height="30" width="30" xml={element.logo} />
                    <UiText
                        color={color}
                        mode
                        Title={element.label}
                    />
                </BottomItem>)
        })(items)}
    </BottomContainer>
);

BottomTabNavigator.propTypes = {
    color: PropTypes.string,
    items: PropTypes.array,
};

BottomTabNavigator.defaultProps = {
    color: "MonoGrey",
    items: [
        { logo: Logo, label: "Slava", onPress: () => { return null }, key: 1 },
        { logo: Logo, label: "Slava", onPress: () => { return null }, key: 2 },
        { logo: Logo, label: "Slava", onPress: () => { return null }, key: 3 },
        { logo: Logo, label: "Slava", onPress: () => { return null }, key: 4 },
    ],
};


export default BottomTabNavigator