import styled from 'styled-components'
import { SvgXml } from 'react-native-svg'

export const BottomContainer = styled.View`
flex-direction:row;
position:absolute;
bottom:0;
display:flex;
`;
export const BottomItem = styled.TouchableOpacity`
display:flex;
width:${props => `${100 / props.lenght}%`};
flex-direction:column;
align-items:center
`;
export const BottomSvg = styled(SvgXml)`
justify-content:center;
`;
