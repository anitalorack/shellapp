/* eslint-disable prefer-destructuring */
/* eslint-disable no-confusing-arrow */
import { SvgXml } from 'react-native-svg'
import styled from 'styled-components/native'

export const InputSearch = styled.TextInput`
font-size:16px;
`;
export const ButtonSearchClick = styled.TouchableOpacity`
border-color:${props => props.active ? "#323232" : "#767767"};
border-bottom-width:${props => props.active ? "2px" : "1px"};
width:${props => props.active ? "80px" : "100px"};
justify-content:space-between;
align-items:center;
`;


export const BlockChildren = styled.View`
flex-direction: row;
`;
export const SearchContainer = styled.View`
align-items: center;
background-color:${props => props.theme.color.uiColor.Yellow} 
`;


export const TextSelecter = styled.Text`
font-weight:${props => props.active ? "bold" : "normal"};
color:${props => props.active ? props.theme.color.uiColor.Very_Dark_Grey : props.theme.color.uiColor.Mid_Grey};
font-size:${props => props.active ? "22px" : "16px"};
/* font-size:16px; */
`;

export const SvgImage = styled(SvgXml)`
height:30px;
width:30px;
margin-right:10px;
`;

export const Block = styled.View`
display:flex;
flex-direction:row;
align-items:center;
background:${props => props.theme.color.uiColor.Yellow};
height:60px;
padding-left:15px;
padding-right:15px;
`;
