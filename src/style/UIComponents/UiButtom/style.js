/* eslint-disable no-confusing-arrow */
/* eslint-disable arrow-body-style */
import styled from 'styled-components/native'
import { buttom } from 'styled/colors'
import * as R from 'ramda'

const dataColor = (mode, css) => {
    return buttom[css][mode]
};

const ButtonText = styled.Text`
font-family:"ShellBold";
text-align: center;
/* font-weight: bold; */
text-transform:${props => R.equals(true, props.unTransform) ? 'null' : 'capitalize'};
color:${props => dataColor('font', props.color)};
`;
const ButtonTextLite = styled.Text`
text-align: center;
font-weight: normal;
text-transform:capitalize;
color:${props => dataColor('font', props.color)};
`;

const ButtonContainer = styled.TouchableOpacity`
background: ${props => dataColor('background', props.color)};
border: ${props => dataColor('border', props.color)};
/* height:60px; */
align-items:center;
justify-content:center;
margin:${props => props.merge ? "0px" : '10px'};
padding:${props => props.merge ? "0px" : '10px'};
opacity:${props => props.disabled ? 0.5 : 1}
`;

export { ButtonText, ButtonTextLite, ButtonContainer } 