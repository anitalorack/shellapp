/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { SvgXml } from 'react-native-svg';
import { RowComponents } from 'screens/TaskEditor/components/table/style';


export const HeaderButton = ({ empty, button, dispatch, svg }) => {
    if (empty) {
        return (
            <View />
        )
    }

    return (
        <RowComponents>
            <TouchesState
                button
                color={R.includes(button, ['DESTROY', "CLOSED"]) ? "VeryRed" : "Yellow"}
                flex={0}
                onPress={() => dispatch()}
                svg={svg}
                title={R.defaultTo("DESTROY")(button)}
            />
        </RowComponents>
    )
};