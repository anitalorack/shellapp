import styled from 'styled-components'

export const RowViewDiscount = styled.View`
padding-right:${props => props.items ? 0 : 10};
margin-left:${props => props.items ? 10 : 0};
`