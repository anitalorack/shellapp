/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { Text } from 'react-native'
import { InputComponents } from 'utils/component';
import { SubHeaderTable } from 'styled/UIComponents/SubHeader'
import styled from 'styled-components';

const initialInputState = {
    id: "DESCRIPTION",
    path: 'name',
    keyboardType: "default",
    label: 'whatPreview',
    title: "Error message",
    value: ["description"],

    maxLength: 350,
    testID: "NameInput",
};

export const ComponentDescription = ({ dispatch, value }) => (
    <BlockDescription style={{ padding: 5 }} >
        <SubHeaderTable
            dispatch={dispatch}
            header="Причина обращения"
            state="ред"
        >
            <Text>{value}</Text>
            <InputComponents
                {...initialInputState}
                onChangeText={(change) => dispatch({ type: "Edit", payload: change })}
                storage={{ description: value }}
            />
        </SubHeaderTable>
    </BlockDescription>
);

const BlockDescription = styled.View`
background: #FFFFFF;
border: 2px solid rgba(196, 196, 196, 0.5);
box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
border-radius: 2px;
`;