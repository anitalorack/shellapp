/* eslint-disable react/jsx-indent-props */
import * as React from 'react';
import { Switch } from 'react-native-paper';
import PropTypes from 'prop-types'

const SwitchButtom = ({ state, onPress }) => (
    <Switch
        onValueChange={() => { onPress(!state) }}
        value={state}
    />
);

SwitchButtom.propType = {
    onPress: PropTypes.func,
    state: PropTypes.boolean,
};
SwitchButtom.getDefaultProps = {
    onPress: () => { return null },
    state: false,
};

export default SwitchButtom