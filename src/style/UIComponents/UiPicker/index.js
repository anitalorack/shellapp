/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { Picker } from '@react-native-community/picker';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View, Text } from 'react-native';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';

export const UiPicker = ({
    element, dispatch, Items, placeholder, main, disabled, horizont
}) => {
    if (isOdd(Items)) {
        return (
            <View style={[horizont ? { flexDirection: 'row', paddingLeft: 10, paddingRight: 10 } : { paddingBottom: 10 }]}>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={{
                        textAlign: 'left',
                        padding: 10,
                        paddingLeft: 0,
                        color: uiColor.Mid_Grey,
                    }}
                    testID="BlockLaker"
                >
                    {i18n.t(R.path(['path'])(element))}
                    {main ? <Text style={{ color: 'red' }}> *</Text> : <Text />}
                </LabelText>
                <View
                    style={[{
                        borderWidth: 1,
                        borderColor: uiColor.Light_Grey,
                    }, horizont ? { flex: 1 } : {}]}
                >
                    <Picker
                        enabled={disabled}
                        mode="dropdown"
                        onValueChange={(itemValue) => {
                            dispatch(itemValue)
                        }}
                        selectedValue={null}
                        style={{
                            height: 50,
                        }}
                    >
                        {
                            R.map(x => (
                                <Picker.Item
                                    key={R.path(['label'])(x)}
                                    label={R.path(['label'])(x)}
                                    value={R.path(['value'])(x)}
                                />
                            ))([{ label: null, value: null }])
                        }
                    </Picker>
                </View>
            </View>
        )
    }

    return (
        <View style={[horizont ? { flexDirection: 'row', paddingLeft: 10, paddingRight: 10 } : { paddingBottom: 10 }]}>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={{
                    textAlign: 'left',
                    padding: 10,
                    paddingLeft: 0,
                }}
                testID="BlockLaker"
            >
                {i18n.t(R.path(['path'])(element))}
                {main ? <Text style={{ color: 'red' }}> *</Text> : <Text />}
            </LabelText>
            <View
                style={[{
                    borderWidth: 1,
                    borderColor: uiColor.Light_Grey,
                }, horizont ? { flex: 1 } : {}]}
            >
                <Picker
                    enabled={disabled}
                    mode="dropdown"
                    onValueChange={(itemValue) => {
                        dispatch(itemValue)
                    }}
                    selectedValue={R.defaultTo(null)(R.path(['id'])(placeholder))}
                    style={{
                        paddingBottom: 0,
                        height: 50,
                        // width: 330,
                    }}
                >
                    {
                        R.map(x => (
                            <Picker.Item
                                key={R.path(['label'])(x)}
                                label={R.path(['label'])(x)}
                                value={R.path(['value'])(x)}
                            />
                        ))(Items)
                    }
                </Picker>
            </View>
        </View>
    )
};