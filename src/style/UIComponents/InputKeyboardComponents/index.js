/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable padded-blocks */
/* eslint-disable arrow-body-style */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { TextFieldComponents } from 'uiComponents/UiPhoneInput/TextFieldComponents';

export const InputKeyboardComponents = (props) => {
    const [state, setState] = useState(null);
    const {
        value, title, label, onChangeText, onFocus, keyboardType, maxLength, focus, disabled, mode, onSubmitEditing
    } = props;

    return (
        <TextFieldComponents
            autoCorrect
            characterRestriction
            clearTextOnFocus
            disabled={disabled}
            enablesReturnKeyAutomatically
            error={state}
            keyboardType={keyboardType}
            label={label}
            maxLength={maxLength}
            mode={mode}
            onBlur={(effect) => {
                onFocus(effect)
            }}
            onChangeText={(change) => {
                if (state) {
                    setState(false)
                }

                return onChangeText(change)
            }}
            onFocus={focus}
            onSubmitEditing={(submit) => onSubmitEditing(submit)}
            secureTextEntry={false}
            title={title}
            value={value}
        />
    )
};

InputKeyboardComponents.propTypes = {
    disabled: PropTypes.bool,
    focus: PropTypes.bool,
    keyboardType: PropTypes.string,
    label: PropTypes.string,
    maxLength: PropTypes.number,
    onChangeText: PropTypes.func,
    onFocus: PropTypes.func,
    value: PropTypes.string,
};

InputKeyboardComponents.getDefaultProps = {
    disabled: true,
    focus: false,
    keyboardType: "",
    label: null,
    maxLength: null,
    onChangeText: () => null,
    onFocus: () => null,
    value: null,
};