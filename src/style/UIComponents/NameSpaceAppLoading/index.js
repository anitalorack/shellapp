/* eslint-disable react/jsx-indent-props */
import Logo from 'assets/svg/shell';
import React from 'react';
import { SvgXml } from 'react-native-svg';
import { LogoCenterBlock } from 'uiComponents/SplashComponents';
import { Dimensions } from 'react-native';
import { uiColor } from 'styled/colors/uiColor.json';
import * as R from 'ramda'
import { View } from 'react-native'

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const NameSpaceAppLoading = (props) => {
    const width = windowWidth < 800 ? windowWidth / 1.8 : '200';
    const height = windowHeight < 800 ? windowHeight / 5 : '175';

    return (
        <View style={{ flex: 1 }}>
            <LogoCenterBlock
                style={{
                    flex: 1,
                    backgroundColor: R.path(['absolute'])(props) ? uiColor.Yellow : uiColor.Very_Pole_Grey
                }}
            >
                <SvgXml
                    height={height}
                    width={width}
                    xml={Logo}
                />
            </LogoCenterBlock>
        </View>
    )
};

export default NameSpaceAppLoading