/* eslint-disable newline-before-return */
/* eslint-disable no-negated-condition */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import close from 'assets/svg/close';
import famous from 'assets/svg/famous';
import back from 'assets/svg/goback';
import menu from 'assets/svg/menu';
import i18n from 'i18n-js';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import React from 'react';
import {
    TouchableHighlight, TouchableOpacity, View, StyleSheet
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import LANGUAGE from './Components/LANGUAGE';
import { HeaderContainer } from './styles';

const svg = R.cond([
    [R.equals('Home'), () => (
        <SvgXml
            height="30"
            width="100"
            xml={famous}
        />)],
    [R.equals('Back'), () => (
        <SvgXml
            height="30"
            width="30"
            xml={back}
        />)],
    [R.equals('close'), () => (
        <SvgXml
            height="30"
            width="30"
            xml={close}
        />)],
    [isOdd, () => (
        <SvgXml
            height="30"
            width="30"
            xml={menu}
        />)],
]);

const HeaderComponent = ({
    title, onPress, absolute, header, mode
}) => (
        <>
            <HeaderContainer
                style={Styles.color}
            >
                {R.equals(absolute)(false) ?
                    <TouchableOpacity onPress={() => onPress({ type: "GoBack" })}>
                        {svg(mode)}
                    </TouchableOpacity> :
                    <View
                        style={Styles.icon}
                    />}
                <LabelText
                    color={ColorCard(header ? 'classic' : 'error').font}
                    fonts="ShellMedium"
                    items="Title"
                    style={[styles.container, Styles.replace]}
                >
                    {!title ? "" : i18n.t(title)}
                </LabelText>
                <TouchableHighlight
                    onPress={() => onPress({ type: "Lang" })}
                >
                    <LANGUAGE />
                </TouchableHighlight>
            </HeaderContainer >
        </>
    );

export const HeaderTitle = ({ header, dispatch, color }) => {
    return (
        <View>
            <View
                style={Styles.card}
            >
                <View
                    style={{ flex: 1 }}
                >
                    {isOdd(R.path(['title'])(header)) ? <View /> :
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SSDescription"
                            style={Styles.text2}
                        >
                            {R.path(['title'])(header)}
                        </LabelText>}
                    {isOdd(R.path(['subHeader'])(header)) ? <View /> : <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                        style={Styles.text}
                    >
                        {R.path(['subHeader'])(header)}
                    </LabelText>}
                </View>
                {isOdd(R.path(['button'])(header)) ?
                    <View /> : <View
                        style={{ flexDirection: 'row' }}
                    >
                        <TouchableOpacity
                            onPress={() => dispatch({ type: "CLICK" })}
                        >
                            <LabelText
                                color={ColorCard(color ? 'trans' : 'classic').font}
                                fonts="ShellMedium"
                                items="SSDescription"
                                style={{
                                    textTransform: 'capitalize',
                                    textAlign: 'center',
                                    padding: 15,
                                    backgroundColor: color ? uiColor.Red : uiColor.Yellow
                                }}
                            >
                                {isOdd(R.path(['button'])(header)) ? i18n.t('UPDATE') : i18n.t(R.path(['button'])(header))}
                            </LabelText>
                        </TouchableOpacity >
                    </View>}
            </View>
            <View>
                {isOdd(R.path(['menu'])(header)) ?
                    <View /> : <View
                        style={Styles.items}
                    >
                        {R.addIndex(R.map)((x, keys) => (
                            <TouchableHighlight
                                key={keys}
                                onPress={() => dispatch({ type: x })}
                                style={{
                                    padding: 10,
                                    borderBottomColor: 'red',
                                    borderBottomWidth: R.equals(x, R.path(['curr'])(header)) ? 2 : 0
                                }}
                            >
                                <LabelText
                                    color={ColorCard(R.equals(x, R.path(['curr'])(header)) ? "error" : 'classic').font}
                                    fonts="ShellMedium"
                                    items="SSDescription"
                                    style={{
                                        textAlign: 'center',
                                    }}
                                >
                                    {i18n.t(x)}
                                </LabelText>
                            </TouchableHighlight>
                        ))(R.path(['menu'])(header))}
                        <View />
                    </View>}
            </View>
        </View>
    )
};

const Styles = StyleSheet.create({
    items: {
        flexDirection: 'row',
        borderColor: uiColor.Very_Pole_Grey,
        borderWidth: 0.8
    },
    text: {
        textTransform: 'capitalize',
        textAlign: 'left',
        color: uiColor.Mid_Grey,
    },
    card: {
        backgroundColor: uiColor.Polo_Grey,
        alignItems: 'center',
        padding: 15,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    replace: {
        textAlign: 'center',
        textTransform: 'uppercase',
        flex: 1,
    },
    text2: {
        textTransform: 'capitalize',
        textAlign: 'left'
    },
    icon: {
        height: 30,
        width: 30
    },
    color: {
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: uiColor.Very_Pole_Grey,
    }
});

HeaderComponent.propTypes = {
    absolute: PropTypes.bool,
    title: PropTypes.string,
};
HeaderComponent.defaultProps = {
    absolute: false,
    title: null,
};

export default HeaderComponent