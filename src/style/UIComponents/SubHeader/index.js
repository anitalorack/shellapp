/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import * as R from 'ramda'
import styled from 'styled-components'

export const SubHeaderTable = (props) => {
    const { state, dispatch, header } = props;

    return (
        <View>
            <Block>
                <LabelText
                    color={ColorCard('classic').font}
                    items="Title"
                    style={{
                        textAlign: 'left',
                        textTransform: "capitalize"
                    }}
                    testID="TITLE"
                >
                    {header}
                </LabelText>
                {R.path(['value'])(state) ?
                    <TouchableHighlight
                        onPress={() => dispatch({ type: state.type })}
                    >
                        <LabelText
                            color={ColorCard('error').font}
                            items="SSDescription"
                            style={{ textAlign: 'left', justifyContent: "flex-end" }}
                            testID="TITLE"
                        >
                            {state.value}
                        </LabelText>
                    </TouchableHighlight> :
                    <View />}
            </Block>
            {props.children}
        </View>
    )
};

const Block = styled.View`
 justify-content: space-between;
flex-direction: row;
margin-top: 15px;
/* margin-bottom: 15px; */
`;