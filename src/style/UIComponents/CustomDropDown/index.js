/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView, StyleSheet, TouchableOpacity, View
} from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { SvgXml } from 'react-native-svg';
import Logo from 'assets/svg/rectangle';

// export const updateSelect = (echo) => R.cond([
//     [R.equals('Scroll'), () => setState({ type: "Scroll", payload: R.path(['payload'])(echo) })],
//     [R.equals('Selected'), () => setState({ type: "Scroll", payload: R.path(['payload'])(echo) })],
// ])(R.path(['type'])(echo))

const styleDropDown = StyleSheet.create({
    Dropdown: {
        position: 'absolute',
        height: 250,
        minWidth: 250,
        // maxHeight: 550,
        bottom: 0,
        top: 0,
        right: 0,
        left: 0,
        zIndex: 100,
        backgroundColor: 'white'
    },
    margin: {
        margin: 2,
        borderRadius: 2,
        borderWidth: 1

    }
});

export const DropdownMagic = ({
    data, onPress, state, style, mode, value
}) => {
    const [magic, setState] = React.useState({
        key: null, data: value, press: false, state: data
    });
    const REf = React.useRef();

    return (
        <>
            {R.cond([
                [R.pipe(
                    R.omit(['state']),
                    R.values,
                    R.uniq,
                    R.equals([null, false])
                ), () => (
                    <>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="Description"
                            style={styles.container}
                            testID="BlockLaker"
                        >
                            {i18n.t(R.path(['title'])(mode))}
                        </LabelText>
                        <TouchableOpacity
                            onPress={() => {
                                onPress({ type: "Scroll", payload: false });
                                setState(R.assocPath(['press'], true, state))
                            }}
                            style={[styleDropDown.margin]}
                        >
                            <CardComponents
                                style={[styles.rowCenter, {
                                    justifyContent: 'space-between',
                                    paddingTop: 4,
                                    paddingLeft: 0,
                                    paddingBottom: 4
                                }]}
                            >
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SSDescription"
                                    style={styles.container}
                                    testID="BlockLaker"
                                >
                                    {i18n.t(R.path(['placeholder'])(mode))}
                                </LabelText>
                                <SvgXml height="10" width="10" xml={Logo} />
                            </CardComponents >
                        </TouchableOpacity>
                    </>
                )],
                [R.pipe(
                    R.omit(['state']),
                    R.path(['press']),
                    R.equals(true)
                ), () => (
                    <View>
                        <CardComponents>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="Description"
                                style={styles.container}
                                testID="BlockLaker"
                            >
                                {i18n.t(R.path(['title'])(mode))}
                            </LabelText>
                        </CardComponents >
                        <CardComponents>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SSDescription"
                                style={styles.container}
                                testID="BlockLaker"
                            >
                                Loading ...
                            </LabelText>
                        </CardComponents >
                        <ScrollView
                            ref={REf}
                            style={[styleDropDown.Dropdown, styles.shadow, { margin: 3, zIndex: 10000 }]}>
                            {R.addIndex(R.map)(x => (
                                <TouchableOpacity
                                    onPress={() => {
                                        onPress({ type: "Selected", payload: x });
                                        setState(R.assocPath(['data'], x, { ...magic, press: false }))
                                    }}
                                >
                                    <CardComponents
                                        key={x}
                                        style={[styleDropDown.margin, {
                                            backgroundColor: R.equals(x, R.path(['value'])(mode)) ? "gold" : "white",
                                            paddingTop: 5,
                                            paddingBottom: 5,
                                            paddingLeft: 0
                                        }]}
                                    >
                                        <LabelText
                                            color={ColorCard('classic').font}
                                            fonts="ShellMedium"
                                            items="SSDescription"
                                            style={styles.container}
                                            testID="BlockLaker"
                                        >
                                            {R.join(' ', R.paths(R.path(['path'])(mode))(x))}
                                        </LabelText>
                                    </CardComponents >
                                </TouchableOpacity>)
                            )(isOdd(data) ? [] : data)}
                        </ScrollView>
                    </View>
                )],
                [R.pipe(
                    R.omit(['state']),
                    R.path(['data'])
                ), () => (
                    <>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="Description"
                            style={styles.container}
                            testID="BlockLaker"
                        >
                            {i18n.t(R.path(['title'])(mode))}
                        </LabelText>
                        <TouchableOpacity
                            onPress={() => {
                                onPress({ type: "Scroll", payload: false });
                                setState(R.assocPath(['press'], true, magic))
                            }}
                            style={[styleDropDown.margin]}
                        >
                            <CardComponents
                                style={[styles.rowCenter, {
                                    justifyContent: 'space-between',
                                    paddingTop: 4,
                                    paddingLeft: 0,
                                    paddingBottom: 4
                                }]}
                            >
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SSDescription"
                                    style={styles.container}
                                    testID="BlockLaker"
                                >
                                    {R.join(' ', R.paths(R.path(['path'])(mode))(value))}
                                </LabelText>
                                <SvgXml height="10" width="10" xml={Logo} />
                            </CardComponents >
                        </TouchableOpacity>
                    </>
                )],
                [R.pipe(
                    R.path(['state']),
                    isOdd
                ), () => (
                    <>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="Description"
                            style={styles.container}
                            testID="BlockLaker"
                        >
                            {i18n.t(R.path(['title'])(mode))}
                        </LabelText>
                        <TouchableOpacity
                            onPress={() => {
                                onPress({ type: "Scroll", payload: false });
                                setState(R.assocPath(['press'], true, magic))
                            }}
                            style={[styleDropDown.margin]}
                        >
                            <CardComponents style={[styles.rowCenter, { justifyContent: 'space-between', paddingTop: 4, paddingLeft: 0, paddingBottom: 4 }]}>
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SSDescription"
                                    style={styles.container}
                                    testID="BlockLaker"
                                >
                                    Loading ...
                                </LabelText>
                                <SvgXml height="10" width="10" xml={Logo} />
                            </CardComponents >
                        </TouchableOpacity>
                    </>
                )],
                [R.pipe(
                    R.omit(['state']),
                    R.values,
                    R.uniq,
                    isOdd,
                    R.equals(false)
                ), () => (
                    <>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="Description"
                            style={styles.container}
                            testID="BlockLaker"
                        >
                            {i18n.t(R.path(['title'])(mode))}
                        </LabelText>
                        <TouchableOpacity
                            onPress={() => {
                                onPress({ type: "Scroll", payload: false });
                                setState(R.assocPath(['press'], true, state))
                            }}
                            style={[styleDropDown.margin]}
                        >
                            <CardComponents
                                style={[styles.rowCenter, {
                                    justifyContent: 'space-between',
                                    paddingTop: 4,
                                    paddingLeft: 0,
                                    paddingBottom: 4
                                }]}
                            >
                                <LabelText
                                    color={ColorCard('classic').font}
                                    fonts="ShellMedium"
                                    items="SSDescription"
                                    style={styles.container}
                                    testID="BlockLaker"
                                >
                                    {i18n.t(R.path(['placeholder'])(mode))}
                                </LabelText>
                                <SvgXml height="10" width="10" xml={Logo} />
                            </CardComponents >
                        </TouchableOpacity>
                    </>
                )],
            ])(magic)}
        </>
    )
};
