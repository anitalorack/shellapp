/* eslint-disable no-confusing-arrow */
/* eslint-disable arrow-body-style */
import styled from 'styled-components/native'
import { buttom } from 'styled/colors'

const dataColor = (mode, css) => {

    return buttom[css][mode]
};

const ButtonText = styled.Text`
display:flex;
text-align: center;
font-size:16px;
font-weight: bold;
color:${props => dataColor('font', props.color)} ;
`;
const ButtonTextLite = styled.Text`
display:flex;
text-align: center;
font-size:16px;
font-weight: normal;
color:${props => dataColor('font', props.color)};
`;

const ButtonContainer = styled.View`
background: ${props => dataColor('background', props.color)};
border: 2px solid ${props => dataColor('border', props.color)};
height:${props => props.mode ? '25px' : '60px'};
align-items: center;
border-radius: 10px;
margin:10px;
justify-content: center;
`;

export { ButtonText, ButtonTextLite, ButtonContainer } 