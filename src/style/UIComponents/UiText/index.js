/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import PropTypes from 'prop-types';
import React from 'react';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { ButtonContainer } from './style';

const UiText = ({
    color, Title, Update, mode
}) => (
        <ButtonContainer
            color={color}
            mode={mode}
            style={{ flex: 1 }}
        >
            <LabelText
                color={ColorCard('error').front}
                fonts="ShellMedium"
                items="SDescription"
            >{Title}</LabelText>
            {Update && <LabelText
                color={ColorCard('warn').font}
                fonts="ShellMedium"
                items="SDescription"
                style={{ textAlign: 'center', padding: 5 }}
            >
                {Update}
            </LabelText>}
        </ButtonContainer>
    );

UiText.propTypes = {
    color: PropTypes.string,
    mode: PropTypes.bool,
    Title: PropTypes.string,
    Update: PropTypes.string
};

UiText.defaultProps = {
    color: "Red",
    mode: false,
    Title: null,
    Update: null
};

export default UiText