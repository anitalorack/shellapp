/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { VEHICLE_GQL } from 'screens/Main/CarsScreen/GQL';
import styled from 'styled-components';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const VehicleInfo = () => {
    const { loading, error, data } = useQuery(VEHICLE_GQL, {
        variables: { "where": { "vin": { "contain": "XTA21140054056875" } } },
        fetchPolicy: "network-only"
    });

    if (error) return (<BlockTable style={{ padding: 5, marginTop: 15 }} />);
    if (loading) return (<BlockTable style={{ padding: 5, marginTop: 15 }} />);

    const dateq = R.pipe(R.path(['vehicles', 'items']), R.head)(data);
    const TableSpecific = [
        { title: "ВЛАДЕЛЕЦ", name: `${R.path(['owner', 'name', 'last'])(dateq)} ${R.path(['owner', 'name', 'first'])(dateq)}` },
        { title: "МОДЕЛЬ", name: `${R.path(['modification', 'model', 'manufacturer', 'name'])(dateq)} ${R.path(['modification', 'model', 'name'])(dateq)}` },
        // { title: "МОДИФИКАЦИЯ", name: "1.6 G4FC, 121 л.с." },
        // { title: "Год эксплуат", name: "2007 - 2011" },
        { title: "ГОСНОМЕР", name: R.prop('plate')(dateq) },
        { title: "VIN", name: R.prop('vin')(dateq) },
        { title: "№ КУЗОВ", name: R.prop('vin')(dateq) },
        // { title: "ТИП ДВИГАТЕЛ", name: "Бензиновый" },
        { title: "ГОД", name: R.prop('year')(dateq) },
        { title: "ПРОБЕГ", name: `${R.prop('mileage')(dateq)} км` },
        { title: "ЦВЕТ", name: R.prop('color')(dateq) }
    ];

    return (
        < BlockTable>
            {R.addIndex(R.map)((x, keys) => (
                <View
                    key={keys}
                    style={{
                        flexDirection: "row",
                        justifyContent: 'space-between',
                        padding: 5
                    }}
                >
                    <LabelText
                        color={ColorCard('classic').font}
                        items="SSDescription"
                        style={{ textTransform: "capitalize" }}
                        testID="TITLE"
                    >
                        {x.title}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        items="SSDescription"
                        style={{
                            textAlign: 'right',
                            fontWeight: "bold"
                        }}
                        testID="TITLE"
                    >
                        {x.name}
                    </LabelText>
                </View>
            ))(TableSpecific)}
        </BlockTable >
    )
};

const BlockTable = styled.View`
border: 2px solid rgba(196, 196, 196, 0.5);
box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
background: rgba(196, 196, 196, 0.25);
border-radius: 2px;
padding: 5px;
margin-top: 15px;
`;