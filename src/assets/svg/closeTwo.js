export default `
<svg width="6423" height="6219" viewBox="0 0 6423 6219" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M6323 3109.5C6323 4768.58 4933 6119 3211.5 6119C1490 6119 100 4768.58 100 3109.5C100 1450.42 1490 100 3211.5 100C4933 100 6323 1450.42 6323 3109.5Z" stroke="black" stroke-width="200"/>
<rect width="4423.83" height="554.576" transform="matrix(0.657075 -0.753825 0.629505 0.776997 1582.88 4561.42)" fill="black"/>
<rect width="4443.48" height="554.483" transform="matrix(-0.638979 -0.769224 0.630208 -0.776427 4455.76 5033.75)" fill="black"/>
</svg>
`