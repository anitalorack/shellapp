export default `
<svg width="6332" height="6332" viewBox="0 0 6332 6332" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="3189.05" y="6331.43" width="1039" height="4444" transform="rotate(-135 3189.05 6331.43)" fill="black"/>
<rect x="3877.07" y="5550.08" width="1039" height="4444" transform="rotate(135 3877.07 5550.08)" fill="black"/>
</svg>
`