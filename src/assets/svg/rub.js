export default `
<svg width="20" height="70" viewBox="0 0 20 58" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.5 4V54" stroke="black" stroke-width="8" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M10 4H27.4945C34.4011 4 40 9.59492 40 16.5C40 23.4036 34.41 29 27.4945 29H10" stroke="black" stroke-width="8" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4 43.5H31.5" stroke="black" stroke-width="8" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
`