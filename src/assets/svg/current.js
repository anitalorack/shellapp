export default `
<svg width="180" height="180" viewBox="0 0 180 180" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="0.5" y="0.5" width="179" height="179" rx="4.5" fill="#FBCE07" stroke="#37474F"/>
<rect x="55" y="85" width="70" height="10" fill="#37474F"/>
<rect x="85" y="125" width="70" height="10" transform="rotate(-90 85 125)" fill="#37474F"/>
<rect x="35.5" y="40.5" width="109" height="100" rx="3.5" stroke="#37474F" stroke-width="3"/>
</svg>
`