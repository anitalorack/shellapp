export default `
<svg width="32" height="31" viewBox="0 0 32 31" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="21" height="3" rx="1.5" transform="matrix(0.7085 -0.705711 0.7085 0.705711 14 27.8203)" fill="#DD1D21"/>
<rect width="21" height="3" rx="1.5" transform="matrix(-0.7085 -0.705711 0.7085 -0.705711 15.8149 29.7627)" fill="#DD1D21"/>
<rect width="10" height="2.71478" rx="1.35739" transform="matrix(0.7085 -0.705711 0.7085 0.705711 14.0635 18.7646)" fill="#585858"/>
<rect width="10" height="2.71" rx="1.355" transform="matrix(-0.7085 -0.705711 0.7085 -0.705711 15.8784 20.7061)" fill="#585858"/>
<rect width="10" height="2.71478" rx="1.35739" transform="matrix(-0.707107 0.707107 -0.70989 -0.704312 17.998 2.97949)" fill="#DD1D21"/>
<rect width="10" height="2.71" rx="1.355" transform="matrix(0.70989 0.704312 -0.707107 0.707107 16.1792 1.04102)" fill="#DD1D21"/>
</svg>

`