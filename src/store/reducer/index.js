/* eslint-disable no-useless-escape */
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import { profilePersistConfig, profileReducer } from 'screens/Drawler/redux';
import { clientPersistConfig, clientReducer } from 'screens/Main/ClientScreen/redux';
import {
  masterPersistConfig,
  masterReducer,
  taskPersistConfig,
  taskReducer,
  aliasePersistConfig,
  aliaseReducer,
  filterPersistConfig,
  filterReducer,
  agentPersistConfig,
  agentReducer,
  depotsGaragePersistConfig,
  depotsGarageReducer,
  employeeSPersistConfig,
  employeeSReducer,
  depotsGoodsPersistConfig,
  depotsGoodsReducer
} from 'screens/Master/Query/redux';

import {
  employeePersistConfig,
  employeeReducer,
  depotsPersistConfig,
  depotsReducer,
  stockPersistConfig,
  stockReducer,
} from 'screens/Main/CreateAdmins/redux';

const rootReducer = combineReducers({
  clients: persistReducer(clientPersistConfig, clientReducer),
  profile: persistReducer(profilePersistConfig, profileReducer),
  master: persistReducer(masterPersistConfig, masterReducer),
  employee: persistReducer(employeePersistConfig, employeeReducer),
  depots: persistReducer(depotsPersistConfig, depotsReducer),
  stock: persistReducer(stockPersistConfig, stockReducer),
  task: persistReducer(taskPersistConfig, taskReducer),
  alise: persistReducer(aliasePersistConfig, aliaseReducer),
  filter: persistReducer(filterPersistConfig, filterReducer),
  agent: persistReducer(agentPersistConfig, agentReducer),
  employees: persistReducer(employeeSPersistConfig, employeeSReducer),
  garage: persistReducer(depotsGaragePersistConfig, depotsGarageReducer),
  goods: persistReducer(depotsGoodsPersistConfig, depotsGoodsReducer),
});

export { rootReducer };