/* eslint-disable new-cap */
import { setContext } from 'apollo-link-context';
import fetch from 'node-fetch'
import { getItem } from 'utils/async'
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import * as R from 'ramda'
import { createUploadLink } from 'apollo-upload-client'

export const uri = 'http://devapi.zengine.it/graphql';

const httpLink = new createHttpLink({ uri, fetch });
const uploadLink = createUploadLink({ uri });

const authLink = setContext(async (_, { headers }) => {
    const jswToken = await getItem();
    const token = `Bearer ${jswToken}`;

    return {
        headers: R.assoc("Authorization", token, headers),
    };
});

const client = new ApolloClient({
    link: authLink.concat(httpLink, uploadLink),
    cache: new InMemoryCache(),
    queryDeduplication: false
});

export default client;
