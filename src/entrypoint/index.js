/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-comment-style */
/* eslint-disable react/jsx-max-depth */
import { ApolloProvider } from '@apollo/react-hooks';
import i18n from 'localization';
import Moment from 'moment/min/moment-with-locales';
import React, { useEffect } from 'react';
import { LocaleConfig } from 'react-native-calendars';
import FlashMessage from "react-native-flash-message";
import 'react-native-gesture-handler';
import { MenuProvider } from 'react-native-popup-menu';
import { Provider, shallowEqual, useSelector } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { SplashScreen } from 'screens/Splash';
import { persistor, store } from 'store';
import { ThemeProvider } from 'styled-components/native';
import Theme from "styled/colors";
import CreateRootNavigator from 'entrypoint/router';
import client from 'entrypoint/apollo';

const calendarLocaleConfig = locale => {
    const momentLocale = Moment.localeData(locale);

    const {
        monthsShort,
        weekdays,
        weekdaysShort,
    } = momentLocale._config;

    return {
        monthNames: momentLocale.months(),
        monthNamesShort: monthsShort,
        dayNames: weekdays,
        dayNamesShort: weekdaysShort
    };
};

const App = () => {
    const user = useSelector(state => state.profile, shallowEqual);

    useEffect(() => {
        const locale = user.language;

        Moment.locale([locale, 'en']);
        LocaleConfig.locales[locale] = calendarLocaleConfig(locale);
        LocaleConfig.defaultLocale = locale;
        i18n.locale = locale
    }, [user.language]);

    return (
        <MenuProvider skipInstanceCheck={true}>
            <ThemeProvider theme={Theme}>
                <PersistGate loading={<SplashScreen state="First" />} persistor={persistor}>
                    <CreateRootNavigator />
                </PersistGate>
                <FlashMessage position="top" />
            </ThemeProvider>
        </MenuProvider>
    )
};

const ZenCared = () => (
    <Provider store={store}>
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>
    </Provider>
);

export default ZenCared;
