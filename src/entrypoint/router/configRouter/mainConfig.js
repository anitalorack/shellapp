/* eslint-disable react/jsx-indent-props */
import React from "react"
import { DashboardScreen } from 'screens/Main/MainScreen/Query'
import { MasterScreen } from "screens/Master/Query"
import CreateEmployeeScreen from "screens/Master/CreateEmployeeScreen"
import { ContragentScreen } from "screens/Contragent/Query"
import { ContragentEditScreen } from "screens/Contragent/Input"
import { ClientScreen } from 'screens/Main/ClientScreen/Query'
import { ClientEditScreen } from 'screens/Main/ClientScreen/Input'
import { WorksList } from 'screens/Main/WorkScreen/Query'
import { WorksEditScreen } from 'screens/Main/WorkScreen/Input'
import { CarsList } from "screens/Main/CarsScreen/Query"
import { CarsEditScreen } from "screens/Main/CarsScreen/Input"
import { PayConnect } from 'screens/Main/Pay/Query'
import { AutoService } from 'screens/Main/AutoServices/Query'
import { Garage } from 'screens/Main/Garage/Query'
import { FindParts } from 'screens/Main/FindParts/Query'
import { AddGoods } from 'screens/Main/AddGoods'
import { PrewiewScreen } from 'screens/Main/CarsScreen/Preview'
import { PreviewGarage } from 'screens/Main/Garage/Preview/index'
import { TaskGarage } from 'screens/Main/TaskBack'
import { Calendar } from 'screens/Main/Calendar'
import { InputPost } from 'screens/Main/PostBuilder/Input'
import { PostScreen } from 'screens/Main/PostBuilder/Query'
import { PrewiewClientScreen } from 'screens/Main/ClientScreen/PreviewScreen'
import {
    HeaderScreen, MainScreen, CreateScreen, Preview
} from 'utils/routing/index.json'
import * as R from 'ramda'
import { CreateTask } from 'screens/Main/TaskBack/screen/CreateTask'
import { Stack } from "../stack"

export const drawlerComp = [
    DashboardScreen,
    DashboardScreen,
    ContragentScreen,
    ContragentEditScreen,
    MasterScreen,
    ClientScreen,
    CreateEmployeeScreen,
    ClientEditScreen,
    CarsEditScreen,
    WorksList,
    CarsList,
    WorksEditScreen,
    PayConnect,
    AutoService,
    Garage,
    FindParts,
    AddGoods,
    PrewiewScreen,
    PreviewGarage,
    TaskGarage,
    CreateTask,
    Calendar,
    InputPost,
    PostScreen,
    PrewiewClientScreen
];

const drawlerRouter = [
    HeaderScreen.Main,
    MainScreen.Main,
    MainScreen.Contragent,
    MainScreen.EditContragent,
    MainScreen.Master,
    MainScreen.Client,
    CreateScreen.MasterScreen,
    CreateScreen.ClientScreen,
    CreateScreen.CarsScreen,
    MainScreen.WorkScreen,
    MainScreen.Cars,
    CreateScreen.WorkEditScreen,
    MainScreen.Pay,
    MainScreen.AutoService,
    MainScreen.Garage,
    MainScreen.FindParts,
    CreateScreen.ADDGOODS,
    Preview.PreviewDashBoardVehilcle,
    Preview.PreviewGarage,
    Preview.PreviewTaskGarage,
    CreateScreen.CreateTask,
    CreateScreen.CalendarInit,
    CreateScreen.Post,
    Preview.PreviewPostGarage,
    Preview.PreviewClient,
];

const Main = () => {
    const drawler = R.addIndex(R.map)((elemementRoute, keys) => (
        <Stack.Screen
            key={`elemementRoute_${keys}_Drawler_Screen`}
            component={drawlerComp[elemementRoute]}
            name={drawlerRouter[elemementRoute]}
            options={{
                headerShown: false,
            }}
        />
    ))(R.keys(drawlerComp));

    return (
        <Stack.Navigator>
            {drawler}
        </Stack.Navigator>
    )
};

export default Main