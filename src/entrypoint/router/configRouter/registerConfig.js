/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
/* eslint-disable max-len */
/* eslint-disable multiline-comment-style */
import React from 'react';
/* eslint-disable object-curly-newline */
import RegisterPhone from 'screens/Home/PhoneScreen';
import RegisterSms from 'screens/Home/SmsScreen';
import { RegisterScreen } from 'utils/routing/index.json'
import { Stack } from '../stack';

const Register = () => (
    <Stack.Navigator>
        <Stack.Screen
            component={RegisterPhone}
            name={RegisterScreen.Phone}
            options={{
                headerShown: false,
            }}
        />
        <Stack.Screen
            component={RegisterSms}
            name={RegisterScreen.Sms}
            options={{
                headerShown: false,
            }}
        />
    </Stack.Navigator>

);

export default Register