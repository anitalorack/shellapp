import gql from 'graphql-tag'

export const QUERY_INDIVIDUAL = gql`
query(
    $where: WhereIndividualInput
    $order: OrderIndividualInput
    $paginate: PageInput
  ) {
    individuals(where: $where, order: $order, paginate: $paginate) {
        info{
            page
            nextPage
            previousPage
            totalCount
          }
      items {
        id
        name
        inn
        email
        phone
        description
        inn
        personalData
        registrationAddress {
          city
          state
          street
          postcode
          building
        }
        physicalAddress {
          state
          street
          postcode
          building
          city
        }
        bankDetails {
          name
          rcbic
        currentAccount
          rcbic
          id
          counterpartyId
          mainBankDetail
          correspondentAccount
        }
      }
    }
  }
`;

export const QUERY_SOLETRADERS = gql`
query(
    $where: WhereSoleTraderInput
    $order: OrderSoleTraderInput
    $paginate: PageInput
  ) {
    soleTraders(where: $where, order: $order, paginate: $paginate) {
        info{
            page
            nextPage
            previousPage
            totalCount
          }
      items {
        id
        name
        inn
        email
        phone
        description
        kpp
        inn
        psrnsp
        certificateNumber
        certificateDate
        legalAddress {
          city
          state
          street
          postcode
          building
        }
        physicalAddress {
          state
          street
          postcode
          building
          city
        }
        bankDetails {
          rcbic
          id
          name
          rcbic
          currentAccount
          counterpartyId
          mainBankDetail
          correspondentAccount
        }
      }
    }
  }
`;

export const QUERY_LEGAL_ENTITY = gql`
query(
    $where: WhereLegalEntityInput
    $order: OrderLegalEntityInput
    $paginate: PageInput
  ) {
    legalEntities(where: $where, order: $order, paginate: $paginate) {
        info{
            page
            nextPage
            previousPage
            totalCount
          }
      items {
        id
        name
        inn
        email
        phone
        description
        psrn
        kpp
        inn
        legalAddress {
          city
          state
          street
          postcode
          building
        }
        physicalAddress {
          state
          street
          postcode
          building
          city
        }
        bankDetails {
          rcbic
          id
          name
          rcbic
          currentAccount
          counterpartyId
          mainBankDetail
          correspondentAccount
        }
      }
    }
  }
`;

export const QUERY_COUNTERPARTIES = gql`
query(
    $where: WhereCounterpartyInput
    $order: OrderCounterpartyInput
    $paginate: PageInput
  ) {
    counterparties(where: $where, order: $order, paginate: $paginate) {
        info{
            page
            nextPage
            previousPage
            totalCount
          }
      items {
        id
        inn
        type
        name
        phone
        email
        description
        psrn
        kpp
        inn
        personalData
        psrnsp
        certificateNumber
        certificateDate
        physicalAddress {
          city
          state
          street
          postcode
          building
        }
        legalAddress {
          city
          state
          street
          postcode
          building
        }
        registrationAddress {
          city
          state
          street
          postcode
          building
        }
        bankDetails {
          rcbic
          id
          name
          rcbic
          currentAccount
          counterpartyId
          mainBankDetail
          correspondentAccount
        }
      }
    }
  }
`;

export const QUERY_COUNTERPARTY = gql`
query($where: WhereCounterpartyInput!) {
    counterparty(where: $where) {
      id
      inn
      type
      name
      phone
      email
      description
      psrn
      kpp
      inn
      personalData
      psrnsp
      certificateNumber
      certificateDate
      physicalAddress {
        city
        state
        street
        postcode
        building
      }
      legalAddress {
        city
        state
        street
        postcode
        building
      }
      registrationAddress {
        city
        state
        street
        postcode
        building
      }
      bankDetails {
        rcbic
        id
        name
        rcbic
        currentAccount
        counterpartyId
        mainBankDetail
        correspondentAccount
      }
    }
  }
`;

export const FIND_COUNTERPARITY = gql`
query($where: WhereFindCounterpartyInput!) {
    findCounterparty(where: $where) {
      id
      inn
      type
      name
      phone
      email
      description
      psrn
      kpp
      inn
      personalData
      psrnsp
      certificateNumber
      certificateDate
      physicalAddress {
        city
        state
        street
          postcode
          building
      }
        legalAddress {
            city
            state
            street
            postcode
            building
        }
        registrationAddress {
            city
            state
            street
            postcode
            building
        }
        bankDetails {
            rcbic
            id
            name
            rcbic
            currentAccount
            counterpartyId
            mainBankDetail
            correspondentAccount
        }
    }
}
`;

export const CREATE_CONTREPARITY = gql`
mutation($input: CreateCounterpartyInput!) {
  createCounterparty(input: $input) {
    id
    inn
    type
    name
    phone
    email
    description
    psrn
    kpp
    inn
    personalData
    psrnsp
    certificateNumber
    certificateDate
    physicalAddress {
      city
      state
      street
      postcode
      building
    }
    legalAddress {
      city
      state
      street
      postcode
      building
    }
    registrationAddress {
      city
      state
      street
      postcode
      building
    }
    bankDetails {
      rcbic
      id
      name
      rcbic
      currentAccount
      counterpartyId
      mainBankDetail
      correspondentAccount
    }
  }
}
`;

export const CREATE_INDIVIDUAL = gql`
    mutation($input: CreateIndividualInput!) {
        createIndividual(input: $input) {
            id
            bankDetails{
                rcbic
                id
                name
                rcbic
                currentAccount
                counterpartyId
                mainBankDetail
                correspondentAccount
            }
        }
    }
`;

export const CREATE_LEGALENTITY = gql`
    mutation($input: CreateLegalEntityInput!) {
        createLegalEntity(input: $input) {
            id
            bankDetails{
                rcbic
                id
                name
                rcbic
                currentAccount
                counterpartyId
                mainBankDetail
                correspondentAccount
            }
        }
    }
`;

export const CREATE_SOLETRADER = gql`
    mutation($input: CreateSoleTraderInput!) {
        createSoleTrader(input: $input) {
            id
            bankDetails{
                rcbic
                id
                name
                rcbic
                currentAccount
                counterpartyId
                mainBankDetail
                correspondentAccount
            }
        }
    }
`;

export const UPDATE_CONTREPARITY = gql`
mutation($input: UpdateCounterpartyInput!) {
    updateCounterparty(input: $input) {
        id
        inn
        type
        name
        phone
        email
        description
        psrn
        kpp
        inn
        personalData
        psrnsp
        certificateNumber
        certificateDate
        physicalAddress {
            city
            state
            street
            postcode
            building
        }
        legalAddress {
            city
            state
            street
            postcode
            building
        }
        registrationAddress {
            city
            state
            street
            postcode
            building
        }
        bankDetails {
            rcbic
            id
            name
            rcbic
            currentAccount
            counterpartyId
            mainBankDetail
            correspondentAccount
        }
    }
}
`;

export const UPDATE_INDIVIDUAL = gql`
mutation($input: UpdateIndividualInput!) {
    updateIndividual(input: $input) {
        id
        inn
        name
        phone
        email
        description
        inn
        personalData
        physicalAddress {
            city
            state
            street
            postcode
            building
        }
        registrationAddress {
            city
            state
            street
            postcode
            building
        }
        bankDetails {
            rcbic
            id
            name
            rcbic
            currentAccount
            counterpartyId
            mainBankDetail
            correspondentAccount
        }
    }
}
`;

export const UPDATE_LEGALENTITY = gql`
mutation($input: UpdateLegalEntityInput!) {
    updateLegalEntity(input: $input) {
        id
        inn
        name
        phone
        email
        description
        kpp
        inn
        physicalAddress {
            city
            state
            street
            postcode
            building
        }
        legalAddress {
            city
            state
            street
            postcode
            building
        }
        bankDetails {
            rcbic
            id
            name
            rcbic
            currentAccount
            counterpartyId
            mainBankDetail
            correspondentAccount
        }
    }
}
`;

export const UPDATE_SOLETRADER = gql`
mutation($input: UpdateSoleTraderInput!) {
    updateSoleTrader(input: $input) {
        id
        inn
        name
        phone
        email
        description
        kpp
        inn
        psrnsp
        certificateNumber
        certificateDate
        physicalAddress {
            city
            state
            street
            postcode
            building
        }
        legalAddress {
            city
            state
            street
            postcode
            building
        }
        bankDetails {
            rcbic
            id
            name
            rcbic
            currentAccount
            counterpartyId
            mainBankDetail
            correspondentAccount
        }
    }
}
`;

export const DESTROY_CONTREPARITY = gql`
    mutation(
        $input: Int!
    ){
        destroyCounterparty(
            id:$input
        ){
            id
            inn
            name
            phone
            email
            description
            physicalAddress{
                city
                state
                street
                postcode
                building
            }
            registrationAddress{
                city
                state
                street
                postcode
                building
            }
            legalAddress{
                city
                state
                street
                postcode
                building
            }
            bankDetails{
                rcbic
                id
                name
                rcbic
                currentAccount
                counterpartyId
                mainBankDetail
                correspondentAccount
            }
        }
    }
`;

export const DESTROY_INDIVIDUAL = gql`
    mutation(
        $input: Int!
    ){
        destroyIndividual(id: $input){
            id
            updatedAt
        }
    }
`;

export const DESTROY_LEGALENTITY = gql`
    mutation(
        $input: Int!
    ){
        destroyLegalEntity(
            id:$input
        ){
            id
        }
    }
`;

export const DESTROY_SOLETRADER = gql`
    mutation(
        $input: Int!
    ){
        destroySoleTrader(
            id:$input
        ){
            id
        }
    }
`;

export const CREATE_BANKDETAIL = gql`
mutation($input: CreateBankDetailInput!) {
  createBankDetail(input: $input) {
    id
    name
    currentAccount
    correspondentAccount
    rcbic
    counterpartyId
    mainBankDetail
  }
}
`;

export const UPGRADE_BANKDETAIL = gql`
mutation($input: UpdateBankDetailInput!) {
  updateBankDetail(input: $input) {
    id
    name
    currentAccount
    correspondentAccount
    rcbic
    counterpartyId
    mainBankDetail
  }
}
`;

export const DESTROY_BANKDETAIL = gql`
    mutation( $input: Int!){
        destroyBankDetail(
            id: $input
        ){
            id
            name
        }
    }`;