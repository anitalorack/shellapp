/* eslint-disable no-confusing-arrow */
/* eslint-disable newline-after-var */
/* eslint-disable max-statements */
/* eslint-disable object-curly-newline */
/* eslint-disable max-len */
import styled from 'styled-components/native';

export const ScrolerView = styled.ScrollView`
background-color: ${props => props.theme.color.uiColor.Very_Pole_Grey};
`;

export const Hererre = styled.View`
background-color: ${props => props.theme.color.uiColor.Yellow};
flex-direction: row;
justify-content: space-around;
align-items: center;
`;

export const BlockLaker = styled.View`
display:flex;
background-color: ${props => props.item ? props.theme.color.uiColor.Very_Pole_Grey : props.theme.color.uiColor.Polo_Grey};
border-radius: 5px;
margin: 15px 1px 1px;
padding: 15px;
`;

export const WarningBlock = styled.View`
justify-content: space-around;
align-items: center;
flex-direction: row;
height:60px;
padding:5px;
background-color: ${props => props.theme.color.uiColor.Yellow};
`;

export const Block = styled.View`
display:flex;
flex-direction:row;
justify-content:space-between;
padding-top:15px;
`;

export const ViewCardBlocke = styled.View`
flex-direction: row;
justify-content: space-between;
align-items: center;
`;
