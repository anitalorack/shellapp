/* eslint-disable max-statements */
/* eslint-disable newline-after-var */
/* eslint-disable max-params */
import * as R from 'ramda';
import { showMessage } from "react-native-flash-message"
import { MainScreen } from 'utils/routing/index.json';
import { isOdd, toInt, toString } from 'utils/helper'
import { requestDataLoader } from 'utils/api/funcComponent/Apollo';
import {
    QUERY_COUNTERPARTIES,
    QUERY_INDIVIDUAL,
    QUERY_LEGAL_ENTITY,
    QUERY_SOLETRADERS
} from 'screens/Contragent/Query/gql';

export const setRequestContext = async (click, client, navigation, dispatch, state) => {
    let role;
    let variablesFinish;
    const ROUTER = MainScreen.EditContragent;

    const variables = R.cond([
        [R.equals('CREATE'), () => navigation.navigate(ROUTER, {
            type: "ALL",
            itemId: null,
        })],
        [R.equals('EDIT'), () => navigation.navigate(ROUTER, {
            mode: "EDIT",
            itemId: null,
        })],
        [R.equals('goBack'), () => navigation.goBack()],
        [R.equals('SOLE_TRADER'), () => {
            dispatch({
                type: "CHANGEROLES",
                payload: "ALL"
            });

            return R.mergeAll([
                state,
                { where: R.path(['where'])(state) },
                { role: "SOLE_TRADER" },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        }],
        [R.equals('LEGAL_ENTITY'), () => {
            dispatch({
                type: "CHANGEROLES",
                payload: "LEGAL_ENTITY"
            });

            return R.mergeAll([
                state,
                { where: R.path(['where'])(state) },
                { role: "LEGAL_ENTITY" },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        }],
        [R.equals('INDIVIDUAL'), () => {
            dispatch({
                type: "CHANGEROLES",
                payload: "INDIVIDUAL"
            });

            return R.mergeAll([
                state,
                { where: R.path(['where'])(state) },
                { role: "INDIVIDUAL" },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        }],
        [R.equals('ALL'), () => {
            dispatch({
                type: "CHANGEROLES",
                payload: "ALL"
            });

            return R.mergeAll([
                state,
                { where: R.path(['where'])(state) },
                { role: "ALL" },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        }],
        [R.equals('INN'), R.always(
            R.mergeAll([
                state,
                { where: { inn: { eq: toInt(R.path(['where', 'q'])(state)) } } },
                { role: R.path(['role'])(state) },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        )],
        [R.equals('NAME'), R.always(
            R.mergeAll([
                state,
                { where: { name: { eq: toString(R.path(['where', 'q'])(state)) } } },
                { role: R.path(['role'])(state) },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        )],
        [R.equals('ID'), R.always(
            R.mergeAll([
                state,
                { where: { id: { eq: toInt(R.path(['where', 'q'])(state)) } } },
                { role: R.path(['role'])(state) },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        )],
        [R.equals("ADDED"), R.always(
            R.mergeAll([
                { where: R.path(['where'])(state) },
                { role: R.path(['role'])(state) },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: R.path(['paginate', 'page'])(state) + 1
                    }
                }
            ])
        )],
        [R.equals("REFRESH"), R.always(
            R.mergeAll([
                state,
                { where: R.path(['where'])(state) },
                { role: R.path(['role'])(state) },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        )],
        [R.equals("SEARCH"), R.always(
            R.mergeAll([
                { where: R.path(['where'])(state) },
                { role: R.path(['role'])(state) },
                { order: { name: 'desc' } },
                {
                    paginate: {
                        limit: 20,
                        page: 1
                    }
                }
            ])
        )]
    ])(click);

    if (R.includes(click, ['goBack', "CREATE", "EDIT"])) {
        return variables
    }
    if (R.includes(click, ["ALL", "LEGAL_ENTITY", "INDIVIDUAL", 'SOLE_TRADER'])) {
        role = click;
        variablesFinish = R.reject(isOdd)(R.omit(['role', 'loading', 'client'])(variables))
    } else {
        role = R.path(['role'])(state);
        variablesFinish = R.reject(isOdd)(R.omit(['role', 'loading', 'client'])(variables))
    }

    const document = R.cond([
        [R.equals('ALL'), R.always(QUERY_COUNTERPARTIES)],
        [R.equals('SOLE_TRADER'), R.always(QUERY_SOLETRADERS)],
        [R.equals('INDIVIDUAL'), R.always(QUERY_INDIVIDUAL)],
        [R.equals('LEGAL_ENTITY'), R.always(QUERY_LEGAL_ENTITY)],
    ])(role);

    try {
        const { data } = await requestDataLoader({
            client,
            variables: variablesFinish,
            query: document,
        });

        if (R.equals("ADDED", click)) {
            const match = {
                ...variables,
                role,
                client: R.path(R.union(R.keys(data), ['items']))(data)
            };

            return dispatch({
                type: "MERGE_CLIENT",
                payload: R.reject(isOdd, match)
            })
        }

        return dispatch({
            type: "UPDATE_CLIENT",
            payload: R.reject(isOdd, {
                ...variables,
                role,
                client: R.path(R.union(R.keys(data), ['items']))(data)
            })
        })
    } catch (error) {
        const match = {
            ...variablesFinish,
            role,
            client: []
        };
        dispatch({
            type: "UPDATE_CLIENT",
            payload: R.reject(isOdd, match)
        });

        return showMessage({
            message: R.toString(error),
            description: "Error",
            type: "info",
        });
    }
};
//  isOdd(R.omit(['delete'], R.mergeAll([blocked, text]))) ? { q: searchText } : R.omit(['delete'], R.mergeAll([blocked, text]))
export const changeText = async (client, event, state, dispatch) => {
    const variables = R.mergeAll([
        isOdd(event) ? { where: null } : { where: { q: event } },
        { role: R.path(['role'])(state) },
        { order: { name: 'desc' } },
        {
            paginate: {
                limit: 20,
                page: 1
            }
        }
    ]);
    const document = R.cond([
        [R.equals('ALL'), R.always(QUERY_COUNTERPARTIES)],
        [R.equals('SOLE_TRADER'), R.always(QUERY_SOLETRADERS)],
        [R.equals('INDIVIDUAL'), R.always(QUERY_INDIVIDUAL)],
        [R.equals('LEGAL_ENTITY'), R.always(QUERY_LEGAL_ENTITY)],
    ])(R.path(['role'])(state));

    try {
        const { data } = await requestDataLoader({
            client,
            variables: R.reject(isOdd, variables),
            query: document,
        });

        return dispatch({
            type: "UPDATE_CLIENT",
            payload: R.reject(isOdd, {
                ...variables,
                role: R.path(['role'])(state),
                client: R.path(R.union(R.keys(data), ['items']))(data)
            })
        })
    } catch (error) {
        dispatch({
            type: "UPDATE_CLIENT",
            payload: R.reject(isOdd, {
                ...variables,
                role: R.path(['role'])(state),
                client: []
            })
        })
    }
};