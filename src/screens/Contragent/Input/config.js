/* eslint-disable complexity */
/* eslint-disable newline-before-return */
/* eslint-disable max-len */
import * as R from 'ramda';
import styled from 'styled-components';
import { uiColor } from 'styled/colors/uiColor.json';
import { StyleSheet } from 'react-native'
import { isOdd } from 'utils/helper'

export const AddressDesription = styled.View`
background-color: ${uiColor.Very_Pole_Grey};
margin: 15px 4px 4px;
padding: 10px;
`;
export const isMode = x => x > 2;

export const ModalDescription = styled.View`
background-color: white;
margin:1px;
`;
export const SwitchDescription = styled.View`
flex-direction: row;
align-items: center;
justify-content: center;
`;

export const input = [
    {
        value: ["name"],
        title: "Error message",
        label: 'nameByAgent',
        main: true,
        multiline: true,
        path: 'AGENT',
        // maxLength: 120,
        keyboardType: "default",
    },
    {
        value: ["inn"],
        main: true,
        example: "",
        title: "Error message",
        label: 'innByAgent',
        path: 'AGENT',
        maxLength: 12,
        keyboardType: "numeric",
    },
];

export const data = [
    {
        value: ["description"],
        title: "Error message",
        example: "",
        label: 'shortByAgent',
        path: 'AGENT',
        multiline: true,
        keyboardType: "default",
    }, {
        value: ["phone"],
        title: "Error message",
        label: 'phoneByAgent',
        path: 'AGENT_phone',
        example: " (___) ___-__-__",
        mask: "+7 ([000]) [000]-[00]-[00]",
        keyboardType: "numeric",
    }, {
        value: ["email"],
        example: "example@example.com",
        title: "Error message",
        label: 'emailByAgent',
        path: 'AGENT',
        multiline: true,
        keyboardType: "default",
    }
];

export const document = [
    {
        id: "psrn",
        value: ["psrn"],
        title: "Error message",
        example: "",
        label: 'psrn',
        path: 'AGENT',
        maxLength: 15,
        keyboardType: "numeric",
    },
    // {
    //     value: ["kpp"],
    //     example: "",
    //     title: "Error message",
    //     label: 'kpp',
    //     path: 'AGENT',
    //     maxLength: 9,
    //     keyboardType: "numeric",
    // },
    {
        value: ["certificateNumber"],
        example: "",
        title: "Error message",
        label: 'certificateNumber',
        path: 'AGENT',
        maxLength: 15,
        keyboardType: "numeric",
    },
    {
        value: ["certificateDate"],
        title: "Error message",
        example: "YYYY-MM-DD",
        label: 'certificateDate',
        path: 'AGENT',
        mask: "[0000]-[00]-[00]",
        keyboardType: "numeric",
    },
    {
        value: ["personalData"],
        title: "Error message",
        example: "",
        label: 'personalData',
        path: 'AGENT',
        keyboardType: "default",
    },
    {
        value: ["psrnsp"],
        title: "Error message",
        label: 'psrnsp',
        example: "",
        path: 'AGENT',
        maxLength: 15,
        keyboardType: "numeric",
    }
];

export const adress = [
    {
        main: true,
        value: ["postcode"],
        title: "Error message",
        label: 'indexByAgent',
        path: 'postcode',
        multiline: true,
        keyboardType: "default",
    },
    {
        main: true,
        value: ["state"],
        example: "",
        title: "Error message",
        label: 'regionByAgent',
        path: 'state',
        multiline: true,
        keyboardType: "default",
    },
    {
        main: true,
        value: ["city"],
        example: "",
        title: "Error message",
        label: 'cityByAgent',
        path: 'city',
        multiline: true,
        keyboardType: "default",
    },
    {
        main: true,
        value: ["street"],
        example: "",
        title: "Error message",
        label: 'streetsByAgent',
        path: 'street',
        multiline: true,
        keyboardType: "default",
    },
    {
        main: true,
        value: ["building"],
        example: "",
        title: "Error message",
        label: 'houseByAgent',
        path: 'building',
        multiline: true,
        keyboardType: "default",
    },

];

// config conterparity
export const bank = [
    {
        id: "name",
        path: 'name',
        main: true,
        label: 'name',
        multiline: true,
        keyboardType: "default",

        value: ["name"],
        example: "",
        error: "Error message",
        errorMessage: 'PhoneError',
    },
    {
        id: "currentAccount",
        path: 'currentAccount',

        main: true,
        label: 'currentAccount',
        maxLength: 20,
        keyboardType: "numeric",
        errorLabel: true,

        value: ["currentAccount"],
        error: "Поле должно состоять из 20 цифр",
        errorMessage: 'PhoneError',
    },
    {
        id: "correspondentAccount",
        path: 'correspondentAccount',

        main: true,
        label: 'correspondentAccount',
        maxLength: 20,
        keyboardType: "numeric",
        errorLabel: true,

        value: ["correspondentAccount"],
        error: "Поле должно состоять из 20 цифр",
        errorMessage: 'PhoneError',
    },
    {
        id: "rcbic",
        path: 'rcbic',

        main: true,
        label: 'rcbic',
        maxLength: 9,
        keyboardType: "numeric",
        errorLabel: true,

        value: ["rcbic"],
        error: "Поле должно состоять из 9 цифр",
        errorMessage: 'PhoneError',
    },
];

export const BankInput = [
    {
        id: 1,
        path: 'bankName',
        main: true,
        label: 'bankName',
        multiline: true,
        keyboardType: "default",
        value: ['bankName'],
        example: "",
        error: "Error message",
        errorMessage: 'PhoneError',
    },
    {
        id: 3,
        path: 'checkingAccount',
        main: true,
        label: 'checkingAccount',
        maxLength: 20,
        keyboardType: "numeric",
        errorLabel: true,
        value: ['checkingAccount'],
        error: "Поле должно состоять из 20 цифр",
        errorMessage: 'PhoneError',
    },
    {
        id: 2,
        path: 'correspondentAccount',
        main: true,
        label: 'correspondentAccount',
        maxLength: 20,
        keyboardType: "numeric",
        errorLabel: true,
        value: ['correspondentAccount'],
        error: "Поле должно состоять из 20 цифр",
        errorMessage: 'PhoneError',
    },
    {
        id: 4,
        path: 'rcbic',
        main: true,
        label: 'rcbic',
        maxLength: 9,
        keyboardType: "numeric",
        value: ['rcbic'],
        errorLabel: true,
        error: "Поле должно состоять из 9 цифр",
        errorMessage: 'PhoneError',
    }
];

export const initialBank = [
    {
        name: null,
        currentAccount: null,
        correspondentAccount: null,
        multiline: true,
        rcbic: null,
        mainBankDetail: false,
    }
];

export const InitialAdress = {
    postcode: " ",
    state: " ",
    building: " ",
    street: " ",
    city: " ",
};

export const EmptyAdress = {
    postcode: null,
    state: null,
    building: null,
    street: null,
    city: null,
};

export const dropdown = {
    id: "Номер",
    path: 'AGENT',
    keyboardType: "num",
    label: 'typeAgent',
    title: "Error message",
    value: ["type"],
    testID: "RoleInput",
    mode: "dropdown",
    data: [
        { value: null },
        { value: "INDIVIDUAL" },
        { value: "LEGAL_ENTITY" },
        { value: "SOLE_TRADER" },
    ]
};

export const Counterparty = [
    { value: "LEGAL_ENTITY" },
    { value: "INDIVIDUAL" },
    { value: "SOLE_TRADER" }
];

export const UnConterparity = R.cond([
    [R.equals(), R.always("LEGAL_ENTITY")],
    [R.equals(), R.always("INDIVIDUAL")],
    [R.equals(), R.always("SOLE_TRADER")],
]);

export const InitialStorage = {
    type: null,
    name: null,
    inn: null,
    description: null,
    phone: null,
    email: null,
    psrn: null,
    kpp: null,
    legalAddress: {
        postcode: null,
        state: null,
        city: null,
        street: null,
        building: null
    },
    physicalAddress: {
        postcode: null,
        state: null,
        city: null,
        street: null,
        building: null
    },
    bankDetails: []
};

export const reBack = R.cond([
    [R.equals("Юридическое лицо"), R.always("LEGAL_ENTITY")],
    [R.equals("Индивидуальный предприниматель"), R.always("INDIVIDUAL")],
    [R.equals("Физическое лицо"), R.always("SOLE_TRADER")]
]);

export const isCheck = (n) => R.anyPass([R.equals(""), R.isNil])(n);

export const checkedRow = (modalInput) => R.pipe(
    R.paths([["postcode"], ["state"], ["city"], ["street"], ["building"]]),
    R.map(isCheck),
    R.includes(true)
)(modalInput);

export const addressTypeLogic = R.cond([
    [R.equals("INDIVIDUAL"), R.always({
        address: ["physicalAddress", 'address'],
        copy: "registrationAddress"
    })],
    [R.equals("LEGAL_ENTITY"), R.always({
        address: ["physicalAddress", 'address'],
        copy: "legalAddress"
    })],
    [R.equals("SOLE_TRADER"), R.always({
        address: ["physicalAddress", 'address'],
        copy: "legalAddress"
    })],
    [isOdd, R.always({
        copy: "legalAddress",
        address: ["address"]
    })],
]);

export const CheckButton = R.pipe(
    R.paths([['name'], ['inn']]),
    R.map(
        R.anyPass([
            R.equals(''),
            R.isNil
        ])),
    R.uniq,
    R.includes(true)
);

export const styleContr = StyleSheet.create({
    scroll: {
        flexGrow: 1,
        justifyContent: 'space-between',
        backgroundColor: uiColor.Very_Pole_Grey,
        padding: 10
    }
});