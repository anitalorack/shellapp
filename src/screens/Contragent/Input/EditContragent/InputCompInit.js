/* eslint-disable max-statements */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-props-no-spreading */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { data, document, input } from 'screens/Contragent/Input/config';
import { InputComponents } from 'utils/component';
import PropTypes from 'prop-types';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useContragentHooks } from 'hooks/useContragentHooks'
import { useFindContragent } from 'hooks/useFindContragent'
import { useAgentType } from 'hooks/useAgentType'

export const InputCompInit = ({ mode }) => {
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const [agentType] = useAgentType()
    const [state, contragentReFind, setState] = useContragentHooks();
    const [update] = useFindContragent()
    const dispatchRedux = useDispatch();
    const documentJoin = R.innerJoin(
        (record, id) => R.equals(R.path(['label'])(record), id),
        document,
    );
    const position = R.cond([
        [R.equals("LEGAL_ENTITY"), R.always(9)],
        [R.equals('INDIVIDUAL'), R.always(11)],
        [R.equals('SOLE_TRADER'), R.always(11)],
        [R.T, R.always(9)],
    ])(R.path(['type'])(agentType));

    const InlineInput = R.cond([
        [R.equals('LEGAL_ENTITY'), R.always(['psrn', 'kpp', 'inn'])],
        [R.equals('INDIVIDUAL'), R.always(['inn', 'personalData'])],
        [R.equals('SOLE_TRADER'), R.always(['psrnsp', 'kpp', 'certificateNumber', 'certificateDate', 'inn'])],
        [R.T, R.always(['certificateNumber', 'certificateDate', 'psrn', 'kpp', 'personalData'])],
        [R.F, R.always(['certificateNumber', 'certificateDate', 'psrn', 'kpp', 'personalData', 'inn'])],
    ]);

    const list = mode ? R.union(data, documentJoin(InlineInput(R.path(['type'])(agentType)))) : input;

    const memoizedCallback = React.useCallback(
        (element, change) => {
            if (R.pipe(R.path(['id']), R.not)(agent)) {
                if (R.equals(element.label, 'innByAgent')) {
                    if (R.length(change) > position) {
                        contragentReFind('find', change)
                    }
                }
            }
        },
        [],
    );

    const success = (element, change) => {
        update(element, change)
        dispatchRedux({
            type: element.path,
            payload: R.assocPath(element.value, change)({})
        })
    };

    const inputStranger = React.useCallback((element) => R.assocPath(['maxLength'],
        R.ifElse(
            R.equals("LEGAL_ENTITY"),
            R.always(R.ifElse(R.equals(['inn']),
                R.always(10),
                R.always(R.path(['maxLength'])(element))
            )(R.path(['value'])(element))),
            R.always(R.path(['maxLength'])(element))
        )(R.path(['type'])(R.mergeAll([agent, agentType]))), element),
        [agent, agentType])

    return (
        <View>
            {R.addIndex(R.map)((element, keys) => (
                <InputComponents
                    key={R.join('input', ['added', keys])}
                    {...inputStranger(element)}
                    onChangeText={(change) => success(element, change)}
                    storage={R.mergeAll([agent, agentType])}
                />
            ))(list)}
        </View>
    )
};

InputCompInit.propTypes = {
    mode: PropTypes.string,
};

InputCompInit.defaultProps = {
    mode: false,
};