/* eslint-disable no-negated-condition */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable padded-blocks */
/* eslint-disable arrow-body-style */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { MarkerUpdate } from 'screens/Contragent/Input/AddressType/MarkerUpdate';
import { dropdown } from 'screens/Contragent/Input/config';
import { InputCompInit } from 'screens/Contragent/Input/EditContragent/InputCompInit';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import PropTypes from 'prop-types';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useAgentType } from 'hooks/useAgentType'
import { BankCard } from '../BankCard';

export const InputContragent = ({
    storage, dispatch
}) => {
    const [agentType] = useAgentType()
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();

    const placeHolder = [
        { label: null, value: null },
        { label: i18n.t("INDIVIDUALS"), value: "INDIVIDUAL" },
        { label: i18n.t("LEGAL_ENTITIES"), value: "LEGAL_ENTITY" },
        { label: i18n.t("SOLE_TRADERS"), value: "SOLE_TRADER" },
    ];

    return (
        <View style={{ padding: 5 }}>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="CLabel"
                style={{
                    textAlign: 'left',
                    textTransform: 'uppercase'
                }}
            >
                {i18n.t('totalAgent')}
            </LabelText>
            <UiPicker
                disabled={R.pipe(R.path(['id']), R.not)(agent)}
                dispatch={(e) => {
                    dispatchRedux({
                        type: dropdown.path,
                        payload: R.assocPath(dropdown.value, e, {})
                    })
                }}
                element={{ path: dropdown.label }}
                Items={placeHolder}
                main
                placeholder={{
                    id: R.defaultTo(null)(R.path(['type'])(agentType))
                }}
            />
            <InputCompInit />
            <InputCompInit mode />
            <MarkerUpdate />
            <BankCard
                dispatch={dispatch}
                storage={storage}
            />
        </View>
    )
};

InputContragent.propTypes = {
    dispatch: PropTypes.func,
    storage: PropTypes.object,
};

InputContragent.defaultProps = {
    dispatch: () => null,
    storage: { title: null },
};