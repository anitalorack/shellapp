/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { AddressDesription } from 'screens/Contragent/Input/config';
import { CopyAddress } from 'screens/Contragent/Input/AddressType/CopyAddress';
import { ModalSetting } from 'screens/Contragent/Input/AddressType/ModalSetting';
import PropTypes from 'prop-types';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

export const AddressType = ({ mode }) => {
    const [state, setState] = useState(R.assocPath(['isVisible'], false, {}));
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();

    React.useEffect(() => {
        setState(R.mergeAll([state, R.zipObj([mode], [R.path([mode])(agent)])]))
    }, [mode]);

    const placeholder = R.pipe(
        R.path([mode]),
        R.paths([['postcode'], ['state'], ['city'], ['street'], ['building']]),
        R.uniq,
        R.reject(isOdd),
        R.join(', ')
    )(agent);

    return (
        <AddressDesription
            style={stylesType.items}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SSDescription"
                style={{ padding: 15, textAlign: 'left' }}
            >
                {i18n.t(mode)}
            </LabelText>
            <CopyAddress
                dispatch={(e) => dispatchRedux({ type: "AGENT", payload: R.mergeAll([agent, e]) })}
                mode={mode}
                storage={R.mergeAll([{ legalAddress: R.path(['legalAddress'])(agent) }, { address: R.path(['address'])(agent) }])}
            />
            <View style={styles.line} />
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SSDescription"
                style={stylesType.label}
            >
                {R.defaultTo(i18n.t('Adress_not_found'))(placeholder)}
            </LabelText>
            <TouchesState
                color="Yellow"
                flex={0}
                onPress={() => setState(R.assocPath(['isVisible'], true, R.zipObj([mode], [R.path([mode])(agent)])))}
                title="EDIT"
            />
            <ModalSetting
                mode={mode}
                setState={setState}
                state={state}
                unit
            />
        </AddressDesription>
    )
};

AddressType.propTypes = {
    dispatch: PropTypes.func,
    mode: PropTypes.string,
    storage: PropTypes.object,
};

AddressType.defaultProps = {
    dispatch: () => null,
    mode: null,
    storage: { title: null },
};

export const stylesType = StyleSheet.create({
    items: {
        flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    label: {
        padding: 10,
        paddingTop: 20,
        paddingBottom: 20,
        textAlign: 'left',
    }
});