/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { AddressDesription } from 'screens/Contragent/Input/config';
import { CopyAddress } from 'screens/Contragent/Input/AddressType/CopyAddress';
import { ModalSetting } from 'screens/Contragent/Input/AddressType/ModalSetting';
import PropTypes from 'prop-types';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useAgentType } from 'hooks/useAgentType'
// const [agentType] = useAgentType()

export const MarkerUpdate = ({ dispatch }) => {
    const [state, setState] = useState(R.assocPath(['isVisible'], false, {}));
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const [agentType] = useAgentType()
    const dispatchRedux = useDispatch();

    const addressVacation = R.cond([
        [R.equals('INDIVIDUAL'), R.always(['registrationAddress', 'physicalAddress'])],
        [R.T, R.always(['legalAddress', 'physicalAddress'])],
    ])(R.path(['type'])(agentType));

    const placeholder = (x) => R.pipe(
        R.path([x]),
        R.paths([['postcode'], ['state'], ['city'], ['street'], ['building']]),
        R.uniq,
        R.reject(isOdd),
        R.join(', ')
    )(agent);

    const success = (x) => setState(R.assocPath(['isVisible'], true, { mode: x }));

    return (
        <View>
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={key}
                    style={{ flex: 1 }}
                >
                    <AddressDesription
                        style={[styles.shadow, { flex: 1 }]}
                    >
                        <View style={{ flex: 1 }}>
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SSDescription"
                                style={{ padding: 15, textAlign: 'left' }}
                            >
                                {i18n.t(x)}
                            </LabelText>
                            <CopyAddress
                                dispatch={(e) => dispatchRedux({ type: "AGENT", payload: e })}
                                mode={x}
                                storage={agent}
                            />
                            <View style={styles.line} />
                            <LabelText
                                color={ColorCard(isOdd(placeholder(x)) ? 'error' : 'classic').font}
                                fonts="ShellBold"
                                items="SSDescription"
                                style={stylesType.label}
                            >
                                {isOdd(placeholder(x)) ? i18n.t('Adress_not_found') : placeholder(x)}
                            </LabelText>
                        </View>
                        <TouchesState
                            color="Yellow"
                            flex={0}
                            onPress={() => success(x)}
                            title={R.path(['id'])(agent) ? "EDIT" : "INDATAADRESS"}
                        />
                    </AddressDesription >
                </View>
            ))(addressVacation)}
            <ModalSetting
                dispatch={dispatch}
                mode={R.path(['mode'])(state)}
                setState={setState}
                state={state}
                unit={false}
            />
        </View>
    )
};

MarkerUpdate.propTypes = {
    dispatch: PropTypes.func,
    storage: PropTypes.object,
};

MarkerUpdate.defaultProps = {
    dispatch: () => null,
    storage: { title: null },
};

export const stylesType = StyleSheet.create({
    items: {
        flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    label: {
        padding: 10,
        paddingTop: 20,
        paddingBottom: 20,
        textAlign: 'left',
    }
});