/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { addressTypeLogic } from 'screens/Contragent/Input/config';

export const CopyAddress = ({ mode, storage, dispatch }) => {
    const variantCopy = addressTypeLogic(R.path(['type'])(storage));

    if (R.includes(mode)(variantCopy.address)) {
        return (
            <TouchableOpacity
                onPress={() => dispatch(R.zipObj([mode], [R.path([variantCopy.copy])(storage)]))}
                style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                }}
            >
                <LabelText
                    color={ColorCard('blue').font}
                    fonts="ShellMedium"
                    items="SDescription"
                >
                    {i18n.t('copy')}{' '}
                    <LabelText
                        color={ColorCard('blue').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={{ textTransform: 'lowercase' }}
                    >
                        {i18n.t(variantCopy.copy)}
                    </LabelText>
                </LabelText>
            </TouchableOpacity>
        )
    }

    return (
        <View />
    )
};
