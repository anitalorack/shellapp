/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import Modal from 'react-native-modal';
import { adress, checkedRow, ModalDescription } from 'screens/Contragent/Input/config';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useContragentHooks } from 'hooks/useContragentHooks';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { InputComponents } from 'utils/component';

export const ModalSetting = ({
    state, setState, mode, unit
}) => {
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();
    const [modalInput, request, setModalInput] = useContragentHooks();

    React.useEffect(() => {
        if (R.equals(unit, true)) {
            setModalInput(R.path([mode])(state))
        } else {
            setModalInput(R.path([mode])(agent))
        }
    }, [unit, state, mode]);

    const success = (func) => {
        if (R.equals(func, 'SAVE')) {
            dispatchRedux({
                type: 'AGENT',
                payload: R.zipObj([mode], [R.omit(['__typename', 'isVisible'])(modalInput)])
            });
        }
        if (R.equals(func, 'RESET')) {
            dispatchRedux({
                type: 'AGENT',
                payload: R.zipObj([mode], [{}])
            });
        }
        setModalInput({});
        setState(R.assocPath(['isVisible'], false, { state }))
    };

    const LineContragent = R.addIndex(R.map)((element, keys) => (
        <View
            key={keys}
            style={{ flex: 1 }}
        >
            <InputComponents
                {...element}
                onChangeText={(change) => setModalInput(
                    R.assocPath(element.value, change, modalInput)
                )}
                storage={modalInput}
            />
        </View>
    ))(adress);

    return (
        <Modal isVisible={R.path(['isVisible'])(state)}>
            <ModalDescription style={{ flex: 1, margin: 10 }}>
                <HeaderApp
                    button="DESTROY"
                    dispatch={() => {
                        dispatchRedux({
                            type: "AGENT",
                            payload: R.mergeAll([
                                agent,
                                R.zipObj([mode], [{}])
                            ])
                        });
                        setState({ isVisible: false })
                    }}
                    empty
                    title={i18n.t(mode)}
                />
                <ScrollView style={{ padding: 10 }}>
                    {LineContragent}
                </ScrollView>
                <RowComponents>
                    {R.addIndex(R.map)((func, key) => (
                        <View
                            key={key}
                            style={{ flex: 1, marginBottom: 10 }}
                        >
                            <TouchesState
                                color={R.equals("CLOSED", func) ? "VeryRed" : "Yellow"}
                                disabled={R.equals('SAVE', func) ? checkedRow(modalInput) : false}
                                flex={1}
                                onPress={() => success(func)}
                                title={func}
                            />
                        </View>
                    ))(["SAVE", "CLOSED"])}
                </RowComponents>
            </ModalDescription>
        </Modal>
    )
};