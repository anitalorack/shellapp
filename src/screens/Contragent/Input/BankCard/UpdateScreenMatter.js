/* eslint-disable newline-after-var */
/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-confusing-arrow */
/* eslint-disable no-negated-condition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-else-return */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { isMode } from 'screens/Contragent/Input/config';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { isOdd } from 'utils/helper';

const checkBtn = (state) => R.pipe(R.uniq, R.includes(false))([
    R.pipe(R.length, isMode)(R.defaultTo(R.path(['bankName'])(state))(R.path(['name'])(state))),
    R.pipe(R.length, R.equals(20))(R.defaultTo(R.path(['checkingAccount'])(state))(R.path(['currentAccount'])(state))),
    R.pipe(R.length, R.equals(20))(R.path(['correspondentAccount'])(state)),
    R.pipe(R.length, R.equals(9))(R.path(['rcbic'])(state))
]);

export const UpdateScreenMatter = ({ storage, onPress, data }) => {
    const list = isOdd(R.path(['original'])(storage)) ? ["ADDEDS", "CLOSED"] : ["SAVE", 'CLOSED'];

    return (
        <RowComponents >
            {R.addIndex(R.map)((func, key) => (
                <View
                    key={key}
                    style={{ flex: 1, marginBottom: 10 }}
                >
                    <TouchesState
                        color={R.equals("CLOSED", func) ? "VeryRed" : "Yellow"}
                        disabled={R.equals(func, 'CLOSED') ? false : checkBtn(R.path(['detail'])(data))}
                        flex={1}
                        onPress={() => onPress(func)}
                        title={func}
                    />
                </View>
            ))(list)}
        </RowComponents>
    )
};