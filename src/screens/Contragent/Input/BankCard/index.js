/* eslint-disable max-len */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { initialBank } from 'screens/Contragent/Input/config';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import styled from 'styled-components';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { ModalSetting } from './ModalSetting';

export const BankCard = ({ storage, unit }) => {
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const [state, setState] = React.useState({ isVisible: false });
    const diffName = ['bankName', 'checkingAccount', 'correspondentAccount', 'rcbic'];
    const contrName = ["name", "currentAccount", "correspondentAccount", "rcbic"];

    const Table = (card) => R.addIndex(R.map)((elem, key) => (
        <RowComponentsBank
            key={key}
            style={{ flex: 1 }}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SDescription"
                style={styleTable.items}
            >
                {i18n.t(elem)}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={styleTable.item}
            >
                {R.path([elem])(card)}
            </LabelText>
        </RowComponentsBank>
    ))(unit ? diffName : contrName);

    const TableAction = () => R.addIndex(R.map)((card, keys) => (
        <TouchableOpacity
            key={keys}
            onPress={() => setState(R.assocPath(['detail'], card, { isVisible: true, original: card }))}
        >
            <CardComponents
                key={keys}
                style={[styles.shadow,
                {
                    flex: 1,
                    margin: 1
                },
                R.equals(R.head(initialBank), card) ? styleTable.lister : {}]}
            >
                {R.equals(R.head(initialBank), card) ?
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                        style={{ textAlign: 'center' }}
                    >
                        {i18n.t('AddedBankTax')}
                    </LabelText>
                    : Table(card)}
            </CardComponents>
        </TouchableOpacity>
    ))(unit ?
        [R.pipe(
            R.paths([['bankName'], ['checkingAccount'], ['correspondentAccount'], ['rcbic']]),
            R.zipObj(["bankName", "checkingAccount", "correspondentAccount", "rcbic"])
        )(storage)] :
        R.prepend(R.head(initialBank), R.path(['bankDetails'])(agent)));

    return (
        <View style={{ flex: 1 }}>
            <View
                style={{
                    paddingTop: 15,
                    paddingBottom: 15
                }}
            >
                <LabelText
                    color={ColorCard('error').font}
                    fonts="ShellMedium"
                    items="CLabel"
                >
                    {i18n.t('BANK')}
                </LabelText>
            </View>
            {TableAction()}
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ModalSetting
                    setState={setState}
                    storage={state}
                    unit={unit}
                />
            </Modal >
        </View>
    )
};

const RowComponentsBank = styled(RowComponents)`
padding-top: 5px;
padding-bottom: 5px;
justify-content: space-between;
`;

const styleTable = StyleSheet.create({
    items: {
        textTransform: 'uppercase',
        alignItems: 'center',
        textAlign: 'left',
        flex: 1
    },
    item: {
        textAlign: 'right',
        flex: 1
    },
    itemses: {
        textAlign: 'left',
        flex: 1,
        paddingTop: 15,
        paddingBottom: 15,
        textTransform: 'uppercase',
    },
    lister: {
        marginBottom: 5,
        height: 45,
        backgroundColor: uiColor.Yellow,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
