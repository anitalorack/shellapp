/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { useBankHooks } from 'hooks/useBankHooks'
import { initialBank } from '../config';
import { SwitchDesc } from './SwitchDesc';
import { UpdateScreenMatter } from './UpdateScreenMatter';

export const ModalSetting = ({
    storage, unit, setState
}) => {
    const [data, setData, updateQuery, alertReq] = useBankHooks({
        storage,
        setState
    });
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();
    const title = "destroy_bankDetail";

    React.useEffect(() => {
        setData(storage)
    }, [storage]);

    const success = (e) => {
        if (R.equals(e, 'CLOSED')) return setState({ isVisible: false });
        if (unit) {
            setState({ isVisible: false });

            return dispatchRedux({
                type: 'AGENT',
                payload: R.mergeAll([agent, R.path(['detail'])(data)])
            })
        }
        if (R.path(['id'])(agent)) {
            if (R.path(['original', 'id'])(storage)) return updateQuery('update');

            return updateQuery('create')
        }
        if (R.equals(R.head(initialBank))(R.path(['original'])(storage))) return updateQuery('createRedux');

        return updateQuery('updateRedux')
    };

    return (
        <ModalDescription style={{ margin: 10, flex: 1 }}>
            <HeaderApp
                button="DESTROY"
                dispatch={() => alertDestroyConfirm({
                    alertReq,
                    title
                })}
                empty={R.pipe(R.path(['original']),
                    R.equals(R.head(initialBank)))(storage)}
                header
                title={i18n.t("BANK")}
            />
            <ScrollView
                contentContainerStyle={{ flexGrow: 1 }}
                style={{
                    margin: 0,
                    padding: 10
                }}
            >
                <SwitchDesc
                    data={data}
                    setData={setData}
                    unit={unit}
                />
            </ScrollView>
            <UpdateScreenMatter
                data={data}
                onPress={success}
                setState={setState}
                storage={storage}
            />
        </ModalDescription>
    )
};

ModalSetting.propTypes = {
    storage: PropTypes.object,
    unit: PropTypes.bool,
};

ModalSetting.defaultProps = {
    storage: {},
    unit: false
};
