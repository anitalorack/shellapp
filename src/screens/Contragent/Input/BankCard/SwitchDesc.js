/* eslint-disable newline-after-var */
/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-confusing-arrow */
/* eslint-disable no-negated-condition */
/* eslint-disable no-unused-vars */
/* eslint-disable no-else-return */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { Switch } from 'react-native-paper';
import { bank, BankInput, SwitchDescription } from 'screens/Contragent/Input/config';
import { LabelText } from 'uiComponents/SplashComponents';
import { InputComponents } from 'utils/component';
import { ColorCard } from 'utils/helper';

export const SwitchDesc = ({ unit, setData, data }) => {
    const Change = (mode, element, change) => R.cond([
        [R.equals('Switch'), () => setData(R.mergeAll([data, {
            detail: R.mergeAll([
                R.path(['detail'])(data),
                R.assocPath(['mainBankDetail'],
                    R.pipe(R.path(['detail', 'mainBankDetail']),
                        R.not)(data), {})
            ])
        }]))],
        [R.equals('Input'), () => setData(R.mergeAll([data, {
            detail: R.assocPath(element.value, change, R.path(['detail'])(data))
        }]))]
    ])(mode);

    const InputComp = R.addIndex(R.map)((element, keys) => (
        <View
            key={keys}
            style={{ flex: 1 }}
        >
            <InputComponents
                {...element}
                onChangeText={(change) => Change('Input', element, change)}
                storage={R.path(['detail'])(data)}
            />
        </View>
    ));

    if (unit) return InputComp(BankInput);

    return (
        <View>
            {InputComp(bank)}
            <SwitchDescription
                style={{ justifyContent: 'space-between' }}
            >
                <Switch
                    onValueChange={() => Change("Switch")}
                    value={R.path(['detail', 'mainBankDetail'])(data)}
                />
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={{ padding: 15, textAlign: 'left' }}
                >
                    {i18n.t('DEFAULT')}
                </LabelText>
            </SwitchDescription>
        </View>
    )
};
