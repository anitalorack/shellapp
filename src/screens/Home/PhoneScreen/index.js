/* eslint-disable no-unneeded-ternary */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-useless-escape */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable jsx-quotes */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable max-statements */
import * as R from 'ramda';
import React, { useState } from 'react';
import { View } from 'react-native';
import { connect, useDispatch } from 'react-redux';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import HeaderComponentsProps from 'uiComponents/HeaderComponents';
import NameSpaceAppLoading from 'uiComponents/NameSpaceAppLoading';
import { SplashContainer } from 'uiComponents/SplashComponents';
import { CreateSmsCode } from 'utils/api';
import { InputComponents } from 'utils/component';
import { RegisterScreen } from 'utils/routing/index.json';
import { navBack } from '../utils';

const isOdd = (x) => R.anyPass([R.equals("-"), R.equals(")"), R.equals("(")])(x);
const phoneInput = {
    id: "ИМЯ",
    path: 'phone',
    main: true, // mainInput
    label: 'phone', // valid for Some product 
    Lenght: 10, // valid for lenght
    keyboardType: "numeric", // valid for symbol
    value: ["phone"],
    example: "+7 (___) ___-__-__",
    mask: "+7 ([000]) [000]-[00]-[00]",
    error: "Error message", // message for title
    errorMessage: 'PhoneError', // message for push
};

const SplashScreen = (props) => {
    const { navigation } = props;
    const [phone, setPhone] = useState({ error: true });
    const dispatch = useDispatch();
    const success = async () => {
        const status = await CreateSmsCode(R.join('', ["+7", R.path(['regex'])(phone)]));

        if (status) {
            const answer = {
                regex: `+7${R.path(['regex'])(phone)}`,
                full: `+7-${R.path(['phone'])(phone)}`,
            };

            dispatch({
                type: 'PROFILE_MERGE',
                payload: {
                    phone: answer
                }
            });

            return navigation.navigate(RegisterScreen.Sms)
        }

    };

    return (
        <SplashContainer>
            <HeaderComponentsProps
                mode="Home"
                onPress={(click) => navBack(click, navigation, dispatch)}
                style={{ backgroundColor: "blue" }}
            />
            <View
                style={{
                    flex: 1,
                    marginTop: 0,
                    margin: 10,
                    justifyContent: 'center',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    elevation: 2,
                    borderWidth: 0
                }}
            >
                <View style={{ flex: 1 }}>
                    <NameSpaceAppLoading absolute />
                </View>
                <View
                    style={{
                        padding: 15,
                    }}
                >
                    <View
                        style={{
                            paddingLeft: 10,
                            paddingRight: 10,
                        }}
                    >
                        <InputComponents
                            {...phoneInput}
                            onChangeText={(change, error) => { // added error input
                                return setPhone({
                                    error,  // added error in reducer
                                    phone: change,
                                    regex: R.pipe(
                                        R.splitEvery(1),
                                        R.reject(isOdd),
                                        R.join('')
                                    )(change)
                                })
                            }}
                            storage={phone}
                        />
                    </View>
                    <TouchesState
                        color="Yellow"
                        disabled={!R.isNil(R.path(['error'])(phone))} //added disabled for input button
                        flex={0}
                        font="ShellBold"
                        onPress={() => success({ data: phone })}
                        title="LOGIN"
                        unTransform
                    />
                </View>
            </View>
        </SplashContainer>)
};

const mapStateToProp = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProp)(SplashScreen);
