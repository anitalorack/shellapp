const navBack = (click, navigation, dispatch) => {
    if (click === 'goBack') {
        try {
            return navigation.goBack()
        } catch (error) {
            return null
        }
    }

    return dispatch({
        type: 'PROFILE_MERGE',
        payload: { languages: 'RU' }
    })
};

export { navBack }