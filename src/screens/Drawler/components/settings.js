/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { MainScreen, Preview } from 'utils/routing/index.json';
import APKUPDATE from 'utils/support/UpdateApk'

const ROUTER = R.cond([
    [R.equals('AutoService'), R.always(MainScreen.AutoService)],
    [R.equals('masters'), R.always(MainScreen.Master)],
    [R.equals("PostRemont"), R.always(Preview.PreviewPostGarage)],
    [R.equals("Tariff"), R.always(MainScreen.Pay)],
]);
const settingItemsRoute = [
    "AutoService",
    "masters",
    "PostRemont",
    "Tariff"
];

export const SettingsApp = ({ navigation }) => (
    <View style={styles.column}>
        <LabelText
            color={ColorCard('error').font}
            fonts="ShellBold"
            items="SSDescription"
            style={styles.container}
            testID="BlockLaker"
        >
            {i18n.t('SETTINGS')}
        </LabelText>
        <View
            style={{ flex: 1, justifyContent: 'space-between' }}
        >
            <View>
                {R.map((x) => (
                    <TouchableOpacity
                        key={x}
                        onPress={() => navigation.navigate(ROUTER(x))}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SRDescription"
                            style={styles.items}
                            testID="BlockLaker"
                        >
                            {i18n.t(x)}
                        </LabelText>
                    </TouchableOpacity>))(settingItemsRoute)}
            </View>
            <APKUPDATE />
        </View>
    </View>
);