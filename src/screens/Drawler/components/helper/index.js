/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import { useSkladLoading } from 'hooks/useSkladLoading';
import i18n from 'localization';
import * as R from 'ramda';
import React, { useState } from 'react';
import { View } from 'react-native';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { InputComponents } from 'utils/component';
import { promoInput } from './config';

export const ModalConnection = ({
    setModal, modal, navigation
}) => {
    const [depots, update, createDepots] = useSkladLoading({ navigation })
    const [state, setState] = useState({});
    const colorButton = (x) => R.equals(x, 'CLOSED') ? "VeryRed" : "Yellow";
    const success = (x) => {
        if (R.equals(x, 'CLOSED')) {
            return setModal(R.assocPath(['isVisible'], false, modal))
        }
        if (R.equals(x, 'CREATE')) {
            createDepots({ variables: { input: { ...state } } });

            return setModal(R.assocPath(['isVisible'], false, modal))
        }
    };

    const ModalPromo = () => R.addIndex(R.map)((x, key) => (
        <InputComponents
            key={R.join('_', ['PromoInput', key])}
            {...x}
            onChangeText={(change) => setState(R.assocPath([x.path], change, state))}
            storage={state}
        />
    ))(promoInput);

    return (
        <ModalDescription
            style={{
                flex: 1,
                justifyContent: 'space-between'
            }}
        >
            <View
                style={{
                    flex: 1,
                    padding: 10,
                }}
            >
                <HeaderApp
                    empty
                    header
                    title={i18n.t("NEWGARAGE")}
                />
                {ModalPromo()}
            </View>
            <RowComponents>
                {R.addIndex(R.map)((x, key) => (
                    <TouchesState
                        key={R.join('_', ['button', key])}
                        color={colorButton(x)}
                        flex={1}
                        onPress={() => success(x)}
                        title={x}
                    />
                ))(['CREATE', 'CLOSED'])}
            </RowComponents>
        </ModalDescription>
    )
};