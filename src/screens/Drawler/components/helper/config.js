/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */

export const promoInput = [
    {
        id: "ИМЯ",
        path: 'name',
        main: true,
        label: 'naiming',
        Lenght: 120,
        keyboardType: "default",
        maxLength: 120,
        value: ["name"],
        example: "",
        error: "Error message",
        errorMessage: 'PhoneError',
    },
    {
        id: "ИМЯ",
        path: 'description',
        main: true,
        label: 'shortByAgent',
        Lenght: 250,
        keyboardType: "default",
        maxLength: 250,
        multiline: true,
        value: ["description"],
        example: "",
        error: "Error message",
        errorMessage: 'PhoneError',
    }
];