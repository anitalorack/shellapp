/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import ADD from 'assets/svg/addTwo';
import { SvgXml } from 'react-native-svg';
import i18n from 'localization'
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { MainScreen } from 'utils/routing/index.json'
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { useDispatch } from 'react-redux';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';


export const ClientsApp = ({ navigation }) => {
    const ROUTERCLIENTS = MainScreen.Client;
    const ROUTERCARS = MainScreen.Cars;
    const dispatchRedux = useDispatch()

    const ListAgent = R.map((x) => (
        <View
            key={x}
        >
            <TouchableOpacity
                onPress={() => navigation.navigate(MainScreen.Contragent, { list: x })}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SRDescription"
                    style={styles.items}
                >
                    {i18n.t(x)}
                </LabelText>
            </TouchableOpacity>
        </View>
    ))(["COUNTREPARITY", 'LEGAL_ENTITY', "SOLE_TRADER", "INDIVIDUAL"])

    return (
        <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="SSDescription"
                style={styles.container}
            >
                {i18n.t('CLIENTS')}
            </LabelText>
            <View
                style={{ flex: 1 }}
            >
                {R.map((x) => (
                    <TouchableOpacity
                        key={x}
                        onPress={() => navigation.navigate(R.equals("CLIENTS", x) ? ROUTERCLIENTS : ROUTERCARS)}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SRDescription"
                            style={styles.items}
                        >
                            {i18n.t(x)}
                        </LabelText>
                    </TouchableOpacity>))(['CLIENTS', "ListCars"])}
            </View>
            <View>
                <LabelText
                    color={ColorCard('error').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={styles.container}
                >
                    {i18n.t('listAgent')}
                </LabelText>
                {ListAgent}
            </View>
            <View
                style={styles.line}
            />
            <View>
                <TouchableOpacity
                    onPress={() => {
                        dispatchRedux({ type: "AGENT_UPDATE", payload: {} })
                        navigation.navigate(MainScreen.EditContragent)
                    }}
                    style={[styles.rowCenter, { padding: 10 }]}
                >
                    <RowComponents style={{ justifyContent: 'flex-start', margin: 10, flex: 1, alignItems: 'center' }}>
                        <View style={{
                            marginTop: 5
                        }}>
                            <SvgXml
                                height="15"
                                width="15"
                                xml={ADD}
                            />
                        </View>
                        <Text
                            style={[styles.items, {
                                fontFamily: 'ShellMedium',
                                color: uiColor.Very_Dark_Grey,
                                paddingLeft: 3,
                                fontSize: 13
                            }]}
                        >
                            {i18n.t('CREATECONTRAGENT')}
                        </Text>
                    </RowComponents>
                </TouchableOpacity>
            </View>
        </View >
    )
};