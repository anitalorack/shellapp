/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import PROFILE from 'assets/svg/userPro';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { shallowEqual, useSelector } from 'react-redux';
import { useProfile } from 'hooks/useProfile'


export const PayAgent = () => {
    const [state, update] = useProfile()

    React.useEffect(() => {
        if (!state) {
            update()
        }
    }, [state])

    return (
        <View
            style={{ flex: 1 }}
        >
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="SSDescription"
                style={styles.container}
                testID="BlockLaker"
            >
                {i18n.t('PROFILE')}
            </LabelText>
            <View>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={[styles.items, { textAlign: 'center', paddingLeft: 0, paddingTop: 10 }]}
                    testID="BlockLaker"
                >
                    {R.join(' ', R.values(R.omit(['__typename'])(R.path(['garage', "name"])(state))))}
                </LabelText>
                <View style={styles.avatar}>
                    <SvgXml height="35" width="35" xml={PROFILE} />
                </View>
                <View>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellBold"
                        items="SDescription"
                        style={[styles.items, { textAlign: 'center', paddingLeft: 0, padding: 3 }]}
                        testID="BlockLaker"
                    >
                        {R.join(' ', R.values(R.omit(['__typename'])(R.path(['name'])(state))))}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={[styles.items, { textAlign: 'center', paddingLeft: 0, padding: 3 }]}
                        testID="BlockLaker"
                    >
                        {i18n.t(R.path(['role'])(state))}
                    </LabelText>
                </View>
            </View>
        </View>
    )
};