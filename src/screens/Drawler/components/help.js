/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

const itemsHelp = ["HelpDesk", 'Tech'];

export const HelpApp = () => (
    <View style={{ flex: 1 }}>
        <View>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="SSDescription"
                style={styles.container}
                testID="BlockLaker"
            >
                {i18n.t('HELP')}
            </LabelText>
            <View>
                {R.map((x) => (
                    <TouchableOpacity
                        key={x}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SRDescription"
                            style={styles.items}
                            testID="BlockLaker"
                        >
                            {i18n.t(x)}
                        </LabelText>
                    </TouchableOpacity>))(itemsHelp)}
            </View>
        </View>
    </View >
);