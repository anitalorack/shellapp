/* eslint-disable no-undefined */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import ADD from 'assets/svg/addTwo';
import i18n from 'localization'
import * as R from 'ramda';
import React from 'react';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchableOpacity, View, Text } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { CreateScreen, MainScreen, Preview } from 'utils/routing/index.json';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { useDispatch } from 'react-redux';
import { uiColor } from 'styled/colors/uiColor.json';


const listRemontItems = [
    "AllWork",
    "preorder",
    "inWork",
    "completed",
    "closed",
    "reconciliation",
    "suspended",
    "canceled",
];
const MainROUTER = MainScreen.Main;
const CreateRouter = Preview.PreviewTaskGarage;
// const PostRouter = "Postupload";

export const RemontApp = ({ navigation }) => {
    const dispatchRedux = useDispatch()

    return (
        <View style={{ flex: 1 }}>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="SSDescription"
                style={styles.container}
            >
                {i18n.t('TaskWorks')}
            </LabelText>
            <View
                style={{ flex: 1 }}
            >
                {R.map((x) => (
                    <TouchableOpacity
                        key={x}
                        onPress={() => navigation.navigate(
                            MainROUTER, { list: R.equals(x, 'AllWork') ? undefined : x }
                        )}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SRDescription"
                            style={styles.items}
                        >
                            {i18n.t(x)}
                        </LabelText>
                    </TouchableOpacity>))(listRemontItems)}
            </View>
            <View
                style={styles.line}
            />
            <View
                style={{ padding: 10, flex: 1 }}
            >
                <TouchableOpacity
                    onPress={() => {
                        dispatchRedux({ type: "TASK_UPDATE", payload: { vehicle: {}, owner: {} } })
                        navigation.navigate(CreateRouter)
                    }}
                    style={styles.rowCenter}
                >
                    <RowComponents style={{ justifyContent: 'flex-start', margin: 10, flex: 1, alignItems: 'center' }}>
                        <View style={{
                            marginTop: 5
                        }}>
                            <SvgXml
                                height="15"
                                width="15"
                                xml={ADD}
                            />
                        </View>
                        <Text
                            style={[styles.items, {
                                fontFamily: 'ShellMedium',
                                color: uiColor.Very_Dark_Grey,
                                paddingLeft: 3,
                                fontSize: 13
                            }]}
                        >
                            {i18n.t('NEW_ListWork')}
                        </Text>
                    </RowComponents>
                </TouchableOpacity>
            </View>
            <View
                style={{ flex: 1 }}
            />
            <View>
                <LabelText
                    color={ColorCard('error').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={styles.container}
                >
                    {i18n.t('Post')}
                </LabelText>
                {/* {R.map((x) => (
                <TouchableOpacity
                    key={x}
                    onPress={() => navigation.navigate(CreateScreen.CalendarInit)}
                >
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SRDescription"
                        style={styles.items}
                    >
                        {i18n.t(x)}
                    </LabelText>
                </TouchableOpacity>)
            )([PostRouter])} */}
            </View>
        </View >
    )
};
