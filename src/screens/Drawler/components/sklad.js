/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import ADD from 'assets/svg/addTwo';
import { useSkladLoading } from 'hooks/useSkladLoading';
import i18n from 'localization';
import * as R from 'ramda';
import React, { useState } from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    ScrollView,
    StyleSheet
} from 'react-native';
import Modal from 'react-native-modal';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Drawler/components/helper/styleSheets';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { MainScreen, Preview } from 'utils/routing/index.json';
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import { uiColor } from 'styled/colors/uiColor.json';
import { ModalConnection } from './helper';

const ROUTER = MainScreen.Garage;

export const SkladApp = ({ navigation, reload }) => {
    const ref = React.useRef()
    const [state, current, createDepots] = useSkladLoading({ navigation })
    const [modal, setModal] = useState({ isVisible: false });
    const { depots } = useSelector(item => item.depots, shallowEqual);
    const dispatchRedux = useDispatch()

    React.useEffect(() => {
        if (!R.equals(ref.current, modal)) {
            current()
            ref.current = modal
        }
    }, [modal, reload, state])

    const ListDepots = React.useCallback(() => {
        const success = (x) => {
            dispatchRedux({
                type: 'CURRENT_GARAGE',
                payload: x
            })
            navigation.navigate(ROUTER, { stock: x.id })
        }

        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                {R.map(x => (
                    <View
                        key={x.id}
                    >
                        <TouchableOpacity
                            onPress={() => success(x)}
                        >
                            <Text
                                style={[styles.items, stylex.zen]}
                            >
                                {x.name}

                            </Text>
                        </TouchableOpacity>
                    </View>
                ))(R.defaultTo([])(state))}
            </ScrollView>
        )
    }, [modal, reload, depots])

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                <View>
                    <Text
                        style={[styles.items, stylex.zen]}
                    >
                        {i18n.t('GARAGES')}
                    </Text>
                    <View>
                        {ListDepots()}
                        <View style={styles.line} />
                        <TouchableOpacity
                            onPress={() => {
                                setModal(R.assocPath(['isVisible'], true, modal))
                            }}
                            style={{ flex: 1 }}
                        >
                            <RowComponents
                                style={{ justifyContent: 'flex-start', margin: 10, flex: 1, alignItems: 'center' }}
                            >
                                <View
                                    style={{
                                        marginTop: 5
                                    }}
                                >
                                    <SvgXml
                                        height="15"
                                        width="15"
                                        xml={ADD}
                                    />
                                </View>
                                <Text
                                    style={[styles.items, stylex.zen]}
                                >
                                    {i18n.t('CREATEGARAGE')}
                                </Text>
                            </RowComponents>
                        </TouchableOpacity>
                    </View>
                </View>
                <View >
                    <Text
                        style={[styles.items, stylex.zen]}
                    >
                        {i18n.t('GARAGEOPERATION')}
                    </Text>
                    <TouchableOpacity onPress={() => navigation.navigate(Preview.PreviewGarage)}>
                        <Text
                            style={[styles.items, stylex.zen]}
                        >
                            {i18n.t('GOODADDGARAGE')}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View >
                    <Text
                        style={[styles.items, stylex.zen]}
                    >
                        {i18n.t('ANOTHER')}
                    </Text>
                    <TouchableOpacity onPress={() => navigation.navigate(MainScreen.FindParts)} >
                        <Text
                            style={[styles.items, stylex.zen]}
                        >
                            {i18n.t('SEARCHGOODS')}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
            <Modal isVisible={modal.isVisible}>
                <ModalConnection
                    modal={modal}
                    navigation={navigation}
                    setModal={(e) => setModal(e)}
                />
            </Modal>
        </View>
    )
};

const stylex = StyleSheet.create({
    zen: {
        fontFamily: 'ShellMedium',
        color: uiColor.Very_Dark_Grey,
        paddingLeft: 15,
        fontSize: 13
    }
})