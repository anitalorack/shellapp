/* eslint-disable newline-after-var */
/* eslint-disable no-confusing-arrow */
/* eslint-disable dot-notation */
/* eslint-disable no-extra-parens */
import { SvgXml } from 'react-native-svg';
import styled from 'styled-components/native';

const TouchDrawler = styled.TouchableOpacity`
display:flex;
flex-direction: row;
`;

const LogoBlock = styled(SvgXml)`
padding: 15px;
margin: 15px;
`;

export const FlatView = styled.View`
align-items: flex-start;
padding: 15px;
flex-direction: column;
justify-content: space-around;
`;


const DrawlerBuck = styled.View`
top: 0;
background: ${props => props.items ? props.theme.color.uiColor.Very_Pole_Grey : 'transparent'};
`;

const DrawlerView = styled.TouchableOpacity`
justify-content:center;
text-align:center;
height: 54px;
background: ${props => (props.theme.drawler[props.items]["background"])};
`;

const DrawlerText = styled.Text`
font-family:${props => props.fonts ? props.fonts : "ShellMedium"};
text-align: ${props => props.theme.drawler[props.items]["align"]};
justify-content: ${props => props.theme.drawler[props.items]["js"]};
align-items: ${props => props.theme.drawler[props.items]["js"]};
padding-left: ${props => props.theme.drawler[props.items]["left"]};
`;

export {
    LogoBlock, DrawlerBuck, DrawlerView, DrawlerText, TouchDrawler
};