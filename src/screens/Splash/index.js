/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import NameSpaceAppLoading from 'uiComponents/NameSpaceAppLoading';
import { LabelText, SplashContainer } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

const PreLoaderScreen = () => (
    <SplashContainer>
        <View style={{ flex: 1 }}>
            <NameSpaceAppLoading asolute={false} />
        </View>
    </SplashContainer>);

const ErrorScreen = ({ msg }) => (
    <SplashContainer color="error">
        <View style={{ flex: 1 }}>
            <NameSpaceAppLoading asolute={false} />
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SSDescription"
                style={{
                    textAlign: 'center',
                    padding: 6,
                    textTransform: 'uppercase',
                    flex: 1
                }}
                testID="BlockLaker"
            >
                {msg}
            </LabelText>
        </View>
    </SplashContainer>
);

const LoadingScreen = () => (
    <SplashContainer color="loading">
        <View style={{ flex: 1 }}>
            <NameSpaceAppLoading asolute={false} />
        </View>
    </SplashContainer >
);

export const SplashScreen = ({ state, msg }) => (
    <View
        style={{ flex: 1 }}
    >
        {R.cond([
            [R.equals('First'), R.always(<PreLoaderScreen />)],
            [R.equals('Loading'), R.always(<LoadingScreen />)],
            [R.equals('Error'), R.always(<ErrorScreen msg={msg} />)],
        ])(state)}
    </View>
);
