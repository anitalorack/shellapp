import styled from 'styled-components/native'

const LoadingText = styled.Text`
font-size:20px;
font-weight:bold;
text-align:center;
padding:5px;
`;

export { LoadingText }