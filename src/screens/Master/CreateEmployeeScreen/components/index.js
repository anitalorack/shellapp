/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-boolean-value */
/* eslint-disable newline-after-var */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-vars */
/* eslint-disable max-statements */
import { useMutation } from '@apollo/react-hooks';
import { useNavigation } from 'hooks/useNavigation';
import { useMutateEmployee } from 'hooks/useEmployee'
import * as R from 'ramda';
import React, { useReducer } from 'react';
import { showMessage } from "react-native-flash-message";
import { Header } from 'screens/TaskEditor/components/header';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { MainScreen } from 'utils/routing/index.json';
import { PolicyAdd } from 'utils/support/policyAdd';
import { shallowEqual, useDispatch, useSelector, } from 'react-redux';
import { initialState, reducer } from '../reducer';
import { ScrolerView } from '../style';
import { initialStage } from './config';
import { InputComponentState } from './InputComponent';

const CreateEmployeeScreen = (props) => {
    const {
        navigation, data, mode, user, garageId, route
    } = props;
    const { employees } = useSelector(state => state.employees, shallowEqual);
    const [current, setMoved] = useNavigation({ navigation, move: 'goBack' });
    const [create, update, destroy, mergeKeys] = useMutateEmployee({ navigation })

    const [storage, dispatch] = useReducer(reducer, R.isNil(mode) ? R.assocPath(['garageId'], garageId, initialState) : initialStage(data, user));
    const [checkbox, setCheckBox] = React.useState(false);
    const ROUTER = MainScreen.Master;

    const title = "destroy_employee";
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => destroy()],
    ])(R.path(['type'])(e));

    const success = ({ action }) => {
        if (R.equals("ADDED")(action)) {
            return create()
        }
        if (R.equals("SAVE")(action)) {
            return update()
        }
        if (R.equals("DESTROY")(action)) {
            return alertDestroyConfirm({ alertReq, title })
        }
    };

    const checkDisabled = () => {
        if (R.path(['id'])(employees)) {
            return R.pipe(R.values, R.uniq, R.length)(R.path(['error'])(employees)) > 1
        }
        if (R.equals(true, checkbox)) {
            return R.pipe(R.values, R.uniq, R.length)(R.path(['error'])(employees)) > 1
        }

        return true
    };

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: R.path(['id'])(employees) ? "edit_masters" : "new_masters" }}
            svg={current}
        >
            <ScrolerView
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between', padding: 5 }}
            >
                <InputComponentState
                    dispatch={mergeKeys}
                    storage={employees}
                />
                <PolicyAdd
                    checkbox={checkbox}
                    id={R.defaultTo(R.path(['id'])(employees))(R.path(['id', 'id'])(employees))}
                    setCheckBox={(e) => setCheckBox(e)}
                />
                <RowComponents>
                    {R.addIndex(R.map)((x, key) => (
                        <TouchesState
                            key={R.join('_', [key, x])}
                            color={R.equals("DESTROY", x) ? "VeryRed" : "Yellow"}
                            disabled={R.equals(x, 'ADDED') ? checkDisabled() : false}
                            flex={1}
                            onPress={() => success({ action: x })}
                            title={x}
                        />
                    ))(R.path(['id'])(employees) ? ["DESTROY", "SAVE"] : ["ADDED"])}
                </RowComponents>
            </ScrolerView>
        </Header>
    )
};

export default CreateEmployeeScreen
