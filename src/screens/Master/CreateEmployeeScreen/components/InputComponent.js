/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable padded-blocks */
/* eslint-disable arrow-body-style */
import * as R from 'ramda';
import React from 'react';
import { InputComponents } from 'utils/component'
import i18n from 'i18n-js'
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { Checkbox } from 'react-native-paper';
import { View } from 'react-native';
import { SwitchDescription } from 'screens/Contragent/Input/config'
import { UiPicker } from 'styled/UIComponents/UiPicker'
import { initialInputState, dropdown } from '../reducer';

export const InputComponentState = ({
    storage, dispatch
}) => {
    const changeText = (change, inputPath, error) => {
        if (R.path(['main'])(inputPath)) dispatch({
            type: "Error",
            payload: R.assocPath([inputPath.path], error, R.path(['error'])(storage))
        });

        return dispatch({
            type: inputPath.path,
            payload: R.assocPath(inputPath.value, R.equals(inputPath.label, 'phone') ? `+7${change}` : change, {})
        })
    };

    return (
        <View style={{ marginLeft: 10, marginRight: 10 }}>
            <UiPicker
                dispatch={(e) => dispatch({
                    type: "role",
                    payload: R.assocPath(dropdown.value, e, storage)
                })}
                element={{ path: dropdown.label }}
                Items={[
                    { label: null, value: null },
                    { label: i18n.t('MASTER'), value: "MASTER" },
                    { label: i18n.t('DIRECTOR'), value: "DIRECTOR" },
                ]}
                main
                placeholder={{ id: R.path(['role'])(storage) }}
            />
            {R.addIndex(R.map)((element, keys) => {
                return (
                    <InputComponents
                        key={keys}
                        {...element}
                        onChangeText={(change, error) => changeText(change, element, error)}
                        storage={storage}
                    />
                )
            })(initialInputState)}
            <SwitchDescription
                style={{
                    justifyContent: 'flex-start'
                }}
            >
                <Checkbox
                    onPress={() => {
                        dispatch({ type: "blocked" })
                    }}
                    status={R.path(['blocked'])(storage) ? 'checked' : 'unchecked'}
                />
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={{
                        textAlign: 'left',
                        textTransform: 'capitalize'
                    }}
                    testID="BlockLaker"
                >
                    {i18n.t('BLOCKED')}
                </LabelText>
            </SwitchDescription>
        </View>
    )
};
