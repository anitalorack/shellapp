/* eslint-disable react/jsx-indent-props */
/* eslint-disable newline-after-var */
/* eslint-disable max-statements */
/* eslint-disable object-curly-newline */
/* eslint-disable max-len */
/* eslint-disable react/jsx-curly-newline */
/* eslint-disable no-console */
import React from 'react';
import { Text, View } from 'react-native';
import * as R from 'ramda'
import i18n from 'localization'
import SwitchButtom from 'styled/UIComponents/SwitchButton';
import { Hererre } from '../style';

export const WarningBlock = ({ garageIdAdmin, storage, dispatch }) => (
    <Hererre>
        <View
            style={{
                width: 49,
                height: 60
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 25,
                    fontFamily: "ShellMedium"
                }}
                testID="TEXTGARAGE"
            >
                {garageIdAdmin}
            </Text>
            <Text
                style={{
                    textAlign: 'center',
                    fontFamily: "ShellMedium",
                    textTransform: 'capitalize'
                }}
            >
                {i18n.t('garage')}
            </Text>
        </View>
        <Text
            style={{
                textAlign: 'center',
                fontFamily: "ShellMedium",
                textTransform: "capitalize"
            }}
            testID="TEXTBLOCK"
        >
            {R.path(['blocked'])(storage) ? i18n.t('BLOCK') : i18n.t('UNBLOCK')}
        </Text>
        <SwitchButtom
            onPress={() => dispatch({
                type: "blocked",
                payload: R.assocPath(['blocked'], !R.path(['blocked'])(storage), {})
            })}
            state={R.path(['blocked'])(storage)}
            testID="SwitchButtom"
        />
    </Hererre>
);