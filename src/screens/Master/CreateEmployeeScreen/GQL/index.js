import gql from 'graphql-tag'

export const CREATE_EMPLOYEE = gql`
mutation($input: CreateEmployeeInput!) {
    createEmployee(input: $input) {
      id
      name {
        last
        first
        middle
      }
      role
      phone
      blocked
      garageId
    }
  }
`;

export const UPDATE_EMPLOYEE = gql`
mutation(
    $input:UpdateEmployeeInput!
){
    updateEmployee(
        input: $input
    ){
        id
        name {
            last
            first
            middle
        }
        role
        phone
        blocked
        garageId
        }
}
`;

export const DESTROY_EMPLOYEE = gql`
    mutation($input:Int!) {
        destroyEmployee(id:$input){
            id
        }
    }
`;