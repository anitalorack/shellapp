/* eslint-disable complexity */
/* eslint-disable no-case-declarations */
/* eslint-disable newline-before-return */
/* eslint-disable no-unneeded-ternary */
import * as R from 'ramda'

export const initialState = {
    name: {
        last: null,
        first: null,
        middle: null
    },
    garageId: null,
    role: "MASTER",
    blocked: false,
    phone: null,
    error: { phone: 'phone', first: 'first', last: 'Last' }
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'first':
            const NameInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(NameInput), state);
        case 'last':
            const LastInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(LastInput), state);
        case 'middle':
            const MiddleInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(MiddleInput), state);
        case 'garageId':
            return R.mergeAll([state, action.payload]);
        case 'blocked':
            return R.mergeAll([state, { blocked: !R.path(['blocked'])(state) }]);
        case 'phone':
            return R.mergeAll([state, action.payload]);
        case 'role':
            return R.mergeAll([state, action.payload]);
        case "Update":
            if (R.path(['payload', 'createEmployee'])(action)) {
                return R.mergeAll([state, R.path(['payload', 'createEmployee'])(action)])
            }
            return R.mergeAll([state, R.path(['payload'])(action)]);
        case "performer":
            return { payload: R.path(['payload'])(action) };
        case "Error":
            return R.assocPath(['error'], action.payload, state);
        default:
            return state
    }
};

export const dropdown = {
    id: "Номер",
    path: 'role',
    label: 'role',
    title: "Error message",
    value: ["role"],
    maxLength: 10,
    testID: "RoleInput",
    mode: "dropdown",
    data: [
        { value: "MASTER" },
        { value: "DIRECTOR" }
    ]
};

export const initialInputState = [

    {
        id: "Фамилия",
        path: 'last',
        keyboardType: "default",
        label: 'last',
        title: "Error message",
        value: ["name", "last"],
        main: true,
        maxLength: 120,
        testID: "LastInput",
        error: "Error message",
        Lenght: 3, // valid for lenght
        errorMessage: 'PhoneError', // message for push
    },
    {
        id: "ИМЯ",
        path: 'first',
        keyboardType: "default",
        label: 'first',
        title: "Error message",
        value: ["name", "first"],
        main: true,
        maxLength: 120,
        testID: "NameInput",
        error: "Error message",
        errorMessage: 'PhoneError', // message for push
        Lenght: 3, // valid for lenght
    },
    {
        id: "Отчество",
        path: 'middle',
        keyboardType: "default",
        label: 'middle',
        title: "Error message",
        value: ["name", "middle"],
        maxLength: 120,
        testID: "LastInput",
    },
    {
        id: "Номер",
        path: 'phone',
        keyboardType: "numeric",
        label: 'phone',
        main: true,
        error: "Error message",
        errorMessage: 'PhoneError', // message for push
        Lenght: 10, // valid for lenght
        mask: "+7 ([000]) [000]-[00]-[00]",
        example: "+7 (___) ___-__-__",
        value: ["phone"],
        testID: "LastInput",

    },

];
