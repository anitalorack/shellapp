/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { connect } from 'react-redux';
import CreateEmployeeScreen from './components';

const AppScreen = (props) => {
    const { navigation } = props;

    return (
        <CreateEmployeeScreen
            navigation={navigation}
        />
    )
};

const mapStateToProp = (state) => ({
    profile: state.profile,
});

export default connect(mapStateToProp)(AppScreen);
