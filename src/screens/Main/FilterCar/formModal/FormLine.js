/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';

export const FormLine = ({ element, dispatch }) => (
    <CardComponents
        style={[styles.shadow, styleForm.loader]}
    >
        <TouchableOpacity
            onPress={() => dispatch(element)}
            style={styleForm.items}
        >
            <Text style={{ flex: 1, height: 40, fontFamily: 'ShellBold', }}>
                {R.pipe(R.path(['name']), R.dissoc('__typename'), R.values, R.join(" "))(element)}
            </Text>
            <Text style={{ width: 120, }}>
                {R.path(['phone'])(element)}
            </Text>
        </TouchableOpacity>
    </CardComponents>
);

const styleForm = StyleSheet.create({
    items: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    loaded: {
        flex: 1,
        padding: 0,
        margin: 2,
        height: 70,
        alignItems: 'center',
        justifyContent: 'center'
    }
});