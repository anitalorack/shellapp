/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable lines-between-class-members */
/* eslint-disable react/require-optimization */
/* eslint-disable react/no-set-state */
/* eslint-disable space-before-function-paren */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-sort-props */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable react/jsx-boolean-value */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { ALL_CLIENT } from 'gql/clients';
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView, View
} from 'react-native';
import { InputComponents } from 'utils/component';
import { isOdd } from 'utils/helper';
import { grees } from 'screens/Main/FilterCar/settings/config'
import { FormLine } from 'screens/Main/FilterCar/formModal/FormLine'
import { ALL_EMPLOYERS } from 'gql/employee';

export const FormModal = ({ dispatch, storage, mode }) => {
    const [state, setState] = React.useState(null);
    const [loadGreeting] = useLazyQuery(ALL_CLIENT, {
        onCompleted: (data) => setState(R.assocPath(['query'],
            R.path(R.concat(R.keys(data), ['items']))(data))(state))
    });

    const [loadEmployee] = useLazyQuery(ALL_EMPLOYERS, {
        onCompleted: (data) => setState(R.assocPath(['query'],
            R.path(R.concat(R.keys(data), ['items']))(data))(state))
    });

    React.useEffect(() => {
        if (R.includes(mode)(['client', 'owner'])) {
            loadGreeting({ variables: { where: { q: R.defaultTo(null)(R.path(['text'])(state)) } } })
        }
        if (R.includes(mode)(['author', 'performer'])) {
            loadEmployee({ variables: { where: R.path(['text'])(state) ? { q: R.path(['text'])(state) } : {} } })
        }
    }, [R.path(['text'])(state), mode]);

    return (
        <View style={{ flex: 1 }}>
            <View style={{ margin: 10 }}>
                <InputComponents
                    {...R.assocPath(['label'], mode)(grees)}
                    onChangeText={(e) => setState(R.assocPath(['text'], e, state))}
                    storage={state}
                />
            </View>
            <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 10 }}>
                {R.map(x => {
                    if (isOdd(x)) return <View />;
                    return (
                        <FormLine
                            key={R.path(['id'])(x)}
                            dispatch={(e) => dispatch(e)}
                            element={x}
                            storage={storage}
                        />
                    )
                })(R.defaultTo([])(R.path(['query'])(state)))}
            </ScrollView>
        </View>
    )
};