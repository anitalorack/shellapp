import * as R from 'ramda'
import { StyleSheet } from 'react-native'
import i18n from 'localization'

export const grees = {
    id: 4,
    path: 'text',
    main: true,
    label: 'text',
    keyboardType: "default",
    value: ['text'],
    example: i18n.t("SEARCHBYLIST"),
    error: "Error message",
    errorMessage: 'PhoneError',
};

export const coment = R.cond([
    [R.equals('author'), R.always('authorId')],
    [R.equals('client'), R.always('clientId')],
    [R.equals('performer'), R.always('performerId')],
    [R.equals('owner'), R.always('ownerId')],
]);

export const styleModal = StyleSheet.create({
    reload: {
        borderColor: 'grey',
        borderWidth: 1,
        flex: 1,
        height: 50,
        justifyContent: 'center',
    }
});