/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable no-unused-expressions */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { coment } from 'screens/Main/FilterCar/settings/config';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { filterRequest } from 'utils/helper';
import { CarsDropDown } from 'utils/support/CarsDropDown';
import { FilterScreenMode } from './FilterScreenMode';

export const FilterCar = ({ dispatch }) => {
    const dispatchStorage = useDispatch();
    const filterData = useSelector(state => state.filter, shallowEqual);
    const [state, setState] = React.useState(R.path(['filter'])(filterData));

    const success = (e) => {
        if (R.equals(e, "Reset")) {
            setState({ modification: null });
            dispatchStorage({ type: 'INITIAL' });

            return dispatch({ type: "Reset" })
        }
        if (R.equals(e, "Apply")) {
            return dispatch({
                type: "Apply",
                payload: filterRequest({ state: R.path(['filter'])(filterData) })
            })
        }
    };

    return (
        <View
            style={{ flex: 1, justifyContent: 'space-between' }}
        >
            <View style={{ flex: 1 }}>
                <HeaderApp
                    button="CLOSED"
                    dispatch={() => {
                        dispatch({ type: "CLOSE" })
                    }}
                    header
                    title={i18n.t("Filter")}
                />
                <ScrollView style={{ padding: 10 }}>
                    <CarsDropDown
                        dispatch={(e) => {
                            if (e) return setState(R.mergeAll([state, { vehicle: e }]))
                        }}
                        filter
                        notModification
                        storage={R.path(['vehicle'])(state)}
                    />
                    {R.map(x => (
                        <FilterScreenMode
                            key={x}
                            dispatch={(e) => setState(R.mergeAll([state, e]))}
                            mode={x}
                            storage={R.path([coment(x), 'eq'])(state)}
                        />)
                    )(['owner', 'client', 'author', 'performer'])}
                </ScrollView>
            </View>
            <RowComponents>
                {R.map(func => (
                    <TouchesState
                        key={func}
                        color={R.equals("Reset", func) ? "VeryGrey" : "Yellow"}
                        flex={1}
                        onPress={() => success(func)}
                        title={func}
                    />
                ))(["Apply", "Reset"])}
            </RowComponents>
        </View>
    )
};