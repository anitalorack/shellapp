/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/no-multi-comp */
/* eslint-disable no-unused-expressions */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { Colors, IconButton } from 'react-native-paper';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { FormModal } from 'screens/Main/FilterCar/formModal';
import { coment } from 'screens/Main/FilterCar/settings/config';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { LabelInputText } from 'utils/component/LabelInputText';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

export const FilterScreenMode = ({ dispatch, storage, mode }) => {
    const [state, setState] = React.useState({ isVisible: false });
    const dispatchStorage = useDispatch();
    const filter = useSelector(state => state.filter, shallowEqual);

    const IconCancel = () => {
        if (R.path(['filter', mode, 'name'])(filter)) return (
            <IconButton
                color={Colors.black500}
                icon="close"
                onPress={() => success("TEXT_RESET")}
                size={20}
            />
        );

        return (<View />)
    };
    const success = (e, data) => R.cond([
        [R.equals("MODAL_OPEN"), () => setState(R.assocPath(['isVisible'], true, state))],
        [R.equals("TEXT_RESET"), () => {
            setState(R.assocPath(['query'], '', state));
            dispatchStorage({ type: "FILTER_DESTROY", payload: mode });
            dispatch(R.dissoc(coment(mode), storage))
        }],
        [R.equals("MODAL_CLOSE"), () => setState({ isVisible: false })],
        [R.equals("ADD_MODAL"), () => {
            dispatch(R.assocPath([coment(mode), 'eq'], R.path(['id'])(e))({}));
            dispatchStorage({
                type: "FILTER_ADD",
                payload: R.assocPath([mode], {
                    name: R.pipe(R.path(['name']), R.dissoc('__typename'), R.values, R.join(' '))(data),
                    id: R.path(['id'])(data)
                })({})
            });
            setState(R.assocPath(['isVisible'], false)(state))
        }]
    ])(e);

    return (
        <View>
            <View style={{ paddingBottom: 10 }}>
                <LabelInputText props={{ label: mode }} />
            </View>
            <View
                style={stylesItem.item}
            >
                <TouchableOpacity
                    onPress={() => success('MODAL_OPEN')}
                    style={{ paddingLeft: 15, flex: 1 }}
                >
                    <Text>
                        {R.path(['filter', mode, 'name'])(filter)}
                    </Text>
                </TouchableOpacity>
                {IconCancel()}
            </View>
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ModalDescription style={{ flex: 1, margin: 10 }} >
                    <HeaderApp
                        button="CLOSED"
                        dispatch={() => success("MODAL_CLOSE")}
                        header
                        title={i18n.t(mode)}
                    />
                    <FormModal
                        dispatch={(data) => success('ADD_MODAL', data)}
                        mode={mode}
                        storage={R.defaultTo("")(R.pipe(R.path(['query']), R.path(['id']))(state))}
                    />
                </ModalDescription>
            </Modal>
        </View>
    )
};

const stylesItem = StyleSheet.create({
    item: {
        borderWidth: 1,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center'
    }
});