/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { Preview } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'
import { useDispatch } from 'react-redux';
// const [current, setMoved] = useNavigation({ navigation, move: 'toggle' })

export const ClientScreen = ({ navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const dispatchRedux = useDispatch()

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "ListClients" }}
            svg={current}
        >
            <VisualTable
                dispatch={(e) => {
                    if (R.equals('Create', R.path(['type'])(e))) {
                        dispatchRedux({ type: 'TASK_UPDATE', payload: {} })

                        return navigation.navigate(Preview.PreviewTaskGarage)
                    }

                    return navigation.navigate(Preview.PreviewClient, { itemId: { eq: e.payload } })
                }}
                mode="client"
                navigation={navigation}
                unfilter
            />
        </Header >
    )
};
