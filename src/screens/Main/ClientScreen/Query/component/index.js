/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import { ALL_CLIENT } from 'gql/clients';
import * as R from 'ramda';
import React from 'react';
import { Linking, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { TableModel } from 'screens/TaskEditor/components/table';
import { CreateScreen, Preview } from 'utils/routing/index.json';

export const FormClient = ({ variables, navigation }) => {
    const ROUTER = CreateScreen.ClientScreen;
    const ROUTERVEHICLE = Preview.PreviewDashBoardVehilcle;
    const { error, loading, data } = useQuery(ALL_CLIENT, {
        variables,
        fetchPolicy: "network-only"
    });

    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );

    return (
        <View style={{ flex: 1 }} >
            <TableModel
                data={R.path(['clients', 'items'])(data)}
                dispatch={(e) => {
                    // if (R.path(['type'])(e) === "Edit") {
                    //     return navigation.navigate(ROUTER, { mode: 'client', itemId: { owner: e.payload } })
                    // }
                    // if (R.path(["type"])(e) === "CALL") {
                    //     return Linking.openURL(`tel:${e.payload}`)
                    // }
                    // if (R.path(["type"])(e) === "Phone") {
                    //     return navigation.navigate(ROUTERVEHICLE, { mode: "preview", itemId: null, phone: R.path(['payload'])(e) })
                    // }
                    // if (R.path(['type'])(e) === "Create") {
                    //     return navigation.navigate(ROUTER, { mode: 'create', itemId: null })
                    // }
                }}
                header="ListClients"
                mode="Date"
                style={{ flex: 1 }}
            />
        </View>
    )
};