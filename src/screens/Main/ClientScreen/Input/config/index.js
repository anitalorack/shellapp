/* eslint-disable no-case-declarations */
/* eslint-disable newline-before-return */
/* eslint-disable no-unneeded-ternary */
export const initialState = {
    name: {
        last: null,
        first: null,
        middle: null
    },
    phone: null,
};

export const initialInputState = [
    {
        id: "ИМЯ",
        path: 'name',
        keyboardType: "default",
        label: 'first',
        title: "Error message",
        value: ["name", "first"],
        main: true,

        maxLength: 120,
        testID: "NameInput",
    },
    {
        id: "Фамилия",
        path: 'last',
        keyboardType: "default",
        label: 'last',
        title: "Error message",
        value: ["name", "last"],
        main: true,

        maxLength: 120,
        testID: "LastInput",
    },
    {
        id: "Отчество",
        path: 'middle',
        keyboardType: "default",
        label: 'middle',
        title: "Error message",
        value: ["name", "middle"],
        maxLength: 120,

        testID: "LastInput",
    },
    {
        id: "Номер",
        path: 'phone',
        keyboardType: "numeric",
        label: 'phone',
        title: "Error message",
        main: true,
        mask: "+7 ([000]) [000]-[00]-[00]",
        example: "+7 (___) ___-__-__",
        value: ["phone"],
        testID: "LastInput",

    },
];
