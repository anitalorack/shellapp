/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import { useNavigation } from 'hooks/useNavigation';
import * as R from 'ramda';
import React, { useReducer } from 'react';
import { FormClientVehicle } from 'screens/Main/CarsScreen/Query/component';
import { FormClient } from 'screens/Main/MainScreen/Query/component';
import HeaderComponentsProps from 'uiComponents/HeaderComponents';
import { SplashContainer } from 'uiComponents/SplashComponents';
import { isOdd, phoneMedd } from 'utils/helper';

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case "Screeen":
            return R.assocPath(['curr'], action.payload, state);
        case "Vehicles":
            return R.assocPath(['vehicle'], action.payload, state);
        case "CreateTask":
            return R.assocPath(['task'], action.payload, state);
        default:
            return state
    }
};

export const ClientInputScreen = ({ data, navigation, mode, cars }) => {
    const [storage, dispatch] = useReducer(reducer, R.path(['client'])(data));
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });

    const header = {
        title: R.pipe(
            R.path(['name']),
            R.omit(['__typename']),
            R.values,
            R.join(' ')
        )(storage),
        subHeader: phoneMedd(R.path(['phone'])(storage)),
        menu: ["CreateTask", "ListClientsCars"],
        curr: R.path(['curr'])(storage)
    };

    const success = (event) => {

        return R.cond([
            [R.equals('ListClientsCars'), () => dispatch({ type: "Screeen", payload: R.path(['type'])(event) })],
            [R.equals('CreateTask'), () => dispatch({ type: "Screeen", payload: R.path(['type'])(event) })],
            [R.equals(current), () => setMoved(current)],
        ])(R.path(['type'])(event))
    };

    return (
        <SplashContainer>
            <HeaderComponentsProps
                header={header}
                onPress={(e) => {
                    success(e)
                }}
                svg={current}
                title={isOdd(R.path(['id'])(storage)) ? "Clients" : "Clients"}
            />
            {R.equals('CreateTask', R.path(['curr'])(storage)) ?
                <FormClient
                    navigation={navigation}
                    storage={storage}
                    variables={{ variables: { where: { client: { id: { eq: R.path(['id'])(storage) } } } } }}
                /> :
                <FormClientVehicle
                    navigation={navigation}
                    storage={storage}
                    variables={{ variables: { where: { client: { id: { eq: R.path(['id'])(storage) } } } } }}
                />}
        </SplashContainer>)
};
