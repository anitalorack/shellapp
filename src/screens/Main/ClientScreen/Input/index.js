/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React, { useEffect, useState } from 'react';
import { SplashScreen } from 'screens/Splash';
import { isOdd } from 'utils/helper';
import { CURRENT_CLIENT } from '../GQL';
import { ClientInputScreen } from './component';

export const ClientEditScreen = (props) => {
    const { navigation } = props;
    const [state] = useState(null);
    const [data, setData] = useState(null);
    const mode = R.path(['route', 'params', 'mode'])(props);
    const modificationId = R.path(['route', 'params', 'itemId', 'modificationId'])(props);
    const owner = R.path(['route', 'params', 'itemId', 'owner'])(props);
    const plate = R.path(['route', 'params', 'itemId', 'plate'])(props);
    const vin = R.path(['route', 'params', 'itemId', 'vin'])(props);
    const [loadgreeting] = useLazyQuery(CURRENT_CLIENT, {
        onCompleted: (data) => setData(data)
    });

    useEffect(() => {
        let data = {};

        if (R.equals(mode, 'create')) {
            setData({ create: true })
        }
        if (R.equals(mode, 'client')) {
            data = R.assocPath(['id'], { eq: owner }, data)
        }
        if (R.equals(mode, 'edit')) {
            if (modificationId) {
                data = R.assocPath(['vehicles', 'modificationId'], { eq: modificationId }, data)
            }
            if (owner) {
                data = R.assocPath(['vehicles', 'ownerId'], { eq: owner }, data)
            }
            if (plate) {
                data = R.assocPath(['vehicles', 'plate'], { eq: plate }, data)
            }
            if (vin) {
                data = R.assocPath(['vehicles', 'vin'], { eq: vin }, data)
            }
        }
        if (!isOdd(data)) {
            loadgreeting({ variables: { where: data, order: { id: 'desc' } } })
        }
    }, [state]);

    const cars = [vin, plate, modificationId, owner];

    if (R.isNil(data)) return (
        <SplashScreen
            state="Loading"
            testID="ISODD"
        />
    );

    return (
        <ClientInputScreen
            cars={cars}
            data={data}
            mode={mode}
            navigation={navigation}
        />
    )
};
