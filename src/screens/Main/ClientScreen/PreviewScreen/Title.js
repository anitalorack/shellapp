/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
import { useCurrentClient } from 'hooks/useCurrentClient';
import * as R from 'ramda';
import React from 'react';
import { Linking, Text, TouchableOpacity } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { phoneMedd } from "utils/helper";

export const Title = ({ id }) => {
    const [data] = useCurrentClient({ id })

    return (
        <CardComponents style={[styles.shadow, { margin: 1 }]}>
            <HeaderApp
                empty
                header
                title={(
                    <Text
                        style={{ fontFamily: 'ShellBold' }}
                    >
                        {R.pipe(R.dissoc('__typename'), R.values, R.join(' '))(R.path(['name'])(data))}
                    </Text>
                )}
            />
            <TouchableOpacity
                onPress={() => Linking.openURL(`tel:${R.path(['phone'])(data)}`)}
            >
                <Text
                    style={{ color: 'grey', marginLeft: 10 }}
                >
                    {phoneMedd(R.path(['phone'])(data))}
                </Text>
            </TouchableOpacity>
        </CardComponents>
    )
};