import gql from 'graphql-tag'

export const CURRENT_CLIENT = gql`
query($where: WhereClientInput!) {
  client(where: $where) {
    id
    name {
      first
      last
      middle
    }
    phone
  }
}
`;

export const DESTROY_CLIENT = gql`
mutation($id: Int!) {
  destroyClient(id: $id) {
    id
  }
}
`;

export const CREATE_CLIENT = gql`
mutation($input: CreateClientInput) {
  createClient(input: $input) {
    id
    name {
      last
      middle
      first
    }
    phone
  }
}
`;

export const UPDATE_CLIENT = gql`
mutation($input: UpdateClientInput!) {
  updateClient(input: $input) {
    id
    name {
      last
      middle
      first
    }
    phone
  }
}
`;