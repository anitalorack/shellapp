import gql from 'graphql-tag'

export const REPAIRSWORK_QUERY = gql`
query(
  $where: WhereRepairWorkInput
  $order: OrderRepairWorkInput
  $paginate: PageInput
) {
  repairWorks(where: $where, order: $order, paginate: $paginate) {
    items {
      id
      repairId
      modificationWorkId
      name
      action
      timeHrs
      unitPrice {
        amount
        currency
      }
      unitCount
      discount {
        amount
        percent
      }
      originalPrice {
        amount
        currency
      }
      totalPrice {
        amount
        currency
      }
      repair {
        id
        status
        description
        mileage
        note
        garageId
        vehicleId
        clientId
        client {
          id
        }
        ownerId
        owner {
          id
          name {
            last
            first
            middle
          }
          phone
        }
        authorId
        author {
          id
          name {
            last
            first
            middle
          }
          phone
        }
        performerId
        performer {
          id
          name {
            last
            first
            middle
          }
          phone
        }
        worksTotalPrice {
          amount
          currency
        }
        partsTotalPrice {
          amount
          currency
        }
        originalPrice {
          amount
          currency
        }
        totalPrice {
          amount
          currency
        }
        testimonial
      }
    }
  }
}
`;

export const CREATE_REPAIRWORK = gql`
mutation($input: CreateRepairWorkInput) {
  createRepairWork(input: $input) {
    id
  }
}
`;

export const UPDATE_REPAIRWORK = gql`
mutation($input: UpdateRepairWorkInput!) {
  updateRepairWork(input: $input) {
    id
  }
}
`;

export const DESTROY_REPAIRWORK = gql`
mutation($input: Int!) {
  destroyRepairWork(id: $input) {
    id
  }
}
`;

export const CREATE_ADDONWORK = gql`
mutation($input: CreateAddonWorkInput) {
  createAddonWork(input: $input) {
    id
  }
}
`;

export const UPDATE_ADDONWORK = gql`
mutation($input: UpdateAddonWorkInput!) {
  updateAddonWork(input: $input) {
    id
  }
}
`;

export const DESTROY_ADDONWORK = gql`
mutation($input: Int!) {
  destroyAddonWork(id: $input) {
    id
  }
}
`;

export const CATALOG_WORKS = gql`
query(
  $where: WhereVehicleModificationWorkCatalogInput
  $order: OrderVehicleModificationWorkCatalogInput
) {
  vehicleModificationWorkCatalogs(where: $where, order: $order) {
    id
    name
    groups {
      id
      name
      works {
        action
        timeHrs
        id
        name
      }
    }
  }
}
`;