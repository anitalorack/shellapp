/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import { useApolloClient } from '@apollo/react-hooks';
import * as R from 'ramda';
import React, { useReducer } from 'react';
import { showMessage } from 'react-native-flash-message';
import {
    CREATE_REPAIRWORK,
    UPDATE_REPAIRWORK,
    DESTROY_REPAIRWORK
} from 'screens/Main/WorkScreen/GQL';
import { reducer } from 'screens/Main/CarsScreen/Input/config';
import { ScrolerView } from 'screens/Master/CreateEmployeeScreen/style';
import HeaderComponentsProps from 'uiComponents/HeaderComponents';
import { SplashContainer } from 'uiComponents/SplashComponents';
import { requestMutation } from 'utils/api/funcComponent/Apollo';
import { isOdd } from 'utils/helper';
import i18n from 'i18n-js'
import { MainScreen } from 'utils/routing/index.json';
import { ButtonBlock } from 'screens/Main/WorkScreen/Input/component/buttomBlock'
import { InputComponentState } from './input';


export const CarsInputScreen = ({ data, navigation }) => {
    const [storage, dispatch] = useReducer(reducer, {});
    const ROUTER = MainScreen.Client;
    const clientApollo = useApolloClient();

    const setUpdateData = async (event) => {
        const variables = R.cond([
            [R.equals("Action"), R.always({
                repairId: R.path(['repairId'])(storage),
                modificationWorkId: R.path(['modificationWorkId'])(storage),
                name: R.path(['name'])(storage),
                action: R.path(['action'])(storage),
                timeHrs: R.path(['timeHrs'])(storage),
                unitPrice: {
                    amount: R.path(['amount'])(storage),
                    currency: {
                        eq: R.path(['amount'])(storage)
                    }
                },
                unitCount: R.path(['unitCount'])(storage),
                discount: R.path(['discount'])(storage),
            })],
            [R.equals("Delete"), R.always({ input: R.path(['id'])(storage) })],
        ]);

        const document = R.cond([
            [R.equals("CREATE"), R.always({ query: CREATE_REPAIRWORK, variables: { input: R.omit(['id'])(variables(R.path(['type'])(event))) } })],
            [R.equals("UPDATE"), R.always({ query: UPDATE_REPAIRWORK, variables: { input: R.omit(['owner'])(variables(R.path(['type'])(event))) } })],
            [R.equals("DESTROY"), R.always({ query: DESTROY_REPAIRWORK, variables: variables(R.path(['type'])(event)) })],
        ])(R.path(['payload'])(event));

        try {
            const data = await requestMutation(R.mergeAll([document, { client: clientApollo }]));

            if (R.equals(R.path(['payload'])(event), 'DESTROY')) return navigation.navigate(ROUTER);
            showMessage({
                message: "Данные обновлены",
                type: "success",
            });

            return dispatch({ type: "Update", payload: R.path(R.keys(data))(data) })
        } catch (error) {
            return showMessage({
                message: R.toString(error),
                description: "Error",
                type: "info",
            })
        }
    };

    return (
        <SplashContainer>
            <HeaderComponentsProps
                goBack
                mode="DELETE"
                onPress={(event) => {
                    if (R.equals('Delete', R.path(['type'])(event))) return setUpdateData(event);
                    if (R.equals(event, 'goBack')) return set
                }}
                testID="HEADER"
                title={isOdd(R.path(['id'])(storage)) ? i18n.t('WORKSELF') : "Редактировать автомобиль"}
            />
            <ScrolerView
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }}
            >
                <InputComponentState
                    dispatch={(e) => {
                        dispatch(e)
                    }}
                    storage={storage}
                    testID="InputComponentState"
                />
                <ButtonBlock
                    dispatch={(e) => {
                        if (R.equals(R.path(['type'])(e))('Action')) return setUpdateData(e);
                        if (R.equals(R.path(['type'])(e))('Cancel')) return navigation.navigate(ROUTER)
                    }}
                    navigation={navigation}
                    storage={storage}
                    style={{ bottom: 0, flex: 1 }}
                    testID="ButtonBlock"
                />
            </ScrolerView>
        </SplashContainer>)
};