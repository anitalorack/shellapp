/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable padded-blocks */
/* eslint-disable arrow-body-style */
import { useLazyQuery } from '@apollo/react-hooks';
import i18n from 'i18n-js';
import * as R from 'ramda';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { QUERY_VEHICLES, QUERY_VEHICLES_MODELS, QUERY_VEHICLES_MODIFICATION } from 'screens/Main/CarsScreen/GQL';
import SwitchButtom from 'styled/UIComponents/SwitchButton';
import { LabelText } from 'uiComponents/SplashComponents';
import { InputComponents } from 'utils/component';
import { ColorCard } from 'utils/helper';
import { initialInputState } from '../config';

export const InputComponentState = ({
    storage, dispatch
}) => {
    const [count] = useState(null);
    const [state, setState] = useState(true);

    const [vehiclesQuery] = useLazyQuery(QUERY_VEHICLES, {
        onCompleted: (data) => dispatch({
            type: "Data",
            payload: data,
        }),
        fetchPolicy: "network-only"
    });
    const [modelsQuery] = useLazyQuery(QUERY_VEHICLES_MODELS, {
        onCompleted: (data) => {
            dispatch({
                type: "Data",
                payload: data,
            })
        },
        fetchPolicy: "network-only"
    });
    const [modificationQuery] = useLazyQuery(QUERY_VEHICLES_MODIFICATION, {
        onCompleted: (data) => {
            dispatch({
                type: "Data",
                payload: data,
            })
        },
        fetchPolicy: "network-only"
    });

    useEffect(() => {
        vehiclesQuery({ variables: { order: { name: "asc" } } })
    }, [count]);

    const changeText = (change, inputPath) => {
        if (R.equals(inputPath.path)('vehicleManufacturers')) {
            modelsQuery({ variables: { where: { manufacturerId: { eq: R.path(['id'])(change) } } } })
        }
        if (R.equals(inputPath.path)('vehicleModels')) {
            modificationQuery({ variables: { where: { modelId: { eq: R.path(['id'])(change) } } } })
        }
        if (inputPath.mode === 'dropdown') {
            return dispatch({
                type: inputPath.label,
                payload: R.assocPath(inputPath.valuePath, change, storage)
            })
        }

        return dispatch({
            type: inputPath.label,
            payload: R.assocPath(inputPath.value, change, {})
        })
    };

    return (
        <>
            <View
                style={{
                    height: 60,
                    flex: 1,
                    flexDirection: 'row',
                    padding: 10,
                    justifyContent: "flex-end",
                    alignItems: 'center'
                }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{ textAlign: 'right' }}
                >
                    {state ? i18n.t('discountCoast') : i18n.t('discountPersent')}
                </LabelText>
                <SwitchButtom
                    onPress={() => setState(!state)}
                    state={state}
                    testID="SwitchButtom"
                />
            </View>
            <KeyboardAwareScrollView>
                {R.addIndex(R.map)((element, keys) => {
                    return (
                        <InputComponents
                            key={keys}
                            {...element}
                            onChangeText={(change) => changeText(change, element)}
                            storage={storage}
                        />
                    )
                })(R.equals(state, false) ? R.without([initialInputState[4]], initialInputState) : R.without([initialInputState[5]], initialInputState))}
            </KeyboardAwareScrollView>

        </>
    )
};
