/* eslint-disable multiline-ternary */
/* eslint-disable max-statements */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-redeclare */
/* eslint-disable react/no-multi-comp */
/* eslint-disable scanjs-rules/call_connect */
/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { SplashScreen } from 'screens/Splash';
import { isOdd } from 'utils/helper';
import { REPAIRSWORK_QUERY } from '../GQL';
import { CarsInputScreen } from './component';

export const WorksEditScreen = (props) => {
    const { navigation } = props;
    const mode = R.path(['route', 'params', 'mode'])(props);
    const vehicleId = R.path(['route', 'params', 'phone'])(props);
    const ownerId = R.path(['route', 'params', 'itemId'])(props);
    const { error, loading, data } = useQuery(REPAIRSWORK_QUERY, {
        variables: { "where": R.ifElse(isOdd, R.always({}), R.always({ id: { eq: vehicleId } }))(vehicleId) }
    });

    if (error) return (
        <SplashScreen
            msg={error}
            state="Error"
            testID="ISODD"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="ISODD"
        />
    );

    return (
        <CarsInputScreen
            data={isOdd(ownerId) ?
                data :
                R.assocPath(['ownerId'], { id: ownerId }, data)}
            navigation={navigation}
        />
    )
};
