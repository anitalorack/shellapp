/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React, { useReducer } from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { isOdd } from 'utils/helper'
import { shallowEqual, useSelector } from 'react-redux';
import { FormClient } from './component';
import {
    initialState, reducer, setRequestDataLoader, StateBuild
} from './config'

export const WorksList = ({ navigation }) => {
    const [storage, dispatch] = useReducer(reducer, initialState);
    const user = useSelector(state => state.profile, shallowEqual);

    return (
        <Header
            check={{
                name: ["name", "action"],
                current: "name",
            }}
            crud={[
                { Route: "CREATE", text: "CREATE" },
                { Route: "GOBACK", text: "GOBACK" }
            ]}
            dispatch={(e) => {
                dispatch(e);
                if (R.includes(R.path(['type'])(e), ['Checker'])) return setRequestDataLoader(StateBuild(e, storage), (e) => dispatch(e), user);
                if (R.includes(R.path(['type'])(e), ['Text', 'Search'])) return setRequestDataLoader(StateBuild(e, storage), (e) => dispatch(e), user);
                if (R.equals(R.path(['type'])(e), "GOBACK")) return navigation.goBack()
            }}
            menu={{ title: "ListWork" }}
            sorted={
                [
                    { title: "ALLWORK" },
                    { title: "MYALLWORK" },
                    { title: "FORTHISCAR" },
                ]
            }
            state={storage}
        >
            <FormClient
                navigation={navigation}
                title={isOdd(R.path(['checker'])(storage)) ? "WORK" : R.path(['checker'])(storage)}
                variables={R.path(['variables'])(storage)}
            />
        </Header >
    )
};
