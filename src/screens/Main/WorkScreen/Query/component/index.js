/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import {
    CATALOG_WORKS,
    REPAIRSWORK_QUERY
} from 'screens/Main/WorkScreen/GQL';
import * as R from 'ramda';
import React from 'react';
import { Linking, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { TableModel } from 'screens/TaskEditor/components/table';
import { CreateScreen } from 'utils/routing/index.json';

export const FormClient = ({ title, variables, navigation }) => {
    const ROUTER = CreateScreen.WorkEditScreen;
    const ROUTERVEHICLE = CreateScreen.DashBoardVehilcle;
    const mode = 'contr';

    const { error, loading, data } = useQuery(R.equals('list')(mode) ? CATALOG_WORKS : REPAIRSWORK_QUERY, {
        variables,
        fetchPolicy: "network-only"
    });

    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );

    return (
        <View style={{ flex: 1 }} >
            <TableModel
                data={R.equals('list')(mode) ? R.head(R.path(['vehicleModificationWorkCatalogs'])(data)) : R.path(['repairWorks', 'items'])(data)}
                dispatch={(e) => {
                    if (R.includes(R.path(['type'])(e), ["Edit", 'Phone'])) {
                        return navigation.navigate(ROUTER, { mode: 'edit', itemId: e.payload })
                    }
                    if (R.path(["type"])(e) === "CALL") {
                        return Linking.openURL(`tel:${e.payload}`)
                    }
                    if (R.path(["type"])(e) === "Phone") {
                        return navigation.navigate(ROUTERVEHICLE)
                    }
                    // if (R.path(['type'])(e) === "Create") {
                    //     return navigation.navigate(ROUTER, { mode: 'create', itemId: null })
                    // }
                }}
                header={title}
                mode="Date"
                style={{ flex: 1 }}
            />
        </View>
    )
};