/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { LoadingScreenHook } from 'screens/Main/TaskBack/hooks';
import { LoaderCarsScreen } from 'screens/Main/TaskBack/Input/createScreen';
import { UpdateScreen } from 'screens/Main/TaskBack/UpdateScreen';
import { SplashScreen } from 'screens/Splash';
import { Header } from 'screens/TaskEditor/components/header';

export const TaskGarage = ({ navigation, route }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const { data, error, loading } = LoadingScreenHook({ route, navigation });

    if (error) {
        return (
            <SplashScreen
                state="Error"
            />
        )
    }

    if (loading) {
        return (
            <SplashScreen
                state="Loading"
            />
        )
    }

    if (R.hasPath(['owner'])(task)) return (
        <Header
            dispatch={() => navigation.goBack()}
            menu={{ title: "NewTaskCars" }}
            search
        >
            <UpdateScreen navigation={navigation} />
        </Header>
    );

    return (
        <Header
            dispatch={() => navigation.goBack()}
            menu={{ title: "NewTaskCars" }}
            search
        >
            <LoaderCarsScreen navigation={navigation} />
        </Header>
    );
};
