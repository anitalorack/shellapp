/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import { StyleSheet } from 'react-native';
import { Colors } from 'react-native-paper';
import { isOdd } from 'utils/helper';

export const placeholder = R.pipe(
  R.paths([
    ['modification', 'model', 'manufacturer', 'name'],
    ['modification', 'model', 'name'],
    ['modification', 'model', 'subbody']
  ]),
  R.reject(isOdd),
  R.join(' ')
);

export const styleCar = StyleSheet.create({
  item: {
    backgroundColor: 'gold',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    margin: 3
  },
  scroll: {
    flexGrow: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  view: {
    backgroundColor: Colors.grey400,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    marginRight: 3
  },
  text: {
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'ShellLight',
    padding: 2
  },
  plate: {
    textAlign: 'center',
    borderWidth: 1,
    alignItems: 'flex-end'
  },
  idea: {
    margin: 1,
    paddingTop: 5,
    paddingBottom: 5,
  },
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    marginLeft: 10,
    backgroundColor: 'lightgrey'
  }
});