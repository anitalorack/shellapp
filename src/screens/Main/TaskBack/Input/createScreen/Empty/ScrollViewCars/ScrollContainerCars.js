/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable react/no-multi-comp */
import ADD from 'assets/svg/car';
import * as R from 'ramda';
import React from 'react';
import {
  ScrollView, Text, TouchableOpacity, View
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { PlateNumber } from 'screens/TaskEditor/components/table/component/repairs';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { ColorCard } from 'utils/helper';
import { useTaskHook } from 'hooks/useTaskHooks'
import { placeholder, styleCar } from './config';

export const ScrollContainerCars = ({ dispatch, onPress, storage }) => {
  const [request] = useTaskHook()

  return (
    <ScrollView contentContainerStyle={styleCar.scroll}>
      {R.addIndex(R.map)((x, key) => (
        <CardComponents
          key={R.join('_', ['NewScrool', key])}
          style={[styles.shadow, styleCar.idea]}
        >
          <TouchableOpacity
            onPress={() => {
              request('SelectVehicle', R.omit(['id', 'ownerId', 'owner', 'clients'])(x))

              return onPress({ isVisible: false })
            }}
            style={{ flex: 1 }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
              <View style={[styleCar.view, { justifyContent: 'center' }]} >
                <SvgXml height="25" width="25" xml={ADD} />
              </View>
              <View style={{ flex: 1, maxWidth: 150, justifyContent: 'center' }}>
                <Text style={[styleCar.text]}>
                  {placeholder(x)}
                </Text>
                <Text style={styleCar.text}>
                  {R.path(['vin'])(x)}
                </Text>
              </View>
              <View style={{ justifyContent: 'flex-end' }}>
                <PlateNumber
                  color={ColorCard('classic').font}
                  fonts="ShellMedium"
                  items="SDescription"
                >
                  {R.path(['plate'])(x)}
                </PlateNumber>
              </View>
            </View>
          </TouchableOpacity>
        </CardComponents>
      ))(storage)}
    </ScrollView>
  )
};