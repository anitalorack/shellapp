/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import i18n from 'localization';
import React from 'react';
import * as R from 'ramda'
import search from 'assets/svg/search';
import {
  StyleSheet, Text, TouchableOpacity, View
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { TextVersion } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/TextVersion';

export const VisualInput = ({ mode, onPress }) => (
  <View>
    <Text style={styleCar.text}>
      {i18n.t(mode)}
    </Text>
    <TouchableOpacity
      onPress={() => onPress({ isVisible: true, mode })}
      style={styleCar.items}
    >
      <View style={{ paddingLeft: 20 }}>
        <SvgXml
          height="15"
          width="15"
          xml={search}
        />
      </View>
    </TouchableOpacity>
    {R.equals(mode, 'vin') ?
      <TextVersion
        version={3}
      /> :
      <View />}
  </View>
);

const styleCar = StyleSheet.create({
  items: {
    height: 50,
    borderWidth: 1,
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'ShellMedium',
    fontSize: 12,
    paddingTop: 10,
    paddingBottom: 10,
    textAlign: 'center',
    textTransform: 'uppercase'
  }
});
