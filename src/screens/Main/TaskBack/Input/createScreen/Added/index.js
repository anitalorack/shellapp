/* eslint-disable react/jsx-indent-props */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undefined */
/* eslint-disable react/no-multi-comp */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { CurrentSelect } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect';
import { ViewCarsSetting } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/ViewCarsSetting';
import { ButtonComponents } from 'screens/Main/TaskBack/first/helper/ButtonComponents';
import { ModalMileage } from 'screens/Main/TaskBack/Input/createScreen/Added/ModalMileage';
import { disabled, LoadingScreenHook } from 'screens/Main/TaskBack/Input/createScreen/hooks';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';

export const CarsEdit = ({ navigation }) => {
  const { task } = useSelector(state => state.task, shallowEqual);
  const [modalMile, setModalMile] = React.useState({ isVisible: false });
  const [__, dispatch] = LoadingScreenHook();
  const [mode, setMode] = React.useState(['vin']);
  const [state, setState] = React.useState(R.path(['vehicle'])(task));

  React.useEffect(() => {
    if (!R.path(['mile'])(modalMile)) {
      if (R.equals(R.hasPath(['vehicle', '__typename'])(task), true)) {
        setModalMile({ isVisible: true })
      }
    }
  }, [task]);

  const success = (e, data) => {
    R.cond([
      [R.equals('BACK'), () => navigation.goBack()],
      [R.equals('added'), () => dispatch('Edit_Cars', R.assocPath(['owner'], { "__typename": 'Owner' })(data))],
      [R.equals('Update'), () => setState(R.mergeAll([state, R.assocPath(['owner'], { "__typename": 'Owner' })(data)]))]
    ])(e)
  };

  return (
    <View style={{ flex: 1, margin: 5 }}>
      <HeaderApp
        empty
        title={i18n.t('ADDEDCARS')}
      />
      <ScrollView
        style={{
          padding: 10,
          paddingTop: 0
        }}
      >
        <CurrentSelect
          mode={mode}
          setMode={setMode}
          setState={(data) => success('Update', data)}
          state={state}
        />
        <ViewCarsSetting
          create
          mode={mode}
          setState={(data) => success('Update', data)}
          state={state}
        />
      </ScrollView>
      <ButtonComponents
        disabled={disabled(state)}
        dispatch={(e) => success(R.path(['type'])(e), state)}
      />
      <Modal isVisible={R.path(['isVisible'])(modalMile)}>
        <ModalMileage modalMile={modalMile} setModalMile={setModalMile} />
      </Modal>
    </View>
  )
};
