/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undefined */
/* eslint-disable react/no-multi-comp */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { InputComponents } from 'utils/component';

const mile = {
  value: ["mile"],
  title: "Error message",
  example: "",
  label: 'mileage',
  path: 'AGENT',
  multiline: true,
  keyboardType: "default",
};

export const ModalMileage = ({ modalMile, setModalMile }) => (
  <ModalDescription style={{ flex: 1, margin: 10 }}>
    <HeaderApp
      empty
      title={i18n.t('mileage')}
    />
    <View style={{ flex: 1, padding: 10 }}>
      <InputComponents
        {...mile}
        onChangeText={(change) => setModalMile(
          R.assocPath(mile.value, change, modalMile)
        )}
        storage={modalMile}
      />
    </View>
    <RowComponents>
      {R.addIndex(R.map)((func, key) => (
        <TouchesState
          key={key}
          color={R.equals("CLOSED", func) ? "VeryRed" : "Yellow"}
          flex={1}
          onPress={() => setModalMile({ isVisible: false })}
          title={func}
        />
      ))(["SAVE", "CLOSED"])}
    </RowComponents>
  </ModalDescription>
);