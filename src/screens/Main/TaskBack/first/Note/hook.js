/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { REPAIR_QUERY, UPDATE_REPAIR } from 'gql/task';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

export const DesribeHooks = () => {
    const { task } = useSelector(state => R.path(['task'])(state), shallowEqual);
    const [update, setUpdate] = React.useState(true);
    const dispatchRedux = useDispatch();

    const momentStreets = ({ data, mode }) => R.cond([
        [R.equals('update'), () => {
            dispatchRedux({ type: 'TASK_UPDATE', payload: data });
            return showMessage({
                message: R.toString("Коментарий исправлен"),
                description: "Success",
                type: "success",
            });
        }]
    ])(mode);

    const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            if (data) {
                momentStreets({ mode: 'update', data: R.path([R.keys(data)])(data) })
            }
        }
    });

    const [descriptionUpdate] = useMutation(UPDATE_REPAIR, {
        onCompleted: (data) => {
            if (data) {
                loadgreeting({ variables: { where: { id: { eq: R.path(['id'])(task) } } } })
            }
        }
    });

    const redux = (data) => {
        setUpdate(false);
        dispatchRedux({
            type: "TASK_UPDATE",
            payload: R.mergeAll([task, {
                note: data
            }])
        })
    };

    const describeUpdate = () => {
        setUpdate(true);
        descriptionUpdate({
            variables: {
                input: {
                    id: R.path(['id'])(task),
                    note: R.path(['note'])(task)
                }
            }
        })
    };

    return [update, redux, describeUpdate]
};
