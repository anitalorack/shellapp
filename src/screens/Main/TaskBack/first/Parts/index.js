/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { EmptyCard } from 'screens/Main/TaskBack/first/Calendar/EmptyCard';
import { AppScreen } from 'screens/Main/TaskBack/first/Parts/component/Main/AppScreen';
import { ModalStage } from 'screens/Main/TaskBack/first/Parts/component/Modal';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { isOdd, statusTaskChecker } from 'utils/helper';
import ADD from 'assets/svg/add';

export const Parts = () => {
  const [state, setState] = React.useState({ isVisible: false });
  const { task } = useSelector(state => state.task, shallowEqual);

  return (
    <View style={{ flex: 1 }}>
      <HeaderApp
        button="withGarage"
        dispatch={() => setState(R.assocPath(['isVisible'], true, {}))}
        empty={!R.includes('parts')(R.path(['edit'])(statusTaskChecker(R.path(['status'])(task))))}
        svg={ADD}
        title={`${i18n.t("Material/Coast")} - ${R.defaultTo(0)(R.length(R.path(['parts'])(task)))}`}
      />
      <View style={{ flex: 1, margin: 10, }}>
        {isOdd(R.path(['parts'])(task)) ? <EmptyCard /> : <AppScreen />}
      </View>
      <Modal isVisible={R.path(['isVisible'])(state)}>
        <ModalStage
          setState={setState}
          storage={R.path(['comp'])(state)}
        />
      </Modal>
    </View >
  )
};