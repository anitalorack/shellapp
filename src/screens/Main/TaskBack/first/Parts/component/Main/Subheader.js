/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { Colors } from 'react-native-paper';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { ModalStage } from 'screens/Main/TaskBack/first/Parts/component/Modal';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd, priceModed } from 'utils/helper';
import Rub from 'assets/svg/rub';
import { SvgXml } from 'react-native-svg';

export const Subheader = ({ state, el }) => {
  const [modal, setModal] = React.useState({ isVisible: false });

  const Title = ({ label }) => (
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="Description"
      style={[styles.container, {
        textAlign: 'center',
        textTransform: 'uppercase',
      }]}
      testID="BlockLaker"
    >
      {i18n.t(label)}
    </LabelText>
  );

  const Value = ({ label, mode }) => (
    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
      <LabelText
        color={ColorCard('classic').font}
        fonts="ShellMedium"
        items="SRDescription"
        style={[styles.container, {
          textAlign: 'center',
          textTransform: 'uppercase',
        }]}
        testID="BlockLaker"
      >
        {label}
      </LabelText>
      {R.equals(mode, true) ? <View /> : <SvgXml height="10" weight="10" xml={Rub} />}
    </View>

  );

  const modemd = (x) => R.cond([
    [R.equals('unitCount'), R.always(R.path(['unitCount'])(el))],
    [R.equals('discountCoast'), R.always(priceModed(
      R.defaultTo(0)(R.product([
        R.pipe(R.paths([['unitCount'], ['depotItem', 'priceOut', 'amount']]), R.product)(el),
        R.add(R.path(['discount', 'percent'])(el) / 100, R.path(['discount', 'amount'])(el))
      ]))
    ))],
    [R.equals('unitPrice'), R.always(priceModed(R.path(['depotItem', 'priceOut', 'amount'])(el)))],
    [isOdd, R.always("")],
  ])(x);

  if (R.equals(state, true)) return (
    <View style={{ flex: 1 }}>
      <TouchableOpacity
        onPress={() => setModal({ isVisible: true })}
      >
        <CardComponents
          style={{ backgroundColor: Colors.grey300 }}
        >
          <RowComponents>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              {Title({ label: 'qty' })}
              {Value({ label: modemd('unitCount'), mode: true })}
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              {Title({ label: 'unitPrice' })}
              {Value({ label: modemd('unitPrice') })}
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              {Title({ label: 'discountCoast' })}
              {Value({ label: modemd('discountCoast') })}
            </View>
          </RowComponents>
        </CardComponents>
      </TouchableOpacity>
      <Modal isVisible={R.path(['isVisible'])(modal)}>
        <ModalStage
          setState={setModal}
          storage={el}
        />
      </Modal>
    </View>
  );

  return (<View />)
};