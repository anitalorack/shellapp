/* eslint-disable radix */
/* eslint-disable newline-before-return */
import Rub from 'assets/svg/rub';
import React from 'react';
import { StyleSheet } from 'react-native'
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, priceModed } from 'utils/helper';
import * as R from 'ramda'

export const totalPartsPrice = (storage) => {
  const total = R.path(['unitCount'])(storage) * R.path(['depotItem', "priceOut", 'amount'])(storage);

  if (R.path(['discount', 'percent'])(storage)) {
    return total * (1 - parseInt(R.path(['discount', 'percent'])(storage)) / 100)
  }
  if (R.path(['discount', 'amount'])(storage)) {
    return R.subtract(total, R.path(['discount', 'amount'])(storage))
  }

  return R.defaultTo("0.00")(total)
};

export const TotalCount = ({ storage }) => {
  return (
    <CardComponents style={[styles.shadow]}>
      <RowComponents style={{ justifyContent: 'center' }}>
        <LabelText
          color={ColorCard('classic').font}
          fonts="ShellBold"
          items="CLabel"
          style={[styles.container, TotalStyle.items]}
          testID="BlockLaker"
        >
          {priceModed(totalPartsPrice(storage))}
        </LabelText>
        <SvgXml height="15" weight="15" xml={Rub} />
      </RowComponents>
    </CardComponents>
  )
};

const TotalStyle = StyleSheet.create({
  items: {
    textAlign: 'center',
    textTransform: 'uppercase',
    margin: 2
  }
});