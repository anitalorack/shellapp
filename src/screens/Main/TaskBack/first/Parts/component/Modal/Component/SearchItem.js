/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-key */
/* eslint-disable radix */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, TouchableOpacity } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { SppScreen } from 'screens/Main/Garage/Query/components/SppScreen';

export const SearchItem = ({ dispatch }) => {
  const [state, setState] = React.useState('Catalog');
  const success = (e) => dispatch({
    type: R.path(['type'])(e),
    payload: R.mergeAll([
      R.pipe(
        R.path(['payload']),
        R.omit(['id'])
      )(e),
      R.assocPath(['unitCount'], 1)({ depotItem: R.head(R.path(['payload', 'depotItems'])(e)) })
    ])
  });

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <CardComponents style={{ height: 40 }}>
        <RowComponents style={{ flex: 1 }}>
          {R.map(x => (
            <TouchableOpacity
              key={x}
              onPress={() => setState(x)}
              style={R.equals(state, x) ? { flex: 1, borderBottomWidth: 2, borderColor: 'red' } : { flex: 1 }}
            >
              <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={[styles.container, { textAlign: 'center' }]}
                testID="BlockLaker"
              >
                {i18n.t(x)}
              </LabelText>
            </TouchableOpacity>
          ))(['SearchInGarage', 'Catalog'])}
        </RowComponents>
      </CardComponents>
      {R.equals(state, 'SearchInGarage') ?
        <VisualTable
          button={false}
          dispatch={success}
          mode="stock"
          style={{ flex: 1 }}
        /> :
        <SppScreen
          dispatch={success}
          style={{ flex: 1 }}
        />}
    </ScrollView>
  )
};
