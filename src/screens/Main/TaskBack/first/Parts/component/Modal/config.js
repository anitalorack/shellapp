import * as R from 'ramda'

export const partInput = [
    {
        id: "ГОД",
        path: 'unitCount',
        keyboardType: "numeric",
        label: 'unitCount',
        title: "Error message",
        value: ["unitCount"],
        maxLength: 4,

        testID: "Year",
    },
    {
        id: "ПРОБЕГ",
        path: 'priceOut',
        keyboardType: "numeric",
        label: 'unitPrice',
        title: "Error message",
        value: ['depotItem', "priceOut", 'amount'],
        string: true,
        price: true,
        testID: "Mileage",
    }
]

export const discount = [
    {
        id: "ЦВЕТ",
        path: 'discountPercent',
        keyboardType: "default",
        label: 'discountPersent',
        title: "Error message",
        value: ['discount', 'percent'],
        mask: '[00]',
    },
    {
        id: "ГосНомер",
        path: 'discountAmount',
        keyboardType: "default",
        label: 'discountCoast',
        title: "Error message",
        value: ['discount', 'amount'],
        maxLength: 6,
        price: true,
    },

];

export const reducer = (state, action) => {
    switch (action.type) {
        case "unitCount":
            return R.mergeAll([state, action.payload]);
        case "priceOut":
            return R.mergeAll([state, {
                depotItem: {
                    id: R.pipe(R.path(['depotItems']), R.head, R.path(['id']))(state),
                    amount: R.pipe(R.path(['depotItems']), R.head, R.path(['amount']))(state),
                    ...R.path(['payload', 'depotItem'])(action)
                }
            }]);
        case "discountPercent":
            return R.mergeAll([state, action.payload]);
        case "discountAmount":
            return R.mergeAll([state, action.payload]);
        case "CREATE":
            return R.mergeAll([state, action.payload, { depotItem: R.pipe(R.path(['payload', 'depotItems']), R.head)(action) }]);
        case "Initilal":
            return {};
        case "ChangeArticle":
            return R.omit(['depotItem', 'measure', 'depotId', 'id', 'name', 'oem', 'brand', 'article'])(state);
        default:
            return state
    }
};

export const dataInput = ({ redux }) => ({
    unitCount: parseFloat(R.path(['unitCount'])(redux)),
    unitPrice: {
        amount: parseFloat(R.defaultTo('0')(R.path(['depotItem', 'priceOut', 'amount'])(redux)))
    },
    discount: {
        percent: parseFloat(R.defaultTo('0')(R.path(['discount', 'percent'])(redux))),
        amount: parseFloat(R.defaultTo('0')(R.path(['discount', 'amount'])(redux)))
    },
    id: R.path(['id'])(redux),
    depotItemId: R.path(['depotItem', 'id'])(redux)
});