/* eslint-disable no-else-return */
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { reducer, dataInput } from 'screens/Main/TaskBack/first/Parts/component/Modal/config'
import {
  CREATE_REPAIR_PARTS, UPDATE_REPAIR_PARTS, DESTROY_REPAIR_PARTS, REPAIR_QUERY
} from 'gql/task'

export const PartsHooks = ({ storage }) => {
  const [redux, setRedux] = React.useReducer(reducer, storage);
  const { task } = useSelector(state => state.task, shallowEqual);
  const dispatchRedux = useDispatch();

  const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
    fetchPolicy: ''network- only'',
    onCompleted: (data) => {
      if (data) {
        dispatchRedux({
          type: 'TASK_UPDATE',
          payload: R.path([R.keys(data)])(data)
        });

        return showMessage({
          message: "Успешно Загруженно",
          description: "Success",
          type: "Info",
        });
      }
    },
    onError: (data) => showMessage({
      message: R.toString(data),
      description: "Error",
      type: "danger",
    })
  });

const [updateQuery] = useMutation(UPDATE_REPAIR_PARTS, {
  onCompleted: (data) => {
    if (data) {
      loadgreeting({
        variables: {
          where: {
            id: {
              eq: R.path(['id'])(task)
            }
          }
        }
      })
    }
  },
  onError: (data) => {
    showMessage({
      message: R.toString(data),
      description: "Error",
      type: "danger",
    })
  }
});

const [createQuery] = useMutation(CREATE_REPAIR_PARTS, {
  onCompleted: (data) => {
    if (data) {
      loadgreeting({
        variables: {
          where: {
            id: {
              eq: R.path(['id'])(task)
            }
          }
        }
      })
    }
  },
  onError: (data) => {
    showMessage({
      message: R.toString(data),
      description: "Error",
      type: "danger",
    })
  }
});

const [destroyQuery] = useMutation(DESTROY_REPAIR_PARTS, {
  onCompleted: (data) => {
    if (data) {
      loadgreeting({
        variables: {
          where: {
            id: {
              eq: R.path(['id'])(task)
            }
          }
        }
      })
    }
  },
  onError: (data) => showMessage({
    message: R.toString(data),
    description: "Error",
    type: "danger",
  })
});

const request = (mode, data) => {
  return R.cond([
    [R.equals('create'), () => {
      if (R.path(['id'])(task)) {
        return createQuery({
          variables: {
            input: R.assocPath(['repairId'],
              R.path(['id'])(task),
              R.omit(['id'])(dataInput({
                redux: data, task
              })))
          }
        })
      }

      return dispatchRedux({
        type: 'TASK_UPDATE',
        payload: R.mergeAll([task, {
          parts: R.prepend(data)(R.path(['parts'])(task))
        }])
      })
    }],
    [R.equals('destroy'), () => {
      if (R.path(['id'])(task) && R.path(['id'])(data)) {
        return destroyQuery({
          variables: {
            id: R.path(['id'])(data)
          }
        })
      }

      return dispatchRedux({
        type: 'TASK_UPDATE',
        payload: R.mergeAll([task, {
          parts: R.without([data])(R.path(['parts'])(task))
        }])
      })
    }],
    [R.equals('update'), () => {
      if (R.path(['id'])(task)) {
        return updateQuery({
          variables: {
            input: R.omit(['depotItemId'])(dataInput({
              redux: data, task
            }))
          }
        })
      }

      return dispatchRedux({
        type: 'TASK_UPDATE',
        payload: R.mergeAll([task, {
          parts: R.append(data)(R.without([storage])(R.path(['parts'])(task)))
        }])
      })
    }],
    [R.equals('fetch'), () => loadgreeting({
      variables: {
        where: {
          id: {
            eq: R.path(['id'])(task)
          }
        }
      }
    })],
  ])(mode)
};

return [redux, (mode, data) => request(mode, data), setRedux]
};
