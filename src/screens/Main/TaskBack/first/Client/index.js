/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { EmployeeComponents } from 'screens/Main/AddGoods/components/EmployeeComponents';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { ClientModal } from 'screens/Main/TaskBack/first/Client/Component/ClientModal';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, statusTaskChecker } from 'utils/helper';
import { CheckerState } from 'screens/Main/TaskBack/first/Client/Component/CheckerState';

export const Client = () => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const [state, setState] = React.useState({ isVisible: false });
    const list = R.path(['id'])(task) ? ['owner', 'author', 'client', 'performer'] : ['owner', 'author', 'client'];

    const mode = (x) => {
        if (R.includes(x)(R.path(['edit'])(statusTaskChecker(R.path(['status'])(task))))) {
            return x
        }

        return R.path(['mode'])(state)
    };

    return (
        <View>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="CLabel"
                style={[styles.container, { textAlign: 'left', flex: 1, padding: 10 }]}
            >
                {i18n.t('Employeers')}
            </LabelText>
            <View style={styles.line} />
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={key}
                    style={{ flex: 1 }}
                >
                    <EmployeeComponents
                        actor={R.path([x])(task)}
                        dispatch={() => setState({ mode: x, isVisible: true })}
                        HEADER={x}
                        mode={mode(x)}
                    />
                    <CheckerState mode={x} />
                </View>
            ))(list)}
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ClientModal
                    mode={R.path(['mode'])(state)}
                    setState={setState}
                />
            </Modal>
        </View>
    )
};