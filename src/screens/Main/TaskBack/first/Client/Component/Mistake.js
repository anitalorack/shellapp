/* eslint-disable no-undefined */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const mistake = (x) => {
    const isOdd = (x) => R.pipe(R.includes(x))(['', null, undefined, ' ']);

    return [
        isOdd(R.path(['name', 'first'])(x)),
        isOdd(R.path(['name', 'last'])(x)),
        isOdd(R.path(['phone'])(x)),
    ]
};

export const obj = [
    'notFirst',
    'notLast',
    'notPhone',
];

export const ClientMistake = ({ state }) => (
    <View>
        {R.addIndex(R.map)((x, key) => {
            if (R.equals(x, false)) {
                return (<View />)
            }

            return (
                <View
                    key={key}
                    style={stylesM.items}
                >
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                    >
                        {i18n.t(obj[key])}
                    </LabelText>
                </View>
            )
        })(mistake(state))}
    </View>
);

const stylesM = StyleSheet.create({
    items: {
        height: 35,
        borderLeftWidth: 2,
        paddingLeft: 10,
        borderColor: 'red',
        margin: 2
    }
});
