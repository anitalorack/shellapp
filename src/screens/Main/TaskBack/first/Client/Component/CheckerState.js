/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Checkbox } from 'react-native-paper';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const CheckerState = ({ mode }) => {
    const [check, setCheck] = React.useState(false);
    const checkbox = check ? 'checked' : 'unchecked';
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchStorage = useDispatch();

    const success = () => {
        dispatchStorage({
            type: "TASK_UPDATE",
            payload: R.mergeAll([task, R.assocPath(['client'], R.path(['owner'])(task))({})])
        });

        return setCheck(true)
    };

    const checkerState = (task) => R.all(
        R.equals(true))([
            R.equals(mode, 'client'),
            R.equals(check, false),
            R.pipe(R.path(['owner']), R.has('phone'))(task),
            R.pipe(R.path(['client']), R.has('phone'), R.not)(task),
        ]);

    if (checkerState(task)) return (
        <TouchableOpacity
            onPress={success}
            style={{ flexDirection: 'row', alignItems: 'center' }}
        >
            <Checkbox
                status={checkbox}
            />
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellLight"
                items="Description"
                testID="BlockLaker"
            >
                Тот же, что и владелец
            </LabelText>
        </TouchableOpacity>
    );

    return (
        <View />
    )
};