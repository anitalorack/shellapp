/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { AgentaCalendar } from 'screens/Main/Calendar/AgentaCalendar';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { TableScreenCalendar } from 'screens/Main/TaskBack/first/Calendar/TableScreenCalendar'

export const CalendarTask = () => {
    const [modal, setModal] = React.useState({ isVisble: false });
    const success = (mode) => R.cond([
        [R.equals('OPEN_MODAL'), () => setModal(R.assocPath(['isVisible'], true)({}))],
        [R.equals("CLOSE_MODAL"), () => setModal(R.assocPath(['isVisible'], false)({}))],
    ])(mode);

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1, marginLeft: 10, marginRight: 10 }}>
                <HeaderApp
                    button="BronePost"
                    dispatch={() => success("OPEN_MODAL")}
                    title={i18n.t("PostTimeupload")}
                />
                <TableScreenCalendar />
            </View>
            <Modal isVisible={R.path(['isVisible'])(modal)}>
                <ModalDescription style={{ padding: 0, flex: 1 }}>
                    <HeaderApp
                        button="CLOSED"
                        dispatch={() => success("CLOSE_MODAL")}
                        header
                        title={i18n.t("Postupload")}
                    />
                    <View style={{ flex: 1 }}>
                        <AgentaCalendar style={{ flex: 1 }} />
                    </View>
                </ModalDescription>
            </Modal>
        </View>
    )
};