/* eslint-disable react/no-multi-comp */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelTextMenu } from 'screens/Main/TaskBack/first/StatusChecker/config';
import i18n from 'localization'
import { shallowEqual, useSelector } from 'react-redux';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm'
import {
    Menu, MenuOption, MenuOptions, MenuTrigger
} from 'react-native-popup-menu';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { isOdd, statusTaskChecker } from 'utils/helper';
import { StatusUpdateHook } from './StatusUpdateHook'

// TODO REFACTORING TO DROPDOWN

export const MenuDropper = () => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const [request] = StatusUpdateHook();

    const title = "DELETESTATUSTASK";
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => request(e.params)],
    ])(R.path(['type'])(e));

    return (
        <CardComponents
            style={[styles.shadow, {
                flex: 1,
            }]}
        >
            <Menu>
                <MenuTrigger>
                    <LabelTextMenu
                        style={{ fontFamily: 'ShellMedium', textTransform: 'uppercase', justifyContent: 'flex-start' }}
                    >
                        {i18n.t(R.path(['status'])(task))}
                    </LabelTextMenu>
                </MenuTrigger>
                <MenuOptions>
                    {R.map(x => {
                        if (isOdd(x)) return <View />;

                        return (
                            <MenuOption
                                key={x}
                                onSelect={() => {
                                    alertDestroyConfirm({ alertReq, title, params: x })
                                }}
                            >
                                <LabelTextMenu
                                    style={{ fontFamily: 'ShellLight', fontSize: 14, flex: 1 }}
                                >
                                    {i18n.t(x)}
                                </LabelTextMenu>
                            </MenuOption>
                        )
                    })(R.path(['status'])(statusTaskChecker(R.path(['status'])(task))))}
                </MenuOptions>
            </Menu>
        </CardComponents >
    )
};