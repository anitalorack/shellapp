/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import Rub from 'assets/svg/info';
import { SvgXml } from 'react-native-svg';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { InfoModal } from 'screens/Main/TaskBack/first/helper/InfoModal';
import { qweqweq } from 'screens/Main/TaskBack/first/StatusChecker/config';
import { MenuDropper } from 'screens/Main/TaskBack/first/StatusChecker/MenuDropper';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization'
import { TouchesState } from 'styled/UIComponents/UiButtom';

export const StatusChecker = () => {
    const [state, setState] = React.useState({ isVisible: false });

    return (
        <RowComponents style={{ margin: 10 }}>
            <MenuDropper />
            <View style={[styles.shadow, qweqweq.qweq, { justifyContent: 'center', alignItems: 'center' }]}>
                <TouchableOpacity
                    onPress={() => setState(R.assocPath(['isVisible'], true, state))}
                >
                    <SvgXml
                        height="20"
                        style={[styles.items, {
                            paddingLeft: 10,
                            justifyContent: 'center'
                        }]}
                        weight="100"
                        xml={Rub}
                    />
                </TouchableOpacity>
            </View>
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ModalDescription style={{ padding: 0, flex: 1 }}>
                    <View style={{ margin: 10 }}>
                        <LabelText
                            color={ColorCard('error').font}
                            fonts="ShellBold"
                            items="CLabel"
                            style={[styles.container, {
                                textAlign: 'left',
                                textTransform: 'uppercase',
                            }]}
                        >
                            {i18n.t('StatusUpdate')}
                        </LabelText>
                    </View>
                    <InfoModal />
                    <TouchesState
                        color="Yellow"
                        flex={1}
                        onPress={() => setState(R.assocPath(['isVisible'], false, state))}
                        title="BACK"
                    />
                </ModalDescription>
            </Modal>
        </RowComponents >
    )
};
