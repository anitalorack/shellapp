/* eslint-disable react/no-multi-comp */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import * as R from 'ramda';
import React from 'react';
import { showMessage } from "react-native-flash-message";
import { View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelTextMenu } from 'screens/Main/TaskBack/first/StatusChecker/config';
import i18n from 'localization'
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { CHANGE_STATUS, REPAIR_QUERY } from 'gql/task';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import {
    Menu, MenuOption, MenuOptions, MenuTrigger
} from 'react-native-popup-menu';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { isOdd, statusTaskChecker } from 'utils/helper';

export const StatusUpdateHook = () => {
    const { task } = useSelector(redux => redux.task, shallowEqual);
    const dispatchRedux = useDispatch();

    const momentStreets = ({ data, mode }) => R.cond([
        [R.equals('error'), () => {
            return showMessage({
                message: R.toString(data),
                description: "Error",
                type: "danger",
            });
        }],
        [R.equals('update'), () => {
            dispatchRedux({ type: 'TASK_UPDATE', payload: data })
        }]
    ])(mode);

    const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
        fetchPolicy: "network-only",
        onError: (data) => momentStreets({ mode: 'error', data }),
        onCompleted: (data) => {
            if (data) {
                momentStreets({ mode: 'update', data: R.path([R.keys(data)])(data) })
            }
        }
    });

    const [changeStatusRepairs] = useMutation(CHANGE_STATUS, {
        onError: (data) => momentStreets({ mode: 'error', data }),
        onCompleted: (data) => {
            if (data) {
                loadgreeting(R.assocPath(['variables', 'where', 'id', 'eq'], R.path(['id'])(task))({}));

                return showMessage({
                    message: R.toString("Статус изменен"),
                    description: "Success",
                    type: "success",
                });
            }
        }
    });

    return [(data) => changeStatusRepairs({
        variables: {
            input: {
                id: R.path(['id'])(task),
                status: data
            }
        }
    })]
};
