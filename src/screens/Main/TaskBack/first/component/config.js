import * as R from 'ramda';

export const reducer = (state, action) => {
    switch (action.type) {
        case "Scroll":
            return R.assocPath([action.type], action.payload)(state);
        case "Query":
            return R.assocPath([action.type], action.payload)(state);
        case "screen":
            return R.assocPath([action.type], action.payload)(state);
        case "Status":
            return R.assocPath(['Query'], action.payload)(state);
        case 'vehicle':
            return { Query: R.mergeAll([R.path(['Query'])(state), action.payload]) };
        case 'client':
            return { Query: R.mergeAll([R.path(['Query'])(state), action.payload]) };
        case 'owner':
            return { Query: R.mergeAll([R.path(['Query'])(state), action.payload]) };
        case "MERGE":
            return { Query: R.assocPath(['parts'], action.payload)(R.path(['Query'])(state)) };
        case "description":
            return R.assocPath(['Query'], action.payload)(state);
        default:
            return state
    }
};

export const dataDoc = R.cond([
    [R.equals("DC"), R.always('repair')],
    [R.equals("CD"), R.always('inspection')],
    [R.equals("HD"), R.always('workOrder')],
    [R.equals("GX"), R.always('act')],
]);

export const WorkDocuments = [
    { value: "DC", name: 'DC', },
    { value: "CD", name: 'CD', },
    { value: "HD", name: 'HD', },
    { value: "GX", name: 'GX', status: `IC` },
];