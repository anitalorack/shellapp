/* eslint-disable react/no-multi-comp */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { CurrentSelect } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect';
// import { currentSelect } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/config';
import { ViewCarsSetting } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/ViewCarsSetting';
import { ButtonComponents } from 'screens/Main/TaskBack/first/helper/ButtonComponents';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { isOdd } from 'utils/helper';
import { LoadingScreenHook, disabled } from 'screens/Main/TaskBack/Input/createScreen/hooks';

export const CCurrentSelect = () => {
  const { task } = useSelector(state => state.task, shallowEqual);

  const obj = () => {
    if (R.path(['id'])(task)) return { data: ['vin', 'frameNumber'] };
    if (R.path(['vehicle'])(task)) {
      const path = R.pipe(
        R.path(['vehicle']),
        R.pick(['vin', 'frameNumber', 'plate']),
        R.keys,
      )(task);

      if (R.isEmpty(path)) return { data: ['vin', 'frameNumber'] };

      return { data: ['vin', 'frameNumber'] }
    }

    return { data: ['vin', 'plate'] }
  };

  return obj()
};

export const CarsEdit = ({ dispatch, create }) => {
  const { data } = CCurrentSelect();
  const [update, request] = LoadingScreenHook();
  const { task } = useSelector(state => state.task, shallowEqual);
  const [mode, setMode] = React.useState(data);
  const [state, setState] = React.useState(R.path(['vehicle'])(task));
  const modic = R.cond([
    [isOdd, R.always({ title: 'ADDEDCARS', bool: false })],
    [R.T, R.always({ title: 'EDITCARS', bool: true })],
  ])(R.path(['id'])(task));

  React.useEffect(() => {
    setState(R.path(['vehicle'])(task));
    setMode(['vin']);
    if (R.equals(update, 'Close')) {
      dispatch({ isVisiable: false })
    }
  }, [task, update]);

  const success = (e, data) => {
    R.cond([
      [R.equals('BACK'), () => dispatch({ isVisiable: false })],
      [R.equals('added'), () => {
        request('Update_Cars', data)
      }],
      [R.equals('Update'), () => setState(R.mergeAll([state, data]))]
    ])(e)
  };

  return (
    <View style={{ flex: 1 }}>
      <HeaderApp
        empty
        style={{ margin: 5 }}
        title={i18n.t(R.path(['title'])(modic))}
      />
      <ScrollView
        style={{ padding: 10, paddingTop: 0 }}
      >
        <CurrentSelect
          create={create}
          mode={mode}
          setMode={setMode}
          setState={(e) => success('Update', e)}
          state={state}
        />
        <ViewCarsSetting
          mode={mode}
          setState={(e) => success('Update', e)}
          state={state}
        />
      </ScrollView>
      <ButtonComponents
        disabled={disabled(state)}
        dispatch={(e) => success(R.path(['type'])(e), state)}
      />
    </View >
  )
};