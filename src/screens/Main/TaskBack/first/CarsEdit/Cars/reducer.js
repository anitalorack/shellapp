/* eslint-disable no-case-declarations */
/* eslint-disable newline-before-return */
/* eslint-disable newline-after-var */
/* eslint-disable complexity */
import * as R from 'ramda';

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'vehicleModifications':
            const vehicleModifications = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload.vehicle]), {});
            return R.mergeAll([state, vehicleModifications]);
        case 'vehicleModels':
            const vehicleModels = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload.vehicle]), {});
            return R.mergeAll([state, vehicleModels]);
        case 'vehicleManufacturers':
            const vehicleManufacturers = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload.vehicle]), {});
            return R.mergeAll([state, vehicleManufacturers]);
        case 'vehicle':
            const vehicle = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, vehicle]);
        case 'plate':
            const plate = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, plate]);
        case 'vin':
            const vin = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, vin]);
        case 'year':
            const year = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, year]);
        case 'model':
            const model = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, model]);
        case 'modification':
            const modification = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, modification]);
        case 'frameNumber':
            const frameNumber = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, frameNumber]);
        case 'color':
            const color = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, color]);
        case 'mileage':
            const mileage = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, mileage]);
        case 'first':
            const NameInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(NameInput), state);
        case 'last':
            const LastInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(LastInput), state);
        case 'middle':
            const MiddleInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(MiddleInput), state);
        case 'phone':
            return R.mergeAll([state, action.payload]);
        case 'Update':
            return R.mergeAll([state, action.payload]);
        case 'Vehicle':
            return R.assocPath(['vehicle'], action.payload, state);
        case 'Data':
            const keys = R.keys(action.payload);

            if (R.equals(keys, ['vehicleManufacturers'])) {
                return R.assocPath(['lib'], action.payload, R.omit(['libModels', 'libModifications', 'vehicleModels', 'vehicleModifications'])(state));
            }
            if (R.equals(keys, ['vehicleModels'])) {
                return R.assocPath(['libModels'], action.payload, R.omit(['libModifications', 'vehicleModifications'])(state));
            }
            if (R.equals(keys, ['vehicleModifications'])) {
                return R.assocPath(['libModifications'], action.payload, state);
            }
            return state;
        default:
            return state
    }
};
