/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { TableComponent } from 'screens/Main/CarsScreen/Preview/TableComponent';
import { CarsEdit } from 'screens/Main/TaskBack/first/CarsEdit/Cars';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { statusTaskChecker } from 'utils/helper';
import { uiColor } from 'styled/colors/uiColor.json';
import ADD from 'assets/svg/edit';

export const TableVehicle = () => {
    const [state, setState] = React.useState({ isVisible: false });
    const { task } = useSelector(element => element.task, shallowEqual);

    return (
        <View style={TableStyle.ite} >
            <HeaderApp
                button="EDIT"
                dispatch={() => setState(R.assocPath(['isVisible'], true, state))}
                empty={!R.includes('vehicle')(R.path(['edit'])(statusTaskChecker(R.path(['status'])(task))))}
                style={{ margin: 0 }}
                svg={ADD}
                title={i18n.t("ListCar")}
            />
            <TableComponent
                storage={{ query: R.path(['vehicle'])(task) }}
            />
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ModalDescription style={{ padding: 0, flex: 1 }}>
                    <CarsEdit
                        dispatch={() => setState(R.assocPath(['isVisible'], false, state))}
                    />
                </ModalDescription>
            </Modal>
        </View>
    )
};

export const TableStyle = StyleSheet.create({
    line: {
        marginTop: 15,
        marginBottom: 15,
        borderColor: uiColor.Mid_Grey,
        borderWidth: 0.2,
        marginLeft: 0,
        marginRight: 0,
    },
    card: {
        margin: 2,
        backgroundColor: uiColor.Very_Pole_Grey,
        padding: 4
    },
    items: {
        textAlign: 'left',
        flex: 1,
        textTransform: 'uppercase',
        marginLeft: 10,
        marginRight: 10,
    },
    ite: {
        marginTop: 10,
        marginBottom: 10
    }
});
