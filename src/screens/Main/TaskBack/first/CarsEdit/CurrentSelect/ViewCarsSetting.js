/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { qweqwe } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/config';
import { InputComponents } from 'utils/component';
import { CarsDropDown } from 'utils/support/CarsDropDown';
import { VehicleMistake } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/Mistake'

export const ViewCarsSetting = ({ setState, state }) => {
  const changeText = ({ change, items }) => setState(R.assocPath([items.label], change)(state));

  return (
    <View
      style={[{ margin: 2, padding: 4 }]}
    >
      <CarsDropDown
        dispatch={(e) => setState(R.mergeAll([state, e]))}
        storage={{ modification: R.path(['modification'])(state) }}
      />
      <View>
        {R.addIndex(R.map)((items, key) => (
          <InputComponents
            key={key}
            {...items}
            onChangeText={(change) => changeText({ change, items })}
            storage={state}
          />
        ))(qweqwe)}
      </View>
      <VehicleMistake state={state} />
    </View>
  )
};