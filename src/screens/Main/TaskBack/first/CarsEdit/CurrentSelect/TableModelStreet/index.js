/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-shadow */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { CCurrentSelect } from 'screens/Main/TaskBack/first/CarsEdit/Cars/index';
import { itemConvState, itemState } from 'screens/Main/TaskBack/first/CarsEdit/CurrentSelect/config';
import { InputComponents } from 'utils/component';

export const TableModelStreet = ({
  state, setState, mode
}) => {
  const { data } = CCurrentSelect();

  const list = R.innerJoin(
    (record, id) => record.path === id,
    R.union(itemState, itemConvState),
    mode
  );

  return (
    <View style={{ flex: 1 }}>
      {R.addIndex(R.map)((x, key) => (
        <InputComponents
          key={R.join('_', ['inputCar', key])}
          {...x}
          onChangeText={(change) => setState(R.assocPath([R.path(['path'])(x)], change)(state))}
          storage={state}
        />))(list)}
    </View>
  )
};