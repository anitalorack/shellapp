/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization';

const mistake = (x) => {
  const isOdd = (x) => R.pipe(R.includes(x))(['', null, undefined, ' ']);
  const checker = (x, value) => isOdd(x) ? false : R.allPass([R.pipe(isOdd, R.not), R.pipe(R.splitEvery(1), R.length, R.equals(value))])(x);

  return [
    isOdd(R.path(['modification', 'model', 'id'])(x)),
    isOdd(R.path(['modification', 'model', 'manufacturer', 'id'])(x)),
    isOdd(R.path(['modification', 'id'])(x)),
    isOdd(R.path(['year'])(x)),
    isOdd(R.path(['plate'])(x)),
    isOdd(R.path(['vin'])(x)),
    // checker(R.path(['frameNumber'])(x), 9),
    isOdd(R.path(['mileage'])(x)),
  ]
};

export const obj = [
  'notmanufacture',
  'notmodels',
  'notmodification',
  'notyear',
  'notplate',
  'notvin',
  'notframeNumber',
  'notmileage',
];

export const VehicleMistake = ({ state }) => (
  <View>
    {R.addIndex(R.map)((x, key) => {
      if (R.equals(x, false)) {
        return (<View />)
      }

      return (
        <View
          key={key}
          style={styles.items}
        >
          <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items="SSDescription"
          >
            {i18n.t(obj[key])}
          </LabelText>
        </View>
      )
    })(mistake(state))}
  </View>
);

const styles = StyleSheet.create({
  items: {
    height: 35,
    borderLeftWidth: 2,
    paddingLeft: 10,
    borderColor: 'red',
    margin: 2
  }
});