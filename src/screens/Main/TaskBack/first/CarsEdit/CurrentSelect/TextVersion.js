import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import i18n from 'localization'

export const TextVersion = ({ version }) => R.cond([
  [R.equals(1), () => (
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="SSDescription"
      style={{ textAlign: 'center', padding: 10 }}
      testID="BlockLaker"
    >
      {i18n.t('TextVersion1')}
    </LabelText >
  )],
  [R.equals(2), () => (
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="SSDescription"
      style={{ textAlign: 'center', padding: 10 }}
      testID="BlockLaker"
    >
      {i18n.t('TextVersion2')}
    </LabelText >
  )],
  [R.equals(3), () => (
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellBold"
      items="SSDescription"
      style={{ textAlign: 'center', padding: 10 }}
      testID="BlockLaker"
    >
      {i18n.t('TextVersion3')}
    </LabelText >
  )],
  [isOdd, () => (<View />)]
])(version);