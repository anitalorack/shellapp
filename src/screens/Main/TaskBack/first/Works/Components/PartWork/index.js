/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { CatalogWork } from 'screens/Main/TaskBack/first/Works/Components/ModalWorkScreen';
import { MyWork } from 'screens/Main/TaskBack/first/Works/Components/MyWork';
import { SelectScreen } from 'screens/Main/TaskBack/first/Works/Components/PartWork/SelectScreen';
import { useWorkHooks } from 'hooks/useWorkHooks';
import { SplashScreen } from 'screens/Splash';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { isOdd } from 'utils/helper';

export const ModalWorkScreen = ({ element, dispatch }) => {
    const [state, fetch, catalog, destroy] = useWorkHooks();
    const [label, setLabel] = React.useState("MyWork");
    const [data, setData] = React.useState(null);

    React.useEffect(() => {
        catalog()
    }, [element]);

    if (isOdd(state)) return (
        <View style={{ flex: 1 }}>
            <CardComponents>
                <HeaderApp
                    dispatch={() => null}
                    empty
                    header
                    title="LOADING"
                />
            </CardComponents>
            <SplashScreen
                state="Loading"
                testID="LOADING"
            />
        </View>
    );

    return (
        <View>
            <CardComponents>
                <HeaderApp
                    dispatch={() => dispatch({ type: "CLOSED" })}
                    header
                    title={i18n.t(label)}
                />
                <SelectScreen
                    label={label}
                    setLabel={setLabel}
                />
            </CardComponents>
            {R.cond([
                [R.equals('CatalogWork'), () => (
                    <CatalogWork
                        dispatch={e => {
                            setData(R.mergeAll([R.omit(['id'])(R.path(['payload'])(e)), R.defaultTo([{}])([data])]));
                            setLabel("MyWork")
                        }}
                        state={state}
                    />
                )],
                [R.equals('MyWork'), () => (
                    <MyWork
                        navigation={(e) => dispatch(e)}
                        state={R.defaultTo(element)(data)}
                    />
                )],
            ])(label)}
        </View>
    )
};