/* eslint-disable react/no-multi-comp */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import { useWorkBankHooks } from 'hooks/useWorkHooks';
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { input, discount } from 'screens/Main/TaskBack/first/Works/Components/MyWork/config';
import { TotalCount } from 'screens/Main/TaskBack/first/Works/Components/MyWork/TotalCount';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { InputComponents } from 'utils/component';
import { RowViewDiscount } from 'styled/UIComponents/RowViewDiscount'
import { isOdd } from 'utils/helper'
import { mistake, WorkMistake } from './Mistake';

export const MyWork = ({ state, navigation }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const [create, update, redux, closed] = useWorkBankHooks({ state, navigation })

    const [addedWork, setAddedWork] = React.useState({
        original: R.reject(isOdd)(state),
        detail: state
    });
    const list = R.path(['id'])(state) ? ['UPDATE', "CLOSED"] : ['ADDED', 'CLOSED'];
    const disabled = R.pipe(R.path(['detail']), mistake, R.uniq, R.equals([false]), R.not)(addedWork);

    const success = ({ type, payload }) =>
        setAddedWork(
            R.mergeAll([
                addedWork,
                R.assocPath(['detail'],
                    R.mergeAll([
                        R.path(['detail'])(addedWork),
                        R.includes('discount')(type) ? {
                            discount: R.mergeAll([
                                R.path(['detail', 'discount'])(addedWork),
                                R.zipObj(R.tail(type), [payload])
                            ])
                        } : R.assocPath(type, payload, {})
                    ])
                )({})
            ])
        );

    const TableComponents = () => (
        <>
            {R.addIndex(R.map)((element, keys) => (
                <InputComponents
                    key={keys}
                    {...element}
                    onChangeText={(change) => success({
                        type: element.value,
                        payload: change
                    })}
                    storage={R.path(['detail'])(addedWork)}
                />
            ))(input)}
        </>
    )

    const successRight = (x) => {
        if (R.equals(x, "CLOSED")) return closed()
        if (R.path(['id'])(task)) {
            if (isOdd(R.path(['original', 'id'])(addedWork))) return create(addedWork)

            return update(addedWork)
        }

        return redux(addedWork)
    }

    const DiscountComponents = () => (
        <>
            {R.addIndex(R.map)((element, keys) => (
                <RowViewDiscount
                    key={keys}
                    items={R.equals(keys, 1)}
                    style={{
                        flex: 1,
                    }}
                >
                    <InputComponents
                        {...element}
                        onChangeText={(change) => success({
                            type: element.value,
                            payload: change
                        })}
                        storage={R.path(['detail'])(addedWork)}
                    />
                </RowViewDiscount>
            ))(discount)}
        </>
    )

    return (
        <View style={{ flex: 1 }}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }} style={{ padding: 10 }}>
                <View>{TableComponents()}</View>
                <View style={{ flexDirection: 'row' }}>
                    {DiscountComponents()}
                </View>
                <TotalCount storage={R.path(['detail'])(addedWork)} />
                <WorkMistake state={R.path(['detail'])(addedWork)} />
            </ScrollView>
            <RowComponents style={{ marginBottom: 10 }}>
                {R.addIndex(R.map)((x, key) => (
                    <View
                        key={R.join('_', [key, 'button'])}
                        style={{ flex: 1 }}
                    >
                        <TouchesState
                            color={R.equals("CLOSED", x) ? "VeryRed" : "Yellow"}
                            disabled={R.equals(x, 'CLOSED') ? false : disabled}
                            flex={1}
                            onPress={() => successRight(x)}
                            title={x}
                        />
                    </View>
                ))(list)}
            </RowComponents>
        </View>
    )
};