/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization';

export const mistake = (x) => {
  const isOdd = (x) => R.pipe(R.includes(x))(['', null, undefined, ' ']);

  return [
    isOdd(R.path(['name'])(x)),
    isOdd(R.path(['timeHrs'])(x)),
    isOdd(R.path(['unitPrice'])(x)),
    isOdd(R.path(['unitCount'])(x)),
  ]
};

export const obj = [
  'notName',
  'notTimeHrs',
  'notUnitPrice',
  'notCount',
];

export const WorkMistake = ({ state }) => (
  <View>
    {R.addIndex(R.map)((x, key) => {
      if (R.equals(x, false)) {
        return (<View />)
      }

      return (
        <View
          key={key}
          style={styles.items}
        >
          <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items="SSDescription"
          >
            {i18n.t(obj[key])}
          </LabelText>
        </View>
      )
    })(mistake(state))}
  </View>
);

const styles = StyleSheet.create({
  items: {
    height: 35,
    borderLeftWidth: 2,
    paddingLeft: 10,
    borderColor: 'red',
    margin: 2
  }
});