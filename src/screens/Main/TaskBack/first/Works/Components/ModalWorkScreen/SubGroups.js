/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { ItemSub } from 'screens/Main/TaskBack/first/Works/Components/ModalWorkScreen/ItemSub'

export const SubGroups = ({ elem, dispatch }) => {
    const [state, setState] = React.useState(null);

    return (
        <View>
            <CardComponents style={[styles.shadow,{margin:1,marginLeft:10,marginRight:10}]}>
                <TouchableOpacity onPress={() => setState(!state)}>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={[styles.container, { textTransform: 'none' }]}
                        testID="BlockLaker"
                    >
                        {elem.name}
                    </LabelText>
                </TouchableOpacity>
                {R.equals(state)(true) ? R.map(el => (
                    <ItemSub
                        key={el.id}
                        dispatch={e => dispatch(e)}
                        el={el}
                    />
                ))(R.path(['subGroups'])(elem)) : <View />}
            </CardComponents>
        </View>
    )
};
