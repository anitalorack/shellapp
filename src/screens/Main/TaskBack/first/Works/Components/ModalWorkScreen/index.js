/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { SubGroups } from 'screens/Main/TaskBack/first/Works/Components/ModalWorkScreen/SubGroups';
import { TouchesState } from 'styled/UIComponents/UiButtom';

export const CatalogWork = ({ state, dispatch }) => (
    <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }} >
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={R.join('_', [key, 'gro'])}
                >
                    {R.addIndex(R.map)((el, key) => (
                        <SubGroups
                            key={R.join('_', [key, 'sub'])}
                            dispatch={e => dispatch(e)}
                            elem={el}
                        />
                    ))(R.path(['groups'])(x))}
                </View>
            ))(R.defaultTo([])(state))}
        </ScrollView>
        <TouchesState
            color="VeryRed"
            flex={0}
            onPress={() => dispatch({ type: "CLOSED" })}
            title="CLOSED"
        />
    </View>
)