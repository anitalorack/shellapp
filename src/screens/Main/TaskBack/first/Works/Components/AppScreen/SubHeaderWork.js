/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import Rub from 'assets/svg/rub';
import { SvgXml } from 'react-native-svg';
import * as R from 'ramda';
import { useLazyQuery } from '@apollo/react-hooks';
import { Colors } from 'react-native-paper';
import React from 'react';
import { TouchableOpacity, View } from 'react-native'
import { ModalWorkScreen } from 'screens/Main/TaskBack/first/Works/Components';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { RowComponents, CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import Modal from 'react-native-modal';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { REPAIR_QUERY } from 'gql/task';
import { showMessage } from 'react-native-flash-message';

export const SubHeaderWork = ({ state, el }) => {
  const [modal, setModal] = React.useState({ isVisible: false });
  const { task } = useSelector(state => state.task, shallowEqual);
  const dispatchRedux = useDispatch();

  const Title = ({ label }) => (
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="Description"
      style={[{
        width: 85,
        minHeight: 25,
        textAlign: 'center',
        textTransform: 'uppercase',
      }]}
      testID="BlockLaker"
    >
      {i18n.t(label)}
    </LabelText>
  );

  const Value = ({ label }) => (
    <RowComponents style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
      <LabelText
        color={ColorCard('classic').font}
        fonts="ShellMedium"
        items="SRDescription"
        style={[styles.container, {
          textAlign: 'center',
          textTransform: 'uppercase',
        }]}
        testID="BlockLaker"
      >
        {label}
      </LabelText>
      <SvgXml height="10" weight="10" xml={Rub} />
    </RowComponents>
  );

  const summary = R.pipe(R.paths([['unitPrice', 'amount'], ['timeHrs'], ['unitCount']]), R.product)(el);
  const discountSummary = summary * (R.pipe(R.path(['discount', 'percent']))(el) / 100);

  const success = (e) => {
    if (R.equals('CLOSED', R.path(['type'])(e))) {
      setModal({ isVisble: false })
    }
  };

  if (R.equals(state, true)) return (
    <View style={{ minHeight: 75, flex: 1 }}>
      <CardComponents
        style={{ backgroundColor: Colors.grey300, flex: 1 }}
      >
        <TouchableOpacity
          onPress={() => setModal({ isVisible: true })}
          style={{ flex: 1 }}
        >
          <RowComponents style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View>
              {Title({ label: 'qty' })}
              {Value({ label: R.path(['unitCount'])(el) })}
            </View>
            <View>
              {Title({ label: 'timeHrs' })}
              {Value({ label: R.pipe(R.paths([['timeHrs'], ['unitCount']]), R.product)(el) })}
            </View>
            <View>
              {Title({ label: 'unitPrice' })}
              {Value({ label: R.pipe(R.paths([['unitPrice', 'amount'], ['timeHrs']]), R.product)(el) })}
            </View>
            <View>
              {Title({ label: 'discountCoast' })}
              {Value({ label: R.defaultTo(0)(R.add(R.path(['discount', 'amount'])(el), discountSummary)) })}
            </View>
          </RowComponents>
        </TouchableOpacity>
      </CardComponents>
      <Modal isVisible={R.path(['isVisible'])(modal)}>
        <ModalDescription style={{ flex: 1 }}>
          <ModalWorkScreen
            dispatch={(e) => success(e)}
            element={el}
          />
        </ModalDescription>
      </Modal>
    </View>

  );

  return (<View />)
};