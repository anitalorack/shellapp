/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
import ADD from 'assets/svg/add';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { shallowEqual, useSelector } from 'react-redux';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { EmptyCard } from 'screens/Main/TaskBack/first/Calendar/EmptyCard';
import { ModalWorkScreen } from 'screens/Main/TaskBack/first/Works/Components';
import { AppScreen } from 'screens/Main/TaskBack/first/Works/Components/AppScreen';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { isOdd, statusTaskChecker } from 'utils/helper';

export const Works = () => {
    const { task } = useSelector(redux => redux.task, shallowEqual);
    const [state, setState] = React.useState({ isVisible: false });
    const [element, setElement] = React.useState({});

    const success = (e) => {
        if (R.equals('CLOSED', R.path(['type'])(e))) {
            setElement({});

            return setState({ isVisible: false });
        }
    };
    const empty = isOdd(R.path(['works'])(task)) ? true : R.includes('works')(R.path(['edit'])(statusTaskChecker(R.path(['status'])(task))));
    const screenLoader = () => {
        const list = isOdd(R.path(['works'])(task)) ? [{ id: 'empty' }] : R.path(['works'])(task);

        return (
            <View
                style={{
                    flex: 1,
                    margin: 10
                }}
            >
                {R.addIndex(R.map)((el, key) => {
                    if (R.equals(R.path(['id'])(el), 'empty')) return (
                        <EmptyCard
                            key={key}
                        />
                    );

                    return (
                        <View
                            key={key}
                        >
                            <AppScreen
                                items={el}
                            />
                        </View>
                    )
                })(list)}
            </View>
        )
    };

    return (
        <View>
            <HeaderApp
                button="ADDED"
                dispatch={(e) => setState({
                    isVisible: true,
                    mode: e
                })}
                empty={!empty}
                popUp
                svg={ADD}
                table={['MyWork', 'CatalogWork']}
                title={`${i18n.t('works')} - ${R.defaultTo(0)(R.length(R.path(['works'])(task)))}`}
            />

            {screenLoader()}
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ModalDescription style={{ flex: 1 }}>
                    <ModalWorkScreen
                        dispatch={success}
                        element={element}
                        mode={R.path(['mode'])(state)}
                    />
                </ModalDescription>
            </Modal>
        </View>
    )
};