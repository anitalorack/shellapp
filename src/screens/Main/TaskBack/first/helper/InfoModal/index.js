/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import * as R from 'ramda';
import React from 'react';
import { View, ScrollView } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization'

const data = [{
    title: 'preorder',
    desc: 'preorderDesc'
}, {
    title: "inWork",
    desc: "inWorkDesc"
}, {
    title: "completed",
    desc: "completedDesc"
}, {
    title: "closed",
    desc: "closedDesc"
}, {
    title: "reconciliation",
    desc: "reconciliationDesc"
}, {
    title: "suspended",
    desc: "suspendedDesc"
}, {
    title: "canceled",
    desc: "canceledDesc"
}];

export const InfoModal = () => (
    <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 10 }}>
        {R.addIndex(R.map)((x, key) => (
            <View
                key={key}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={[styles.container, {
                        textAlign: 'left',
                        textTransform: 'uppercase',
                    }]}
                    testID="BlockLaker"
                >
                    {i18n.t(x.title)}
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={[styles.container, {
                        textAlign: 'left',
                    }]}
                    testID="BlockLaker"
                >
                    {i18n.t(x.desc)}
                </LabelText>
            </View>
        ))(data)}
    </ScrollView >
);