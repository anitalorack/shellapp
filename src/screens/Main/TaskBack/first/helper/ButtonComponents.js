/* eslint-disable radix */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable no-confusing-arrow */
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_REPAIR } from 'gql/task';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { showMessage } from 'react-native-flash-message';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { MainScreen } from 'utils/routing/index.json';

export const ButtonComponents = ({ dispatch, disabled }) => {
    const color = (x) => R.equals(x, "CLOSED") ? "VeryRed" : "Yellow";
    const success = (e) => R.cond([
        [R.equals('SAVE'), () => dispatch({ type: "added" })],
        [R.equals("CLOSED"), () => dispatch({ type: "BACK" })],
    ])(e);

    return (
        <RowComponents>
            {R.map(x => (
                <TouchesState
                    key={x}
                    color={color(x)}
                    disabled={R.equals(x, 'SAVE') ? disabled : false}
                    flex={1}
                    onPress={() => success(x)}
                    title={x}
                />
            ))(['SAVE', 'CLOSED'])}
        </RowComponents>
    )
};

export const ButtonCarsComponent = ({
    state, navigation, dispatch, mode, disabled
}) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const dispatchRedux = useDispatch();
    const ROUTER = MainScreen.Main;

    const [vehicleUpdate] = useMutation(UPDATE_REPAIR, {
        onCompleted: (data) => {
            dispatchRedux({ type: 'TASK_UPDATE', payload: R.path([R.keys(data)])(data) });
            dispatch({ type: "UPDATE" });

            return showMessage({
                message: R.toString(i18n.t('succesAddedTask')),
                description: "Success",
                type: "success",
            });
        },
    });

    const data = {
        id: R.path(['id'])(task),
        vehicle: {
            vin: R.path(['vin'])(state),
            year: parseInt(R.path(['year'])(state)),
            mileage: parseInt(R.path(['mileage'])(state)),
            plate: R.path(['plate'])(state),
            color: R.path(['color'])(state),
            modificationId: parseInt(R.path(['modification', 'id'])(state)),
        }
    };

    const success = (e) => {
        if (R.equals(e, 'ADDED')) return dispatchRedux({
            type: "TASK_UPDATE",
            payload: R.assocPath(['vehicle'], {}, {})
        });
        if (R.equals(e, 'added')) {
            if (R.path(['vehicle'])(task)) dispatch(e);

            return dispatchRedux({
                type: "TASK_UPDATE",
                payload: R.assocPath(['vehicle'], R.omit(['id'])(state), task)
            })
        }
        if (R.equals(e, 'SAVE')) {
            return vehicleUpdate({ variables: { input: data } })
        }
        if (R.equals(e, 'BACK')) {
            if (R.path(['vehicle'])(task)) return dispatch(e)
        }
        if (!R.path(['vehicle'])(task)) return navigation.navigate(ROUTER);

        return dispatch(e)
    };

    const buttonState = () => {
        if (R.path(['id'])(task)) return ["SAVE", 'CLOSED'];
        if (R.length(mode) == 2) return ['ADDED', 'CLOSED'];

        return ["added", 'BACK']
    };

    return (
        <RowComponents>
            {R.map(func => (
                <TouchesState
                    key={func}
                    color={R.includes(func, ['BACK', 'CLOSED']) ? "VeryRed" : "Yellow"}
                    disabled={R.equals(func, 'added') ? disabled : false}
                    flex={1}
                    onPress={() => success(func)}
                    title={func}
                />
            ))(buttonState())}
        </RowComponents>
    )
};