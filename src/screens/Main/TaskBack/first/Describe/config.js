import i18n from 'localization';

export const initialStateState = {
    id: "ИМЯ",
    path: 'description',
    main: true,
    multiline: true,
    scrollEnabled: true,
    example: i18n.t('DescribeTask'),
    keyboardType: "default",
    maxLenght: 250,
    value: ['description'],
    testID: "NameInput",
};
