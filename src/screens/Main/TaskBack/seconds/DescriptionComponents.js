/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { InputComponents } from 'utils/component';

export const initialStateState = {
    id: "ИМЯ",
    path: 'description',
    // main: true,
    multiline: true,
    scrollEnabled: true,

    keyboardType: "default",
    // label: 'description',
    // title: "Error message",
    example: "Объясните клиенту необходимость проведения / установки дополнительных работ / запчастей ",
    value: ['description'],

    maxLength: 250,
    testID: "NameInput",
};

export const Describe = ({ storage, dispatch }) => (
    <View style={{ margin: 10 }}>
        <InputComponents
            {...initialStateState}
            onChangeText={(change) => dispatch({
                type: R.path(['path'])(initialStateState),
                payload: change
            })}
            storage={storage}
        />
    </View>
);