/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import { useLazyQuery } from '@apollo/react-hooks';
import { showMessage } from "react-native-flash-message";
import { REPAIR_QUERY } from 'gql/task';
import * as R from 'ramda';
import React from 'react';
import { useDispatch } from 'react-redux';

export const LoadingScreenHook = ({ route, navigation }) => {
  const dispatchRedux = useDispatch();
  const [update, setUpdate] = React.useState({ data: null, error: null, loading: null });

  const momentStreets = ({ data, mode }) => R.cond([
    [R.equals('data'), () => {
      dispatchRedux({ type: 'TASK_UPDATE', payload: data });
      setUpdate({ data: 'EDITTASK', error: false, loading: false });
      return showMessage({
        message: "Успешно загруженно",
        description: "Success",
        type: "success",
      });
    }],
    [R.equals('loading'), () => setUpdate({ data: null, error: false, loading: true })],
    [R.equals('error'), () => {
      setUpdate({ data: null, error: true, loading: false });
      return showMessage({
        message: R.toString(data),
        description: "Error",
        type: "danger",
      });
    }],
    [R.equals('update'), () => {
      dispatchRedux({ type: 'TASK_UPDATE', payload: data });
    }]
  ])(mode);

  const [loadgreeting] = useLazyQuery(REPAIR_QUERY, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      if (data) {
        momentStreets({ mode: 'data', data: R.path([R.keys(data)])(data) })
      }
    }
  });

  React.useEffect(() => {
    dispatchRedux({ type: 'TASK_UPDATE', payload: {} });
    if (R.isNil(R.path(['params', 'id'])(route))) {
      return setUpdate({ data: 'SearchCars', error: false, loading: false })
    }
    momentStreets({ mode: 'loading' });
    if (R.path(['params', 'id'])(route)) {
      loadgreeting({
        variables: {
          where: { id: { eq: R.path(['params', 'id'])(route) } },
          vehicle: {
            plate: { eq: R.path(['params', 'plate'])(route) },
            vin: { eq: R.path(['params', 'vin'])(route) }
          }
        }
      })
    }
  }, [route, navigation]);

  return update
};
