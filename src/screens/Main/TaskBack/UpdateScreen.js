/* eslint-disable radix */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import { useMutation } from '@apollo/react-hooks';
import { CREATE_TASK } from 'gql/task';
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { showMessage } from "react-native-flash-message";
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { TableVehicle } from 'screens/Main/TaskBack/first/CarsEdit/TableVehicle';
import { Client } from 'screens/Main/TaskBack/first/Client';
import { Describe } from 'screens/Main/TaskBack/first/Describe';
import { Works } from 'screens/Main/TaskBack/first/Works';
import { Parts } from 'screens/Main/TaskBack/first/Parts';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { isOdd } from 'utils/helper';
import { App } from 'screens/Document'
import { useTaskNavigation } from 'hooks/useSingle/useTaskHooks'

export const UpdateScreen = ({ navigation }) => {
  const dispatchRedux = useDispatch();
  const { task } = useSelector(state => state.task, shallowEqual);
  const [Taskagent, _, __] = useTaskNavigation({ navigation })
  const [createTask] = useMutation(CREATE_TASK, {
    onError: (data) => {
      showMessage({
        message: R.toString(data),
        description: "Error",
        type: "danger",
      });
    },
    onCompleted: (data) => {
      dispatchRedux({ type: 'TASK_UPDATE', payload: { ...R.path(R.keys(data), data), task: 'Edit' } });
      showMessage({
        message: "Автомобиль успешно изменен",
        description: "Success",
        type: "success",
      });
      return Taskagent({ type: "Edit", payload: R.path(R.concat(R.keys(data), ['id']), data) })
    }
  });

  const variables = React.useMemo(() => {
    const worksVariables = (x) => R.reject(isOdd)({
      unitCount: parseFloat(R.path(['unitCount'])(x)),
      unitPrice: {
        amount: parseFloat(R.defaultTo('0')(R.path(['unitPrice', 'amount'])(x)))
      },
      timeHrs: parseFloat(R.path(['timeHrs'])(x)),
      discount: {
        percent: parseFloat(R.defaultTo('0')(R.path(['discount', 'percent'])(x))),
        amount: parseFloat(R.defaultTo('0')(R.path(['discount', 'amount'])(x)))
      },
      name: R.path(['name'])(x),
    })

    const partsVariables = (x) => R.reject(isOdd)({
      unitCount: parseInt(R.path(['unitCount'])(x)),
      unitPrice: {
        amount: parseInt(R.path(['depotItem', 'priceOut', 'amount'])(x))
      },
      discount: {
        percent: R.defaultTo(0)(parseInt(R.path(['discount', 'percent'])(x))),
        amount: R.defaultTo(0)(parseInt(R.path(['discount', 'discount', 'amount'])(x)))
      },
      depotItemId: R.path(['depotItem', 'id'])(x)
    })

    const people = (x) => R.zipObj([x], [R.reject(isOdd)({
      name: {
        first: R.path([x, 'name', 'first'])(task),
        last: R.path([x, 'name', 'last'])(task),
        middle: R.path([x, 'name', 'middle'])(task)
      },
      phone: R.path([x, 'phone'])(task),
    })])

    return {
      input: {
        vehicle: R.reject(isOdd)({
          vin: R.path(['vehicle', 'vin'])(task),
          frameNumber: R.path(['vehicle', 'frameNumber'])(task),
          year: parseInt(R.path(['vehicle', 'year'])(task)),
          color: R.path(['vehicle', 'color'])(task),
          plate: R.path(['vehicle', 'plate'])(task),
          mileage: parseInt(R.path(['vehicle', 'mileage'])(task)),
          modificationId: R.path(['vehicle', 'modification', 'id'])(task)
        }),
        description: R.path(['description'])(task),
        ...R.mergeAll(R.map(people)(['client', 'owner'])),
        works: R.pipe(
          R.path(['works']),
          R.defaultTo([]),
          R.map(
            R.pipe(
              worksVariables,
              R.reject(isOdd)
            )
          ),
          R.defaultTo([])
        )(task),
        parts: R.pipe(
          R.path(['parts']),
          R.defaultTo([]),
          R.map(
            R.pipe(
              partsVariables,
              R.reject(isOdd)
            )
          ),
          R.defaultTo([])
        )(task)
      }
    }
  }, [task])

  return (
    <ScrollView>
      <TableVehicle />
      <Client />
      <Describe />
      <Works />
      <Parts />
      {/* <CalendarTask /> */}
      <App />
      <TouchesState
        color="Yellow"
        flex={1}
        onPress={() => createTask({ variables })}
        title="ADDED"
      />
    </ScrollView >
  )
};
