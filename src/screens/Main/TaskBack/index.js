/* eslint-disable complexity */
/* eslint-disable id-length */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable max-statements */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { LoaderCarsScreen } from 'screens/Main/TaskBack/Input/createScreen';
import { CarsEdit } from 'screens/Main/TaskBack/Input/createScreen/Added';
import { ScreenEdit } from 'screens/Main/TaskBack/ScreenEdit';
import { UpdateScreen } from 'screens/Main/TaskBack/UpdateScreen';
import { Header } from 'screens/TaskEditor/components/header';
import { isOdd } from 'utils/helper'
import { InWebView } from 'screens/Document/VideoPlayer/InWebView'

export const TaskGarage = ({ navigation, route }) => {
  const { task } = useSelector(state => state.task, shallowEqual);
  const allListEmpty = (x) => R.all(R.equals(true))([R.hasPath(['owner'])(x), R.pipe(R.path(['id']), R.not)(x)])
  const addedVehicle = (x) => R.all(R.equals(true))([R.hasPath(['vehicle', '__typename'])(x), R.pipe(R.path(['id']), R.not)(x)])

  if (allListEmpty(task)) {
    return (
      <Header
        dispatch={() => navigation.goBack()}
        menu={{ title: R.path(['id'])(task) ? R.join(' ', [i18n.t("TaskWork"), '#', R.path(['id'])(task)]) : "NewTaskCars" }}
        search
      >
        <UpdateScreen navigation={navigation} />
      </Header>
    )
  }

  if (addedVehicle(task)) {
    return (
      <Header
        dispatch={() => navigation.goBack()}
        menu={{ title: R.path(['id'])(task) ? R.join(' ', [i18n.t("TaskWork"), '#', R.path(['id'])(task)]) : "NewTaskCars" }}
        search
      >
        <CarsEdit navigation={navigation} />
      </Header>
    )
  }

  if (R.pipe(R.path(['id']), R.not)(task)) {
    return (
      <Header
        dispatch={() => navigation.goBack()}
        menu={{ title: R.path(['id'])(task) ? R.join(' ', [i18n.t("TaskWork"), '#', R.path(['id'])(task)]) : "NewTaskCars" }}
        search
      >
        <LoaderCarsScreen navigation={navigation} />
      </Header>
    )
  }
  const mode = ["inWebView", 'inOfficeView', 'videoView', 'imageView']
  const inVision = R.innerJoin(
    (record, id) => record === id,
    R.defaultTo([])(R.keys(task)),
    mode
  )

  if (R.pipe(isOdd, R.not)(inVision)) {
    return (
      <Header
        dispatch={() => navigation.goBack()}
        menu={{ title: R.path(['id'])(task) ? R.join(' ', [i18n.t("TaskWork"), '#', R.path(['id'])(task)]) : "NewTaskCars" }}
        search
      >
        <InWebView
          mode={R.head(inVision)}
          document={R.path([mode])(task)}
        />
      </Header>
    )
  }


  return (
    <Header
      dispatch={() => navigation.goBack()}
      menu={{ title: R.path(['id'])(task) ? R.join(' ', [i18n.t("TaskWork"), '#', R.path(['id'])(task)]) : "NewTaskCars" }}
      search
    >
      <ScreenEdit navigation={navigation} />
    </Header>
  )
};
