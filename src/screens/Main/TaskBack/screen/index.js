/* eslint-disable react/jsx-indent-props */
import React from 'react'
import * as R from 'ramda'
import { isOdd } from 'utils/helper'
import { MainScreen } from 'screens/Main/TaskBack/screen/MainScreen'
import { RecompomitionScreen } from 'screens/Main/TaskBack/three'
import { ConclutionScreen } from 'screens/Main/TaskBack/screen/Conclution'

export const AppScreen = ({ screen }) => R.cond([
    [R.equals("Mistake"), () => (
        <MainScreen />
    )],
    [isOdd, () => (
        <MainScreen />
    )],
    [R.equals("Conclution"), () => (
        <ConclutionScreen />
    )],
    [R.equals("Recompomition"), () => (
        <RecompomitionScreen />
    )],
])(screen);