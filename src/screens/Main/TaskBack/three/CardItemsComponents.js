/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
import * as R from 'ramda'
import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style'
import { CardComponents } from 'screens/TaskEditor/components/table/style'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard, isOdd } from 'utils/helper'
import { uiColor } from 'styled/colors/uiColor.json';
import { Info } from 'screens/Main/TaskBack/three/Info'
import { AppScreen } from 'screens/Main/TaskBack/three/AppScreen'


export const CardItemsComponents = ({ elem }) => {
  const [state, setState] = React.useState(false);

  return (
    <CardComponents style={[styles.shadow, { marginRight: 10, marginLeft: 10 }]}>
      <CardComponents>
        <TouchableOpacity onPress={() => setState(!state)}>
          <LabelText
            color={ColorCard('classic').font}
            fonts="ShellBold"
            items="CLabel"
            style={{ textTransform: 'uppercase' }}
            testID="BlockLaker"
          >
            {R.path(['description'])(elem)}
          </LabelText>
        </TouchableOpacity>
      </CardComponents>
      {R.equals(state, true) ?
        <View
          style={[styles.shadow, {
            padding: 5,
            backgroundColor: uiColor.Very_Pole_Grey
          }]}
        >
          <CardComponents>
            <Info capacities={isOdd(R.path(['capacities'])(elem)) ? [R.path(['capacity'])(elem)] : R.path(['capacities'])(elem)} />
            <View style={{ flex: 1, padding: 10 }}>
              <AppScreen elem={elem} />
            </View>
          </CardComponents>
        </View > :
        <View />}
    </CardComponents>
  )
};
