/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-key */
/* eslint-disable newline-before-return */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda'
import React from 'react'
import { View } from 'react-native'
import { styled } from 'screens/Main/TaskBack/three/styled'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard, isOdd } from 'utils/helper'

export const TableLine = ({ value }) => R.addIndex(R.map)((x, keys) => (
  <View
    key={keys}
    style={[styled.row, { backgroundColor: 'white' }]}
  >
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="SSDescription"
      style={styled.text}
      testID="BlockLaker"
    >
      {R.defaultTo(R.keys(x))(R.path(['description'])(x))}
    </LabelText>
    <LabelText
      color={ColorCard('classic').font}
      fonts="ShellMedium"
      items="SSDescription"
      style={styled.text}
      testID="BlockLaker"
    >
      {R.join(' ', R.values(R.reject(isOdd, R.omit(['__typename', 'description', 'range'])(x))))}
    </LabelText>
  </View>))(value);
