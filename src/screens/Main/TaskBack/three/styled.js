import { StyleSheet } from 'react-native'
import { uiColor } from 'styled/colors/uiColor.json'

export const styled = StyleSheet.create({
  circle: {
    width: 90,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: uiColor.Yellow,
    borderRadius: 60
  },
  ewq: {
    backgroundColor: uiColor.Red,
    textTransform: 'uppercase',
    padding: 5,
    textAlign: 'center',
    alignSelf: 'center'
  },
  text: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: uiColor.Pole_Grey,
    flex: 1,
    padding: 5
  },
  qwe: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    margin: 40
  },
  row: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
  }
});