import * as R from 'ramda'
import React from 'react'
import { View } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style'
import { HeaderLine } from 'screens/Main/TaskBack/three/HeaderLine'
import { TableLine } from 'screens/Main/TaskBack/three/TableLine'
import { isOdd } from 'utils/helper'

export const AppScreen = ({ elem }) => {
  if (isOdd(elem)) return (<View />);
  if (R.includes('options', R.keys(elem))) {
    return R.addIndex(R.map)((x, key) => (
      <View
        key={key}
      >
        <HeaderLine title={R.path(['__typename'])(x)} />
        {R.addIndex(R.map)((xe, keys) => (
          <TableLine
            key={keys}
            value={R.path([xe])(x)}
          />
        ))(R.keys(R.omit(['__typename', 'temperature'])(x)))}
      </View>
    ))(R.path(['options'])(elem))
  }

  return (
    R.addIndex(R.map)((x, key) => (
      <View
        key={key}
        style={[styles.shadow, { marginTop: 5, marginBottom: 5 }]}
      >
        <HeaderLine title={x} />
        <TableLine value={R.path([x])(elem)} />
      </View>
    ))(R.pipe(
      R.reject(isOdd),
      R.omit(["description", "type", "__typename"]),
      R.keys,
      R.reject(isOdd)
    )(elem))
  )
};
