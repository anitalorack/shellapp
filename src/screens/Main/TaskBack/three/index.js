import { useLazyQuery } from '@apollo/react-hooks'
import { SHELLRECOMP } from 'gql/autoService'
import * as R from 'ramda'
import React from 'react'
import { ScrollView } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style'
import { CardItemsComponents } from 'screens/Main/TaskBack/three/CardItemsComponents'
import { CardComponents } from 'screens/TaskEditor/components/table/style'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard, isOdd } from 'utils/helper'

export const RecompomitionScreen = ({ storage }) => {
  const [state, setState] = React.useState({});
  const [loadgreeting] = useLazyQuery(SHELLRECOMP, {
    onCompleted: (data) => {
      setState(R.path(R.keys(data))(data))
    },
  });

  React.useEffect(() => {
    loadgreeting({
      variables: {
        where: {
          modificationId: R.path(['Query', 'vehicle', 'modification', 'id'])(storage)
        }
      }
    })
  }, [storage]);

  if (isOdd(R.path(['engine', 'oils'])(state))) return (
    <CardComponents>
      <LabelText
        color={ColorCard('classic').font}
        fonts="ShellMedium"
        items="Description"
        style={styles.items}
        testID="BlockLaker"
      >
        Ничего не найдено
      </LabelText>
    </CardComponents>
  );

  const lubricants = R.path(['lubricants'])(state);
  const oils = R.path(['engine', 'oils'])(state);

  return (
    <ScrollView >
      {R.addIndex(R.map)((x, key) => (
        <CardItemsComponents
          key={key}
          elem={x} />
      ))(oils)}
      {R.addIndex(R.map)((x, keys) => (
        <CardItemsComponents
          key={keys}
          elem={x}
        />
      ))(lubricants)}
    </ScrollView >
  )
};
