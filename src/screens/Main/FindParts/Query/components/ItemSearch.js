/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import { QUERY_FIND_PARTS } from 'gql/findparts';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';

export const ItemSearch = ({ storage, dispatch }) => {
    const { error, loading, data } = useQuery(QUERY_FIND_PARTS, {
        variables: { where: { article: R.path(['text'])(storage) } },
        fetchPolicy: "network-only",
    });

    if (loading) return (<View />);
    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );

    if (isOdd(R.path(['findParts'])(data))) return (<View />);

    return (
        <>
            {R.addIndex(R.map)((x, key) => (
                <TouchableOpacity
                    key={key}
                    onPress={() => dispatch({ type: "Article", payload: R.zipObj(['article', 'brandId'], R.paths([['article'], ['brand', 'id']])(x)) })}
                >
                    <CardComponents style={{ flex: 1, marginLeft: 10, marginRight: 10, margin: 1 }}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SSDescription"
                            style={{
                                textAlign: 'left',
                                textTransform: 'uppercase',
                            }}
                            testID="BlockLaker"
                        >
                            {R.path(['brand', 'name'])(x)}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SSDescription"
                            style={{
                                textAlign: 'left',
                            }}
                            testID="BlockLaker"
                        >
                            {R.path(['name'])(x)}
                        </LabelText>
                    </CardComponents>
                </TouchableOpacity >
            ))(R.path(['findParts'])(data))}
        </>
    )
};