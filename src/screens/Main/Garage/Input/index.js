/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import React, { useReducer, useState } from 'react'
import { View, ScrollView, Text } from 'react-native'
import { TouchesState } from 'styled/UIComponents/UiButtom';
import Modal from 'react-native-modal';
import { TableModel } from 'screens/TaskEditor/components/table';
import { SearchInputLine } from 'screens/TaskEditor/components/header/component/SearchInputLine';
import { LabelText } from 'uiComponents/SplashComponents';
import { isOdd, ColorCard } from 'utils/helper'
import { ModalDescription } from 'screens/Contragent/Input/config';
import * as R from 'ramda'

const reducer = (state = {}, action) => {
    switch (action.type) {
        case "Contragent":
            return R.assocPath(['contragent'], action.payload, state);
        case "Modal":
            return R.assocPath(['modal'], action.payload, state);
        default:
            return state
    }
};

export const AddGoods = ({ navigation }) => {
    const [storage, dispatch] = useReducer(reducer, {});

    return (
        <ScrollView style={{ flex: 1 }}>
            <Text>Поставщик</Text>
            <TouchesState
                color="Yellow"
                flex={1}
                onPress={() => dispatch({
                    type: 'Modal',
                    payload: {
                        state: true, screen: "contragent"
                    }
                })}
                title="ADDCONTRAGENT"
            />
            <View />
            <Text>Товарные позиции</Text>
            <TouchesState
                color="Yellow"
                flex={1}
                onPress={() => dispatch({
                    type: 'Modal',
                    payload: {
                        state: true, screen: "goods"
                    }
                })}
                title="ADDGOODS"
            />
            <Modal isVisible={!isOdd(R.path(['modal', 'state'])(storage))}>
                <ModalDesc
                    dispatch={(e) => dispatch(e)}
                    storage={storage}
                />
            </Modal>
        </ScrollView>
    )
};

const ModalDesc = ({ storage, dispatch }) => {
    const [state, setState] = useState(null);

    const ModalComponents = R.cond([
        [R.equals('contragent'), () => <ContragentSearchScreen style={{ flex: 1 }} />],
        [R.equals('goods'), () => <View style={{ flex: 1 }} />],
        [R.T, () => <View style={{ flex: 1 }} />],
    ]);

    return (
        <ModalDescription>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellBold"
                items="SSDescription"
                style={{
                    textAlign: 'left',
                    padding: 10,
                    textTransform: 'uppercase'
                }}
                testID="BlockLaker"
            >
                {R.path(['modal', 'screen'])(storage)}
            </LabelText>
            {ModalComponents(R.path(['modal', 'screen'])(storage))}
            <View style={{ flexDirection: 'row' }}>
                <TouchesState
                    color="Yellow"
                    flex={1}
                    onPress={() => dispatch({
                        type: 'Modal',
                        payload: null
                    })}
                    title="CLOSED"
                />
                <TouchesState
                    color="Yellow"
                    flex={1}
                    onPress={() => dispatch({
                        type: 'Modal',
                        payload: {
                            state: true, screen: "goods"
                        }
                    })}
                    title="ADDGOODS"
                />
            </View>
        </ModalDescription>
    )
};

const reducerAgent = (state = {}, action) => {
    switch (action.type) {
        case "Search":
            return;
        default:
            return state
    }
};

const ContragentSearchScreen = () => {
    const [contragent, setContragent] = useReducer(reducerAgent, {});
    return (
        <View >
            <SearchInputLine
                // dispatch={(e) => dispatch(e)}
                mode={false}
                state={{ text: "SearchContragent" }}
            />
            <TableModel
                // data={R.path(R.concat(R.keys(data), ['items']))(data)}
                dispatch={(e) => {
                    // if (R.equals(R.path(['type'])(e), 'Phone')) {
                    //     return Linking.openURL(`tel:${R.path(['phone'])(e)}`)
                    // }
                    // if (R.equals(R.path(['type'])(e), 'Edit')) {
                    //     return navigation.navigate(ROUTER, { itemId: R.path(['payload'])(e), mode: "Edit", type: R.path(['checker'])(state) })
                    // }
                    // if (R.equals(R.path(['type'])(e), "Create")) {
                    //     return navigation.navigate(ROUTER, { itemId: null, mode: null, type: R.path(['checker'])(state) })
                    // }
                }}
                // header={R.path(['checker'])(state)}
                mode="Date"
                style={{ flex: 1 }}
            />
        </View>
    )
};