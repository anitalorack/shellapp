/* eslint-disable react/jsx-indent-props */
import React from 'react'
import { ReceiveScreen } from 'screens/Main/Garage/Query/components/ReceiveScreen/ListDocuments'
import { Header } from 'screens/TaskEditor/components/header';
import { useNavigation } from 'hooks/useNavigation'

export const PreviewGarage = ({ navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'goBack' });

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "GARAGES" }}
            svg={current}
        >
            <ReceiveScreen
                dispatch={() => null}
                navigation={navigation}
            />
        </Header>
    )
};