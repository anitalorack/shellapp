/* eslint-disable react/jsx-max-depth */
/* eslint-disable max-statements */
/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import i18n from 'localization'
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import {
    ScrollView, View, Text, StyleSheet
} from 'react-native'
import { SearchInputLine } from 'screens/TaskEditor/components/header/component/SearchInputLine';
import { useNavigation } from 'hooks/useNavigation'
import { Navigator } from 'screens/TaskEditor/components/table/component/Navigator';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { ElementGarage } from 'screens/Main/Garage/Query/components/SppScreen/ElementGarage';
import { HeaderTitleGarage } from 'screens/Main/Garage/Query/components/HeaderTitleGarage'
import { useReceiveScreenHooks } from 'hooks/useDepotsHoks'
import { ListDocuments } from 'screens/Main/Garage/Query/components/ReceiveScreen/ListDocuments'
import { CardScrollComponents } from 'screens/TaskEditor/components/table/component/addgoods';
import { SplashScreen } from 'screens/Splash';
import { isOdd } from 'utils/helper'
import { TouchesState } from 'styled/UIComponents/UiButtom';
import addTwo from 'assets/svg/add';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { CreateScreen } from 'utils/routing/index.json';

export const Garage = ({ navigation, route }) => {
    const routeID = R.path(["params", "stock"])(route);
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const { depots } = useSelector(state => state.garage, shallowEqual);
    const [dataRequest, requestFetch, dataInfo] = useReceiveScreenHooks()
    const ref = React.useRef()
    const [text, setText] = React.useState(null)
    const ROUTER = CreateScreen.ADDGOODS;
    const dispatchRedux = useDispatch()

    React.useEffect(() => {
        if (!R.equals({ dataRequest, depots }, ref)) {
            requestFetch({ screen: R.defaultTo('Current')(R.path(['screen'])(depots)) })
            ref.current = { dataRequest, depots }
        }
    }, [R.path(['screen'])(depots)])

    const Navigation = React.useCallback(() => (
        <View style={{ flex: 1 }}>
            <Navigator
                setDispatch={(e) => {
                    requestFetch({ screen: R.path(['screen'])(depots), page: R.path(['payload'])(e) })
                    ref.current = { dataRequest, depots }
                }}
                storage={{ query: { info: dataInfo } }}
            />
        </View>
    ), [R.path(['screen'])(depots), dataInfo])

    const qwewqe = React.useCallback(({ item }) => R.cond([
        [R.equals('StockGroup'), () => (
            <ElementGarage
                dispatch={(e) => console.log(e)}
                element={item}
            />
        )],
        [R.equals('Stock'), () => (
            <CardScrollComponents
                data={item}
                dispatch={() => null}
                info
                state={item}
                updata
            />
        )],
        [R.equals('ReceiveDepotOperation'), () => (
            <ListDocuments
                item={item}
                navigation={navigation}
            />
        )],
        [R.equals('EmptyCurrent'), () => (
            <View style={{ flex: 1 }}>
                <CardComponents>
                    <Text
                        style={[style.text, {
                            textAlign: 'center',
                            textTransform: "uppercase"
                        }]}
                    >
                        {i18n.t('empty_garage')}
                    </Text>
                    <TouchesState
                        color="Yellow"
                        flex={1}
                        onPress={() => navigation.navigate(ROUTER)}
                        svg={addTwo}
                        title="ADDEDINGARAGE"
                    />
                </CardComponents>
                <View style={{ flex: 1 }} />
            </View>
        )],
        [R.F, () => (
            <SplashScreen
                msg="Нет данных для показа"
                state="Error"
                testID="Error"
            />
        )],
        [R.T, () => (
            <SplashScreen
                msg="Нет данных для показа"
                state="Error"
            />
        )],
    ])(R.path(['__typename'])(item)), [depots])

    const Components = () => {
        if (isOdd(dataRequest)) {
            if (R.equals('Current', R.path(['screen'])(depots))) {
                return qwewqe({ item: [{ typename: 'EmptyCurrent' }] })
            }

            return (
                <SplashScreen
                    msg={R.isEmpty(dataRequest) ? "Нет данных для показа" : "Загрузка"}
                    state="Error"
                    style={{ flex: 1 }}
                />)
        }

        return (
            <View>
                {R.addIndex(R.map)((x, key) => (
                    <View key={key}>
                        {qwewqe({ item: x })}
                    </View>
                ))(dataRequest)}
                {Navigation()}
            </View>
        )
    }

    const success = (e) => {
        setText(e)

        return R.cond([
            [R.equals({ type: 'Create' }), () => {
                dispatchRedux({ type: 'ADD_GOODS', payload: {} })
                navigation.navigate('ADDGOODS')
            }],
            [R.T, () => null],
            [R.F, () => null],
        ])(e)
    }

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "GARAGE" }}
            search
            svg={current}
        >
            <ScrollView contentContainerStyle={{ flexGrow: 1, marginRight: 10, marginLeft: 10 }}>
                <HeaderTitleGarage
                    navigation={navigation}
                    routeID={routeID}
                />
                {R.equals('Received', R.path(['screen'])(depots)) ?
                    <View>
                        <SearchInputLine
                            dispatch={success}
                            state={text}
                            unfilter
                            unsearch
                        />
                    </View> :
                    <View />}
                <View style={{ flex: 1 }}>
                    {Components()}
                </View>
            </ScrollView>
        </Header>
    )
};

const style = StyleSheet.create({
    touch: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1,
        marginBottom: 10,
        marginTop: 10
    },
    text: {
        fontFamily: 'ShellMedium',
        fontSize: 13,
    }
})