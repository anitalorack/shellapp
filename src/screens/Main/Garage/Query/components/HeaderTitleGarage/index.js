/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    TouchableOpacity, Text, StyleSheet, View
} from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { SvgXml } from 'react-native-svg';
import Modal from 'react-native-modal';
import EDIT from 'assets/svg/edit';
import { ModalGarage } from 'screens/Main/Garage/Query/components/Header/ModalGarage';
import DESTROY from 'assets/svg/closeWhite';
import { RowComponents, CardComponents } from 'screens/TaskEditor/components/table/style';
import { useDepotsHoks } from 'hooks/useDepotsHoks'
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

export const HeaderTitleGarage = ({ navigation, routeID }) => {
    const [load, update, destroy] = useDepotsHoks({ navigation, routeID })
    const { depots } = useSelector(state => state.garage, shallowEqual);
    const [variables, setVariables] = React.useState(R.assocPath(['isVisible'], false)(depots))
    const dispatchRedux = useDispatch()

    const success = (x) => {
        R.cond([
            [R.equals('EDIT'), () => setVariables(R.assocPath(['isVisible'], true)(variables))],
            [R.equals('DESTROY'), () => destroy(depots.id)],
            [R.equals('CLOSED'), () => setVariables(R.assocPath(['isVisible'], false)(variables))],
            [R.equals('Current'), () => dispatchRedux({
                type: 'CURRENT_SCREEN',
                payload: { screen: x }
            })],
            [R.equals('BLOCK'), () => dispatchRedux({
                type: 'CURRENT_SCREEN',
                payload: { screen: x }
            })],
            [R.equals('Received'), () => dispatchRedux({
                type: 'CURRENT_SCREEN',
                payload: { screen: x }
            })],
            [R.F, () => console.log({ false: x })],
        ])(x)
    }

    return (
        <View>
            <CardComponents style={[styles.shadow, { margin: 1, flex: 1 }]}>
                <Text style={[styleGarage.text, { justifyContent: 'center', fontFamily: 'ShellBold' }]}>{R.path(['name'])(depots)}</Text>
                <Text style={[{ justifyContent: 'center' }, styleGarage.text]}>{R.path(['description'])(depots)}</Text>
                <RowComponents
                    style={{ flex: 1, marginTop: 10, marginBottom: 10 }}
                >
                    {R.addIndex(R.map)((x, key) => (
                        <TouchableOpacity
                            key={key}
                            onPress={() => success(x)}
                            style={[styleGarage.item, styleGarage.row, {
                                backgroundColor: R.equals('EDIT')(x) ? 'gold' : 'red',
                            }]}
                        >
                            <SvgXml
                                height="20"
                                width="20"
                                xml={R.equals('EDIT')(x) ? EDIT : DESTROY}
                            />
                            <Text
                                style={[styleGarage.text, {
                                    color: R.equals('EDIT')(x) ? 'black' : 'white',
                                    marginRight: 10
                                }]}
                            >
                                {i18n.t(x)}
                            </Text>
                        </TouchableOpacity>
                    ))(['EDIT', 'DESTROY'])}
                </RowComponents>
                <Modal isVisible={R.path(['isVisible'])(variables)}>
                    <ModalGarage
                        dispatch={success}
                        storage={variables}
                    />
                </Modal>

            </CardComponents>
            <RowComponents
                style={{ margin: 10 }}
            >
                {R.addIndex(R.map)((x, key) => (
                    <TouchableOpacity
                        key={key}
                        onPress={() => success(x)}
                        style={R.equals(R.defaultTo('Current')(depots.screen), x) ? {
                            borderBottomColor: 'red', borderBottomWidth: 2
                        } : {}}
                    >
                        <Text
                            style={styleGarage.text}
                        >
                            {i18n.t(x)}
                        </Text>
                    </TouchableOpacity>))(['Current', 'BLOCK', 'Received'])}
            </RowComponents>
        </View>
    )
}

const styleGarage = StyleSheet.create({
    text: {
        textAlign: 'center', fontFamily: 'ShellMedium', fontSize: 13
    },
    item: {
        flex: 1, padding: 5,
    },
    row: {
        marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 10
    }
})
