/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import { useQuery } from '@apollo/react-hooks';
import { STOCK_TREE_DEPOTS } from 'gql/garage';
import * as R from 'ramda';
import i18n from 'localization'
import React, { useState } from 'react';
import { ScrollView, View } from 'react-native';
import { SplashScreen } from 'screens/Splash';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { ElementGarage } from 'screens/Main/Garage/Query/components/SppScreen/ElementGarage';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import addTwo from 'assets/svg/add';

export const SppScreen = ({ routeID, dispatch }) => {
    const [state, setState] = useState([]);
    const { error, loading } = useQuery(STOCK_TREE_DEPOTS, {
        variables: {
            where: {
                stocks: {
                    amount: {
                        available: { gt: 0 }
                    },
                    depotItems: {
                        depotId: isOdd(routeID) ? {} : { eq: routeID },
                        amount: { available: { gt: 0 } }
                    }
                }
            }
        },
        onCompleted: (data) => {
            setState(R.path(['stockGroupsTree'])(data))
        }
    });

    // TODO GO TO CREATE DOCUMENTS

    if (error) return (
        <SplashScreen
            msg={R.toString(error)}
            state="Error"
            testID="Error"
        />
    );
    if (loading) return (
        <SplashScreen
            state="Loading"
            testID="Loading"
        />
    );

    const fhdjsfh = R.cond([
        [isOdd, () => (
            <CardComponents>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SRDescription"
                    style={{
                        textAlign: 'center',
                        textTransform: "uppercase"
                    }}
                >
                    склад пуст
                </LabelText>
                <TouchesState
                    color="Yellow"
                    flex={1}
                    svg={addTwo}
                    onPress={() => console.log({ type: 'NEW GARAGE' })}
                    title="ADDEDINGARAGE"
                />
            </CardComponents>
        )],
        [R.T, () => (
            <ScrollView>
                {R.addIndex(R.map)((x, keys) => {
                    return (
                        <CardComponents
                            key={R.join('_', [keys, 'keys'])}
                            style={[styles.shadow, { margin: 1 }]}
                        >
                            <ElementGarage
                                dispatch={(e) => dispatch(e)}
                                element={x}
                                routeID={routeID}
                            />
                        </CardComponents>
                    )
                })(state)}
            </ScrollView>)]
    ])(state)


    return (<View style={{ flex: 1, backgrounColor: 'red' }}>{fhdjsfh}</View>)
}