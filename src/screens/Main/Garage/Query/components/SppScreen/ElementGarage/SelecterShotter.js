/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import Down from 'assets/svg/arrayDown';
import Left from 'assets/svg/arrayLeft';
import * as R from 'ramda';
import React from 'react';
import {
    TouchableOpacity, View, StyleSheet, Text
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { ElementHooks } from 'hooks/useDepotsHoks'
import { SelectedItems } from 'screens/Main/Garage/Query/components/SppScreen/ElementGarage/SelectedItems'

export const SelecterShotter = ({
    element, margin, dispatch
}) => {
    const [state, success] = ElementHooks();
    const [click, setClick] = React.useState(false);

    if (!element) return (<View />);

    return (
        <View style={[styles.shadow, { marginLeft: margin ? 40 : 0 }]}>
            <CardComponents>
                <TouchableOpacity
                    onPress={() => {
                        setClick(!click);
                        success(R.path(['id'])(element))
                    }}
                    style={style.touch}
                >
                    <View
                        style={{ paddingRight: 10 }}
                    >
                        <SvgXml
                            height="10"
                            width="10"
                            xml={click ? Down : Left}
                        />
                    </View>
                    <Text style={style.text}>
                        {R.path(['name'])(element)}
                    </Text>
                </TouchableOpacity>
            </CardComponents>
            <SelectedItems
                click={click}
                dispatch={dispatch}
                state={state}
            />
        </View>
    )
};

const style = StyleSheet.create({
    touch: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1
    },
    text: {
        fontSize: 13,
        margin: 5
    }
})
