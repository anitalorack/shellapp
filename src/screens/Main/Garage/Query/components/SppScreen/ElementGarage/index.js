/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/no-multi-comp */
import Down from 'assets/svg/arrayDown';
import Left from 'assets/svg/arrayLeft';
import { ElementHooks } from 'hooks/useDepotsHoks';
import * as R from 'ramda';
import React from 'react';
import {
    TouchableOpacity, View, StyleSheet, Text
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { SelectedItems } from 'screens/Main/Garage/Query/components/SppScreen/ElementGarage/SelectedItems';
import { SelecterShotter } from 'screens/Main/Garage/Query/components/SppScreen/ElementGarage/SelecterShotter';

export const ElementGarage = ({
    element, margin, dispatch
}) => {
    const [click, setClick] = React.useState(false);
    const [mode, setMode] = React.useState(false);
    const [state, success] = ElementHooks();

    if (!element) return (<View />);

    if (R.pipe(R.path(['children']), R.isEmpty, R.not)(element)) {
        return R.addIndex(R.map)((elem, keys) => (
            <View
                key={R.join('_', [keys, R.toString(elem)])}
            >
                <TouchableOpacity
                    onPress={() => {
                        setClick(!click);
                        setMode(!mode);
                        success(R.path(['parentId'])(elem))
                    }}
                    style={style.touch}
                >
                    <View
                        style={{ paddingRight: 10 }}
                    >
                        <SvgXml
                            height="10"
                            width="10"
                            xml={mode ? Down : Left}
                        />
                    </View>
                    <Text
                        style={style.text}
                    >
                        {R.path(['parent', 'name'])(elem)}
                    </Text>
                </TouchableOpacity>
                <SelectedItems
                    click={click}
                    dispatch={(e) => dispatch(e)}
                    state={state}
                />
                {mode ? <SelecterShotter
                    dispatch={dispatch}
                    element={elem}
                    margin={margin}
                /> : <View />}
            </View>
        ))(R.path(['children'])(element))
    }

    return (
        <SelecterShotter
            dispatch={dispatch}
            element={element}
            margin={margin}
        />
    )
};

const style = StyleSheet.create({
    touch: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 1,
        marginBottom: 10,
        marginTop: 10
    },
    text: {
        fontFamily: 'ShellMedium',
        fontSize: 13,
    }
})