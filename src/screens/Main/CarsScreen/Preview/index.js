/* eslint-disable react/jsx-max-depth */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import * as R from 'ramda';
import React, { useReducer } from 'react';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { VEHICLE_QUERY } from 'screens/Main/CarsScreen/GQL';
import { CarsInfo } from 'screens/Main/CarsScreen/Preview/CarsInfo';
import { reducer } from 'screens/Main/CarsScreen/Preview/config';
import { TitleHeader } from 'screens/Main/CarsScreen/Preview/TitleHeader';
import { FilterCar } from 'screens/Main/FilterCar';
import { SplashScreen } from 'screens/Splash';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { isOdd } from 'utils/helper';
import { Preview } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'
import { useDispatch } from 'react-redux';

export const PrewiewScreen = ({ route, navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'goBack' });
    const [storage, dispatch] = useReducer(reducer, {});
    const [state, setState] = React.useState('ListCar');
    const [modal, setModal] = React.useState({ isVisible: false });
    const [id, setId] = React.useState({ vehicleId: { eq: R.path(['params', 'itemId', 'owner'])(route) } });
    const dispatchRedux = useDispatch()

    const [loadgreeting] = useLazyQuery(VEHICLE_QUERY, {
        fetchPolicy: "network-only",
        onCompleted: (data) => {
            dispatch({
                type: 'Query',
                payload: R.head(R.path(['vehicles', 'items'])(data))
            })
        }
    });

    React.useEffect(() => {
        loadgreeting({
            variables: {
                where: {
                    id: { eq: R.path(['params', 'itemId', 'owner'])(route) },
                }
            }
        })
    }, []);

    if (isOdd(R.path(['query'])(storage))) {
        return (
            <SplashScreen
                state="Loading"
                testID="LOADING"
            />
        )
    }

    const success = (e) => {
        return R.cond([
            [R.equals(current), () => setMoved(current)],
            [R.equals('Filter'), () => setModal({ isVisible: true })],
            [R.equals('Reset'), () => {
                setId({ vehicleId: { eq: R.path(['params', 'itemId', 'owner'])(route) } });

                return setModal({ isVisible: false })
            }],
            [R.equals('Create'), () => {
                dispatchRedux({
                    type: 'TASK_UPDATE',
                    payload: {
                        vehicle: {}
                    }
                })

                return navigation.navigate(Preview.PreviewTaskGarage)
            }],
            [R.equals('CLOSE'), () => setModal({ isVisible: false })],
            [R.equals('Apply'), () => {
                setId(R.mergeAll([id, R.path(['payload'])(e)]));

                return setModal({ isVisible: false })
            }],
            [R.equals('Edit'), (e) => {
                dispatchRedux({
                    type: 'TASK_UPDATE',
                    payload: e
                })

                return navigation.navigate(Preview.PreviewTaskGarage)
            }],
        ])(R.path(['type'])(e))
    }

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "ListCar" }}
            svg={current}
        >
            <TitleHeader
                setState={(e) => setState(e)}
                state={state}
                storage={storage}
            />
            {R.equals("ListCar", state) ?
                <CarsInfo route={route} /> :
                <VisualTable
                    button
                    dispatch={success}
                    id={id}
                    mode="repairs"
                    navigation={navigation}
                    unfilter
                />}
            <Modal isVisible={R.path(['isVisible'])(modal)}>
                <ModalDescription style={{ flex: 1, margin: 10 }}>
                    <FilterCar
                        dispatch={(e) => success(e)}
                        storage={id}
                    />
                </ModalDescription>
            </Modal>
        </Header>
    )
};
