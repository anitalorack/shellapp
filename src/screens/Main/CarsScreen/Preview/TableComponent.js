/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization'
import * as R from 'ramda'
import React from 'react'
import { View, StyleSheet } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style'
import { PlateNumber } from 'screens/TaskEditor/components/table/component/repairs';
import {
    list, model, modification, value
} from 'screens/Main/CarsScreen/Preview/config'
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style'
import { LabelText } from 'uiComponents/SplashComponents'
import { ColorCard } from 'utils/helper'

export const TableComponent = ({ storage }) => {
    const modificationDataSplit = R.zipObj(list, [
        model(R.path(['query'])(storage)),
        modification(R.path(['query'])(storage)),
        ...value(R.path(['query'])(storage))
    ]);

    const TitleHeader = ({ title }) => (
        <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items="SRDescription"
            style={stylesI.items}
            testID="BlockLaker"
        >
            {title}
        </LabelText>
    );

    const LineUnSplit = (x) => R.cond([
        [R.equals('plate'), () => (
            <PlateNumber
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SDescription"
            >
                {R.path([x])(modificationDataSplit)}
            </PlateNumber>
        )],
        [R.equals('mileage'), () => TitleHeader({
            title: R.path([x])(modificationDataSplit) ? R.join(' ', [R.path([x])(modificationDataSplit), 'км']) : "--"
        })],
        [R.equals('frameNumber'), () =>
            TitleHeader({
                title: R.defaultTo(R.path(['vin'])(modificationDataSplit))(R.path([x])(modificationDataSplit))
            })],
        [R.equals('fuel'), () => TitleHeader({
            title: R.path([x])(modificationDataSplit) ? i18n.t(R.path([x])(modificationDataSplit)) : "--"
        })],
        [R.equals('vehicleModels'), () => TitleHeader({
            title: R.path([x])(modificationDataSplit) ? R.path([x])(modificationDataSplit) : "--"
        })],
        [R.equals('vehicleModifications'), () => TitleHeader({
            title: R.includes("л.   л.c -")(R.path([x])(modificationDataSplit)) ? "--" : R.path([x])(modificationDataSplit)
        })],
        [R.T, () =>
            TitleHeader({
                title: R.path([x])(modificationDataSplit) ? R.path([x])(modificationDataSplit) : "--"
            })],
    ])(x);

    return (
        <CardComponents style={[styles.shadow, { margin: 10 }]}>
            {R.addIndex(R.map)((x, keys) => (
                <View key={R.join('_', ['table', keys])} style={{ minHeight: 35 }}>
                    <RowComponents
                        style={{ marginTop: 2, marginBottom: 2, flex: 1 }}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellLight"
                            items="SDescription"
                            style={stylesI.text}
                            testID="BlockLaker"
                        >
                            {i18n.t(x)}
                        </LabelText>
                        {LineUnSplit(x)}
                    </RowComponents>
                    {R.equals(x, 'color') ? <View /> : <View style={[styles.line, { borderWidth: 0.1 }]} />}
                </View>
            ))(list)}
        </CardComponents>
    )
};

const stylesI = StyleSheet.create({
    items: {
        textAlign: 'right',
        flex: 1,
        textTransform: 'uppercase'
    },
    text: {
        textAlign: 'left',
        flex: 1,
        textTransform: 'uppercase'
    }
});