/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { Preview } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'
import { useDispatch } from 'react-redux';
// const [current, setMoved] = useNavigation({ navigation, move: 'toggle' })

export const CarsList = ({ navigation, route }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const dispatchRedux = useDispatch()

    const ROUTERVEHICLE = Preview.PreviewDashBoardVehilcle;

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "ListCars" }}
            svg={current}
        >
            <VisualTable
                dispatch={(e) => {
                    if (R.equals('Create', R.path(['type'])(e))) {
                        dispatchRedux({ type: 'TASK_UPDATE', payload: {} })

                        return navigation.navigate(Preview.PreviewTaskGarage)
                    }
                    if (R.path(['type'])(e) === "Edit") {
                        return navigation.navigate(ROUTERVEHICLE, { mode: 'client', itemId: e.payload })
                    }
                }}
                mode="vehicle"
                navigation={navigation}
                route={R.path(['params'])(route)}
                unfilter
            />
        </Header >
    )
};
