/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable padded-blocks */
/* eslint-disable arrow-body-style */
import { useLazyQuery } from '@apollo/react-hooks';
import i18n from 'localization'
import * as R from 'ramda';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Dropdown } from 'react-native-material-dropdown';
import { QUERY_VEHICLES, QUERY_VEHICLES_MODELS, QUERY_VEHICLES_MODIFICATION } from 'screens/Main/CarsScreen/GQL';
import SwitchButtom from 'styled/UIComponents/SwitchButton';
import { TextFieldComponents } from 'uiComponents/UiPhoneInput/TextFieldComponents';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { dropdown, initialInputState, qweqwe } from '../config';

export const InputComponentState = ({
    storage, dispatch
}) => {
    const [count] = useState(null);
    const [state, setState] = useState(true);

    const [vehiclesQuery] = useLazyQuery(QUERY_VEHICLES, {
        onCompleted: (data) => dispatch({
            type: "Data",
            payload: data,
        }),
        fetchPolicy: "network-only"
    });
    const [modelsQuery] = useLazyQuery(QUERY_VEHICLES_MODELS, {
        onCompleted: (data) => {
            dispatch({
                type: "Data",
                payload: data,
            })
        },
        fetchPolicy: "network-only"
    });
    const [modificationQuery] = useLazyQuery(QUERY_VEHICLES_MODIFICATION, {
        onCompleted: (data) => {
            dispatch({
                type: "Data",
                payload: data,
            })
        },
        fetchPolicy: "network-only"
    });

    useEffect(() => {
        vehiclesQuery({ variables: { order: { name: "asc" } } })
    }, [count]);

    const changeText = (change, inputPath) => {
        if (R.equals(inputPath.path)('vehicleManufacturers')) {
            modelsQuery({ variables: { where: { manufacturerId: { eq: R.path(['id'])(change) } } } })
        }
        if (R.equals(inputPath.path)('vehicleModels')) {
            modificationQuery({ variables: { where: { modelId: { eq: R.path(['id'])(change) } } } })
        }
        if (inputPath.mode === 'dropdown') {
            return dispatch({
                type: inputPath.label,
                payload: R.assocPath(inputPath.valuePath, change, R.path(['vehicle'])(storage))
            })
        }

        return dispatch({
            type: inputPath.label,
            payload: R.assocPath(inputPath.value, change, R.path(['vehicle'])(storage))
        })
    };


    return (
        <>
            <View
                style={{
                    height: 60,
                    flex: 1,
                    flexDirection: 'row',
                    padding: 10,
                    justifyContent: "flex-end",
                    alignItems: 'center'
                }}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SSDescription"
                    style={{ textAlign: 'right' }}
                >
                    {state ? i18n.t('vin') : i18n.t('frameNumber')}
                </LabelText>
                <SwitchButtom
                    onPress={() => setState(!state)}
                    state={state}
                />
            </View>
            <KeyboardAwareScrollView>
                {R.addIndex(R.map)((element, keys) => {
                    return (
                        <TextFieldComponents
                            key={keys}
                            keyboardType={element.keyboardType}
                            label={i18n.t(element.label)}
                            maxLength={element.maxLength}
                            onBlur={() => null}
                            onChangeText={(change) => {
                                changeText(change, element)
                            }}
                            value={R.path(element.value)(storage)}
                        />
                    )
                })(R.without(R.equals(false)(state) ? [initialInputState[1]] : [initialInputState[0]])(initialInputState))}
                {R.addIndex(R.map)((element, keys) => {
                    if (!isOdd(R.path(['modification', 'modificationId'])(storage)) && !R.hasPath([element.lib])(storage)) return <View />;

                    return (
                        <Dropdown
                            key={keys}
                            data={R.path([element.lib, element.path])(storage)}
                            label={i18n.t(element.label)}
                            onChangeText={(_change, index, item) => changeText(item[index], element)}
                            value={
                                R.equals(element.label, 'vehicleModifications') ?
                                    R.toString(
                                        R.values(
                                            R.omit(['__typename', 'model', 'id'])(
                                                R.path(element.valuePath)(storage))
                                        )
                                    )
                                    : R.path(element.valuePath)(storage)
                            }
                            valueExtractor={(item) => {
                                return R.cond([
                                    [R.equals('vehicleManufacturers'), R.always(R.path([element.value])(item))],
                                    [R.equals('vehicleModels'), R.always(R.path([element.value])(item))],
                                    [R.equals('vehicleModifications'), R.always(R.toString(R.values(R.omit(['id', '__typename'])(item))))],
                                ])(element.label)
                            }}
                        />
                    )
                })(dropdown)}
                {R.addIndex(R.map)((element, keys) => {
                    return (
                        <TextFieldComponents
                            key={keys}
                            keyboardType={element.keyboardType}
                            label={i18n.t(element.label)}
                            maxLength={element.maxLength}
                            onBlur={() => null}
                            onChangeText={(change) => {
                                changeText(change, element)
                            }}
                            value={R.path(element.value)(storage)}
                        />
                    )
                })(qweqwe)}
            </KeyboardAwareScrollView>
        </>
    )
};
