/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
import * as R from 'ramda';
import React, { useReducer } from 'react';
import { FormClientVehicle } from 'screens/Main/CarsScreen/Query/component';
import { FormClient } from 'screens/Main/MainScreen/Query/component';
import HeaderComponentsProps from 'uiComponents/HeaderComponents';
import { SplashContainer } from 'uiComponents/SplashComponents';
import { isOdd } from 'utils/helper';
import { useNavigation } from 'hooks/useNavigation'

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case "Screeen":
            return R.assocPath(['curr'], action.payload, state);
        case "Vehicles":
            return R.assocPath(['vehicle'], action.payload, state);
        case "CreateTask":
            return R.assocPath(['task'], action.payload, state);
        default:
            return state
    }
};

export const CarsInputScreen = ({ data, navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const [storage, dispatch] = useReducer(reducer, data);
    const header = {
        title: R.pipe(
            R.paths([
                ['year'],
                ['modification', 'model', 'manufacturer', 'name'],
                ['modification', 'model', 'name'],
                ['modification', 'litres'],
            ]),
            R.join(', ')
        )(storage),
        subHeader: R.path(['phone'])(storage),
        menu: ["CreateTask", "ListClientsCars"],
        curr: R.path(['curr'])(storage)
    };
    const success = (event) => {

        return R.cond([
            [R.equals('ListClientsCars'), () => dispatch({ type: "Screeen", payload: R.path(['type'])(event) })],
            [R.equals('CreateTask'), () => dispatch({ type: "Screeen", payload: R.path(['type'])(event) })],
            [R.equals(current), () => setMoved(current)],
        ])(R.path(['type'])(event))
    };

    return (
        <SplashContainer>
            <HeaderComponentsProps
                header={header}
                onPress={(e) => {
                    success(e)
                }}
                svg={current}
                title={isOdd(R.path(['id'])(storage)) ? "ListClientsCars" : "ListClientsCars"}
            />
            <Screen
                navigation={navigation}
                storage={storage}
            />
        </SplashContainer>
    )
};

const Screen = ({ storage, navigation }) => R.cond([
    [R.equals('CreateTask'), () => <FormClient
        navigation={navigation}
        storage={storage}
        variables={{ variables: { where: { client: { id: { eq: R.path(['id'])(storage) } } } } }}
    />],
    [R.pipe(R.equals('CreateTask'), R.not), () => <FormClientVehicle
        navigation={navigation}
        storage={storage}
        variables={{ variables: { where: { client: { id: { eq: R.path(['id'])(storage) } } } } }}
    />]
])(R.path(['curr'])(storage));