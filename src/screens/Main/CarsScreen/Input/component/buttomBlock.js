/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import MutationButton from 'utils/api/funcComponent/MutationButton';
import { isOdd } from 'utils/helper';
import { ButtonStyleBLock } from 'screens/Master/CreateEmployeeScreen/style'

export const ButtonBlock = (props) => {
    const {
        storage, dispatch
    } = props;

    return (
        <ButtonStyleBLock>
            <MutationButton
                color="VeryRed"
                onPress={() => dispatch({
                    type: "Action",
                    payload: R.ifElse(
                        isOdd,
                        R.always("CREATE"),
                        R.always("UPDATE"))(R.path(['id'])(storage))
                })}
                testID="ACTION_BUTTON"
                Title={isOdd(R.path(['id'])(storage)) ? "Создать автомобиль" : "Редактировать автомобиль"}
            />
            <MutationButton
                color="VeryGrey"
                onPress={() => dispatch({ type: "Cancel" })}
                testID="RETURN_BUTTON"
                Title="Вернуться назад"
            />
        </ButtonStyleBLock>
    )
};
