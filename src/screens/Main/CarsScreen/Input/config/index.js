/* eslint-disable complexity */
/* eslint-disable no-case-declarations */
/* eslint-disable newline-before-return */
/* eslint-disable no-unneeded-ternary */
import * as R from 'ramda'


export const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'vehicleModifications':
            const vehicleModifications = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload.vehicle]), {});
            return R.mergeAll([state, vehicleModifications]);
        case 'vehicleModels':
            const vehicleModels = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload.vehicle]), {});
            return R.mergeAll([state, vehicleModels]);
        case 'vehicleManufacturers':
            const vehicleManufacturers = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload.vehicle]), {});
            return R.mergeAll([state, vehicleManufacturers]);
        case 'vehicle':
            const vehicle = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, vehicle]);
        case 'plate':
            const plate = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, plate]);
        case 'vin':
            const vin = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, vin]);
        case 'year':
            const year = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, year]);
        case 'model':
            const model = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, model]);
        case 'modification':
            const modification = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, modification]);
        case 'frameNumber':
            const frameNumber = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, frameNumber]);
        case 'color':
            const color = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, color]);
        case 'mileage':
            const mileage = R.assocPath(['vehicle'], R.mergeAll([R.path(['vehicle'])(state), action.payload,]), {});
            return R.mergeAll([state, mileage]);
        case 'first':
            const NameInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(NameInput), state);
        case 'last':
            const LastInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(LastInput), state);
        case 'middle':
            const MiddleInput = R.mergeAll([state.name, R.path(['payload', 'name'])(action)]);

            return R.assocPath(['name'], R.omit(['__typename'])(MiddleInput), state);
        case 'phone':
            return R.mergeAll([state, action.payload]);
        case 'Update':
            return R.mergeAll([state, action.payload]);
        case 'Vehicle':
            return R.assocPath(['vehicle'], action.payload, state);
        case 'Data':
            const keys = R.keys(action.payload);

            if (R.equals(keys, ['vehicleManufacturers'])) {
                return R.assocPath(['lib'], action.payload, R.omit(['libModels', 'libModifications', 'vehicleModels', 'vehicleModifications'])(state));
            }
            if (R.equals(keys, ['vehicleModels'])) {
                return R.assocPath(['libModels'], action.payload, R.omit(['libModifications', 'vehicleModifications'])(state));
            }
            if (R.equals(keys, ['vehicleModifications'])) {
                return R.assocPath(['libModifications'], action.payload, state);
            }
            return state;
        default:
            return state
    }
};

export const initialInputState = [
    {
        id: "VIN",
        path: 'name',
        keyboardType: "numeric",
        label: 'vin',
        title: "Error message",
        value: ["vin"],

        maxLength: 17,
        testID: "NameInput",
    },
    {
        id: "frameNumber",
        path: 'name',
        keyboardType: "numeric",
        label: 'frameNumber',
        title: "Error message",
        value: ["frameNumber"],

        maxLength: 9,
        testID: "NameInput",
    },
    {
        id: "ГосНомер",
        path: 'plate',
        keyboardType: "default",
        label: 'plate',
        title: "Error message",
        value: ["plate"],
        maxLength: 6,

        testID: "Plate",
    },
    {
        id: "ЦВЕТ",
        path: 'color',
        keyboardType: "default",
        label: 'color',
        title: "Error message",
        value: ["color"],
        maxLength: 32,

        testID: "Color",
    },
];

export const qweqwe = [
    {
        id: "ГОД",
        path: 'middle',
        keyboardType: "numeric",
        label: 'year',
        title: "Error message",
        value: ["year"],
        maxLength: 4,

        testID: "Year",
    },
    {
        id: "ПРОБЕГ",
        path: 'mileage',
        keyboardType: "numeric",
        label: 'mileage',
        title: "Error message",
        value: ["mileage"],
        maxLength: 6,

        testID: "Mileage",
    },
];

export const dropdown = [
    {
        id: "МАРКА",
        path: 'vehicleManufacturers',
        keyboardType: "default",
        label: 'vehicleManufacturers',
        title: "Error message",
        value: ["name"],
        valuePath: ["vehicle", "modification", 'model', 'manufacturer', 'name'],
        mode: "dropdown",
        maxLength: 120,
        testID: "Vehicles",
        lib: "lib",
    },
    {
        id: "МОДЕЛЬ",
        path: 'vehicleModels',
        keyboardType: "default",
        label: 'vehicleModels',
        title: "Error message",
        value: ['name'],
        valuePath: ["vehicle", "modification", 'model', 'name'],
        mode: "dropdown",
        maxLength: 120,
        testID: "Model",
        lib: "libModels",
    },
    {
        id: "МОДИФИКАЦИЯ",
        path: 'vehicleModifications',
        keyboardType: "default",
        label: 'vehicleModifications',
        title: "Error message",
        value: ["enginecode"],
        valuePath: ["vehicle", "modification"],
        mode: "dropdown",
        maxLength: 120,
        testID: "Modification",
        lib: "libModifications",
    },
];