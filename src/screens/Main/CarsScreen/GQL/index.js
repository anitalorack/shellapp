import gql from 'graphql-tag'

export const VEHICLE_QUERY = gql`
query(
  $where: WhereVehicleInput
  $order: OrderVehicleInput
  $paginate: PageInput
) {
  vehicles(where: $where, order: $order, paginate: $paginate) {
    info{
      page
      nextPage
      previousPage
      totalCount
    }
    items {
      id
      vin
      frameNumber
      plate
      createdAt
      year
      color
      mileage
      modificationId
      modification {
        model{
          id
          name
          manufacturer{
            id
            models{
              modifications{
                dinHp
              }
            }
            name
          }
          subbody
        }
        id
        litres
        dinHp
        fuel
        enginecode
        startYear
        endYear
      }
      plateHistory {
        value
      }
      mileageHistory {
        value
        createdAt
      }
      ownerId
      owner {
        id
        name {
          last
          first
          middle
        }
        phone
      }
      clients {
        name {
          last
          first
          middle
        }
        phone
      }
    }
  }
}
`;

export const CREATE_VEHICLE = gql`
mutation($input: CreateVehicleInput) {
  createVehicle(input: $input) {
    id
  }
}
`;

export const UPDATE_VEHICLE = gql`
mutation($input: UpdateVehicleInput!) {
  updateVehicle(input: $input) {
    id
  }
}
`;

export const DESTROY_VEHICLE = gql`
mutation($input: Int!) {
  destroyVehicle(id: $input) {
    id
  }
}
`;

export const QUERY_VEHICLES = gql`
query(
  $where: WhereVehicleManufacturerInput
  $order: OrderVehicleManufacturerInput
) {
  vehicleManufacturers(where: $where, order: $order) {
    id
    name
  }
}
`;
export const QUERY_VEHICLES_MODELS = gql`
query($where: WhereVehicleModelInput!) {
  vehicleModels(where: $where) {
    id
    name
    subbody
  }
}
`;
export const QUERY_VEHICLES_MODIFICATION = gql`
query($where: WhereVehicleModificationInput) {
  vehicleModifications(where: $where) {
    id
    litres
    fuel
    enginecode
    startYear
    endYear
    dinHp
    kw
    modelId
    name
    provider
    providerId
    rpm
    subbody
  }
}
`;