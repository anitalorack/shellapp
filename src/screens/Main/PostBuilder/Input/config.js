import * as R from 'ramda'

export const input = [
    {
        id: "ИМЯ",
        path: 'name',
        main: true,
        label: 'naiming',
        multiline: true,
        Lenght: 120,
        keyboardType: "default",
        maxLength: 120,
        value: ["name"],
        example: "",
        error: "Error message",
        errorMessage: 'PhoneError',
    },
    {
        id: "ИМЯ",
        path: 'name',
        main: true,
        label: 'numberType',
        Lenght: 120,
        keyboardType: "numeric",
        maxLength: 120,
        value: ["position"],
        example: "",
        error: "Error message",
        errorMessage: 'PhoneError',
    }]
    ;

export const initialState = {
    garagePostType: {
        alias: "general_with_lift",
        category: "locksmith_repair",
        id: 2
    }
};

export const validator = R.pipe(
    R.paths([['garagePostType', 'alias'], ['garagePostType', 'category'], ['name'], ['position']]),
    R.map(R.anyPass([R.isNil, R.isEmpty, R.equals('')])),
    R.uniq,
    R.includes(true)
);