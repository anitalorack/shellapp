/* eslint-disable radix */
/* eslint-disable react/jsx-curly-newline */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import { useNavigation } from 'hooks/useNavigation';
import * as R from 'ramda';
import React from 'react';
import { ScrollView } from 'react-native';
import { validator } from 'screens/Main/PostBuilder/Input/config';
import { usePostCurrentHooks } from 'hooks/usePostCurrentHooks';
import { Header } from 'screens/TaskEditor/components/header';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { ActionScreen } from './ActionScreen';
import { SplashScreen } from 'screens/Splash';


export const InputPost = ({ navigation, route }) => {
    const [current, setMoved] = useNavigation({
        navigation,
        move: 'goBack'
    });
    const [state, create, update, destroy, modify] = usePostCurrentHooks({
        navigation,
        route
    });

    const success = R.cond([
        [R.equals('CREATE'), () => create(state)],
        [R.equals('DESTROY'), () => destroy(state)],
        [R.equals('GOBACK'), () => setMoved('goBack')],
        [R.equals('SAVE'), () => update(state)],
    ]);

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: R.path(['id'])(state) ? "EditPostUpload" : "NewPostUpload" }}
            style={{ flex: 1 }}
            svg={current}
        >
            <ScrollView contentContainerStyle={{
                flexGrow: 1,
                justifyContent: 'space-between'
            }}>
                <ActionScreen
                    setState={(e) => modify(e)}
                    state={state}
                />
                <RowComponents>{
                    R.map(func => (
                        <TouchesState
                            key={func}
                            color={R.equals(func)("DESTROY") ? "VeryRed" : "Yellow"}
                            disabled={validator(state)}
                            flex={1}
                            onPress={() => success(func)}
                            title={func}
                        />
                    ))(R.path(['id'])(state) ? ["DESTROY", "SAVE"] : ["CREATE"])}
                </RowComponents>
            </ScrollView>
        </Header>
    )
};
