/* eslint-disable newline-after-var */
/* eslint-disable max-statements */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { GARAGEPOST } from 'gql/postGarage';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import i18n from 'localization'

export const AliaseScreen = ({ state, setState }) => {
    const dispatchRedux = useDispatch();
    const [start] = React.useState(null);
    const aliasePost = useSelector(state => R.path(['alise', 'aliase', 'garagePostTypes'])(state), shallowEqual);

    const [loadgreeting] = useLazyQuery(GARAGEPOST, {
        onCompleted: (data) => {
            dispatchRedux({
                type: "ALIASE_ADD",
                payload: data
            })
        }
    });

    React.useEffect(() => loadgreeting(), [start]);

    if (!aliasePost) return (<View />);

    const itemsCategory = R.uniq(
        R.map(x => ({
            label: i18n.t(R.path(['category'])(x)),
            value: R.path(['category'])(x)
        }))(aliasePost));

    const itemsGaragePostType = R.map(x => ({
        label: i18n.t(R.path(['alias'])(x)),
        value: R.path(['id'])(x)
    }))(R.innerJoin((record, id) => R.equals(record.category, id),
        aliasePost,
        [R.path(['garagePostType', 'category'])(state)])
    );

    const success = ({ type, e }) => R.cond([
        [R.equals('category'), () => setState(
            R.assocPath(
                ['garagePostType'],
                { category: e },
                state)
        )],
        [R.equals('garagePostType'), () => setState(
            R.assocPath(['garagePostType'],
                R.head(
                    R.innerJoin(
                        (record, id) => R.equals(record.id, id),
                        aliasePost
                    )([e])),
                state))],
        [R.F, () => null],
        [R.T, () => null],
    ])(type);

    return (
        <View>
            <UiPicker
                dispatch={(e) => success({ type: 'category', e })}
                element={{ path: "category" }}
                Items={itemsCategory}
                main
                placeholder={{ id: R.defaultTo(null)(R.path(['garagePostType', 'category'])(state)) }}
            />
            <UiPicker
                disabled={R.hasPath(['garagePostType', 'category'])(state)}
                dispatch={(e) => success({ type: 'garagePostType', e })}
                element={{ path: "type" }}
                Items={itemsGaragePostType}
                main
                placeholder={{ id: R.defaultTo(null)(R.path(['garagePostType', 'id'])(state)) }}
            />
        </View>
    )
};