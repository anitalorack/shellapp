/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { CreateScreen } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'
// const [current, setMoved] = useNavigation({ navigation, move: 'toggle' })

export const PostScreen = ({ navigation }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });

    const ROUTER = CreateScreen.Post;
    const success = (e) => R.cond([
        [R.equals('Edit'), () => navigation.navigate(ROUTER, { itemId: e.payload })],
        [R.T, () => navigation.navigate(ROUTER)],
    ])(R.path(['type'])(e));

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "PostRemont" }}
            svg={current}
        >
            <VisualTable
                button
                dispatch={success}
                mode="post"
                navigation={navigation}
                unfilter
                unsearch
            />
        </Header >
    )
};
