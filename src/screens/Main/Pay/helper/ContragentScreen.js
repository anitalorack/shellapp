/* eslint-disable react/jsx-indent-props */
import { useQuery } from '@apollo/react-hooks';
import { GaragesId } from 'gql/autoService';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/Pay/helper';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';

export const ContragentScreen = () => {
    const { loading, error, data } = useQuery(GaragesId, {
        fetchPolicy: "network-only"
    });

    if (loading) return (
        <View
            style={[styles.block, { minHeight: 250 }]}
        />
    );
    if (error) return (
        <View
            style={[styles.block, { minHeight: 250 }]}
        />
    );

    const CompanyInfo = {
        "name": R.pipe(
            R.path(['garages', 'items']),
            R.head,
            R.path(['name']),
        )(data),
        "legalAddress": R.pipe(
            R.path(['garages', 'items']),
            R.head,
            R.path(['legalAddress']),
            R.omit(['__typename']),
            R.values,
            R.join(', ')
        )(data),
        "inn": R.pipe(
            R.path(['garages', 'items']),
            R.head,
            R.path(['inn']),
        )(data),
        "kpp": R.pipe(
            R.path(['garages', 'items']),
            R.head,
            R.path(['kpp']),
        )(data)
    };
    const companyInfo = ['name', 'legalAddress', 'inn', 'kpp'];

    return (
        <View
            style={[styles.block, { minHeight: 250, }]}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SRDescription"
                style={[{
                    textAlign: 'center',
                    marginBottom: 10
                }]}
            >
                {i18n.t('LabelNextAgent')}
            </LabelText>
            <CardComponents
                style={[
                    styles.shadow, {
                        marginTop: 10,
                        backgroundColor: '#f7f7f7'
                    }
                ]}
            >
                {R.addIndex(R.map)((x, key) => (
                    <View
                        key={R.join('_', ['Agent', key])}
                        style={styles.container}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SRDescription"
                        >
                            {i18n.t(x)}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SRDescription"
                        >
                            {isOdd(data) || R.path([x])(CompanyInfo)}
                        </LabelText>
                    </View>
                ))(companyInfo)}
            </CardComponents>
        </View>
    )
};