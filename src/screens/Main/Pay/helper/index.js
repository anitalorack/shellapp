import { StyleSheet } from 'react-native'


export const styles = StyleSheet.create({
    container: {
        textAlign: 'left',
        padding: 10,
        textTransform: 'uppercase'
    },
    items: {
        textAlign: 'left',
        textTransform: 'capitalize'
    },
    block: { margin: 10, },
    create: {
        textAlign: 'left',
        paddingLeft: 20,
        padding: 10,
        textTransform: 'capitalize'
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    rowCenter: { flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', },
    center: { padding: 10, justifyContent: 'center', alignItems: 'center' },
    pdfButton: {
        top: 0,
        right: 0,
        height: 40,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    dropdownStyles: {
        color: 'black',
        fontFamily: 'ShellMedium',
        textTransform: 'uppercase',
        marginBottom: 35
    }
});