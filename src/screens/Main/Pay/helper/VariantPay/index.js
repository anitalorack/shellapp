/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View, Text } from 'react-native';
import { styles } from 'screens/Main/Pay/helper';
import { CheckerState } from 'screens/Main/Pay/helper/VariantPay/CheckerState';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd, ColorCard } from 'utils/helper';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';

export const VariantPay = ({ data, state, setState }) => {
    if (isOdd(data)) return (<CardComponents />);

    return (
        <CardComponents>
            <View
                style={styles.block}
            >
                <UiPicker
                    dispatch={(e) => {
                        setState(R.assocPath(['bill'], { data: e }, state))
                    }}
                    element={{ path: "VarianPay" }}
                    Items={[
                        { label: i18n.t("nal"), value: "nal" },
                        { label: i18n.t("pay"), value: "pay" },
                    ]}
                    main
                    placeholder={{ id: R.defaultTo(null)(R.path(['bill', 'data'])(state)) }}
                />
            </View>
            <View >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SDescription"
                    style={{
                        textAlign: 'left',
                        paddingLeft: 10,
                        color: uiColor.Mid_Grey,
                    }}
                    testID="BlockLaker"
                >
                    {i18n.t("PeriodEditor")}
                    <Text style={{ color: 'red' }}> *</Text>
                </LabelText>
                {R.map(item => (
                    <CheckerState
                        key={R.join("_", [R.toString(item), R.path(['title'])(item)])}
                        item={item}
                        setState={(e) => setState(e)}
                        state={state}
                    />
                ))(data)}
            </View>
        </CardComponents>
    )
};
