/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import Rub from 'assets/svg/rub';
import { SvgXml } from 'react-native-svg';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { styles } from 'screens/Main/Pay/helper/index';

export const CardPay = ({ elem, mode, keys }) => (
    <View
        key={R.join('_', ['mode', mode, keys])}
        style={[styles.rowCenter, { justifyContent: 'space-between' }]}
    >
        <LabelText
            key={R.path(['id'])(elem)}
            color={ColorCard('classic').font}
            fonts="ShellBold"
            items="SSDescription"
            style={[styles.container, { paddingLeft: 0 }]}
        >
            {i18n.t(mode)}
        </LabelText>
        <View style={styles.rowCenter}>
            <LabelText
                key={R.path(['id'])(elem)}
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SRDescription"
            >
                {R.path([mode, 'amount'])(elem)}
            </LabelText>
            <SvgXml height="10" weight="10" xml={Rub} />
        </View>
    </View>
);