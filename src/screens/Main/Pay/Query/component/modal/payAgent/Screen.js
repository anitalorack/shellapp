/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { ContragentScreen } from 'screens/Main/Pay/helper/ContragentScreen';
import { VariantPay } from 'screens/Main/Pay/helper/VariantPay';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { isOdd } from 'utils/helper'

export const Screen = ({ state, dispatch, setState }) => R.cond([
    [R.equals(true), () => (
        <View style={{ flex: 1, justifyContent: 'flex-start' }}>
            <ContragentScreen />
            <TouchesState
                color="Yellow"
                flex={0}
                onPress={() => dispatch({ type: "Agent" })}
                style={{ alignItems: 'flex-end' }}
                title="EDIT_NAME"
            />
        </View>
    )],
    [R.equals(false), () => (
        <VariantPay
            data={R.path(['data', 'billServices', 'items'])(state)}
            setState={(e) => setState(e)}
            state={state}
        />
    )],
    [isOdd, () => (
        <VariantPay
            data={R.path(['data', 'billServices', 'items'])(state)}
            setState={(e) => setState(e)}
            state={state}
        />
    )]
])(R.path(['check'])(state));