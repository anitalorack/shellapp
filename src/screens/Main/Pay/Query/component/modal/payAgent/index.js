/* eslint-disable no-confusing-arrow */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable max-statements */
import { useLazyQuery } from '@apollo/react-hooks';
import { BILL_SERVICES } from 'gql/billAccount';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { Screen } from 'screens/Main/Pay/Query/component/modal/payAgent/Screen';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { usePayHooks } from 'hooks/usePayHooks'

export const PayModal = ({ dispatch, route }) => {
    const initialStorage = R.assocPath(['account'], route)({ error: true, bill: { data: 'nal' } })
    const [success, state, setState] = usePayHooks({ dispatch, initialStorage })

    const buttonColor = (x) => R.includes(x, ['CLOSED', 'BACK']) ? "VeryRed" : "Yellow"
    const buttonMode = R.path(['check'])(state) ? ["COAST", "BACK"] : ['PAY', 'CLOSED'];
    const buttonDisabled = (x) => (
        R.equals(x, 'CLOSED') ?
            false :
            R.all(R.equals(false), [
                R.hasPath(['bill', 'data'])(state),
                R.hasPath(['mode', 'price', 'amount'])(state),
                R.hasPath(['mode', 'name'])(state)
            ])
    );
    const [billServiceIds] = useLazyQuery(BILL_SERVICES, {
        onCompleted: (data) => setState(R.assocPath(['data'], data)(state)),
        onError: (data) => showMessage({
            message: R.toString(data),
            description: "Error",
            type: "danger",
        }),
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        billServiceIds({
            variables: { where: { employeeVisible: { eq: true } }, order: { id: "asc" } }
        })
    }, [R.path(['billServicesIds'])(state)]);

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <Screen
                    dispatch={dispatch}
                    setState={setState}
                    state={state}
                />
            </View>
            <RowComponents>
                {R.addIndex(R.map)((x, key) => (
                    <View
                        key={key}
                        style={{ flex: 1, marginBottom: 15 }}
                    >
                        <TouchesState
                            color={buttonColor(x)}
                            disabled={buttonDisabled(x)}
                            flex={1}
                            onPress={() => success(x, null)}
                            title={x}
                        />
                    </View>
                ))(buttonMode)}
            </RowComponents>
        </View>
    )
};