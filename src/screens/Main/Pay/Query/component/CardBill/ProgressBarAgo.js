/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { View } from 'react-native';
import { ProgressBar } from 'react-native-paper';
import { styles } from 'screens/Main/Pay/helper';
import { useSelector, shallowEqual } from 'react-redux';
import * as R from 'ramda'
import { useEmployeeProfile } from 'hooks/useProfile';

export const ProgressBarAgo = () => {
    const [state, update] = useEmployeeProfile()
    const asd = useSelector(state => state.depots.bill, shallowEqual);

    React.useEffect(() => {
        if (!state) {
            update()
        }
    }, [state])

    return (
        <View
            style={styles.container}
        >
            <ProgressBar
                color="#66bb6a"
                progress={R.path(['billAccount', 'balance'])(asd) / R.path(['billAccount', 'totalBalance'])(asd)}
                style={{ backgroundColor: 'grey' }}
            />
        </View>
    )
};
