/* eslint-disable newline-before-return */
/* eslint-disable no-case-declarations */
/* eslint-disable complexity */
import * as R from 'ramda'

export const details = [{
    id: 1,
    path: 'name',
    main: true,
    label: 'nameSto',
    Lenght: 2,
    keyboardType: "default",
    price: true,
    value: ['name'],
    example: "",
    multiline: true,
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 2,
    path: 'defaultPartSurcharge',
    label: 'coastSto',
    Lenght: 2,
    keyboardType: "numeric",
    string: true,
    price: true,
    value: ['settings', 'defaultPartSurcharge'],
    example: "",
    maxLenght: 14,
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 2,
    path: 'costPerHour',
    string: true,
    main: true,
    label: 'coastTime',
    Lenght: 10,
    keyboardType: "numeric",
    value: ['settings', 'costPerHour', 'amount'],
    maxLenght: 2,
    example: "",
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 2,
    path: 'phone',
    main: true,
    label: 'ContactPhone',
    Lenght: 10,
    keyboardType: "numeric",
    value: ['phone'],
    example: "",
    mask: "+7 ([000]) [000]-[00]-[00]",
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 3,
    path: 'email',
    main: true,
    label: 'email',
    Lenght: 4,
    keyboardType: "default",
    value: ['email'],
    example: "",
    error: "Error message",
    errorMessage: 'PhoneError',
}];

export const grees = [{
    id: 4,
    path: 'inn',
    main: true,
    label: 'inn',
    maxLenght: 12,
    keyboardType: "numeric",
    mask: "[000000000000]",
    value: ['inn'],
    example: "",
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 5,
    path: 'kpp',
    main: true,
    label: 'kpp',
    maxLenght: 9,
    mask: "[000000000]",
    keyboardType: "numeric",
    value: ['kpp'],
    example: "",
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 6,
    path: 'psrn',
    main: true,
    label: 'psrn',
    maxLenght: 15,
    keyboardType: "numeric",
    value: ['psrn'],
    example: "",
    mask: "[0000000000000]",
    error: "Error message",
    errorMessage: 'PhoneError',
}];

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case "legalName":
            return R.assocPath([action.type], action.payload, state);
        case "inn":
            return R.assocPath([action.type], action.payload, state);
        case "kpp":
            return R.assocPath([action.type], action.payload, state);
        case "psrn":
            return R.assocPath([action.type], action.payload, state);
        case "coastSto":
            return R.assocPath([action.type], action.payload, state);
        case "costPerHour":
            return R.assocPath(['settings'], R.mergeAll([R.path(['settings'])(state), R.assocPath(['costPerHour', 'amount'], action.payload)({})]))(state);
        case "phone":
            return R.assocPath([action.type], `${"+7"}${action.payload}`, state);
        case "email":
            return R.assocPath([action.type], action.payload, state);
        case "name":
            return R.assocPath([action.type], action.payload, state);
        case "QUERY":
            return action.payload;
        case "defaultPartSurcharge":
            return R.assocPath(['settings'], R.mergeAll([R.path(['settings'])(state), R.assocPath((['defaultPartSurcharge']), action.payload)({})]))(state);
        case "legalAddress":
            return R.mergeAll([state, action.payload]);
        case "address":
            return R.mergeAll([state, action.payload]);
        case "bankDetails":
            return R.mergeAll([state, action.payload]);
        default:
            return state
    }
};

export const variables = (storage) => ({
    input: {
        id: R.path(['id'])(storage),
        name: R.path(['name'])(storage),
        address: R.omit(['__typename'])(R.path(['address'])(storage)),
        phone: R.path(['phone'])(storage),
        email: R.path(['email'])(storage),
        costPerHour: parseInt({ amount: R.path(['settings', 'costPerHour', 'amount'])(storage) }),
        checkingAccount: R.path(['checkingAccount'])(storage),
        correspondentAccount: R.path(['correspondentAccount'])(storage),
        rcbic: R.path(['rcbic'])(storage),
        inn: R.path(['inn'])(storage),
        kpp: R.path(['kpp'])(storage),
        legalName: R.path(['legalName'])(storage),
        bankName: R.path(['bankName'])(storage),
        psrn: R.path(['psrn'])(storage),
        defaultPartSurcharge: parseInt(R.path(['settings', 'defaultPartSurcharge'])(storage)),
        legalAddress: R.omit(['__typename'])(R.path(['legalAddress'])(storage)),
    }
});