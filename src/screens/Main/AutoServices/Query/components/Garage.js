/* eslint-disable max-statements */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { AddressType } from 'screens/Contragent/Input/AddressType';
import { BankCard } from 'screens/Contragent/Input/BankCard';
import { LabelText } from 'uiComponents/SplashComponents';
import { InputComponents } from 'utils/component';
import { ColorCard } from 'utils/helper';
import { details, grees } from 'screens/Main/AutoServices/Query/components/config';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

export const Garage = () => {
    const { agent } = useSelector(state => state.agent, shallowEqual);
    const dispatchRedux = useDispatch();

    const success = (change, element) => {
        let payload;

        if (R.equals(R.path(['path'])(element), 'defaultPartSurcharge')) {
            payload = { settings: R.assocPath(R.without(['settings'])(R.path(['value'])(element)), change)(R.path(['settings'])(agent)) };

            return dispatchRedux({
                type: "AGENT",
                payload: R.mergeAll([agent, payload])
            })
        }
        if (R.equals(R.path(['path'])(element), 'costPerHour')) {
            payload = { settings: R.assocPath(R.without(['settings'])(R.path(['value'])(element)), change)(R.path(['settings'])(agent)) };

            return dispatchRedux({
                type: "AGENT",
                payload: R.mergeAll([agent, payload])
            })
        }
        if (R.equals(R.path(['path'])(element), 'phone')) {
            payload = R.zipObj([R.path(['path'])(element)], [R.join('', ['+7', change])]);

            return dispatchRedux({
                type: "AGENT",
                payload: R.mergeAll([agent, payload])
            })
        }
        payload = R.zipObj([R.path(['path'])(element)], [change]);

        dispatchRedux({
            type: "AGENT",
            payload: R.mergeAll([agent, payload])
        })
    }

    return (
        <View style={{ flex: 1 }}>
            <LabelText
                color={ColorCard('error').font}
                fonts="ShellMedium"
                items="CLabel"
                style={{
                    textAlign: 'left',
                    textTransform: 'uppercase',
                    padding: 10,
                }}
            >
                {i18n.t('LEGAL_ENTITIES')}
            </LabelText>
            <InputComponents
                bold
                error="Error message"
                errorMessage="PhoneError"
                example=""
                id={0}
                keyboardType="default"
                label="legalName"
                Lenght={10}
                main
                onChangeText={(change) => dispatchRedux({
                    type: "AGENT",
                    payload: R.mergeAll([agent,
                        { legalName: change }])
                })}
                path="legalName"
                storage={agent}
                value={["legalName"]}
            />
            <AddressType
                mode="legalAddress"
            />
            <BankCard
                storage={agent}
                unit
            />
            <View
                style={{ padding: 5, paddingTop: 15 }}
            >
                {R.addIndex(R.map)((element, key) => (
                    <InputComponents
                        key={R.join('_', ['mode', key])}
                        {...element}
                        onChangeText={(change) => dispatchRedux({
                            type: 'AGENT',
                            payload: R.mergeAll([
                                agent,
                                R.zipObj([element.path], [change])
                            ])
                        })}
                        storage={agent}
                    />
                ))(grees)}
                <LabelText
                    color={ColorCard('error').font}
                    fonts="ShellMedium"
                    items="CLabel"
                    style={{ textAlign: 'left', textTransform: 'uppercase', padding: 10 }}
                    testID="BlockLaker"
                >
                    {i18n.t('DatterService')}
                </LabelText>
                <InputComponents
                    {...R.head(details)}
                    onChangeText={(change) => dispatchRedux({
                        type: "AGENT",
                        payload: R.mergeAll([
                            agent,
                            R.zipObj([
                                R.path(['path'])(R.head(details))
                            ], [change])
                        ])
                    })}
                    storage={agent}
                />
            </View>
            <AddressType
                mode="address"
            />
            <View
                style={{ padding: 5, paddingTop: 15 }}
            >
                {R.addIndex(R.map)((element, key) => (
                    <InputComponents
                        key={R.join('_', [element.label, key])}
                        {...element}
                        onChangeText={(change) => success(change, element)}
                        storage={agent}
                    />
                ))(R.without([R.head(details)], details))}
            </View>
        </View>
    )
};