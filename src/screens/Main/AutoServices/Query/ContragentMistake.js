/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import i18n from 'localization';
import {isOdd} from 'utils/helper'

const useValidatorHooks =()=>{
  return []
}




export const ContragentMistake = ({ state }) => {
  const disabled =(agent)=> [
    isOdd(R.path(['name'])(agent)),
    isOdd(R.path(['phone'])(agent)),
    isOdd(R.path(['email'])(agent)),
    isOdd(R.path(['settings', 'costPerHour', 'amount'])(agent)),
    isOdd(R.path(['checkingAccount'])(agent)),
    isOdd(R.path(['correspondentAccount'])(agent)),
    isOdd(R.path(['rcbic'])(agent)),
    isOdd(R.path(['inn'])(agent)),
    isOdd(R.path(['kpp'])(agent)),
    isOdd(R.path(['psrn'])(agent)),
    isOdd(R.path(['settings', 'defaultPartSurcharge'])(agent)),
  ];

  const obj = [
    'notname',
    'notphone',
    'notemail',
    'notcostPerHour',
    'notcheckingAccount',
    'notcorrespondentAccount',
    'notrcbic',
    'notinn',
    'notkpp',
    'notpsrn',
    'notdefaultPartSurcharge',
  ];

  return (
    <View>
      {R.addIndex(R.map)((x, key) => {
        if (R.equals(x, false)) {
          return (<View key={R.join('_',[key,'empty'])}/>)
        }

        return (
          <View
              key={R.join('_',[key,'full'])}
            style={styles.items}
          >
            <LabelText
              color={ColorCard('classic').font}
              fonts="ShellMedium"
              items="SSDescription"
            >
              {i18n.t(obj[key])}
            </LabelText>
          </View>
        )
      })(disabled(state))}
    </View>
  )
};

const styles = StyleSheet.create({
  items: {
    height: 35,
    borderLeftWidth: 2,
    paddingLeft: 10,
    borderColor: 'red',
    margin: 2
  }
});