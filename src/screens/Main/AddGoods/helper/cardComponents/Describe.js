/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd, priceModed } from 'utils/helper';
import { IconState } from './IconState';

export const Describe = ({ data, magic }) => {
    if (isOdd(data)) return (<View />);

    const key = ['createdAt', 'inNal', 'Reserved', 'Sales', 'Tax', 'CoastIn', 'CoastOut'];
    const value = [
        R.join(' ', [moment(R.path(['updatedAt'])(data)).format("DD-MM-YYYY HH:mm")]),
        R.join(' ', [R.path(['amount', 'available'])(data)]),
        R.join(' ', [R.path(['amount', 'reserved'])(data)]),
        R.join(' ', [R.path(['amount', 'released'])(data)]),
        R.join(" ", [R.path(['vat'])(R.head(R.path(['depotItems'])(data)))]),
        priceModed(R.path(['priceIn', 'amount'])(R.head(R.path(['depotItems'])(data)))),
        priceModed(R.path(['priceOut', 'amount'])(R.head(R.path(['depotItems'])(data)))),
    ];

    const list = R.zipObj(key, value);

    if (R.path(['click'])(magic)) {
        return (
            <CardComponents
                style={[styles.shadow, {
                    flexDirection: 'column',
                    margin: 2
                }]}
            >
                {R.addIndex(R.map)((x, kw) => (
                    <RowComponents key={R.toString(x)}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="Description"
                            style={[styles.container, {
                                padding: 0,
                                justifyContent: 'space-between'
                            }]}
                        >
                            {i18n.t(x)}
                        </LabelText>
                        <View
                            style={[styles.rowCenter, {
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]}
                        >
                            <LabelText
                                color={ColorCard('classic').font}
                                fonts="ShellMedium"
                                items="SDescription"
                                style={[styles.container, {
                                    padding: 0,
                                    justifyContent: 'space-between',
                                    textTransform: 'lowercase'
                                }]}
                            >
                                {R.path([x])(list)}
                            </LabelText>
                            <IconState data={data} item={kw} />
                        </View>
                    </RowComponents>
                ))(key)}
            </CardComponents>
        )
    }

    return (<View />)
};
