/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const Header = ({ state }) => (
    <View>
        <RowComponents>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={[styles.container, { textAlign: 'left' }]}
            >
                {R.path(['brand'])(state)}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={[styles.container, { textAlign: 'right' }]}
            >
                {R.path(['name'])(state)}
            </LabelText>
        </RowComponents>
        <RowComponents style={{ justifyContent: 'flex-start' }}>
            <LabelText
                color={ColorCard('download').font}
                fonts="ShellMedium"
                items="SDescription"
                style={[styles.container, { textAlign: 'left' }]}
            >
                {(R.path(['article'])(state))}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={[styles.container, { textAlign: 'right', flex: 1 }]}
            >
                {R.path(['description'])(state)}
            </LabelText>
        </RowComponents>
    </View>
);