/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import Rub from 'assets/svg/rub';
import * as R from 'ramda';
import React from 'react';
import { Text } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { shallowEqual, useSelector } from 'react-redux';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const IconState = ({ item, data }) => {
    const depots = useSelector(state => state.depots, shallowEqual);

    const mesureUpdate = R.anyPass([R.equals(3), R.equals(1), R.equals(2)]);
    const svgXml = R.anyPass([R.equals(6), R.equals(5)]);

    return R.cond([
        [R.equals(4), () => (
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SRDescription"
                style={[styles.items, {
                    paddingLeft: 10,
                    justifyContent: 'center'
                }]}
                testID="BlockLaker"
            >
                %
            </LabelText>
        )],
        [mesureUpdate, () => (
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellBold"
                items="SRDescription"
                style={[styles.items, {
                    paddingLeft: 10,
                    textTransform: 'lowercase',
                    justifyContent: 'center'
                }]}
                testID="BlockLaker"
            >
                {R.path(['abbr'])(R.head(R.innerJoin(
                    (record, id) => R.equals(record.code, id),
                    R.path(['measures'])(depots) || [],
                    [R.path(['measure', 'code'])(data)])))}
            </LabelText>
        )],
        [svgXml, () => (
            <SvgXml
                height="10"
                style={[styles.items, { paddingLeft: 10, justifyContent: 'center' }]}
                weight="10"
                xml={Rub}
            />
        )],
        [R.T, () => (
            <Text
                style={[styles.items,
                { paddingLeft: 10, justifyContent: 'center' }]}
            />)],
        [R.F, () => (
            <Text
                style={[styles.items,
                { paddingLeft: 10, justifyContent: 'center' }]}
            />)],
    ])(item)
};
