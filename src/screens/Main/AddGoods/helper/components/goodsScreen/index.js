/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { Text, StyleSheet } from 'react-native'
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { styles } from 'screens/Main/AddGoods/helper/style'

export const CardScrollComponents = ({ state, data }) => (
    <CardComponents
        style={[styles.shadow, stylex.card, {
            backgroundColor: R.equals(R.path(['stock'])(data), state) ? 'gold' : 'white',
        }]}
    >
        <RowComponents>
            {R.addIndex(R.map)((x, keys) => (
                <Text
                    key={keys}
                    style={stylex.text}
                >
                    {x}
                </Text>
            ))(R.paths([['name'], ['brand']])(state))}
        </RowComponents>
        <RowComponents style={{ justifyContent: 'center' }} >
            {R.addIndex(R.map)((x, keys) => (
                <Text
                    key={keys}
                    style={stylex.text}
                >
                    {x}
                </Text>
            ))(R.paths([['article'], ['description']])(state))}
        </RowComponents>
    </CardComponents>
);

const stylex = StyleSheet.create({
    text: {
        textAlign: 'left',
        textTransform: 'uppercase',
        fontFamily: 'ShellMedium',
        fontSize: 12
    },
    card: {
        flexDirection: 'column',
        margin: 2
    }
})