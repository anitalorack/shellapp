/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { InputComponents } from 'utils/component';

export const InputCoast = ({ state, setState }) => R.addIndex(R.map)((element, key) => (
    <InputComponents
        key={key}
        {...element}
        onChangeText={(change) => setState({
            type: R.path(['paths'])(element), payload: change
        })}
        storage={state}
    />
))(CoastInput);

const CoastInput = [{
    id: 2,
    path: ['priceIn', 'amount'],
    paths: 'priceIn',
    main: true,
    label: 'PriceBuy',
    Lenght: 12,
    keyboardType: "numeric",
    value: ['priceIn', 'amount'],
    example: "",
    error: "Error message",
    errorMessage: 'PhoneError',
}, {
    id: 3,
    path: ['amount', "available"],
    paths: 'amount',
    main: true,
    label: 'qty',
    Lenght: 10,
    keyboardType: "numeric",
    value: ['amount'],
    example: "",
    error: "Error message",
    errorMessage: 'PhoneError',
}];