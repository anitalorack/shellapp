/* eslint-disable max-statements */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { Search } from 'screens/Main/AddGoods/helper/components/goodsScreen/Search';
import { CardScrollComponents } from 'screens/Main/AddGoods/helper/cardComponents';
import { DropDown } from 'screens/Main/AddGoods/helper/components/goodsScreen/Update/DropDown';
import { InputCoast } from 'screens/Main/AddGoods/helper/components/goodsScreen/Update/InputCoast';
import { reducerUpdate } from 'screens/Main/AddGoods/helper/components/reducer';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { isOdd } from 'utils/helper';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { Create } from 'screens/Main/AddGoods/helper/components/goodsScreen/Create';

export const Update = ({ dispatch, data }) => {
    const [state, setState] = React.useReducer(reducerUpdate, data);
    const [modal, setModal] = React.useState({ isVisible: false })
    const [screen, setScreen] = React.useState(false)

    const successModal = (e) => {
        setScreen(false)

        return R.cond([
            [R.equals('Goods'), () => {
                setState({ type: 'DepotItem', payload: R.assocPath(['depotItem'], { stock: R.path(['payload'])(e) })(state) })
            }],
            [R.equals('UpdateStock'), () => {
                setState({ type: 'DepotItem', payload: R.assocPath(['depotItem'], { stock: R.path(['payload'])(e) })(state) })
            }],
            [R.equals('Modal'), () => setModal({ isVisible: false })],
            [R.equals('CLOSED'), () => dispatch({ type: "Modal", payload: false })],
            [R.equals("EDIT_MODAL"), () => {
                if (R.path(['id'])(state)) {
                    return null
                }
                setModal({ isVisible: true })
            }],
            [R.equals('UPDATE'), () => {
                dispatch({ type: "Modal", payload: false })
                dispatch({ type: "Goods", payload: state })
            }],
            [R.equals('NEXT'), () => {
                dispatch({ type: "Goods", payload: state })

                return dispatch({ type: "Modal", payload: false })
            }],
            [R.equals('ADDED'), () => {
                dispatch({ type: "Goods", payload: state })

                return dispatch({ type: "Modal", payload: false })
            }]
        ])(R.path(['type'])(e))
    }

    const list = [R.cond([
        [R.equals(true), R.always('ADDED')],
        [R.equals(false), R.always('UPDATE')],
        [isOdd, R.always('UPDATE')],
    ])(screen), 'CLOSED']

    const modalScreen = React.useCallback(() => R.cond([
        [R.equals(false), () => (
            <Search
                data={R.path(['depotItem'])(state)}
                dispatch={successModal}
                setScreen={setScreen}
            />
        )],
        [R.equals(true), () => (
            <Create
                dispatch={successModal}
                setScreen={setScreen}
            />
        )]
    ])(screen), [screen])

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                style={{ padding: 10 }}
            >
                <Modal isVisible={R.path(['isVisible'])(modal)}>
                    <ModalDescription style={{ flex: 1, margin: 10 }}>
                        {modalScreen()}
                    </ModalDescription>
                </Modal>
                <CardScrollComponents
                    data={R.path(['depotItem', 'stock'])(state)}
                    dispatch={() => successModal({ type: 'EDIT_MODAL' })}
                    state={R.path(['depotItem', 'stock'])(state)}
                />
                <DropDown
                    setState={setState}
                    state={state}
                />
                <InputCoast
                    setState={setState}
                    state={state}
                />
            </ScrollView>
            <RowComponents>
                {R.map(x => (
                    <TouchesState
                        key={x}
                        color={R.equals(x, 'CLOSED') ? "VeryRed" : "Yellow"}
                        flex={1}
                        onPress={() => successModal({ type: x })}
                        title={x}
                    />
                ))(list)}
            </RowComponents>
        </View>
    )
};
