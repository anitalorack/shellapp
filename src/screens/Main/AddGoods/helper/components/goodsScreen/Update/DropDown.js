/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper';
import { View } from 'react-native'

export const useDepotsSelectHooks = ({ state }) => {
    const depots = useSelector(state => state.depots, shallowEqual);

    const action = React.useCallback(
        (data) => R.head(R.innerJoin((record, id) => R.equals(record.id, id),
            R.path(['depots', 'items'])(depots)
        )([data])),
        [depots, state],
    )

    const position = React.useMemo(() => {
        const currentDroped = R.head(R.innerJoin(
            (record, id) => record.id === id,
            R.defaultTo([])(R.path(['depots', 'items'])(depots)),
            R.pipe(R.reject(isOdd))([
                R.defaultTo(R.path(['depotsId'])(state))(R.path(['depots', 'id'])(state)),
            ])
        ));

        return {
            placeholder: R.defaultTo(null)(currentDroped),
            items: R.append(
                { label: 'выбрать', value: null },
                R.map(x => ({
                    label: R.path(['name'])(x),
                    value: R.path(['id'])(x)
                }))(R.path(['depots', 'items'])(depots)))
        }
    }, [state])

    return [action, position]
}

export const DropDown = ({ state, setState }) => {
    const [action, position] = useDepotsSelectHooks({ state })

    return (
        <View>
            <UiPicker
                dispatch={(e) => {
                    setState({
                        type: 'depots',
                        payload: action(e)
                    })
                }}
                element={{ path: "GARAGE" }}
                Items={R.path(['items'])(position)}
                main
                placeholder={{ id: R.path(['id'])(R.path(['placeholder'])(position)) }}
            />
            <UiPicker
                dispatch={(e) => {
                    setState({
                        type: 'Tax',
                        payload: e
                    })
                }}
                element={{ path: "Tax" }}
                Items={[
                    { label: "12", value: 12, },
                    { label: "20", value: 20, },
                    { label: "6", value: 6 },
                    { label: 'выбрать', value: null },
                ]}
                main
                placeholder={{ id: R.defaultTo(null)(R.path(['vat'])(state)) }}
            />
        </View>
    )
};