/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper';

const useSubDownHooks = ({ state }) => {
    const stock = useSelector(state => state.stock, shallowEqual);

    const currentSubStockGroupTree = React.useCallback(
        (data) => {
            if (isOdd(R.path(['stockGroupsTreeSelect'])(state))) return null;
            if (isOdd(R.path(['stockGroupsTreeSelect', 'children'])(state))) return null;

            return R.head(R.innerJoin((record, id) => R.equals(record.id, id),
                R.path(['stockGroupsTreeSelect', 'children'])(state)
            )([data]))
        }, [stock, state],
    )

    const updateSubStockGroupTree = React.useMemo(() => {
        if (isOdd(R.path(['stockGroupsTreeSelect'])(state))) return null;
        if (isOdd(R.path(['stockGroupsTreeSelect', 'children'])(state))) return null;

        return ({
            placeholder: R.defaultTo(null)(R.path(['children', 'id'])(state)),
            items: R.append({ label: 'выбрать', value: null },
                R.map(x => ({
                    label: R.path(['name'])(x),
                    value: R.path(['id'])(x)
                }))(R.path(['stockGroupsTreeSelect', 'children'])(state)))
        })
    }, [state, stock])

    return [currentSubStockGroupTree, updateSubStockGroupTree]
}

const useParentDownHooks = ({ state }) => {
    const stock = useSelector(state => state.stock, shallowEqual);
    const currentStockGroupTree = React.useCallback(
        (data) => R.head(R.innerJoin((record, id) => R.equals(record.id, id),
            R.path(['stockGroupsTree'])(stock)
        )([data])),
        [stock, state]
    )

    const updateStockGroupTree = React.useMemo(() => ({
        placeholder: R.defaultTo(null)(R.path(['stockGroupsTreeSelect', 'id'])(state)),
        items: R.append({ label: 'выбрать', value: null },
            R.map(x => ({
                label: R.path(['name'])(x),
                value: R.path(['id'])(x)
            }))(R.path(['stockGroupsTree'])(stock)))
    }),
        [stock, state]
    )

    return [currentStockGroupTree, updateStockGroupTree]
}

export const DropDown = ({ state, setState }) => {
    const [currentStockGroupTree, updateStockGroupTree] = useParentDownHooks({ state })

    return (
        <View>
            <UiPicker
                dispatch={(elem) => {
                    setState({
                        type: 'stockGroupsTreeSelect',
                        payload: currentStockGroupTree(elem)
                    })
                }}
                element={{ path: "GROUP" }}
                Items={R.path(['items'])(updateStockGroupTree)}
                main
                placeholder={{ id: R.path(['placeholder'])(updateStockGroupTree) }}
            />
            <AppScreen
                setState={(e) => setState(e)}
                state={state}
            />
        </View>
    )
};

const AppScreen = ({ state, setState }) => {
    const [currentSubStockGroupTree, updateSubStockGroupTree] = useSubDownHooks({ state })

    if (isOdd(R.path(['stockGroupsTreeSelect'])(state))) return (<View />);
    if (isOdd(R.path(['stockGroupsTreeSelect', 'children'])(state))) return (<View />);

    return (
        <UiPicker
            dispatch={(e) => {
                setState({
                    type: 'children',
                    payload: currentSubStockGroupTree(e)
                })
            }}
            element={{ path: "SUBGROUP" }}
            Items={R.path(['items'])(updateSubStockGroupTree)}
            main
            placeholder={{ id: R.path(['placeholder'])(updateSubStockGroupTree) }}
        />
    )
};