/* eslint-disable react/jsx-indent-props */

import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native'
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { shallowEqual, useSelector } from 'react-redux';

export const DropMeasure = ({ setState, state }) => {
    const depots = useSelector(state => state.depots, shallowEqual);

    return (
        <View>
            <UiPicker
                dispatch={(e) => {
                    const data = R.head(R.innerJoin((record, id) => R.equals(record.abbr, id),
                        R.path(['measures'])(depots)
                    )([e]));
                    setState({
                        type: 'measure',
                        payload: { all: data, depotId: R.path(['payload', 'id'])(data) }
                    })
                }}
                element={{ path: "CoastMeasure" }}
                Items={R.append({ label: 'выбрать', value: null },
                    R.map(x => ({
                        label: R.path(['name'])(x),
                        value: R.path(['abbr'])(x)
                    }))(R.path(['measures'])(depots)))}
                main
                placeholder={{ id: R.defaultTo(null)(R.path(['measure', 'abbr'])(state)) }}
            />
        </View>
    )
};
