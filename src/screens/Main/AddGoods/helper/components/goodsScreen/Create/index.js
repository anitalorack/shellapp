/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { reducerCreate } from 'screens/Main/AddGoods/helper/components/reducer';
import { DropDown } from 'screens/Main/AddGoods/helper/components/goodsScreen/Create/DropDown'
import { DropMeasure } from 'screens/Main/AddGoods/helper/components/goodsScreen/Create/DropMeasure'
import { InputDescription, InputInitial, InputName } from 'screens/Main/AddGoods/helper/components/goodsScreen/Create/Input'
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { TouchesState } from 'styled/UIComponents/UiButtom';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import i18n from 'localization'

export const Create = ({ dispatch, setScreen }) => {
    const [state, setState] = React.useReducer(reducerCreate, {});

    const success = (x) => {
        if (R.equals(x, 'NEXT')) {
            dispatch({
                type: 'UpdateStock',
                payload: {
                    article: R.path(['article'])(state),
                    oem: R.path(['oem'])(state),
                    name: R.path(['name'])(state),
                    description: R.path(['description'])(state),
                    brand: R.path(['brand'])(state),
                    measure: R.path(['measure'])(state),
                    stockGroupId: R.path(['children'])(state),
                }
            })

            return dispatch({ type: "Modal", payload: false })
        }
        if (R.equals(x, 'CLOSED')) {
            return dispatch({ type: "Modal", payload: false })
        }
    };

    return (
        <View
            style={{ flex: 1 }}
        >
            <HeaderApp
                button="BACK"
                dispatch={() => setScreen(false)}
                empty={false}
                header
                title={i18n.t("NEWGOODSTASK")}
            />
            <ScrollView>
                <View style={{ marginLeft: 10, marginRight: 10, marginTop: 20 }}>
                    <InputInitial
                        setState={setState}
                        state={state}
                    />
                    <DropDown
                        setState={setState}
                        state={state}
                    />
                    <InputName
                        setState={setState}
                        state={state}
                    />
                    <DropMeasure
                        setState={e => setState(e)}
                        state={state}
                    />
                    <InputDescription
                        setState={setState}
                        state={state}
                    />
                </View>
            </ScrollView>
            <RowComponents>
                {R.map(x => (
                    <TouchesState
                        key={x}
                        color={R.equals(x, 'NEXT') ? "Yellow" : "VeryRed"}
                        flex={1}
                        onPress={() => success(x)}
                        title={x}
                    />
                ))(['NEXT', 'BACK'])}
            </RowComponents>
        </View>
    )
};