/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-indent-props */
import Rub from 'assets/svg/rub';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    Text,
    TouchableOpacity, View
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { priceModed } from 'utils/helper';
import { stylex } from './stylex';

export const CurrentComponent = ({ click, setSetModal, state }) => {
    const list = [
        { article: R.pipe(R.path(['depotItem', 'stock', 'article']))(state) },
        { tax: R.join(' ', [R.path(['vat'])(state), '%']) },
        { qty: R.join(' ', R.paths([['amount'], ['depotItem', 'stock', 'measure', 'abbr']])(state)) },
        { CoastIn: priceModed(R.path(['priceIn', 'amount'])(state)) }
    ]

    const success = ({ e, params }) => R.cond([
        [R.equals('DESTROY'), () => console.log('DESTROY')],
        [R.equals('EDIT'), () => {
            setSetModal({ isVisible: true, params })
        }],
    ])(e)

    if (click) return (
        <View
            style={stylex.side}
        >
            <TouchableOpacity
                onPress={() => success({ e: 'EDIT', params: state })}
                style={stylex.touch}
            >
                {R.addIndex(R.map)((x, key) => (
                    <View
                        key={key}
                        style={[stylex.view]}
                    >
                        <Text
                            style={[stylex.text, stylex.text2]}
                        >
                            {i18n.t(R.keys(x))}
                        </Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Text
                                style={[stylex.text, stylex.text3]}
                            >
                                {R.values(x)}
                            </Text>
                            {R.cond([
                                [R.equals(3), () => <SvgXml height="10" weight="10" xml={Rub} />],
                                [R.T, () => <View />],
                                [R.F, () => <View />],
                            ])(key)}
                        </View>
                    </View>))(list)}
                <View style={[styles.line, { marginTop: 10, paddingTop: 3, paddingLeft: 3 }]} />
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text
                        style={[stylex.text, stylex.text2]}
                    >
                        {i18n.t('Total')}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Text
                            style={[stylex.text, stylex.text3]}
                        >
                            {priceModed(R.product(R.paths([['amount'], ['priceIn', 'amount']])(state)))}
                        </Text>
                        <SvgXml height="10" weight="10" xml={Rub} />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )

    return (
        <View />
    )
}
