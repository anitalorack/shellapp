import { StyleSheet } from 'react-native';
import { Colors } from 'react-native-paper';

export const stylex = StyleSheet.create({
    card: {
        margin: 1,
        height: 80,
        backgroundColor: Colors.grey300,
        justifyContent: 'center',
        alignItems: 'center'
    },
    side: {
        backgroundColor: Colors.grey300,
        paddingBottom: 5
    },
    touch: {
        marginRight: 10,
        marginLeft: 10,
    },
    text: {
        fontFamily: 'ShellBold',
        color: '#404040',
        fontSize: 13,
        justifyContent: 'center',
        textAlign: 'center'
    },
    view: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 3,
        justifyContent: 'space-between',
    },
    start: {
        flex: 1,
    },
    view2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5
    },
    footer: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-between'
    },
    text2: {
        textAlign: 'left',
        fontSize: 11,
        color: '#404040',
        fontFamily: 'ShellMedium',
        textTransform: 'uppercase'
    },
    text3: {
        textAlign: 'right',
        color: '#404040',
        fontFamily: 'ShellMedium',
        fontSize: 11,
    }
})