/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import React from 'react';
import ADD from 'assets/svg/add';
import { View } from 'react-native';
import { ModalUpdate } from 'screens/Main/AddGoods/helper/components/GoodHomeScreen/ModalUpdate'
import { GoodHomeScreen } from 'screens/Main/AddGoods/helper/components/GoodHomeScreen'
import * as R from 'ramda'
import { useSelector, shallowEqual } from 'react-redux';

export const ComponentsGoods = () => {
    const [modal, setSetModal] = React.useState({ isVisible: false })
    const { goods } = useSelector(state => state.goods, shallowEqual);

    return (
        <View style={{ flex: 1 }}>
            <HeaderApp
                button="ADD"
                dispatch={() => setSetModal({ isVisible: true })}
                empty={R.path(['id'])(goods)}
                svg={ADD}
                title={i18n.t("ADDGoods")}
            />
            <View style={{ marginLeft: 10, marginRight: 10 }}>
                <GoodHomeScreen />
            </View>
            <ModalUpdate
                current={{}}
                modal={modal}
                setSetModal={setSetModal}
            />
        </View>
    )
};