/* eslint-disable complexity */
import * as R from 'ramda';

export const reducer = (state = { goods: [] }, action) => {
    switch (action.type) {
        case "Contragent":
            return R.assocPath(['query', 'counterparty'], action.payload.data)(state);
        case "Goods":
            return R.assocPath(['query', 'items'], action.payload)(state);
        case "Modal":
            return R.assocPath(['modal'], action.payload)(state);
        case "Query":
            return R.assocPath(['query'], action.payload)(state);
        case "Docs":
            return R.assocPath(['docs'], action.payload)(state);
        case "New":
            return { query: { items: [] }, goods: [] };
        default:
            return state
    }
};

export const reducerGoods = (state = { query: [] }, action) => {
    console.log(action)
    switch (action.type) {
        case "name":
            return R.assocPath(['data'], action.payload, state);
        case "Query":
            return R.assocPath(['query'], action.payload, state);
        case "Data":
            return R.assocPath(['depotItem'], action.payload, state);
        case "UpdateStock":
            return R.assocPath(['depotItem'], action.payload, state);
        case "action":
            return R.assocPath(['action'], action.payload, state);
        case "article":
            return R.assocPath(['data'], R.mergeAll([R.path(['data'])(state), action.payload]), state);
        case "oem":
            return R.assocPath(['data'], R.mergeAll([R.path(['data'])(state), action.payload]), state);
        case "priceIn":
            return R.assocPath(['data'], action.payload, state);
        case "Tax":
            return R.assocPath(['data'], R.mergeAll([R.path(['data'])(state), { vat: action.payload }]), state);
        case "amount":
            return R.assocPath(['data'], action.payload, state);
        case "description":
            return R.assocPath(['data'], R.mergeAll([R.path(['data'])(state), action.payload]), state);
        case "brand":
            return R.assocPath(['data'], R.mergeAll([R.path(['data'])(state), action.payload]), state);
        case "depots":
            return R.mergeAll([state, R.path(['payload', 'all'])(action)]);
        case "stockGroupsTree":
            return R.assocPath(['stockGroupsTree'], action.payload, state);
        case 'children':
            return R.assocPath(['children'], action.payload, state);
        default:
            return state;
    }
};

export const reducerSelect = (state = { query: [] }, action) => {
    switch (action.type) {
        case "Query":
            return R.assocPath(['query'], action.payload, state);
        case "Data":
            return R.assocPath(['data'], action.payload, state);
        case "Text":
            return R.assocPath(['text'], action.payload, state);
        default:
            return state
    }
};

export const reducerUpdate = (state = {}, action) => {
    switch (action.type) {
        case "UpdateStock":
            return R.mergeAll([state, action.payload])
        case 'priceIn':
            return R.assocPath(['priceIn', 'amount'], action.payload, state);
        case 'amount':
            return R.assocPath(['amount'], action.payload, state);
        case 'Tax':
            return R.assocPath(['vat'], action.payload, state);
        case 'depots':
            return R.assocPath(['depots'], action.payload, state);
        case 'DepotItem':
            return action.payload
        default:
            return state
    }
};


export const reducerAgent = (state = { query: [] }, action) => {
    switch (action.type) {
        case "Query":
            return R.assocPath(['query'], action.payload)(state);
        case "Data":
            return R.assocPath(['data'], action.payload)(state);
        case "Modal":
            return R.assocPath(['isVisible'], action.payload)(state);
        case "Text":
            return R.assocPath(['text'], action.payload)(state);
        default:
            return state;
    }
};


export const docsReducer = (state = {}, action) => {
    switch (action.type) {
        case "Type":
            return R.assocPath(["type"], action.payload)(state);
        case "number":
            return R.assocPath(["number"], action.payload)(state);
        case "date":
            return R.assocPath(["date"], action.payload)(state);
        default:
            return state
    }
};

export const reducerCreate = (state, action) => R.cond([
    [R.equals('name'), R.always(R.assocPath(['name'], action.payload, state))],
    [R.equals('Scroll'), R.always(R.assocPath(['Scroll'], action.payload, state))],
    [R.equals('oem'), R.always(R.assocPath(['oem'], action.payload, state))],
    [R.equals('brand'), R.always(R.assocPath(['brand'], action.payload, state))],
    [R.equals('article'), R.always(R.assocPath(['article'], action.payload, state))],
    [R.equals('description'), R.always(R.assocPath(['description'], action.payload, state))],
    [R.equals('measure'), R.always(R.assocPath(['measure'], R.path(['payload', 'all'])(action), state))],
    [R.equals('stockGroupsTreeSelect'), R.always(R.assocPath(['stockGroupsTreeSelect'], R.path(['payload'])(action), state))],
    [R.equals('stockGroupsTree'), R.always(R.assocPath(['stockGroupsTree'], R.path(['payload', 'children'])(action), state))],
    [R.equals('children'), R.always(R.assocPath(['children'], R.path(['payload'])(action), state))],
    [R.F, R.always(state)],
])(action.type);
