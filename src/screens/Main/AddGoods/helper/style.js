import { StyleSheet } from 'react-native'
import { uiColor } from 'styled/colors/uiColor.json';

export const styles = StyleSheet.create({
    container: {
        padding: 5,
        textTransform: 'uppercase',
    },
    items: {
        textAlign: 'center',
        textTransform: 'capitalize'
    },
    block: { margin: 10, },
    create: {
        textAlign: 'left',
        paddingLeft: 20,
        padding: 10,
        textTransform: 'capitalize'
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: 'grey'
    },
    rowCenter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    center: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    pdfButton: {
        position: 'absolute',
        width: 40,
        height: 40,
        borderWidth: 1,
        right: 0,
        backgroundColor: uiColor.Light_Green,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    dropdownStyles: {
        color: 'black',
        fontFamily: 'ShellMedium',
        textTransform: 'uppercase',
        marginBottom: 35
    },
    avatar: {
        height: 50,
        width: 50,
        justifyContent: 'center',
        borderRadius: 50,
        backgroundColor: uiColor.Light_Green,
        alignItems: 'center',
    },
    ScrollView: {
        minHeight: 150,
        maxHeight: 350
    },
    modal: {
        margin: 10,
        right: 0,
        left: 0,
        top: 75,
        bottom: 0,
        position: 'absolute',
        backgroundColor: uiColor.Very_Pole_Grey,
        zIndex: 1000
    },
    font: {
        fontFamily: 'ShellMedium',
    }
});

export const Styles = StyleSheet.create({
    background: {
        width: 80,
        height: 100,
        textAlign: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        color: 'white',
    },
    card: {
        height: 175,
        width: 125,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    snap: {
        flex: 0,
        backgroundColor: '#fff',
        padding: 5,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    row: {
        position: 'absolute',
        bottom: 20,
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row'
    }
});