/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';

export const useCounterGoods = () => {
    const { goods } = useSelector(state => state.goods, shallowEqual);
    const data = R.path(['counterparty'])(goods);

    const CounterList = React.useCallback(() => {
        const list = [
            R.path(['name'])(data),
            R.join(': ', [i18n.t('inn'), R.path(['inn'])(data)]),
            i18n.t(R.path(['type'])(data))
        ];

        return R.map(x => (
            <Text
                key={x}
                style={[
                    stylex.text, {
                        fontFamily: R.equals(R.head(list), x) ? "ShellBold" : "ShellLight",
                        fontSize: R.equals(R.head(list), x) ? 13 : 11,
                        padding: 2
                    }
                ]}
            >
                {x}
            </Text>
        ))(list)
    }, [goods])

    return [CounterList]
}

export const stylex = StyleSheet.create({
    text: {
        textTransform: 'uppercase',
        textAlign: 'left',
        fontSize: 13,
    },
    card: {
        minHeight: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    card2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        justifyContent: 'center'
    },
    column: {
        flexDirection: 'column',
        flex: 1,
    },
    view: {
        flex: 1,
        padding: 10,
        borderWidth: 1
    },
    touch: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    }
})