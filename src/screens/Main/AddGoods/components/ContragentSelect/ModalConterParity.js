/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { QUERY_COUNTERPARTIES } from 'gql/depots';
import * as R from 'ramda';
import React from 'react';
import { reducerAgent } from 'screens/Main/AddGoods/helper/components/reducer';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { InputComponents } from 'utils/component';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import i18n from 'localization';
import {
    TouchableOpacity, Text, StyleSheet, ScrollView, View
} from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

export const ModalConterParity = ({
    dispatch
}) => {
    const ref = React.useRef()
    const [state, setState] = React.useReducer(reducerAgent, { query: [] });
    const dispatchRedux = useDispatch()
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const [loadgreeting] = useLazyQuery(QUERY_COUNTERPARTIES, {
        onCompleted: (data) => {
            setState({ type: "Query", payload: R.path(['counterparties', 'items'])(data) })
        },
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        if (!ref.current) {
            loadgreeting({ variables: { where: {} } });
            ref.current = true
        }
    }, [state])

    const CardAgent = ({ data, state }) => {
        const summary = [
            R.path(['name'])(data),
            R.join(': ', [i18n.t('inn'), R.path(['inn'])(data)]),
            i18n.t(R.path(['type'])(data))
        ]

        return (
            <CardComponents
                style={[
                    styles.shadow,
                    stylex.card, {
                        backgroundColor: R.equals(data, R.path(['data'])(state)) ? 'gold' : 'white',
                    }
                ]}
            >
                <TouchableOpacity onPress={() => {
                    dispatchRedux({
                        type: "ADD_GOODS",
                        payload: R.assocPath(['counterparty'], data)(goods)
                    })
                    dispatch(false)
                }}
                >
                    {R.addIndex(R.map)((x, key) => (
                        <Text
                            key={key}
                            style={[
                                { fontFamily: R.equals(key, 0) ? 'ShellBold' : 'ShellLight' },
                                { fontSize: R.equals(key, 0) ? 13 : 10 },
                                stylex.text
                            ]}
                        >
                            {x}
                        </Text>
                    ))(summary)}
                </TouchableOpacity>
            </CardComponents>
        )
    }

    return (
        <CardComponents style={{ flex: 1 }}>
            <HeaderApp
                button="CLOSED"
                dispatch={() => dispatch(false)}
                header
                title={R.path(['data', 'name'])(state) ? R.join(' ', [R.path(['data', 'name'])(state)]) : i18n.t('searchAgent')}
            />
            <InputComponents
                {...initialInputState}
                onChangeText={(change) => {
                    loadgreeting({ variables: { where: { q: change } } });
                    setState({ type: "Text", payload: change })
                }}
                storage={state}
            />
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                {R.addIndex(R.map)((data, keys) => (
                    <View
                        key={keys}
                        style={{ flex: 1 }}
                    >
                        {CardAgent({ data, state })}
                    </View>
                ))(R.path(['query'])(state))}
            </ScrollView>
        </CardComponents>
    )
};

const initialInputState = {
    id: "text",
    path: 'name',
    keyboardType: "default",
    label: 'SEARCHBYLIST',
    title: "Error message",
    value: ["text"],
    maxLength: 120,
};

const stylex = StyleSheet.create({
    card: {
        flexDirection: 'column',
        textAlign: 'center',
        margin: 1
    },
    text: {
        textAlign: 'left',
        textTransform: 'uppercase',
        paddingTop: 2,
        paddingBottom: 2
    }
})