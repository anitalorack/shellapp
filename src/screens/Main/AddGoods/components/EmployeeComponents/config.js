/* eslint-disable no-negated-condition */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import edit from 'assets/svg/edit';
import next from 'assets/svg/next';
import phone from 'assets/svg/phone';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { Linking } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { isOdd, phoneMedd } from 'utils/helper';

export const list = ({ state }) => R.reject(isOdd, [
    R.pipe(
        R.path(['actor', 'name']),
        R.omit(["__typename"]),
        R.reject(isOdd),
        R.values,
        R.reject(isOdd),
        R.join(' '),
    )(state),
    !isOdd(R.path(['actor', 'role'])(state)) ? i18n.t(R.path(['actor', 'role'])(state)) : "",
    phoneMedd(R.path(['actor', 'phone'])(state))
]);

export const SVGAction = ({ mode }) => R.cond([
    [R.equals("author"), () => <SvgXml height="30" width="30" xml={phone} />],
    [R.equals("client"), () => <SvgXml height="30" width="30" xml={edit} />],
    [R.equals("performer"), () => <SvgXml height="30" width="30" xml={next} />],
    [R.equals("owner"), () => <SvgXml height="30" width="30" xml={edit} />],
    [isOdd, () => <SvgXml height="30" width="30" xml={phone} />],
])(mode);

export const color = R.cond([
    [R.equals('author'), R.always('green')],
    [R.equals('performer'), R.always('#ededed')],
    [isOdd, R.always('green')],
    [R.F, R.always('#ededed')],
    [R.T, R.always('#ededed')],
]);

export const action = ({ state, mode, dispatch }) => R.cond([
    [R.equals("author"), () => Linking.openURL(`tel:${R.path(['actor', 'phone'])(state)}`)],
    [R.equals("client"), () => dispatch({ type: "Client" })],
    [R.equals("performer"), () => dispatch({ type: "Performer" })],
    [R.equals("owner"), () => dispatch({ type: "Owner" })],
    [isOdd, () => Linking.openURL(`tel:${R.path(['actor', 'phone'])(state)}`)],
])(mode);