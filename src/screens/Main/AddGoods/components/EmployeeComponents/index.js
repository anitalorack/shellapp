/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import user from 'assets/svg/userPro';
import { useEmployeeHooks } from 'hooks/useEmployeeHooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import {
    StyleSheet, Text, TouchableOpacity, View
} from 'react-native';
import { SvgXml } from 'react-native-svg';
import {
    action, color, list, SVGAction
} from 'screens/Main/AddGoods/components/EmployeeComponents/config';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { isOdd } from 'utils/helper';

export const EmployeeComponents = ({
    actor, HEADER, mode, dispatch
}) => {
    const [state] = useEmployeeHooks({ mode, actor })

    const CardStrokeInfo = ({ state }) => R.map(x => {
        if (isOdd(x)) return (<View />);

        return (
            <Text
                key={R.toString(x)}
                style={[styles.container, {
                    fontFamily: R.equals(R.head(list({ state })), x) ? "ShellMedium" : "ShellLight",
                    fontSize: R.equals(R.head(list({ state })), x) ? 12 : 10
                }]}
            >
                {x}
            </Text>
        )
    })(list({ state }))

    return (
        <View>
            <HeaderApp
                empty
                title={i18n.t(R.defaultTo('author')(HEADER))}
            />
            <CardComponents
                style={[styles.shadow, {
                    marginLeft: 10,
                    marginRight: 10
                }]}
            >
                <RowComponents>
                    <View
                        style={stylex.card}
                    >
                        <SvgXml height="30" width="30" xml={user} />
                    </View>
                    <View style={{ flex: 1 }} >
                        {CardStrokeInfo({ state })}
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => action({ state, mode, dispatch })}
                            style={[styles.avatar, {
                                backgroundColor: color(mode)
                            }]}
                        >
                            <SVGAction mode={mode} />
                        </TouchableOpacity>
                    </View>
                </RowComponents>
            </CardComponents>
        </View>
    )
};

const stylex = StyleSheet.create({
    card: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 13,
        fontFamily: 'ShellMedium',
        paddingTop: 0,
    }
})