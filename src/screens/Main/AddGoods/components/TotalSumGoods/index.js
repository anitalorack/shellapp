/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import Rub from 'assets/svg/rub';
import { SvgXml } from 'react-native-svg';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { priceModed, isOdd } from 'utils/helper';
import { styles } from 'screens/Main/AddGoods/helper/style';

export const TotalSumGoods = () => {
    const { goods } = useSelector(state => state.goods, shallowEqual);

    if (isOdd(goods.items)) return (<View />)

    return (
        <CardComponents
            style={[styles.shadow, { margin: 1 }]}
        >
            <RowComponents style={{ marginTop: 10, marginBottom: 10 }}>
                <View>
                    <Text
                        style={stylex.text}
                    >
                        {i18n.t('TotalAccount')}
                    </Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text
                        style={stylex.text}
                    >
                        {priceModed(R.pipe(
                            R.path(['items']),
                            R.map(
                                R.pipe(
                                    R.paths([['amount'], ['priceIn', 'amount']]),
                                    R.product)),
                            R.sum
                        )(goods))}
                    </Text>
                    <SvgXml height="10" weight="10" xml={Rub} />
                </View>
            </RowComponents>
        </CardComponents>
    )
};

const stylex = StyleSheet.create({
    text: {
        fontFamily: 'ShellBold',
        color: "#404040",
        fontSize: 13,
    }
})