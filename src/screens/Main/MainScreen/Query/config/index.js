import * as R from 'ramda';
import { isOdd } from 'utils/helper';

export const initialState = {
    right: false,
    left: false,
    text: '',
    swipe: false,
    current: null,
    checker: "AllWork",
    page: 1,
    variables: { where: {}, order: { id: "desc" }, paginate: { page: 1, limit: 20 } },
    query: { items: [] }
};

export const reducer = (state = initialState, action) => {
    const initialSearch = null;

    switch (action.type) {
        case "Query":
            return R.assocPath(['query'], action.payload, state);
        case "Right":
            return R.mergeAll([state, { right: !R.path(['right'])(state) }]);
        case "Left":
            return R.mergeAll([state, R.assocPath(['search'], initialSearch, { left: !R.path(['left'])(state) })]);
        case "Text":
            return R.mergeAll([state, { text: R.path(['payload'])(action) }]);
        case "Swipe":
            return R.mergeAll([state, { swipe: R.path(['payload'])(action) }]);
        case "Checker":
            if (R.equals(R.path(['payload'])(action))(R.path(['checker'])(state))) return R.mergeAll([state, { checker: "AllWork" }]);

            return R.mergeAll([state, { checker: R.path(['payload'])(action) }]);
        case "Search":
            return R.mergeAll([state, { search: R.path(['payload'])(action) }]);
        case "Updata":
            return R.assocPath(['data'], R.path(['payload'])(action), state);
        case "Variables":
            return R.assocPath(['variables'], R.path(['payload'])(action), state);
        default:
            return state
    }
};

export const UpdateState = (searchText, path) => {
    if (isOdd(searchText)) {
        return { delete: null }
    }

    return R.assocPath(path, searchText, {})
};

export const StateBuild = (state, storage) => {
    const connecter = R.cond([
        [R.equals("Checker"), R.always({ checker: R.equals(state.type, 'Checker') ? state.payload : storage.checker })],
        [R.equals("Search"), R.always({ search: R.equals(state.type, 'Search') ? state.payload : storage.search })],
        [R.equals("Text"), R.always({ text: R.equals(state.type, 'Text') ? state.payload : storage.text })],
        [R.equals("Page"), R.always({ page: R.equals(state.type, 'Page') ? state.payload : storage.page })],
    ]);

    return R.pipe(
        R.map(connecter),
        R.mergeAll,
        R.reject(isOdd)
    )(["Checker", 'Search', 'Text', 'Page'])
};

export const setRequestDataLoader = (state) => {
    const searchText = R.path(['text'])(state);
    const checker = R.path(['checker'])(state);
    const vehicleId = R.path(['id'])(state);

    const blocked = R.cond([
        [R.equals("canceled"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("suspended"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("reconciliation"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("closed"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("completed"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("inWork"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("preorder"), R.always(R.assocPath(['status', 'eq'], checker, {}))],
        [R.equals("AllWork"), R.always({ delete: null })],
    ])(R.path(['checker'])(state));

    const text = R.cond([
        [R.equals('LAST'), R.always(UpdateState(searchText, ['client', 'name', 'last', 'contain']))],
        [R.equals('NAME'), R.always(UpdateState(searchText, ['client', 'name', 'first', 'contain']))],
        [R.equals('PHONE'), R.always(UpdateState(searchText, ['client', 'phone', 'contain']))],
        [R.equals('VIN'), R.always(UpdateState(searchText, ['vehicle', 'vin', 'contain']))],
        [R.equals('PLATE'), R.always(UpdateState(searchText, ['vehicle', 'plate', 'contain']))],
        [isOdd, R.always(UpdateState(searchText, ['q']))],
    ])(R.path(['search'])(state));

    const mergeText = () => {
        const mainQuery = isOdd(R.omit(['delete'], R.mergeAll([blocked, text]))) ? { q: searchText } : R.omit(['delete'], R.mergeAll([blocked, text]));

        if (isOdd(vehicleId)) return mainQuery;
        const queryReq = R.mergeAll([vehicleId, mainQuery]);

        return queryReq
    };

    return { where: { ...R.reject(isOdd, mergeText()) }, paginate: { limit: 20, page: R.defaultTo(1)(R.path(['page'])(state)) }, order: { id: 'desc' } }
};
