/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-before-return */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { FilterCar } from 'screens/Main/FilterCar';
import { Header } from 'screens/TaskEditor/components/header';
import { VisualTable } from 'screens/TaskEditor/components/table/component';
import { isOdd } from 'utils/helper';
import { Preview } from 'utils/routing/index.json';
import { useNavigation } from 'hooks/useNavigation'
import { useDispatch } from 'react-redux';

export const DashboardScreen = ({ navigation, route }) => {
    const [current, setMoved] = useNavigation({ navigation, move: 'toggle' });
    const [modal, setModal] = React.useState({ isVisible: false });
    const [id, setId] = React.useState(null);
    const CreateRouter = Preview.PreviewTaskGarage;
    const dispatchRedux = useDispatch()

    const success = ({ type, e }) => R.cond([
        [R.equals('Apply'), () => {
            setId(R.path(['payload'])(e));
            return setModal({ isVisible: false })
        }],
        [R.equals('CLOSE'), () => setModal({ isVisible: false })],
        [R.equals(current), () => setMoved(current)],
        [R.equals('Create'), () => {
            dispatchRedux({ type: 'TASK_UPDATE', payload: {} })
            navigation.navigate(CreateRouter, { id: null, modificationId: null })
        }],
        [R.equals('Filter'), () => setModal({ isVisible: true })],
        [R.equals('Edit'), () => navigation.navigate(
            CreateRouter,
            R.reject(isOdd, R.path(['payload'])(e))
        )],
        [R.equals('Reset'), () => {
            setId(R.path(['payload'])(e));
            return setModal({ isVisible: false })
        }],
        [R.T, () => null],
        [R.F, () => null]
    ])(type);

    return (
        <Header
            dispatch={setMoved}
            menu={{ title: "TaskWorks" }}
            style={{ flex: 1 }}
            svg={current}
        >
            <VisualTable
                button
                dispatch={(e) => success({
                    type: R.path(['type'])(e),
                    e
                })}
                id={id}
                mode="repairs"
                navigation={navigation}
                route={R.path(['params', 'list'])(route)}
            />
            <Modal isVisible={R.path(['isVisible'])(modal)}>
                <ModalDescription
                    style={{ flex: 1, margin: 10 }}
                >
                    <FilterCar
                        dispatch={(e) => success({ type: R.path(['type'])(e), e })}
                        storage={id}
                    />
                </ModalDescription>
            </Modal>
        </Header >
    )
};
