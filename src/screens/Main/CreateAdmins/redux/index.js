import * as R from 'ramda'
import createSensitiveStorage from 'redux-persist-sensitive-storage'

export const employeeStorage = createSensitiveStorage({
    keychainService: 'employeeChainKeychain',
    sharedPreferencesName: 'employeeChainSharedPrefs',
});

export const employeePersistConfig = {
    key: 'employee',
    storage: employeeStorage,
};

export const initialState = {};

export const employeeReducer = (state = initialState, action) => R.cond([
    [R.equals('EMPLOYEE_ADD'), R.always(action.payload)],
    [R.equals('EMPLOYEE_MERGE'), R.mergeRight(state, action.payload)],
    [R.equals('EMPLOYEE_REMOVE'), R.always(initialState)],
    [R.T, R.always(state)],
])(action.type);


export const depotsStorage = createSensitiveStorage({
    keychainService: 'depotsChainKeychain',
    sharedPreferencesName: 'depotsChainSharedPrefs',
});

export const depotsPersistConfig = {
    key: 'depots',
    storage: depotsStorage,
};

export const depotsReducer = (state = initialState, action) => {
    return R.cond([
        [R.equals('DEPOTS_ADD'), R.always(action.payload)],
        [R.equals('DEPOTS_MERGE'), R.always(R.mergeRight(state, action.payload))],
        [R.equals('DEPOTS_REMOVE'), R.always(initialState)],
        [R.T, R.always(state)],
    ])(action.type)
};


export const stockStorage = createSensitiveStorage({
    keychainService: 'stockChainKeychain',
    sharedPreferencesName: 'stockChainSharedPrefs',
});

export const stockPersistConfig = {
    key: 'stock',
    storage: stockStorage,
};

export const stockReducer = (state = initialState, action) => {
    return R.cond([
        [R.equals('DEPOTS_ADD'), R.always(action.payload)],
        [R.equals('STOCK_TREE_DEPOTS_MERGE'), R.always(R.mergeRight(state, action.payload))],
        [R.equals('DEPOTS_REMOVE'), R.always(initialState)],
        [R.T, R.always(state)],
    ])(action.type)
};
