export const initialStateAdmin = {
    lastInput: null,
    firstInput: null,
    middleInput: null,
    phoneInput: null
};

export const reducerAdmin = (state = initialStateAdmin, action) => {
    switch (action.type) {
        case 'lastInput':
            return { ...state, lastInput: action.payload };
        case 'firstInput':
            return { ...state, firstInput: action.payload };
        case 'middleInput':
            return { ...state, middleInput: action.payload };
        case 'phoneInput':
            return { ...state, phoneInput: action.payload };
        default:
            return { state }
    }
};

export const initialInputStateAdmin = [
    {
        key: 1,
        keyboardType: null,
        label: "Фамилия",
        maxLength: 45,
        path: 'lastInput',
    },
    {
        key: 2,
        keyboardType: null,
        label: "Имя",
        maxLength: 45,
        path: 'firstInput',
    },
    {
        key: 3,
        keyboardType: null,
        label: "Отчество",
        maxLength: 45,
        path: 'middleInput',
    },
    {
        key: 4,
        keyboardType: "num",
        label: "Номер телефона",
        maxLength: 11,
        path: 'phoneInput',
    },
];

export const initialStateWorker = {
    lastInput: null,
    firstInput: null,
    middleInput: null,
    phoneInput: null,
};

export const reducerWorker = (state = initialStateWorker, action) => {
    switch (action.type) {
        case 'lastInput':
            return { ...state, lastInput: action.payload };
        case 'firstInput':
            return { ...state, firstInput: action.payload };
        case 'middleInput':
            return { ...state, middleInput: action.payload };
        case 'phoneInput':
            return { ...state, phoneInput: action.payload };
        case 'blokedInput':
            return { ...state, blokedInput: action.payload };
        default:
            return { state }
    }
};
export const initialInputStateWorker = [
    {
        key: 1,
        keyboardType: null,
        label: "Фамилия",
        maxLength: 45,
        path: 'lastInput',
    },
    {
        key: 2,
        keyboardType: null,
        label: "Имя",
        maxLength: 45,
        path: 'firstInput',
    },
    {
        key: 3,
        keyboardType: null,
        label: "Отчество",
        maxLength: 45,
        path: 'middleInput',
    },
    {
        key: 4,
        keyboardType: "numeric",
        label: "Номер телефона",
        maxLength: 11,
        path: 'phoneInput',
    },
];

export const popupMenu = [{
    key: 1, Route: 'EDIT', params: { itemId: 59 }, text: 'EDIT',
},
{
    key: 2, Route: 'BLOCKED', params: { itemId: 59 }, text: 'BLOCKED',
},
{
    key: 3, Route: 'DELETE', params: { itemId: 59 }, text: 'DELETE',
}];
