/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { useLazyQuery } from '@apollo/react-hooks';
import { GARAGEPOST } from 'gql/calendar';
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { Agenda } from 'react-native-calendars';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/config';
import { renderEmptyDate } from 'screens/Main/Calendar/AgentaCalendar/renderEmptyDate';
import { renderItem } from 'screens/Main/Calendar/AgentaCalendar/renderItem';
import { calendar } from 'screens/Main/Calendar/calendar';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { ChangeData } from 'utils/helper';
import { Preview } from 'utils/routing/index.json';
import { WorkerScreen } from './WorkerScreen'

export const AgentaCalendar = ({ navigation, post }) => {
  const [state, setState] = React.useState({ isVisible: false });

  const ROUTER = Preview.PreviewTaskGarage;
  const [render, renderData] = React.useState({ items: [{}] });
  const [day, setDay] = React.useState(moment().format("YYYY-MM-DD"));
  const [loadgreeting] = useLazyQuery(GARAGEPOST, {
    fetchPolicy: "network-only",
    onCompleted: (data) => {
      renderData(R.assocPath([day], ChangeData({ data, day }), {}))
    }
  });

  React.useEffect(() => {
    loadgreeting(R.assocPath(['variables', 'where'], {
      garagePost: { id: { eq: R.path(['id'])(post) } },
      startAt: { date: day }
    })({}))
  }, [day, post]);

  return (
    <View style={{ flex: 1 }}>
      <Agenda
        futureScrollRange={1}
        items={render}
        onDayPress={(e) => {
          setDay(R.path(['dateString'])(e))
        }}
        pastScrollRange={1}
        renderEmptyDate={(e) => {
          renderEmptyDate(e)
        }}
        renderItem={(e) => renderItem({
          item: R.defaultTo(null)(R.head(R.path(['data'])(e))),
          time: R.path(['time'])(e),
          navigation: (e) => navigation.navigate(ROUTER, { id: R.path(['itemId'])(e) }),
          setState: () => setState({
            isVisible: true,
            time: R.path(['time'])(e),
          })
        })}
        selected={R.defaultTo('2020-05-30')(day)}
        theme={calendar}
      />
      <Modal isVisible={R.path(['isVisible'])(state)}>
        <ModalDescription
          style={{ flex: 1, margin: 10 }}
        >
          <HeaderApp
            button="CLOSED"
            dispatch={() => setState(R.assocPath(['isVisible'], false, {}))}
            // empty={isOdd(R.omit(['isVisible'])(state))}
            title={i18n.t("SELECTTASK")}
          />
          <WorkerScreen
            day={day}
            post={post}
            time={R.path(['time'])(state)}
          />
        </ModalDescription>
      </Modal>
    </View>
  )
};
