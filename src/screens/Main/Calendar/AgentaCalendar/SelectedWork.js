/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const SelectedWork = ({ state, setState }) => (
  <CardComponents style={[styles.shadow, { margin: 2 }]}>
    <RowComponents>
      {R.map(x => (
        <TouchableOpacity
          key={x}
          onPress={() => setState(x)}
          style={R.equals(state, x) ? { borderBottomWidth: 2, borderColor: 'red' } : {}}
        >
          <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items="SRDescription"
            style={{
              textAlign: 'center',
              margin: 5
            }}
          >
            {i18n.t(x)}
          </LabelText>
        </TouchableOpacity>
      ))(["preorder", "reconciliation"])}
    </RowComponents>
    <TouchableOpacity
      onPress={() => setState("inWork")}
      style={R.equals(state, "inWork") ? { borderBottomWidth: 2, borderColor: 'red' } : {}}
    >
      <LabelText
        color={ColorCard('classic').font}
        fonts="ShellMedium"
        items="SRDescription"
        style={{
          textAlign: 'center',
          margin: 10,
        }}
      >
        {i18n.t("inWork")}
      </LabelText>
    </TouchableOpacity >
  </CardComponents >
);