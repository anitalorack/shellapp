/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


export const renderEmptyDate = () => (
    <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
    </View>
);


const styles = StyleSheet.create({
    item: {
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        backgroundColor: 'red',
        height: 15,
        flex: 1,
        paddingTop: 30
    },
});