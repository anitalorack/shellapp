import { uiColor } from 'styled/colors/uiColor.json';

export const calendar = {
    backgroundColor: uiColor.Very_Pole_Grey,
    calendarBackground: uiColor.Very_Pole_Grey,
    textSectionTitleColor: uiColor.Black,
    selectedDayBackgroundColor: uiColor.Yellow,
    selectedDayTextColor: uiColor.Black,
    todayTextColor: uiColor.Black,
    dayTextColor: uiColor.Mid_Grey,
    textDisabledColor: uiColor.Red,
    dotColor: uiColor.Black,
    selectedDotColor: uiColor.Black,
    arrowColor: uiColor.Black,
    disabledArrowColor: uiColor.Black,
    monthTextColor: uiColor.Black,
    indicatorColor: uiColor.Black,
    textDayFontFamily: 'ShellLight',
    textMonthFontFamily: 'ShellMedium',
    textDayHeaderFontFamily: 'ShellLight',
    textDayFontWeight: 'bold',
    textDayFontSize: 14,
    textMonthFontSize: 14,
    textDayHeaderFontSize: 14,
    agendaDayTextColor: uiColor.Black,
    agendaDayNumColor: uiColor.Black,
    agendaTodayColor: uiColor.Red,
    agendaKnobColor: uiColor.Green
};
