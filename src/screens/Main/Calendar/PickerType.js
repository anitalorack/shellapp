/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import { useLazyQuery } from '@apollo/react-hooks';
import { QUERYPOST } from 'gql/postGarage';
import * as R from 'ramda';
import React from 'react';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper';

export const PickerType = ({ state, setState, horizont }) => {
    const [start] = React.useState();
    const [redux, setRedux] = React.useState(null);
    const [loadGreeting] = useLazyQuery(QUERYPOST, {
        onCompleted: (data) => setRedux(data),
        fetchPolicy: "network-only"
    });

    React.useEffect(() => {
        loadGreeting()
    }, [start]);

    const items = () => {
        return R.prepend({ label: 'выбрать', value: null })(
            R.map(x => {
                return {
                    label: R.path(['name'])(x),
                    value: R.path(['id'])(x)
                }
            })(R.path(['garagePosts', 'items'])(redux))
        )
    };

    const success = (e) => setState(R.head(R.innerJoin(
        (record, id) => R.equals(R.path(['id'])(record), id),
        R.path(['garagePosts', 'items'])(redux),
        [e]
    )));

    if (isOdd(redux)) return (
        <UiPicker
            disabled
            element={{ path: 'PostRemont' }}
            horizont={R.defaultTo(true)(horizont)}
            Items={[{ label: null, value: null }]}
            placeholder={{ id: null }}
            style={{ flex: 1 }}
        />
    );

    return (
        <UiPicker
            dispatch={(e) => success(e)}
            element={{ path: 'PostRemont' }}
            horizont={R.defaultTo(true)(horizont)}
            Items={items()}
            placeholder={{ id: R.defaultTo(null)(R.path(['id'])(state)) }}
            style={{ flex: 1 }}
        />
    )
};
