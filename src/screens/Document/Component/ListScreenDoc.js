/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable max-statements */
import { useMutation } from '@apollo/react-hooks';
import { DESTROY_FILE } from 'gql/depots';
import { useDocumentHook } from 'hooks/useDocumentHook';
import { useFileAttach } from 'hooks/useFileAttach';
import { usePdfManipulationFile } from 'hooks/usePdfManipulationFile';
import { useTaskHook } from 'hooks/useTaskHooks';
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    StyleSheet,
} from 'react-native';
import { isOdd } from 'utils/helper'
import { showMessage } from 'react-native-flash-message';
import { Colors, IconButton } from 'react-native-paper';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import i18n from 'localization'

export const ListScreenDoc = ({ hack }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const modificator = hack ? goods : task

    const [attacments] = useFileAttach({ hack });
    const [document, setDocument] = React.useState(null)
    const [currentFunction, actionFunction] = useDocumentHook({ hack })
    const [select, shareFile, alertPdfMessage] = usePdfManipulationFile({ hack })
    const [update] = useTaskHook({ hack });
    const dispatchRedux = useDispatch()

    const [destroyFile] = useMutation(DESTROY_FILE, {
        onCompleted: (data) => {
            if (data) {
                update('update', { id: R.path(['id'])(modificator) })
                showMessage({
                    message: "что то удален успешно",
                    description: "Error",
                    type: "success",
                })
            }
        }
    });

    const HeaderType = (x) => {

        return (
            <View style={{ justifyContent: 'flex-start' }}>
                {R.addIndex(R.map)((x, keys) => (
                    <Text
                        style={{
                            fontFamily: R.equals(1, keys) ? "ShellLight" : "ShellMedium",
                            fontSize: R.equals(1, keys) ? 11 : 13,
                            color: 'black'
                        }}
                    >
                        {x}
                    </Text>
                ))(R.paths([['filename'], ['mimetype']])(x))}
            </View>
        )
    }

    const Icon = (x) => {
        const title = 'destroy_docs';
        const alertReq = (e) => R.cond([
            [R.equals('Cancel'), () => null],
            [R.equals('Destroy'), () => destroyFile({ variables: { id: R.path(['params', 'id'])(e) } })],
        ])(R.path(['type'])(e))

        const success = R.cond([
            [R.equals('Share'), () => shareFile(R.path(['file', 'url'])(x))],
            [R.equals('Alert'), () => alertDestroyConfirm({ alertReq, title, params: x })],
            [R.T, () => null],
            [R.F, () => null]
        ])

        return (
            <RowComponents >
                <IconButton
                    color={Colors.red500}
                    icon="share"
                    onPress={() => success('Share')}
                    size={20}
                />
                <IconButton
                    color={Colors.black500}
                    icon="delete"
                    onPress={() => success('Alert')}
                    size={20}
                />
            </RowComponents>
        )
    }

    const setActionDocument = (x) => R.cond([
        [R.equals(document), () => setDocument(null)],
        [R.T, () => setDocument(x)],
        [R.F, () => setDocument(x)],
    ])(x)

    const Button = (x) => {
        // this function build action Button and navigation 
        const modificationDoc = actionFunction(document)
        const success = React.useCallback(() => {
            if (R.equals(x, document)) return R.cond([
                [R.isNil, () => alertPdfMessage({ params: { url: R.path(['file', 'url'])(x) } })],
                [R.T, () => dispatchRedux({ type: hack ? "ADD_GOODS" : 'TASK_UPDATE', payload: modificationDoc })],
            ])(modificationDoc)

            return null
        }, [document])

        return (
            <TouchableOpacity
                onPress={success}
                style={R.equals(x, document) ? {
                    flex: 1,
                    backgroundColor: R.isNil(modificationDoc) ? 'white' : 'black',
                    height: R.isNil(modificationDoc) ? 0 : 300
                } : {}}
            >
                {R.equals(x, document) ? currentFunction(document) : <View />}
            </TouchableOpacity>
        )
    }

    const ModificationComp = () => {
        if (isOdd(attacments(R.path(['screen'])(modificator)))) {
            return (
                <View
                    style={stylex.empty}
                >
                    <Text style={stylex.text}>{i18n.t('falsePay')}</Text>
                </View>
            )
        }

        return R.addIndex(R.map)((x, key) => (
            <View
                key={key}
                style={stylex.view}
            >
                <CardComponents
                    style={[stylex.card, styles.shadow]}
                >
                    <RowComponents
                        style={stylex.flex}
                    >
                        <Text style={stylex.num}>{key + 1}</Text>
                        <TouchableOpacity
                            onPress={() => setActionDocument(x)}
                            style={stylex.flex}
                        >
                            {HeaderType(x)}
                        </TouchableOpacity>
                        {Icon(x)}
                    </RowComponents>
                </CardComponents>
                {Button(x)}
            </View>
        ))(attacments(R.defaultTo('image')(R.path(['screen'])(modificator))))
    }

    return (
        <ScrollView>
            {ModificationComp()}
        </ScrollView>
    )
}

const stylex = StyleSheet.create({
    card: {
        flex: 1,
        margin: 1,
        backgroundColor: Colors.grey300,
        justifyContent: 'center'
    },
    view: {
        marginRight: 10,
        marginLeft: 10,
    },
    num: {
        height: 20,
        width: 20
    },
    flex: {
        flex: 1
    },
    text: {
        fontSize: 15,
        fontFamily: 'ShellLight'
    },
    empty: {
        flex: 1,
        height: 80,
        backgroundColor: Colors.grey300,
        marginRight: 10,
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center'
    }
})