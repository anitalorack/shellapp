/* eslint-disable max-len */
/* eslint-disable multiline-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable no-confusing-arrow */
/* eslint-disable id-length */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-after-var */
/* eslint-disable no-else-return */
/* eslint-disable no-console */
/* eslint-disable scanjs-rules/call_addEventListener */
/* eslint-disable max-statements */
import ADD from 'assets/svg/add';
import { usePermissions } from 'hooks/usePermissions';
import { useTaskHook } from 'hooks/useTaskHooks';
import { useUploadFile } from 'hooks/useUpload';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { shallowEqual, useSelector } from 'react-redux';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { ListScreenDoc } from './Component/ListScreenDoc';
import { TitleSelecter } from './Component/TitleSelecter';

export const App = ({ hack }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const modificator = hack ? goods : task

    const [permission] = usePermissions();
    const [refresh, setRefresh] = React.useState(false)
    const [state, photo, video, document] = useUploadFile();
    const [update] = useTaskHook({ hack });
    const [goog, setGoog] = React.useState(null)

    React.useEffect(() => {
        permission
    }, [task]);

    React.useEffect(() => {
        if (state) {
            update('update', { id: R.path(['id'])(modificator) })
        }
        if (refresh) {
            update('update', { id: R.path(['id'])(modificator) })
            setRefresh(false)
        }
    }, [state, refresh]);

    const data = {
        "upload": null,
        "objectId": R.path(['id'])(modificator),
        "objectType": hack ? "Document" : "Repair",
        "objectProperty": "attachments"
    }

    if (!hack) {
        if (R.pipe(R.path(['status']), R.not)(task)) return (<View />)
    }
    if (hack) {
        if (!R.path(['id'])(goods)) return (<View />)
    }

    return (
        <View style={{ flex: 1 }}>
            <View
                style={{ marginBottom: 10 }}
            >
                <HeaderApp
                    button="ADDED"
                    dispatch={(e) => setGoog(e)}
                    empty={false}
                    popUp
                    svg={ADD}
                    table={['image', 'video', 'document']}
                    title={i18n.t('document')}
                />
            </View>
            {R.cond([
                [R.equals('document'), () => document(data)],
                [R.equals('video'), () => video(data)],
                [R.equals('image'), () => photo(data)],
                [R.T, () => <View />]
            ])(goog)}
            <TitleSelecter hack={hack} />
            <ListScreenDoc hack={hack} />
        </View>
    );
};
