/* eslint-disable no-unused-vars */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable newline-after-var */
/* eslint-disable no-else-return */
/* eslint-disable no-console */
/* eslint-disable scanjs-rules/call_addEventListener */
/* eslint-disable max-statements */
import { useMutation } from '@apollo/react-hooks';
import { DESTROY_FILE } from 'gql/depots';
import { useDownload } from 'hooks/useDownload';
import { useTaskHook } from 'hooks/useTaskHooks';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { IconButton, Colors } from 'react-native-paper';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Styles, styles } from 'screens/Main/AddGoods/helper/style';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { alertDestroyConfirm } from 'utils/api/funcComponent/alertConfirm';
import { ColorCard } from 'utils/helper';
import { WebView } from 'react-native-webview';
import { OfficeViewer } from '@sishuguojixuefu/react-native-office-viewer'
import ImageViewer from 'react-native-image-zoom-viewer';
import VideoPlayer from './VideoPlayer';

export const InWebView = ({ mode, hack }) => {
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const moderator = hack ? goods : task

    const dispatchRedux = useDispatch();
    
    const QWeqweqw = R.cond([
        [R.equals('inWebView'), () => (
            <WebView
                source={{ uri: R.path([mode, 'file', 'url'])(moderator) }}
                style={{ flex: 1 }}
            />
        )],
        [R.equals('inOfficeView'), () => (
            <OfficeViewer
                containerStyle={{ flexGrow: 1, marginTop: 10 }}
                source={{ uri: R.path([mode, 'file', 'url'])(moderator) }}
            />
        )],
        [R.equals('videoView'), () => (
            <VideoPlayer
                video={R.path([mode, 'file', 'url'])(moderator)}
            />
        )],
        [R.equals('imageView'), () => (
            <ImageViewer
                enableImageZoom
                getMenu={null}
                imageUrls={[{ url: R.path([mode, 'file', 'url'])(moderator) }]}
                onSwipeDown={() => dispatchRedux({
                    type: hack ? "ADD_GOODS" : "TASK_UPDATE", payload: R.omit([mode])(moderator)
                })}
            />
        )],
    ])

    if (R.pipe(R.path([mode]), R.isNil, R.not)(task)) {
        return (
            <View style={{ flex: 1 }}>
                {QWeqweqw(mode)}
                <FooterPage hack={hack} mode={mode} />
            </View>
        )
    }

    return (
        <View />
    );
}

export const FooterPage = (props) => {
    const { mode, hack } = props
    const { task } = useSelector(state => state.task, shallowEqual);
    const { goods } = useSelector(state => state.goods, shallowEqual);

    const moderator = hack ? goods : task

    const dispatchRedux = useDispatch();
    const [update] = useTaskHook()
    const [download] = useDownload({ hack })
    const file = R.path([mode])(moderator)

    const [destroyFile] = useMutation(DESTROY_FILE, {
        onCompleted: () => {
            update('update', { id: R.path(['id'])(task) })
            // TODO UPDATE STORAGE ADD_GOODS
            // update('update', { id: R.path(['id'])(goods) })
            showMessage({
                message: "что то удален успешно",
                description: "Success",
                type: "success",
            })
        },
        onError: (data) => showMessage({
            message: R.toString(data),
            description: "Error",
            type: "danger",
        })
    });

    const title = 'destroy_post';
    const alertReq = (e) => R.cond([
        [R.equals('Cancel'), () => null],
        [R.equals('Destroy'), () => {
            destroyFile({ variables: { id: R.path(['id'])(file) } })
        }],
    ])(R.path(['type'])(e));

    return (
        <RowComponents>
            <TouchableOpacity
                onPress={() => {
                    alertDestroyConfirm({
                        alertReq,
                        title,
                    })
                }}
                style={[Styles.snap, { left: 0, backgroundColor: 'red' }]}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={styles.container}
                >
                    {i18n.t("DESTROY")}
                </LabelText>
            </TouchableOpacity>
            <IconButton
                color={Colors.black500}
                icon="download"
                onPress={() => download(file)}
                size={20}
            />
            <TouchableOpacity
                onPress={() => dispatchRedux({
                    type:
                        hack ? "ADD_GOODS" : 'TASK_UPDATE',
                    payload: R.omit([mode])(moderator)
                })}
                style={[Styles.snap, { left: 0, backgroundColor: 'grey' }]}
            >
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    style={styles.container}
                >
                    {i18n.t("CLOSED")}
                </LabelText>
            </TouchableOpacity>
        </RowComponents>
    )
}
