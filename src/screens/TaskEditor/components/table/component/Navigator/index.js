/* eslint-disable no-plusplus */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { isOdd } from 'utils/helper';

export const Navigator = ({ storage, setDispatch }) => {
    const indexButtom = () => {
        let data = [];

        for (let i = 1; i < R.add(1, R.path(['query', 'info', 'totalCount'])(storage) / R.path(['query', 'info', 'limit'])(storage)); i++) {
            data = [i, ...data]
        }

        return R.reverse(data)
    };

    if (R.equals([1])(indexButtom())) return (<View />);

    return (
        <ScrollView
            contentContainerStyle={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center', }}
            horizontal
        >
            {R.addIndex(R.map)((x, key) => {
                if (isOdd(x)) return (<View />);

                return (
                    <CardComponents
                        key={key}
                        style={[
                            styleNav.card, {
                                backgroundColor: R.pipe(
                                    R.path(['query', 'info', 'page']),
                                    R.equals(x)
                                )(storage) ? uiColor.Yellow : 'transparent'
                            }
                        ]}
                    >
                        <TouchableOpacity
                            onPress={() => setDispatch({ type: "Page", payload: x })}
                        >
                            <Text
                                style={styleNav.text}
                            >
                                {x}
                            </Text>
                        </TouchableOpacity>
                    </CardComponents>
                )
            })(indexButtom())}
        </ScrollView>
    )
};

const styleNav = StyleSheet.create({
    card: {
        margin: 1,
        justifyContent: 'center',
        width: 80,
        height: 30,
        marginBottom: 30
    },
    text: { fontFamily: 'ShellBold', textAlign: 'center' }
});