/* eslint-disable react/no-multi-comp */
/* eslint-disable multiline-ternary */
/* eslint-disable no-confusing-arrow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View, Text } from 'react-native'
import { styles } from 'screens/Main/AddGoods/helper/style';
import { RowComponents } from 'screens/TaskEditor/components/table/style';

export const Header = ({ state }) => (
    <View style={{ marginRight: 10, marginLeft: 10 }}>
        <RowComponents>
            <Text
                style={[styles.container, {
                    textAlign: 'left',
                    textTransform: 'none',
                    fontSize: 12,
                    color: 'green',
                    fontFamily: 'ShellBold'
                }]}
            >
                [ {R.path(['brand'])(state)} ]
            </Text>
            <Text
                style={[styles.container, {
                    textAlign: 'right',
                    textTransform: 'none',
                    fontSize: 12,
                    fontFamily: 'ShellBold'
                }]}
            >
                {R.path(['name'])(state)}
            </Text>
        </RowComponents>
        <RowComponents style={{ justifyContent: 'flex-start' }}>
            <Text
                style={[styles.container, {
                    textAlign: 'right',
                    textTransform: 'none',
                    fontSize: 12,
                    fontFamily: 'ShellMedium'
                }]}
            >
                {(R.pipe(R.paths([['article'], ['description']]), R.join(', '))(state))}
            </Text>
        </RowComponents>
    </View>
);