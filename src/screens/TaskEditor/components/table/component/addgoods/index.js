/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { Describe, stylesStock } from 'screens/TaskEditor/components/table/component/addgoods/Describe';
import { Header } from 'screens/TaskEditor/components/table/component/addgoods/Header';
import { InfoScreen } from 'screens/TaskEditor/components/table/component/addgoods/InfoScreen';

export const CardScrollComponents = ({
    data, updata
}) => {
    const [upload] = React.useState(data);
    const [magic, setMagic] = React.useState(false);
    const color = R.equals(R.path(['stock'])(data), upload) ? uiColor.Yellow : uiColor.Very_Pole_Grey;

    return (
        <CardComponents
            style={[styles.shadow, stylesStock.afafa, { backgroundColor: color }]}
        >
            <TouchableOpacity
                onPress={() => setMagic(!magic)}
                style={{ backgroundColor: magic ? 'gold' : 'transparent' }}
            >
                <Header
                    state={upload}
                />
                <InfoScreen
                    state={upload}
                    updata={updata}
                />
            </TouchableOpacity>
            <Describe
                data={upload}
                magic={magic}
                setMagic={setMagic}
                style={stylesStock.margin}
            />
        </CardComponents>
    )
};
