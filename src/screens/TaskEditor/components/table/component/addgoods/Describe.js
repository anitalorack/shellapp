/* eslint-disable max-statements */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import {
    View, StyleSheet, TouchableOpacity, Text
} from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { isOdd, priceModed } from 'utils/helper';
import Rub from 'assets/svg/rub';
import { SvgXml } from 'react-native-svg';

export const Describe = ({ data, magic, setMagic }) => {
    const upperCase = React.useCallback(() => R.addIndex(R.map)((item, key) => {
        const listConsumer = {
            inNal: R.pipe(R.paths([['amount', 'available']]), R.append(R.pipe(R.paths([['measure', 'code']]))(data)), R.join(' '))(item),
            Reserved: R.pipe(R.paths([['amount', 'reserved']]), R.append(R.pipe(R.paths([['measure', 'code']]))(data)), R.join(' '))(item),
            Sales: R.pipe(R.paths([['amount', 'released']]), R.append(R.pipe(R.paths([['measure', 'code']]))(data)), R.join(' '))(item),
            Tax: R.pipe(R.paths([['vat']]), R.append('%'), R.join(' '))(item),
            CoastIn: R.pipe(R.path(['priceIn', 'amount']), R.defaultTo(''), priceModed)(item),
            CoastOut: R.pipe(R.path(['priceOut', 'amount']), R.defaultTo(''), priceModed)(item),
        }

        const informer = (x) => R.includes(x, ['CoastIn', 'CoastOut']) ? <SvgXml height="8" weight="5" xml={Rub} /> : <View />

        return (
            <TouchableOpacity
                key={R.join('_', [key, data, item])}
            //  TODO disabled components
            // disabled
            >
                <CardComponents
                    style={[styles.shadow, stylesStock.column]}
                >
                    {R.addIndex(R.map)((x, key) => (
                        <RowComponents
                            key={R.join('_', [key, data, item])}
                        >
                            <Text
                                style={[
                                    styles.container,
                                    stylesStock.flat, {
                                        fontFamily: 'ShellMedium',
                                        fontSize: 10
                                    }
                                ]}
                            >
                                {i18n.t(x)}
                            </Text>
                            <View
                                style={[styles.rowCenter, stylesStock.flat]}
                            >
                                <Text
                                    style={[styles.container, stylesStock.item, {
                                        fontFamily: 'ShellLight',
                                        fontSize: 10
                                    }]}
                                >
                                    {R.path([x])(listConsumer)}
                                </Text>
                                {informer(x)}
                            </View>
                        </RowComponents>
                    ))(['inNal', 'Reserved', 'Sales', 'Tax', 'CoastIn', 'CoastOut'])}
                </CardComponents>
            </TouchableOpacity>
        )
    })(R.path(['depotItems'])(data)), [magic])

    if (isOdd(data)) return (<View />);
    const value = {
        createdAt: R.join(' ', [moment(R.path(['updatedAt'])(data)).format("DD.MM.YYYY - HH:mm")]),
        inNal: R.pipe(R.paths([['amount', 'available'], ['measure', 'code']]), R.join(' '))(data),
        Reserved: R.pipe(R.paths([['amount', 'reserved'], ['measure', 'code']]), R.join(' '))(data),
        Sales: R.pipe(R.paths([['amount', 'released'], ['measure', 'code']]), R.join(' '))(data),
    };

    if (magic) {
        return (
            <View>
                <TouchableOpacity onPress={() => setMagic(false)}>
                    {R.addIndex(R.map)((x, kw) => (
                        <RowComponents
                            key={R.join('_', [kw, data])}
                        >
                            <Text
                                style={[
                                    styles.container,
                                    stylesStock.flat,
                                    stylesStock.text
                                ]}
                            >
                                {i18n.t(x)}
                            </Text>
                            <View
                                style={[
                                    styles.rowCenter,
                                    stylesStock.flat
                                ]}
                            >
                                <Text
                                    style={[
                                        styles.container,
                                        stylesStock.flat,
                                        stylesStock.text, {
                                            fontFamily: 'ShellLight',
                                            textTransform: 'lowercase'
                                        }
                                    ]}
                                >
                                    {R.path([x])(value)}
                                </Text>
                            </View>
                        </RowComponents>
                    ))(['createdAt', 'inNal', 'Reserved', 'Sales'])}
                </TouchableOpacity>
                {upperCase()}
            </View>
        )
    }

    return (<View />)
};

export const stylesStock = StyleSheet.create({
    item: {
        padding: 0,
        justifyContent: 'space-between',
        textTransform: 'lowercase',
        textAlign: 'right'
    },
    flat: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    column: {
        flexDirection: 'column',
        margin: 2
    },
    afafa: {
        flexDirection: 'column',
        margin: 2,
        flex: 1
    },
    margin: {
        marginTop: 5,
        marginBottom: 5
    },
    text: {
        marginRight: 10,
        marginLeft: 10,
        fontFamily: 'ShellMedium',
        fontSize: 10
    }
});