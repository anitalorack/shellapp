/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { uiColor } from 'styled/colors/uiColor.json';
import { isOdd } from 'utils/helper';
import { useDispatch } from 'react-redux';
import { CurrentItem } from './CurrentItem';

export const Item = ({
    data, dispatch, list, current
}) => {
    const dispatchRedux = useDispatch()
    const memorizedData = R.cond([
        [R.equals('taskEmployee'), R.always(R.assocPath(['__typename'], 'TaskItems')(data))],
        [isOdd, R.always(data)],
    ])(list);

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity
                onPress={() => {
                    dispatch({
                        type: "Edit",
                        payload: R.cond([
                            [R.equals('Vehicle'), R.always({
                                owner: R.path(['id'])(data),
                                vin: R.path(['vin'])(data),
                                plate: R.path(['plate'])(data),
                                modificationId: R.path(['modificationId'])(data)
                            })],
                            [R.equals('Repair'), () => {
                                dispatchRedux({ type: 'TASK_UPDATE', payload: data })

                                return {
                                    id: R.path(['id'])(data),
                                }
                            }],
                            [R.equals('Counterparty'), () => {
                                dispatchRedux({ type: 'AGENT_UPDATE', payload: data })

                                return {
                                    id: R.path(['id'])(data),
                                }
                            }],
                            [R.equals('LegalEntity'), () => {
                                dispatchRedux({ type: 'AGENT_UPDATE', payload: data })

                                return {
                                    id: R.path(['id'])(data),
                                }
                            }],
                            [R.equals('SoleTrader'), () => {
                                dispatchRedux({ type: 'AGENT_UPDATE', payload: data })

                                return {
                                    id: R.path(['id'])(data),
                                }
                            }],
                            [R.equals('Individual'), () => {
                                dispatchRedux({ type: 'AGENT_UPDATE', payload: data })

                                return {
                                    id: R.path(['id'])(data),
                                }
                            }],
                            [R.equals("Employee"), R.always({ ...data })],
                            [R.equals("Post"), R.always({ itemId: R.path(['garagePostTypeId'])(data) })],
                            [R.T, R.always(R.path(['id'])(data))]
                        ])(R.path(['__typename'])(data))
                    })
                }}
                style={{
                    margin: R.includes(R.path(['__typename'])(data), ['Repair']) ? 5 : 0.3,
                    flexDirection: 'row',
                    borderColor: uiColor.Polo_Grey,
                    flex: 1
                }}
            >
                <CurrentItem
                    current={current}
                    data={memorizedData}
                    dispatch={dispatch}
                    style={{ flex: 1 }}
                    type={R.path(['__typename'])(data)}
                />
            </TouchableOpacity >
        </View>
    )
};