/* eslint-disable complexity */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/jsx-indent-props */
import { BILL_INVOICE } from 'gql/billAccount';
import { ALL_CLIENT } from 'gql/clients';
import {
    QUERY_COUNTERPARTIES,
    QUERY_INDIVIDUAL,
    QUERY_LEGAL,
    QUERY_SOLO
} from 'gql/depots';
import { ALL_EMPLOYERS } from 'gql/employee';
import { RESERVED_DEPOTS } from 'gql/garage';
import { QUERYPOST } from 'gql/postGarage';
import { VEHICLE_QUERY } from 'screens/Main/CarsScreen/GQL';
import { ALL_REPAIRS } from 'screens/Main/MainScreen/GQL';
import * as R from 'ramda';
import * as ALLCONTRAGENT from 'screens/Contragent/Query/config';
import * as ALLVEHICLE from 'screens/Main/CarsScreen/Query/config';
import * as ALLCLIENT from 'screens/Main/ClientScreen/Query/config';
import * as ALLREPAIRSCONFIG from 'screens/Main/MainScreen/Query/config';
import * as ALLEMPLOYEE from 'screens/Master/Query/config';
import { isOdd } from 'utils/helper';

export const variavblesContragent = R.cond([
    [R.equals("INDIVIDUAL"), () => {
        return QUERY_INDIVIDUAL
    }],
    [R.equals("SOLE_TRADER"), () => {
        return QUERY_SOLO
    }],
    [R.equals("LEGAL_ENTITY"), () => {
        return QUERY_LEGAL
    }],
    [R.equals("COUNTREPARITY"), () => {
        return QUERY_COUNTERPARTIES
    }],
    [isOdd, () => {
        return QUERY_COUNTERPARTIES
    }],
]);

export const InitialState = R.cond([
    [R.equals('repairs'), R.always(ALLREPAIRSCONFIG.initialState)],
    [R.equals('contragent'), R.always(ALLCONTRAGENT.initialState)],
    [R.equals('vehicle'), R.always(ALLVEHICLE.initialState)],
    [R.equals('client'), R.always(ALLCLIENT.initialState)],
    [R.equals('employee'), R.always(ALLEMPLOYEE.initialState)],
    [R.equals('stock'), R.always(ALLEMPLOYEE.initialState)],
    [R.equals('pay'), R.always({ page: 1 })],
]);

export const query = (storage, mode, route) => {
    return R.cond([
        [R.equals('post'), R.always({
            query: QUERYPOST,
            variables: {
                where: R.path(['id'])(storage),
                order: { id: 'desc' },
                paginate: { page: R.path(['page'])(storage), limit: 20 }
            }
        })],
        [R.equals('stock'), R.always({
            query: RESERVED_DEPOTS,
            variables: {
                where: R.path(['id'])(storage),
                order: { id: "desc" },
                paginate: { page: R.path(['page'])(storage), limit: 20 }
            }
        })],
        [R.equals('searchStock'), R.always({
            query: RESERVED_DEPOTS,
            variables: {
                where: { q: R.path(['text'])(storage) },
                order: { id: "desc" },
                paginate: { page: R.path(['page'])(storage), limit: 20 }
            }
        })],
        [R.equals('pay'), R.always({ query: BILL_INVOICE, variables: { where: {}, order: { id: "desc" }, paginate: { page: R.path(['page'])(storage), limit: 20 } } })],
        [R.equals('repairs'), R.always({ query: ALL_REPAIRS, variables: ALLREPAIRSCONFIG.setRequestDataLoader(storage) })],
        [R.equals('contragent'), R.always({ query: variavblesContragent(route), variables: ALLCONTRAGENT.setRequestDataLoader(storage) })],
        [R.equals('vehicle'), R.always({ query: VEHICLE_QUERY, variables: ALLVEHICLE.setRequestDataLoader(storage) })],
        [R.equals('client'), R.always({ query: ALL_CLIENT, variables: ALLCLIENT.setRequestDataLoader(storage) })],
        [R.equals('employee'), R.always({ query: ALL_EMPLOYERS, variables: ALLEMPLOYEE.setRequestDataLoader(storage) })],
    ])(mode)
};

export const reducer = (state = {}, action) => {
    switch (action.type) {
        case "Query":
            return R.assocPath(['query'], action.payload, state);
        case "Update":
            return R.assocPath(['query'], { info: R.path(['payload', 'info'])(action), items: R.uniq(R.path(['query', 'items'])(state), action.payload.items) }, state);
        case "Right":
            return R.mergeAll([state, { right: !R.path(['right'])(state) }]);
        case "Left":
            return R.mergeAll([state, R.assocPath(['search'], null, { left: !R.path(['left'])(state) })]);
        case "Text":
            return R.mergeAll([state, { text: R.path(['payload'])(action) }]);
        case "Swipe":
            return R.mergeAll([state, { swipe: R.path(['payload'])(action) }]);
        case "Checker":
            if (R.equals(R.path(['payload'])(action))(R.path(['checker'])(state))) return R.mergeAll([state, { checker: "AllWork" }]);

            return R.mergeAll([state, { checker: R.path(['payload'])(action) }]);
        case "Search":
            return R.mergeAll([state, { search: R.path(['payload'])(action) }]);
        case "Updata":
            return R.assocPath(['data'], R.path(['payload'])(action), state);
        case "Variables":
            return R.assocPath(['variables'], R.path(['payload'])(action), state);
        case "Page":
            return R.assocPath(['page'], R.path(['payload'])(action))(state);
        case 'Initial':
            return action.payload;
        default:
            return state
    }
};
