/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-indent-props */
import print from 'assets/svg/print-button-svgrepo-com';
import i18n from 'localization';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { styles } from 'screens/Main/Pay/helper/index';
import Modal from 'react-native-modal';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import * as R from 'ramda'
import { usePdfManipulationFile } from 'hooks/usePdfManipulationFile'

export const HeaderInfo = ({ state }) => {
    const [document, shareFile, alertPdfMessage] = usePdfManipulationFile()
    const [color] = React.useState(false);

    return (
        <RowComponents>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="CLabel"
                style={[styles.container, { textTransform: 'none' }]}
                testID="HEADER_STATUS"
            >
                {i18n.t('Payment_Bill')}
            </LabelText>
            <TouchableOpacity
                onPress={() => alertPdfMessage({ title: "invoice", params: { id: R.path(['id'])(state), url: 'invoice' } })}
                style={[styles.pdfButton, {
                    backgroundColor: color ? uiColor.Yello : uiColor.Very_Pole_Grey
                }]}
            >
                <SvgXml height="30" width="30" xml={print} />
            </TouchableOpacity>
        </RowComponents>
    )
};