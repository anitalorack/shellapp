/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { styles } from 'screens/Main/Pay/helper/index';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const NumberInfo = ({ state }) => {
    const line = R.join(" ", [
        i18n.t('from'),
        moment(R.path(['createdAt'])(state)).format("DD.MM.YYYY HH:mm:ss")
    ]);

    return (
        <View
            style={[styles.block, {
                marginTop: 5,
            }]}
        >
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={styles.items}
                testID="BlockLaker"
            >
                № {R.path(['name'])(state)}
            </LabelText>
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellMedium"
                items="SDescription"
                style={styles.items}
                testID="BlockLaker"
            >
                {line}
            </LabelText>
        </View>
    )
};