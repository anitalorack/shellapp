/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-wrap-multilines */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { styles } from 'screens/Main/Pay/helper/index';
import { CardPay } from 'screens/Main/Pay/Query/component/CardPay';
import { CardInfo } from 'screens/TaskEditor/components/table/component/payHistory/CardInfo';
import { HeaderInfo } from 'screens/TaskEditor/components/table/component/payHistory/HeaderInfo';
import { NumberInfo } from 'screens/TaskEditor/components/table/component/payHistory/NumberInfo';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import FlipCard from 'react-native-flip-card'
import { BackFlip, checkStatus } from 'screens/TaskEditor/components/table/component/payHistory/BackFlip'

export const PayHistoryItem = ({ data }) => {
    const [state] = React.useState({ flip: false });
    const payElement = R.equals(checkStatus(data), 'succeeded') ? i18n.t('SuccessPay') : i18n.t('UnSuccessPay');

    return (
        <FlipCard
            key={R.path(['id'])(data)}
            alignHeight
            clickable
            flip={state.flip}
            flipHorizontal
            flipVertical={false}
            friction={2}
            perspective={2000}
            style={[stylesAnim.card]}
        >
            <RowComponents
                style={[{ flex: 1 }, stylesAnim.face]}
            >
                <CardComponents
                    key={R.join('_', [R.path(['id'])(data), '_CARD'])}
                    style={[
                        styles.shadow,
                        styleCard.text,
                        { borderLeftColor: R.equals(checkStatus(data), 'succeeded') ? 'green' : 'grey' }]}
                >
                    <HeaderInfo
                        state={data}
                    />
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellBold"
                        items="SDescription"
                        style={[styles.items, styleCard.item, {
                            color: R.equals(checkStatus(data), 'succeeded') ? 'green' : 'grey',
                        }]}
                    >
                        {payElement}
                    </LabelText>
                    <NumberInfo state={data} />
                    <View style={styles.line} />
                    <View style={[styles.block, { flex: 1 }]}>
                        <CardInfo
                            data={R.path(['billServices'])(data)}
                        />
                    </View>
                    <View style={styles.line} />
                    <View style={[styles.block]}>
                        {R.addIndex(R.map)((mode, key) => {
                            const uniqKey = R.join('_', [R.path(['id'])(data), key, mode, 'CardPay']);

                            return (
                                <CardPay
                                    key={uniqKey}
                                    elem={data}
                                    keys={uniqKey}
                                    mode={mode}
                                />
                            )
                        })(['amountPayable', 'paymentAmount'])}
                    </View>
                </CardComponents>
            </RowComponents>
            <CardComponents
                key={R.join('_', [R.path(['id'])(data), '_CARDBACK'])}
                style={[styles.shadow, styleCard.text, {
                    borderLeftColor: R.equals(checkStatus(data), '') ? 'green' : 'grey', flex: 1
                }, stylesAnim.back]}
            >
                <View style={{ alignSelf: 'center' }}>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="CLabel"
                        style={[styles.items, styleCard.item, { textTransform: 'uppercase' }]}
                    >
                        {i18n.t("paymentPay")}
                    </LabelText>
                </View>
                <BackFlip
                    data={data}
                    status={checkStatus(data)}
                />
            </CardComponents>
        </FlipCard>
    )
};

const styleCard = StyleSheet.create({
    item: {
        padding: 10,
        paddingBottom: 0,
        textTransform: 'uppercase',
        paddingTop: 0,
    },
    text: {
        borderLeftWidth: 4,
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
    }
});

const stylesAnim = StyleSheet.create({
    face: {
        flex: 1,
    },
    back: {
        flex: 1,
    },
});