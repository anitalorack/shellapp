/* eslint-disable no-unsanitized/method */
/* eslint-disable react/no-multi-comp */
/* eslint-disable no-negated-condition */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import close from 'assets/svg/close';
import Rub from 'assets/svg/rub';
import i18n from 'localization';
import React from 'react';
import { View, Text } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, priceModed } from 'utils/helper';
import * as R from 'ramda'

export const CardInfo = ({ data }) => R.addIndex(R.map)((x, key) => (
    <RowComponents
        key={key}
    >
        <LabelText
            color={ColorCard('classic').font}
            fonts="ShellMedium"
            items="SRDescription"
            style={{ flex: 1 }}
        >
            {i18n.t(x.name)}
        </LabelText>
        <RowComponents >
            <RowComponents style={{ alignItems: 'center' }}>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SRDescription"
                >
                    1
                </LabelText>
                <View style={{ marginLeft: 15, marginTop: 2 }} >
                    <SvgXml height="20" width="20" xml={close} />
                </View>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SRDescription"
                    style={{ textAlign: 'right' }}
                >
                    {R.pipe(R.paths([['priceCount'], ['price', 'amount']]), R.product)(x)}
                </LabelText>
            </RowComponents>
            <SvgXml height="10" weight="10" xml={Rub} />
        </RowComponents>
    </RowComponents>
))(data)