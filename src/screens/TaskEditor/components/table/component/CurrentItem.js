/* eslint-disable react/jsx-indent-props */
/* eslint-disable import/extensions */
import * as R from 'ramda';
import React from 'react';
import { isOdd } from 'utils/helper'
import { View } from 'react-native'
import { ClientItem } from 'screens/TaskEditor/components/table/component/client';
import { ConterparityItem } from 'screens/TaskEditor/components/table/component/conterparity';
import { EmplloyeeTaskItem } from 'screens/TaskEditor/components/table/component/employeeTask';
import { RepairsItem } from 'screens/TaskEditor/components/table/component/repairs';
import { RepairWorkItem } from 'screens/TaskEditor/components/table/component/repairWork';
import { VehicleItem } from 'screens/TaskEditor/components/table/component/vehicle';
import { CardScrollComponents } from 'screens/TaskEditor/components/table/component/addgoods';
import { PostWorkItem } from 'screens/TaskEditor/components/table/component/post'
import { PayHistoryItem } from 'screens/TaskEditor/components/table/component/payHistory'
import { EmplloyeeItem } from 'screens/TaskEditor/components/table/component/employee';

export const CurrentItem = ({ data, dispatch, current }) => {
    return R.cond([
        [R.equals('GaragePost'), () => (
            <PostWorkItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('RepairWork'), () => (
            <RepairWorkItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('LegalEntity'), () => (
            <ConterparityItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('Counterparty'), () => (
            <ConterparityItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('SoleTrader'), () => (
            <ConterparityItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('Individual'), () => (
            <ConterparityItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('Employee'), () => (
            <EmplloyeeItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('TaskItems'), () => (
            <EmplloyeeTaskItem
                data={data}
                dispatch={dispatch}
                setState={dispatch}
                state={current}
            />
        )],
        [R.equals('Client'), () => (
            <ClientItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('Repair'), () => (
            <RepairsItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('Vehicle'), () => (
            <VehicleItem
                data={data}
                dispatch={dispatch}
            />
        )],
        [R.equals('Stock'), () => (
            <CardScrollComponents
                data={data}
                dispatch={dispatch}
                info
                state={data}
                updata
            />
        )],
        [R.equals('BillInvoice'), () => (
            <PayHistoryItem
                data={data}
            />
        )],
        [isOdd, () => (
            <View />
        )]
    ])(R.path(['__typename'])(data))
}