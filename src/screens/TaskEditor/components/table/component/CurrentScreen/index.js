// /* eslint-disable react/jsx-key */
// /* eslint-disable react/no-multi-comp */
// /* eslint-disable newline-after-var */
// /* eslint-disable react/jsx-indent-props */
// import * as R from 'ramda';
// import React from 'react';
// import { View, FlatList } from 'react-native';
// import { SplashScreen } from 'screens/Splash';
// import { SearchInputLine } from 'screens/TaskEditor/components/header/component/SearchInputLine';
// import { Item } from 'screens/TaskEditor/components/table/component/Item';
// import { isOdd } from 'utils/helper';
// import { Tag } from 'screens/TaskEditor/components/table/component/CurrentScreen/Tag'

// export const CurrentScreen = ({
//     dispatch, button, unsearch, unfilter, list, storage, setDispatch, loading, mode, current
// }) => {
//     const [currentItems, setCurrent] = React.useState(current);
//     const SearchScreen = () => {
//         if (R.includes(mode)(['pay'])) return (<View />);
//         if (R.equals('taskEmployee', list)) return (<View />)
//         if (R.all(R.equals(true))([R.equals(true, unsearch), isOdd(button), R.equals('stock')(mode)])) return (<View />);

//         return (
//             <View style={{ margin: 5 }}>
//                 <SearchInputLine
//                     dispatch={(e) => {
//                         if (R.equals(R.path(['type'])(e), 'Create')) return dispatch(e);
//                         if (R.equals(R.path(['type'])(e), 'Filter')) dispatch(e);
//                         setDispatch(e)
//                     }}
//                     mode={button}
//                     state={{ text: R.path(['text'])(storage) }}
//                     unfilter={unfilter}
//                     unsearch={unsearch}
//                 />
//                 <Tag />
//             </View>
//         )
//     };

//     if (isOdd(R.path(['query', 'items'])(storage))) {
//         return (
//             <View style={{ flex: 1 }}>
//                 {SearchScreen()}
//                 <View style={{ flex: 1 }}>
//                     <SplashScreen
//                         msg={loading ? "Загрузка" : "Ничего не найдено"}
//                         state="Error"
//                         style={{ margin: 4, flex: 1 }}
//                         testID="Error"
//                     />
//                 </View>
//             </View>
//         )
//     }

//     const renderItem = ({ item }) => (
//         <Item
//             current={currentItems}
//             data={R.assocPath(['__typename'], 'Repair')(item)}
//             dispatch={(e) => {
//                 setCurrent(R.path(['id'])(e));
//                 dispatch(e)
//             }}
//             list={list}
//         />
//     );


//     return (
//         <View style={{ flex: 1 }}>
//             {SearchScreen()}
//             <FlatList
//                 data={R.path(['query', 'items'])(storage)}
//                 initialNumToRender={3}
//                 renderItem={renderItem}
//                 keyExtractor={item => item.id}
//             />
//         </View>
//     )
// };
