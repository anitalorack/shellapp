/* eslint-disable max-len */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
import { StyleSheet } from 'react-native';
import { Colors } from 'react-native-paper';

export const style = StyleSheet.create({
    item: {
        width: 40,
        flex: 1,
        backgroundColor: Colors.grey300
    },
    text: {
        color: 'red',
        fontSize: 12,
        flex: 1
    },
    title: {
        fontSize: 10,
        fontFamily: 'ShellLight',
        flex: 1
    },
    view: {
        alignItems: 'baseline',
        padding: 5,
        flex: 1
    },
    container: {
        borderWidth: 1,
        margin: 2,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.grey300
    }
})