/* eslint-disable max-len */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';

export const iconCancelData = (filter) => ({
    vehicleModels: R.trim(R.join(' ',
        R.paths([
            ['modification', 'model', 'name'],
            ['modification', 'model', 'subbody']
        ])(filter)
    )),
    vehicleManufacturers: R.path(['modification', 'model', 'manufacturer', 'name'])(filter),
    author: R.path(['author', 'name'])(filter),
    client: R.path(['client', 'name'])(filter),
    owner: R.path(['owner', 'name'])(filter),
    performer: R.path(['performer', 'name'])(filter)
});
