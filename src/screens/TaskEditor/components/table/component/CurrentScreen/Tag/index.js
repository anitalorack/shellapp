/* eslint-disable max-len */
/* eslint-disable react/jsx-key */
/* eslint-disable react/no-multi-comp */
/* eslint-disable newline-after-var */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { isOdd } from 'utils/helper';
import { iconCancelData } from './iconCancelData';
import { IconSearch } from './IconSearch';

export const Tag = () => {
    const { filter } = useSelector(state => state.filter, shallowEqual);
    const dispatchStorage = useDispatch();

    const destroy = React.useCallback((element) => R.cond([
        [R.equals('vehicleManufacturers'), () => dispatchStorage({
            type: "FILTER_DESTROY",
            payload: 'modification'
        })],
        [R.equals('vehicleModels'), () => dispatchStorage({
            type: "FILTER_DESTROY_MODELS"
        })],
        [R.T, () => dispatchStorage({
            type: "FILTER_DESTROY",
            payload: element
        })],
        [R.F, () => null]
    ])(element), []);

    const data = React.useMemo(() => iconCancelData(filter), [filter])

    return (
        <ScrollView
            horizontal
        >
            {R.addIndex(R.map)((x, key) => (
                <View
                    key={key}
                >
                    <IconSearch
                        key={key}
                        dispatch={() => destroy(x)}
                        title={x}
                        value={R.prop(x)(data)}
                    />
                </View>
            ))(R.keys(R.reject(isOdd, data)))}
        </ScrollView>
    )
};
