/* eslint-disable newline-after-var */
/* eslint-disable scanjs-rules/call_setTimeout */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import * as R from 'ramda';
import React, { useEffect } from 'react';
import { View } from 'react-native';
import { Colors, ProgressBar } from 'react-native-paper';

const TimerHook = () => {
    const [time, setTime] = React.useState(0);
    const [loading, setLoading] = React.useState(0);

    useEffect(() => {
        let timeOut = null;
        if (R.equals(true, loading)) {
            timeOut = setTimeout(() => {
                setTime(R.add(time, 5));
            }, 5)
        }

        return () => {
            clearInterval(timeOut)
        };
    }, [loading, time]);

    const timer = React.useMemo(() => { return time }, [loading, time])

    return [timer, (e) => setLoading(e)]
}

export const ActivityIndicator = ({ loading }) => {
    const [timer, setLoading] = TimerHook()

    if (loading) {
        return (
            <ProgressBar
                color={Colors.yellow700}
                progress={timer / 100}
                style={{ backgroundColor: 'white', margin: 2 }}
            />
        )
    }

    return (
        <View />
    )
};