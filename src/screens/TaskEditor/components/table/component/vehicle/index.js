/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { PlateNumber } from 'screens/TaskEditor/components/table/component/repairs';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, isOdd } from 'utils/helper';
import { modification } from 'screens/Main/CarsScreen/Preview/config'

export const VehicleItem = ({ data }) => {
    const keys = ['num', 'vehicleManufacturers', 'vehicleModels', 'vehicleModifications', 'year', 'vin', 'plate'];
    const value = R.paths([['id'], ['modification', 'model', 'manufacturer', 'name'], ['modification', 'model'], ['modification'], ['year'], ['vin'], ['plate']])(data);
    const moderator = R.addIndex(R.map)((x, key) => R.zipObj([keys[key]], [value[key]]))(keys);

    const Items = (x) => R.cond([
        [R.equals(['vehicleModels']), () => (
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellLight"
                items="SSDescription"
                style={[styles.items, { flex: 1 }]}
            >{R.pipe(
                R.head,
                R.paths([['name'], ['subbody']]),
                R.join(' '))(R.values(x))}
            </LabelText>
        )],
        [R.equals(['vehicleModifications']), () => (
            <Text style={{ textTransform: 'lowercase', fontFamily: 'ShellLight' }}>
                {modification(data)}
            </Text>
        )],
        [R.equals(['plate']), () => (
            <>
                <PlateNumber
                    color={ColorCard('classic').font}
                    fonts="ShellBold"
                    items="SDescription"
                    testID="frameNumber"
                >
                    {R.values(x)}
                </PlateNumber>
            </>
        )],
        [R.T, () => (
            <LabelText
                color={ColorCard('classic').font}
                fonts="ShellLight"
                items="SSDescription"
                style={[styles.items, { flex: 1 }]}
            >
                {R.values(x)}
            </LabelText>
        )]
    ])(R.keys(x));

    return (
        <CardComponents
            style={[styles.shadow]}
        >
            {R.addIndex(R.map)((x, z) => {
                return (
                    <RowComponents key={z} style={{ marginTop: 1, marginBottom: 3 }}>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellBold"
                            items="SDescription"
                            style={styles.container}
                        >
                            {i18n.t(R.keys(x))}
                        </LabelText>
                        {Items(x)}
                    </RowComponents>
                )
            })(moderator)}
        </CardComponents >
    )
};

export const styles = StyleSheet.create({
    container: {
        textAlign: 'left',
        textTransform: 'uppercase'
    },
    items: {
        textAlign: 'right',
        textTransform: 'capitalize',
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        margin: 5,
        marginTop: 1,
        marginBottom: 1
    },
});