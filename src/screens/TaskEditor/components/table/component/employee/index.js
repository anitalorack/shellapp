/* eslint-disable no-extra-parens */
/* eslint-disable id-length */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-max-depth */
/* eslint-disable no-unsanitized/method */
/* eslint-disable react/jsx-indent-props */
import Logo from 'assets/svg/next';
import Phone from 'assets/svg/phone';
import i18n from 'localization';
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, phoneMedd } from 'utils/helper';
import { uiColor } from 'styled/colors/uiColor.json';

export const EmplloyeeItem = ({ data }) => {
    const keys = ['num', 'phone', 'last', 'first', 'role', 'createdAt'];
    const value = R.paths([['id'], ['phone'], ['name', 'last'], ['name', 'first'], ['role'], ['createdAt']])(data);
    const moderator = R.addIndex(R.map)((x, key) => (R.zipObj([keys[key]], [value[key]])
    ))(keys);

    return (
        <View style={{ padding: 1, flex: 1, backgroundColor: R.path(['blocked'])(data) ? uiColor.Polo_Grey : uiColor.Very_Pole_Grey }}>
            <CardComponents
                style={[styles.shadow, { backgroundColor: uiColor.Very_Pole_Grey }]}
            >
                {R.addIndex(R.map)((elem, item) => (
                    <RowComponents
                        key={item}
                        style={{
                            paddingTop: 2,
                            paddingBottom: 2
                        }}
                    >
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellMedium"
                            items="SDescription"
                            style={styles.container}
                        >
                            {i18n.t(R.keys(elem))}
                        </LabelText>
                        <LabelText
                            color={ColorCard('classic').font}
                            fonts="ShellLight"
                            items="SSDescription"
                            styles={styles.items}
                        >
                            {R.cond([
                                [R.equals(['phone']), R.always(phoneMedd(R.head(R.values(elem))))],
                                [R.equals(['role']), R.always(i18n.t(R.values(elem)))],
                                [R.equals(['createdAt']), () => (moment(R.head(R.values(elem))).format('DD.MM.YYYY HH:mm'))],
                                [R.T, R.always(R.values(elem))]
                            ])(R.keys(elem))}
                        </LabelText>
                    </RowComponents>
                ))(moderator)}
            </CardComponents >
        </View>
    )
};

export const styles = StyleSheet.create({
    container: {
        textAlign: 'left',
        textTransform: 'uppercase'
    },
    items: {
        textAlign: 'right',
        textTransform: 'capitalize',
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        margin: 5,
        marginTop: 1,
        marginBottom: 1
    },
});