/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { SearchInputLine } from 'screens/TaskEditor/components/header/component/SearchInputLine';

export const SearchInput = ({
    unbutton,
    setDispatch,
    dispatch,
    button,
    storage,
    unsearch
}) => {
    if (unbutton) {
        return (<View />)
    }

    console.log({ storage })

    return (
        <SearchInputLine
            dispatch={(e) => {
                if (R.equals(R.path(['type'])(e), 'Create')) return dispatch(e);
                setDispatch(e)
            }}
            mode={button}
            state={{ text: R.path(['text'])(storage) }}
            unsearch={unsearch}
        />
    )
};