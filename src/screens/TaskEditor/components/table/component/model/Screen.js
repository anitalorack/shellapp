/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { SplashScreen } from 'screens/Splash';


export const Screen = ({ error, loading }) => {
    if (error) {
        return (
            <SplashScreen
                msg="Ошибка интернет соединения"
                state="Error"
                style={{ margin: 4, borderWidth: 1 }}
                testID="Error"
            />
        )
    }

    return (
        <SplashScreen
            msg={loading ? "Загрузка" : "Ничего йдено"}
            state="Error"
            style={{ margin: 4, borderWidth: 1 }}
            testID="Error"
        />
    )
};