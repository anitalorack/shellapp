/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import * as R from 'ramda';
import React from 'react';
import { StyleSheet } from 'react-native'
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard, phoneMedd } from 'utils/helper';
import i18n from 'i18n-js'

export const ClientItem = ({ data }) => {
    const keys = ['num', 'phone', 'last', 'first'];
    const value = R.paths([['id'], ['phone'], ['name', 'last'], ['name', 'first']])(data);
    const moderator = R.addIndex(R.map)((x, key) => R.zipObj([keys[key]], [value[key]]))(keys);

    return (
        <CardComponents
            style={styles.shadow}
        >
            {R.addIndex(R.map)((x, z) => (
                <RowComponents key={z}>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SDescription"
                        style={styles.container}
                    >
                        {i18n.t(R.keys(x))}
                    </LabelText>
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellLight"
                        items="SSDescription"
                        style={styles.items}
                    >
                        {R.cond([
                            [R.equals(['phone']), R.always(phoneMedd(R.head(R.values(x))))],
                            [R.T, R.always(R.values(x))]
                        ])(R.keys(x))}
                    </LabelText>
                </RowComponents>
            ))(moderator)}
        </CardComponents>
    )
};

export const styles = StyleSheet.create({
    container: {
        textAlign: 'left',
        textTransform: 'uppercase'
    },
    items: {
        textAlign: 'right',
        textTransform: 'capitalize',
        flex: 1
    },
    line: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        borderStyle: 'dashed',
        borderRadius: 15,
        borderWidth: 1,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
        margin: 5,
        marginTop: 1,
        marginBottom: 1
    },
});