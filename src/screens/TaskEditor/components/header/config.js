import * as R from 'ramda'

export const initialState = {
    right: false,
    left: false,
    text: '',
    swipe: false,
    checker: true
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "Right":
            return R.mergeAll([state, { right: !R.path(['right'])(state) }]);
        case "Left":
            return R.mergeAll([state, { left: !R.path(['left'])(state) }]);
        case "Text":
            return R.mergeAll([state, { text: R.path(['payload'])(action) }]);
        case "Swipe":
            return R.mergeAll([state, { swipe: R.path(['payload'])(action) }]);
        case "Checker":
            if (R.equals(R.path(['payload'])(action))(R.path(['checker'])(state))) return R.mergeAll([state, { checker: null }]);

            return R.mergeAll([state, { checker: R.path(['payload'])(action) }]);
        case "Search":
            return R.mergeAll([state, { search: R.path(['payload'])(action) }]);
        default:
            return state
    }
};
