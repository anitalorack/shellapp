/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import LANGUAGE from 'styled/UIComponents/HeaderComponents/Components/LANGUAGE';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import PropTypes from 'prop-types';
import { Colors, IconButton } from 'react-native-paper';

export const Header = (props) => {
    const { menu, dispatch, svg } = props;
    const title = R.cond([
        [R.isNil, R.always(null)],
        [R.includes('#'), R.always(R.path(['title'])(menu))],
        [R.T, R.always(i18n.t(R.path(['title'])(menu)))],
    ])(R.defaultTo(null)(R.path(['title'])(menu)));

    const position = R.cond([
        [R.equals('goBack'), R.always('arrow-left')],
        [R.equals('toggle'), R.always('menu')],
        [R.T, R.always('rocket')],
    ])(svg);

    return (
        <SafeAreaView
            style={{ flex: 1 }}
        >
            <View style={Style.header}>
                <IconButton
                    color={Colors.black500}
                    icon={position}
                    onPress={() => dispatch(svg)}
                    size={25}
                />
                <LabelText
                    color={ColorCard('error').font}
                    fonts="ShellMedium"
                    items="CLabel"
                    style={Style.item}
                >
                    {R.defaultTo('')(title)}
                </LabelText>
                <LANGUAGE style={{ margin: 10 }} />
            </View>
            <View style={{ flex: 1 }}>
                {props.children}
            </View>
        </SafeAreaView >
    )
};

Header.propTypes = {
    dispatch: PropTypes.func,
    menu: PropTypes.object,
    svg: PropTypes.string,
};

Header.defaultProps = {
    dispatch: () => null,
    menu: { title: null },
    svg: 'goBack'
};

const Style = StyleSheet.create({
    item: {
        flex: 1,
        textTransform: 'uppercase',
        textAlign: 'center'
    },
    header: {
        alignItems: "center",
        justifyContent: 'space-between',
        flexDirection: 'row',
        height: 40,
        margin: 5,
    }
});