/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import en from 'assets/svg/languages/en';
import ru from 'assets/svg/languages/ru';
import MenuHeader from 'assets/svg/menu';
import i18n from 'localization'
import * as R from 'ramda';
import React, { useState } from 'react';
import { SvgXml } from 'react-native-svg';
import { TouchesStyle } from '../style';

const location = R.cond([
    [R.equals('ru'), R.always(en)],
    [R.equals('en'), R.always(ru)],
]);

const setLocation = R.cond([
    [R.equals('ru'), R.always("en")],
    [R.equals('en'), R.always("ru")],
]);

export const HeaderIcon = ({
    dispatch, state, mode
}) => {
    const [lang, setState] = useState('ru');

    return R.cond([
        [R.equals('GoBack'), () => (
            <TouchesStyle
                activeOpacity={3.6}
                current
                onPress={() => dispatch({ type: "GOBACK" })}
                state={state}
                testID="HEADER_MENU_OPEN"
                underlayColor="#DDDDDD"
            >
                <SvgXml
                    height="30"
                    width="30"
                    xml={MenuHeader}
                />
            </TouchesStyle>)],
        [R.equals('Language'), () => (
            <TouchesStyle
                activeOpacity={3.6}
                current
                onPress={() => {
                    i18n.locale = setLocation(lang);
                    setState(setLocation(lang))
                }}
                testID="HEADER_MENU_OPEN"
                underlayColor="#DDDDDD"
            >
                <SvgXml height="25" width="25" xml={location(lang)} />
            </TouchesStyle>
        )],
    ])(mode)
};