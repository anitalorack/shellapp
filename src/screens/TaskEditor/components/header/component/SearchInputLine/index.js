/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { Block } from 'screens/TaskEditor/components/header/style';
import { ButtonScreen } from 'screens/TaskEditor/components/header/component/SearchInputLine/ButtonScreen';
import { SearchScreen } from 'screens/TaskEditor/components/header/component/SearchInputLine/SearchScreen';
import PropTypes from 'prop-types';

export const SearchInputLine = ({
    state, mode, dispatch, unsearch, unfilter
}) => {
    return (
        <View
            style={Styles.view}
        >
            <Block
                style={[styles.shadow, Styles.block]}
            >
                <ButtonScreen
                    dispatch={dispatch}
                    mode={mode}
                    state={state}
                    unfilter={unfilter}
                />
                <SearchScreen
                    dispatch={dispatch}
                    state={state}
                    unsearch={unsearch}
                />
            </Block>
        </View >
    )
};

SearchInputLine.propTypes = {
    dispatch: PropTypes.func,
    mode: PropTypes.bool,
    state: PropTypes.object,
    unfilter: PropTypes.bool,
    unsearch: PropTypes.bool,
};

SearchInputLine.defaultProps = {
    dispatch: () => null,
    mode: true,
    state: { text: null },
    unfilter: false,
    unsearch: null,
};

const Styles = StyleSheet.create({
    block: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    view: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 55
    }
});