/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import add from 'assets/svg/add';
import i18n from 'localization';
import React from 'react';
import { View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { TouchBtn } from 'screens/TaskEditor/components/header/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { FilterScreen } from './FilterScreen';

export const ButtonScreen = ({
    dispatch, mode, state, unfilter
}) => {
    if (mode) {
        return (
            <>
                <TouchBtn
                    activeOpacity={3.6}
                    current
                    onPress={() => {
                        dispatch({ type: "Create" })
                    }}
                    state={state}
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        background: 'yellow',
                        width: 55,
                        height: 55
                    }}
                    underlayColor="#DDDDDD"
                >
                    <SvgXml height="21" width="21" xml={add} />
                    <LabelText
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="Description"
                        style={{
                            paddingTop: 3,
                            textTransform: 'uppercase'
                        }}
                    >
                        {i18n.t('ADDED')}
                    </LabelText>
                </TouchBtn>
                <FilterScreen
                    dispatch={dispatch}
                    state={state}
                    unfilter={unfilter}
                />
            </>
        )
    }

    return (
        <View />
    )
};