/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import Filter from 'assets/svg/filter';
import i18n from 'localization';
import React from 'react';
import { View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import { TouchBtn } from 'screens/TaskEditor/components/header/style';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';

export const FilterScreen = ({ state, dispatch, unfilter }) => {
    if (!unfilter) {
        return (
            <TouchBtn
                activeOpacity={3.6}
                onPress={() => {
                    dispatch({ type: "Filter" })
                }}
                state={state}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    background: 'red',
                    width: 55,
                    height: 55
                }}
                underlayColor="#DDDDDD"
            >
                <SvgXml height="21" width="21" xml={Filter} />
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="Description"
                    style={{
                        paddingTop: 3,
                        textTransform: 'uppercase'
                    }}
                >
                    {i18n.t('FILTER')}
                </LabelText>
            </TouchBtn>
        )
    }

    return (
        <View />
    )
};
