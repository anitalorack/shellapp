/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import Search from 'assets/svg/search';
import * as R from 'ramda';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { SvgXml } from 'react-native-svg';
import UiTextInput from 'uiComponents/UiPhoneInput/PhoneText';
import { isOdd } from 'utils/helper';
import i18n from 'localization'
import PropTypes from 'prop-types';
import { useDispatch, shallowEqual, useSelector } from 'react-redux'

export const SearchScreen = ({ state, dispatch, unsearch }) => {
    const success = (e) => {
        return dispatch(e)
    }



    if (isOdd(unsearch)) {
        return (
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }} >
                <View style={{ padding: 5 }}>
                    <SvgXml
                        height="20"
                        weight="20"
                        xml={Search}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <UiTextInput
                        errorMessage={R.path(['error'])(state)}
                        keyboardType="default"
                        labelStyle={styleSearch.mix}
                        multiline
                        onBlur={() => null}
                        onEndEditing={() => null}
                        onChangeText={success}
                        // onSubmitEditing={success}
                        placeholder={i18n.t("SearchDataModel")}
                        placeholderTextColor={styleSearch.text}
                        style={styleSearch.item}
                        value={state}
                    />
                </View>
            </View>
        )
    }

    return (<View style={{ flex: 1 }} />)
}

SearchScreen.propTypes = {
    dispatch: PropTypes.func,
    state: PropTypes.object,
    unsearch: PropTypes.bool,
};

SearchScreen.defaultProps = {
    dispatch: () => null,
    state: { text: 'новый запрос готов к дому', error: 'Error message' },
    unsearch: null,
};

const styleSearch = StyleSheet.create({
    item: {
        fontSize: 13,
        fontFamily: "ShellMedium"
    },
    text: {
        flex: 1,
        fontSize: 12,
    },
    mix: {
        fontFamily: "ShellMedium"
    }
});