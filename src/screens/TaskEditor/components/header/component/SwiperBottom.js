/* eslint-disable react/jsx-max-depth */
/* eslint-disable arrow-body-style */
/* eslint-disable multiline-ternary */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
import i18n from 'localization'
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { Checkbox } from 'react-native-paper';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';
import { Block, BottomLabel, TouchesStyle } from '../style';

export const VisualMode = ({
    dispatch, state, sorted, mode
}) => {
    const conditions = R.cond([
        [R.equals('checkbox'), () => R.map(x => (
            <View
                key={`Checkbox_${x.title}`}
                style={{
                    justifyContent: "space-between",
                    flexDirection: "row",
                    alignItems: "center",
                    paddingLeft: 15,
                    paddingRight: 15,
                }}
            >
                <Checkbox
                    onPress={() => {
                        dispatch({ type: "Checker", payload: x.title })
                    }}
                    status={R.equals(state.checker, x.title) ? 'checked' : 'unchecked'}
                />
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    // items={R.equals(state.checker, x.title) ? "CLabel" : "SSDescription"}
                    testID="TITLE"
                >
                    {i18n.t(R.path(['title'])(x))}
                </LabelText>
            </View>
        )
        )(sorted)],
        [R.equals('finder'), () => R.map(x => (
            <View
                key={`NAME_SPACE_${x}`}
                testID={`NAME_SPACE_${x}`}
            >
                <TouchesStyle
                    onPress={() => dispatch({ type: "Search", payload: x })}
                    testID={`CHECK_FILTER_${x}`}
                >
                    <BottomLabel
                        current={R.equals(R.path(['search'])(state), x)}
                    >
                        {i18n.t(x)}
                    </BottomLabel>
                </TouchesStyle>
            </View>
        ))(R.path(['name'])(sorted))]
    ]);

    return conditions(mode)
};

export const SwiperBottom = ({
    dispatch, check, mode, state
}) => {
    if (R.path(['text'])(state)) {
        return (
            <Block>
                <VisualMode
                    dispatch={e => dispatch(e)}
                    mode="finder"
                    sorted={check}
                    state={state}
                />
            </Block>)
    }
    if (!R.equals(mode, 'Search')) {
        return (
            <Block small >
                <View style={{ height: 1, width: 60, backgroundColor: "red" }} />
            </Block>
        )
    }

    return (<View />)
};