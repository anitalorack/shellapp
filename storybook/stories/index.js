/* eslint-disable react/jsx-indent-props */
/* eslint-disable react/jsx-max-depth */
import { ApolloProvider } from '@apollo/react-hooks';
import { storiesOf } from '@storybook/react-native';
import client from 'entrypoint/apollo';
import React from 'react';
import FlashMessage from "react-native-flash-message";
import { MenuProvider } from 'react-native-popup-menu';
import { Provider } from 'react-redux';
import { store } from 'store';
import { ThemeProvider } from 'styled-components/native';
import Theme from "styled/colors";
import { App } from 'screens/Document'

storiesOf(`Stories`, module)
    .add("App", () => (
        <MenuProvider>
            <Provider store={store}>
                <ApolloProvider client={client} >
                    <ThemeProvider theme={Theme}>
                        <App />
                    </ThemeProvider>
                    <FlashMessage position="top" />
                </ApolloProvider>
            </Provider>
        </MenuProvider>
    ))
