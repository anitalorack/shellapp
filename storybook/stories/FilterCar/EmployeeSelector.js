/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { ALL_EMPLOYERS } from 'gql/employee';
import * as R from 'ramda';
import React from 'react';
import { UiPicker } from 'styled/UIComponents/UiPicker';
import { isOdd } from 'utils/helper';

export const grees = {
    id: 4,
    path: 'text',

    main: true,
    label: 'text',
    // Lenght: 12,
    keyboardType: "default",
    // mask: "[00000] [00000] [00]",
    value: ['text'],
    example: "что ищем",
    error: "Error message",
    errorMessage: 'PhoneError',
};

export const EmployeeSelector = ({ storage, dispatch, mode }) => {
    const [state, setState] = React.useState(null);
    const [loadGreeting] = useLazyQuery(ALL_EMPLOYERS, {
        onCompleted: (data) => {
            const dasdas = R.map(x => ({
                label: R.pipe(R.path(['name']), R.dissoc('__typename'), R.values, R.join(' '))(x),
                value: R.path(['id'])(x)
            }))(R.path(R.concat(R.keys(data), ['items']))(data));

            setState(R.prepend({ label: null, value: null }, dasdas))
        }
    });

    React.useEffect(() => {
        if (isOdd(R.path(['query'])(state))) {
            loadGreeting({ variables: { where: {} } })
        }
    }, []);

    return (
        <UiPicker
            disabled={!isOdd(state)}
            dispatch={(e) => dispatch(e)}
            element={{ path: mode }}
            Items={R.defaultTo([{ label: null, value: null }])(state)}
            placeholder={{ id: R.defaultTo(null)(R.path([mode, 'eq'])(storage)) }}
        />
    )
};
