/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/jsx-indent-props */
import { useLazyQuery } from '@apollo/react-hooks';
import { ALL_CLIENT } from 'gql/clients';
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import {
    Text, TouchableOpacity, View, StyleSheet
} from 'react-native';
import Modal from 'react-native-modal';
import { ModalDescription } from 'screens/Contragent/Input/BankCard/ModalSetting';
import { HeaderApp } from 'styled/UIComponents/HeaderTitle';
import { LabelInputText } from 'utils/component/LabelInputText';
import { ModalScreen } from './ModalScreen';

export const grees = {
    id: 4,
    path: 'text',
    main: true,
    label: 'text',
    keyboardType: "default",
    value: ['text'],
    example: "что ищем",
    error: "Error message",
    errorMessage: 'PhoneError',
};

export const AppScreen = ({ mode, dispatch, storage }) => {
    const [state, setState] = React.useState({ isVisible: false });

    const [loadGreeting] = useLazyQuery(ALL_CLIENT, {
        onCompleted: (data) => setState(R.assocPath(['query'],
            R.head(R.path(R.concat(R.keys(data), ['items']))(data)))(state))
    });

    React.useEffect(() => {
        if (R.path(['id'])(storage)) {
            loadGreeting({ variables: { where: { id: { eq: R.path(['id'])(storage) } } } })
        }
    }, []);

    return (
        <View style={{ paddingTop: 10, paddingBottom: 10 }}>
            <View style={{ paddingBottom: 10 }}>
                <LabelInputText props={{ label: mode }} />
            </View>
            <View style={style.reload}>
                <TouchableOpacity
                    onPress={() => setState(
                        R.mergeAll([state, {
                            isVisible: true
                        }]))}
                    style={{ paddingLeft: 15 }}
                >
                    <Text>
                        {R.defaultTo('')(
                            R.defaultTo("Slava")(
                                R.pipe(
                                    R.path(['query']),
                                    R.path(['name']),
                                    R.dissoc('__typename'),
                                    R.values,
                                    R.join(' ')
                                )(state)
                            )
                        )}
                    </Text>
                </TouchableOpacity>
            </View>
            <Modal isVisible={R.path(['isVisible'])(state)}>
                <ModalDescription
                    large
                    style={{ flex: 1, margin: 10 }}
                >
                    <HeaderApp
                        button="CLOSED"
                        dispatch={() => setState(
                            R.assocPath(
                                ['isVisible'], false, state))}
                        title={i18n.t("SELECTTASK")}
                    />
                    <ModalScreen
                        dispatch={(e) => {
                            dispatch(R.assocPath([mode, 'eq'],
                                R.path(['id'])(e))({}));
                            loadGreeting({ variables: { where: { id: { eq: R.path(['id'])(e) } } } });
                            setState(R.assocPath(['isVisible'], false)(state))
                        }}
                        mode={mode}
                        storage={R.defaultTo("")(R.pipe(R.path(['query']), R.path(['id']))(state))}
                    />
                </ModalDescription>
            </Modal>
        </View>
    )
};

const style = StyleSheet.create({
    reload: {
        borderColor: 'grey',
        borderWidth: 1,
        flex: 1,
        height: 50,
        justifyContent: 'center',
    }
});