/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import i18n from 'localization';
import * as R from 'ramda';
import React from 'react';
import { View } from 'react-native';
import { UiPicker } from 'styled/UIComponents/UiPicker';

export const PickerType = () => (
    <View >
        <UiPicker
            // disabled={R.path(['id'])(storage)}
            dispatch={(e) => console.log({
                type: 'type',
                payload: R.assocPath(['work'], e, {})
            })}
            element={{ path: 'INDIVIDUAL' }}
            horizont
            Items={[
                { label: i18n.t("INDIVIDUAL"), value: "INDIVIDUAL" },
                { label: i18n.t("LEGAL_ENTITY"), value: "LEGAL_ENTITY" },
                { label: i18n.t("SOLE_TRADER"), value: "SOLE_TRADER" },
                { label: 'выбрать', value: null },
            ]}
            placeholder={{ id: R.defaultTo(null)("INDIVIDUAL") }}
            style={{ flex: 1 }}
        />
    </View>
);
