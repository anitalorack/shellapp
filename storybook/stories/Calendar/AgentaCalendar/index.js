/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import moment from 'moment';
import * as R from 'ramda';
import React from 'react';
import { Agenda } from 'react-native-calendars';
import { loadItems } from 'storybook/stories/Calendar/AgentaCalendar/loadItems';
import { renderEmptyDate } from 'storybook/stories/Calendar/AgentaCalendar/renderEmptyDate';
import { renderItem } from 'storybook/stories/Calendar/AgentaCalendar/renderItem';
import { calendar } from 'storybook/stories/Calendar/calendar';

export const AgentaCalendar = ({ navigation }) => {
    const [state, setState] = React.useState({ items: {} });
    const rowHasChanged = (r1) => r1.name;

    return (
        <Agenda
            displayLoadingIndicator
            items={state.items}
            loadItemsForMonth={(e) => loadItems({ day: e, state, setState })}
            renderEmptyDate={(e) => renderEmptyDate({ item: e, navigation })}
            renderItem={(e) => renderItem({ item: e, navigation })}
            rowHasChanged={(e) => rowHasChanged(e)}
            selected={R.defaultTo('2019-05-16')(moment().format("YYYY-MM-DD"))}
            theme={calendar}
        />
    )
};
