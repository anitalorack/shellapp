/* eslint-disable react/jsx-max-depth */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-indent-props */
/* eslint-disable max-statements */
import React from 'react';
import {
    Alert, StyleSheet, TouchableOpacity, View
} from 'react-native';
import { styles } from 'screens/Main/AddGoods/helper/style';
import { PlateNumber } from 'screens/TaskEditor/components/table/component/repairs';
import { CardComponents, RowComponents } from 'screens/TaskEditor/components/table/style';
import { uiColor } from 'styled/colors/uiColor.json';
import { LabelText } from 'uiComponents/SplashComponents';
import { ColorCard } from 'utils/helper';


// eslint-disable-next-line no-unused-vars
export const renderItem = ({ item, navigation }) => (
    <View
        style={{
            flex: 1,
            backgroundColor: uiColor.Yellow,
        }}
    >
        <CardComponents
            style={[styles.shadow, TableStyle.card, { margin: 3 }]}
        >
            <TouchableOpacity
                onPress={() => Alert.alert(item.name)}
            >
                <RowComponents>
                    <LabelText
                        color={ColorCard('error').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                        style={{ textAlign: 'center', }}
                        testID="BlockLaker"
                    >
                        7:30 - 9:00
                    </LabelText>
                    <PlateNumber
                        color={ColorCard('classic').font}
                        fonts="ShellMedium"
                        items="SSDescription"
                        style={{ textAlign: 'center', borderWidth: 1 }}
                        testID="BlockLaker"
                    >
                        ГосЗнак
                    </PlateNumber>
                </RowComponents>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SSDescription"
                    style={{ textAlign: 'left', }}
                    testID="BlockLaker"
                >
                    автомобиль
                </LabelText>
                <LabelText
                    color={ColorCard('classic').font}
                    fonts="ShellMedium"
                    items="SSDescription"
                    style={{ textAlign: 'left', }}
                    testID="BlockLaker"
                >
                    Стоимость
                </LabelText>
            </TouchableOpacity>
        </CardComponents>
    </View>
);

const TableStyle = StyleSheet.create({
    line: {
        marginTop: 15,
        marginBottom: 15,
        borderColor: uiColor.Mid_Grey,
        borderWidth: 0.2,
        marginLeft: 0,
        marginRight: 0,
    },
    card: {
        margin: 2,
        backgroundColor: uiColor.Very_Pole_Grey,
        padding: 4
    }
});