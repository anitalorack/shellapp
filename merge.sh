BRANCH="TEST_102" &&
  ANDROID="./android/app/build/outputs/apk/release" &&
  UTILS="./src/utils/update" &&
  APKURL="https://raw.githubusercontent.com/Js-Nanodegree/ShellApkVersion/${BRANCH}/app-release.apk" &&
  APKURLVERSION="https://raw.githubusercontent.com/Js-Nanodegree/ShellApkVersion/${BRANCH}/version.json" &&
  rm -rf version.json &&
  echo $APKURL &&
  echo $APKURLVERSION &&
  JSON="{
  \"apkUrl\":\"$APKURL\",
  \"apkUrlVersion\":\"$APKURLVERSION\",
  \"forceUpdate\":false,
  \"versionName\":\"99.6.0\",
  \"versionCode\":\"99\"
  }" &&
  echo $JSON >${ANDROID}/version.json &&
  echo $JSON
cd ${ANDROID} &&
  rm -rf ./.git &&
  git init &&
  git checkout -b ${BRANCH} &&
  git branch &&
  git add . &&
  git commit -m 'Init package Name' &&
  git remote add origin https://JS-Nanodegree:ZaqMlp123!2@github.com/Js-Nanodegree/ShellApkVersion.git &&
  git push origin ${BRANCH} -f &&
  cd ../../../../../../ &&
  JSON="{
  \"apkUrl\":\"$APKURL\",
  \"apkVersionUrl\":\"$APKURLVERSION\",
  \"versionName\":\"998.0\",
  \"versionCode\":\"998\",
  \"DescriptionRnUpdate\":\"$APKURLVERSION\",
  \"forceUpdate\":false,
  \"fileProviderAuthority\":\"com.myapp.provider\",
  \"TitleRnUpdate\":\"Update Available\"
  }" &&
  echo $JSON >${UTILS}/app.json &&
  echo "Compleated"
